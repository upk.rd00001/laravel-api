<?php

namespace App\Http\Controllers;

use App\Http\Enum\CONST_DBPG_SMS_TYPE;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\SMSHttp;
use UPKclass_Dashboard_Parking;

class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
	 * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
		$headers = array('Content-Type' => 'application/json; charset=utf-8');
		return  Response()->json(["result"=>1], Response::HTTP_OK, $headers, JSON_UNESCAPED_UNICODE);
    }

	/**
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function send(Request $request)
	{
		ReturnUnicodeJsonModule([]);
		$ans = json_decode($this->sendsmsfunc("SEND SMS", "+886", $request->get("cellphone"), $request->get("content")),true);
		$headers = array('Content-Type' => 'application/json; charset=utf-8');
		return Response()->json($ans, Response::HTTP_OK, $headers, JSON_UNESCAPED_UNICODE);
	}

	function sendsmsfunc(
		$activity,
		$cellphone_country_code,
		$cellphone,
		$content,
		$is_update_sms_sent_time = 0,
		$args = []
	) {

		$pure_data = file_get_contents('php://input');
		//$data_back = json_decode($pure_data);
		$content_origin = $content;
		//間訊王才要作以下三行，everybody不需要
		//$content = str_replace("\n", "%0A", $content);//urlencode換行符號，但是不能urlencode中文字
		//$content = str_replace(" ", "%20", $content);//urlencode換行符號，但是不能urlencode中文字
		//$content = iconv("UTF-8", "big5//TRANSLIT", $content);

		global $conn, $dbName;
		check_conn($conn, $dbName);
		#check input data
		$language = "zh-tw";
		$dbpg_sms_type = 0;
		if (isset($args["dbpg_sms_type"])) {
			$dbpg_sms_type = $args["dbpg_sms_type"];
		}
		$dbpg_sms_description = "";
		//登入, 建立超商繳費單, 忘記密碼 (放到發簡訊功能內), 未出場通知簡訊
		if ($dbpg_sms_type == CONST_DBPG_SMS_TYPE::LOGIN) {
			//登入
			$dbpg_sms_description = "登入";
		}
		elseif ($dbpg_sms_type == CONST_DBPG_SMS_TYPE::BARCODE) {
			//建立超商繳費單
			$dbpg_sms_description = "建立超商繳費單";
		}
		elseif ($dbpg_sms_type == CONST_DBPG_SMS_TYPE::FORGOT_PASSWORD) {
			//忘記密碼
			$dbpg_sms_description = "忘記密碼";
		}
		elseif ($dbpg_sms_type == CONST_DBPG_SMS_TYPE::OUT_NOTIFY) {
			//未出場通知簡訊
			$dbpg_sms_description = "未出場通知簡訊";
		}
		else {
			$dbpg_sms_description = "其他".$content;
		}
		if ($activity == null || $cellphone_country_code == null || $cellphone == null || $content == null) {
			rg_activity_log($conn, "", "簡訊發送失敗", "資料不完整", $pure_data, "");
			$ans = GetSystemCode("50027", $language, $conn);
			return json_encode(["result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]]);
		}
		else {
			if ($activity != "SEND SMS") {
				rg_activity_log($conn, "", "簡訊發送失敗", "activity錯誤", $pure_data, "");
				$ans = GetSystemCode("50028", $language, $conn);
				return json_encode([
					"result" => 0,
					"systemCode" => $ans[0],
					"title" => $ans[1],
					"description" => $ans[2]
				]);
			}
			else {
				$sql = "SELECT m_validation_sms_count,m_validation_sms_sent_time FROM tb_Member WHERE m_cellphone_country_code='".$cellphone_country_code."' AND ".sql_cellphone_compare("m_cellphone", $cellphone)." ";
				$result = mysql_query($sql, $conn);
				if (! $result) {
					return json_encode(["result" => 0, "title" => "簡訊發送錯誤", "description" => mysql_error($conn)]);
				}
				else {
					if (mysql_num_rows($result) == 0) {
						#無此會員?
					}
					else {
						$ans = mysql_fetch_assoc($result);
						$m_validation_sms_sent_time = new DateTime($ans["m_validation_sms_sent_time"]);
						$now_datetime = new DateTime();
						$now_datetime->modify("-1 day");
						if ($now_datetime > $m_validation_sms_sent_time) {
							#已過24HR 重置
							$update_sms_sent_time_sql = "";
							if ($is_update_sms_sent_time == 1) {
								$update_sms_sent_time_sql = " m_validation_sms_sent_time=now(), ";
							}
							$sql_reset = "UPDATE tb_Member SET ".$update_sms_sent_time_sql." m_validation_sms_sent_time=now(),m_validation_sms_count='0' WHERE m_cellphone_country_code='".$cellphone_country_code."' AND ".sql_cellphone_compare("m_cellphone", $cellphone)." ";
							$result = mysql_query($sql_reset, $conn);
							if (! $result) {
								return json_encode([
									"result" => 0,
									"title" => "簡訊發送更新錯誤",
									"description" => mysql_error($conn)
								]);
							}
						}
						elseif ($ans["m_validation_sms_count"] >= 10) {
							rg_activity_log($conn, "", "簡訊發送失敗", "已達每日發送最大上限", $pure_data, "");
							$ans = GetSystemCode("20063", $language, $conn);
							return json_encode([
								"result" => 0,
								"systemCode" => $ans[0],
								"title" => $ans[1],
								"description" => $ans[2]
							]);
						}
						else {
							$update_sms_sent_time_sql = "";
							if ($is_update_sms_sent_time == 1) {
								$update_sms_sent_time_sql = " m_validation_sms_sent_time=now(), ";
							}
							$sql_reset = "UPDATE tb_Member SET ".$update_sms_sent_time_sql." m_validation_sms_count=m_validation_sms_count+1 WHERE m_cellphone_country_code='".$cellphone_country_code."' AND ".sql_cellphone_compare("m_cellphone", $cellphone)." ";
							$result = mysql_query($sql_reset, $conn);
							if (! $result) {
								return json_encode([
									"result" => 0,
									"title" => "簡訊發送更新錯誤",
									"description" => mysql_error($conn)
								]);
							}
						}
					}
				}
				if ($cellphone_country_code != null && substr($cellphone, 0, 1) === "0") {
					$cellphone = substr($cellphone, 1);
				}
				for ($i = 0; $i < 3; $i++) {

					//require_once('SMSHttp.php');
					$sms = new SMSHttp();
					$userID = "52307327";    //發送帳號
					$password = "R4KW5Q";    //發送密碼
					$subject = "";    //簡訊主旨，主旨不會隨著簡訊內容發送出去。用以註記本次發送之用途。可傳入空字串。
					$content = $content;    //簡訊內容

					$mobile = $cellphone_country_code.$cellphone;    //接收人之手機號碼。格式為: +886912345678或09123456789。多筆接收人時，請以半形逗點隔開( , )，如0912345678,0922333444。
					$sendTime = "";        //簡訊預定發送時間。-立即發送：請傳入空字串。-預約發送：請傳入預計發送時間，若傳送時間小於系統接單時間，將不予傳送。格式為YYYYMMDDhhmnss；例如:預約2009/01/31 15:30:00發送，則傳入20090131153000。若傳遞時間已逾現在之時間，將立即發送。
					$is_success = true;
					//取餘額
					if ($sms->getCredit($userID, $password)) {
						$output = "取得餘額成功，餘額為：".$sms->credit."<br />";
					}
					else {
						$is_success = false;
						$output = "取得餘額失敗，".$sms->processMsg."<br />";
					}
					rg_activity_log($conn, "", "簡訊發送LOG", $output, $pure_data, "");
					//傳送簡訊
					if ($sms->sendSMS($userID, $password, $subject, $content, $mobile, $sendTime)) {
						$output = "傳送簡訊成功，餘額為：".$sms->credit."，此次簡訊批號為：".$sms->batchID."<br />";
					}
					else {
						$is_success = false;
						$output = "傳送簡訊失敗，".$sms->processMsg."<br />";
					}
					rg_activity_log($conn, "", "簡訊發送LOG", $output, $pure_data, "");
					if ($is_success) {

						$DT_now = new Datetime();
						$this_dashboard_parking = new UPKclass_Dashboard_Parking();
						$allData["dbpg_type"] = CONST_DBPG_TYPE::SEND_SMS;
						$allData["m_plots_id"] = "";
						$allData["m_pk_id"] = "";
						$allData["m_bk_id"] = "";
						$allData["m_dpt_id"] = "";
						$allData["m_id"] = "";
						$allData["dbpg_create_datetime"] = $DT_now->format("Y-m-d H:i:s");
						$allData["m_ve_id"] = "";
						$allData["dbpg_desc"] = "簡訊發送";
						$allData["para1"] = $cellphone_country_code;
						$allData["para2"] = $cellphone;
						$allData["para3"] = $dbpg_sms_type;
						$allData["para4"] = $dbpg_sms_description;
						$this_dashboard_parking->insert($allData);
						return json_encode(["result" => 1, "receive" => "Kmsgid="]);
					}
					else {
						#發送三次跑回圈
					}
				}

				if ($cellphone != "933891362") {
					sendkotsmsfunc("SEND SMS", "+886", "0933891362", "請Luke轉發給這個會員".$cellphone.",".$content_origin);
				}
				#居然三次都發失敗就發email
				$email_content = "手機: ".$cellphone."<br>內容: ".$content;
				$G_SYSTEM_ERROR_RECEIVE_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
				include_once("/../template/email_template.php");

				EmailTemplate($conn, "易停網", $G_SYSTEM_ERROR_RECEIVE_EMAIL, "易停網 寄送簡訊失敗 請盡速處理", $email_content);
				EmailTemplate($conn, "易停網", "upk.rd00001@gmail.com", "易停網 寄送簡訊失敗 請盡速處理", $email_content);
				rg_activity_log($conn, "", "簡訊發送失敗", $email_content, $pure_data, "");
				$ans = GetSystemCode("12", $language, $conn);
				return json_encode([
					"result" => 0,
					"systemCode" => $ans[0],
					"title" => $ans[1],
					"description" => $ans[2]
				]);
			}
		}
		//mysql_close($conn);
	}

	function sendkotsmsfunc($activity, $cellphone_country_code, $cellphone, $content, $is_update_sms_sent_time = 0)
	{


		include_once("fn_package.php");
		$pure_data = file_get_contents('php://input');

		$content_origin = $content;
		$content = str_replace("\n", "%0A", $content);//urlencode換行符號，但是不能urlencode中文字
		$content = str_replace(" ", "%20", $content);//urlencode換行符號，但是不能urlencode中文字
		$content = iconv("UTF-8", "big5//TRANSLIT", $content);

		global $conn, $dbName;
		check_conn($conn, $dbName);
		#check input data
		$language = "zh-tw";

		$G_SEND_SMS_RESPONSE_ROUTE = GetSystemParameter($conn, "send_sms_response_route");
		if ($activity == null || $cellphone_country_code == null || $cellphone == null || $content == null) {
			rg_activity_log($conn, "", "簡訊發送失敗", "資料不完整", $pure_data, "");
			$ans = GetSystemCode("1", $language, $conn);
			return json_encode(["result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]]);
		}
		else {
			if ($activity != "SEND SMS") {
				rg_activity_log($conn, "", "簡訊發送失敗", "activity錯誤", $pure_data, "");
				$ans = GetSystemCode("1", $language, $conn);
				return json_encode([
					"result" => 0,
					"systemCode" => $ans[0],
					"title" => $ans[1],
					"description" => $ans[2]
				]);
			}
			else {
				$sql = "SELECT m_validation_sms_count,m_validation_sms_sent_time FROM tb_Member WHERE m_cellphone_country_code='".$cellphone_country_code."' AND ".sql_cellphone_compare("m_cellphone", $cellphone)." ";
				$result = mysql_query($sql, $conn);
				if (! $result) {
					return json_encode(["result" => 0, "title" => "簡訊發送錯誤", "description" => mysql_error($conn)]);
				}
				else {
					if (mysql_num_rows($result) == 0) {
						#無此會員?
					}
					else {
						$ans = mysql_fetch_assoc($result);
						$m_validation_sms_sent_time = new DateTime($ans["m_validation_sms_sent_time"]);
						$now_datetime = new DateTime();
						$now_datetime->modify("-1 day");
						if ($now_datetime > $m_validation_sms_sent_time) {
							#已過24HR 重置
							$update_sms_sent_time_sql = "";
							if ($is_update_sms_sent_time == 1) {
								$update_sms_sent_time_sql = " m_validation_sms_sent_time=now(), ";
							}
							$sql_reset = "UPDATE tb_Member SET ".$update_sms_sent_time_sql." m_validation_sms_sent_time=now(),m_validation_sms_count='0' WHERE m_cellphone_country_code='".$cellphone_country_code."' AND ".sql_cellphone_compare("m_cellphone", $cellphone)." ";
							$result = mysql_query($sql_reset, $conn);
							if (! $result) {
								return json_encode([
									"result" => 0,
									"title" => "簡訊發送更新錯誤",
									"description" => mysql_error($conn)
								]);
							}
						}
						elseif ($ans["m_validation_sms_count"] >= 10) {
							rg_activity_log($conn, "", "簡訊發送失敗", "已達每日發送最大上限", $pure_data, "");
							$ans = GetSystemCode("20063", $language, $conn);
							return json_encode([
								"result" => 0,
								"systemCode" => $ans[0],
								"title" => $ans[1],
								"description" => $ans[2]
							]);
						}
						else {
							$update_sms_sent_time_sql = "";
							if ($is_update_sms_sent_time == 1) {
								$update_sms_sent_time_sql = " m_validation_sms_sent_time=now(), ";
							}
							$sql_reset = "UPDATE tb_Member SET ".$update_sms_sent_time_sql." m_validation_sms_count=m_validation_sms_count+1 WHERE m_cellphone_country_code='".$cellphone_country_code."' AND ".sql_cellphone_compare("m_cellphone", $cellphone)." ";
							$result = mysql_query($sql_reset, $conn);
							if (! $result) {
								return json_encode([
									"result" => 0,
									"title" => "簡訊發送更新錯誤",
									"description" => mysql_error($conn)
								]);
							}
						}
					}
				}
				if ($cellphone_country_code != null && substr($cellphone, 0, 1) === "0") {
					$cellphone = substr($cellphone, 1);
				}
				for ($i = 0; $i < 3; $i++) {
					$api_host = 'http://202.39.48.216/kotsmsapi-1.php?username=uparking&password=faceid12345678&dstaddr='.$cellphone_country_code.$cellphone.'&response='.urlencode($G_SEND_SMS_RESPONSE_ROUTE).'&smbody='.$content;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "$api_host");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$output = curl_exec($ch);
					curl_close($ch);
					rg_activity_log($conn, "", "簡訊發送LOG", $output, $pure_data, $api_host);
					$ans = preg_split("/[a-zA-Z\[\]\"\{\}:,]+/", $output, -1, PREG_SPLIT_NO_EMPTY);
					if (isset($ans[0]) && $ans[0] >= 0) {
						if (strpos($ans[0], '-60014') !== false && $cellphone != "933891362") {
							sendsmsfunc("SEND SMS", "+886", "0933891362", "請Luke轉發給這個會員".$cellphone.",".$content_origin);
						}
						return json_encode(["result" => 1, "receive" => "Kmsgid=".$ans[0]]);
					}
					else {
						#發送三次跑回圈
					}
				}
				#居然三次都發失敗就發email
				$email_content = "手機: ".$cellphone."<br>內容: ".$content;
				$G_SYSTEM_ERROR_RECEIVE_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
				include_once("/../template/email_template.php");

				EmailTemplate($conn, "易停網", $G_SYSTEM_ERROR_RECEIVE_EMAIL, "易停網 寄送簡訊失敗 請盡速處理", $email_content);
				EmailTemplate($conn, "易停網", "upk.rd00001@gmail.com", "易停網 寄送簡訊失敗 請盡速處理", $email_content);
				rg_activity_log($conn, "", "簡訊發送失敗", $email_content, $pure_data, "");
				$ans = GetSystemCode("12", $language, $conn);
				return json_encode([
					"result" => 0,
					"systemCode" => $ans[0],
					"title" => $ans[1],
					"description" => $ans[2]
				]);
			}
		}
		//mysql_close($conn);

	}
}
