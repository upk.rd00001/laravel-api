<?php

namespace App\Http\Controllers;

use AllInOne;
use App\Http\Enum\CONST_M_DPT_TRADE_PLATFORM;

use App\Http\Pay\GamapayHelper;
use App\Http\Pay\TappayHelper;
use App\Http\Pay\TappayType;
use App\Http\Pay\JkopayHelper;
use App\Http\Enum\CONST_DBPG_SMS_TYPE;
use DateTime;
use ECPay_AllInOne;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\SMSHttp;

class PayController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index()
	{
		$headers = array('Content-Type' => 'application/json; charset=utf-8');
		return  Response()->json(["result"=>1], Response::HTTP_OK, $headers, JSON_UNESCAPED_UNICODE);
	}

    public function record(Request $request) {
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$m_dpt_id = $request->input("m_dpt_id");
		$token = $request->input("token");
		$pure_data = $request->input("pure_data");
		$result_record = [];
		$ans = DB::table('tb_Member_Deposit')->select('m_dpt_trade_platform')->where('m_dpt_id',$m_dpt_id)->get()->first();
		if(isset($ans->m_dpt_trade_platform)) {
			$m_dpt_trade_platform = $ans->m_dpt_trade_platform;

			if (is_m_dpt_trade_platform_tappay($m_dpt_trade_platform)) {
				$tappay = new TappayHelper($conn, TappayType::RECORD);
				$result_record = $tappay->RecordRequest($m_dpt_id);
				if (isset($result_record["result"]) && $result_record["result"] == 1) {
					$result_record["data_tappay"] = $result_record["data"];
					unset($result_record["data"]);
				}
				else {

				}
			}
			elseif ($m_dpt_trade_platform == CONST_M_DPT_TRADE_PLATFORM::EC_PAY || $m_dpt_trade_platform == CONST_M_DPT_TRADE_PLATFORM::EC_PAY_BARCODE) {

				$G_ECPAY_SERVER = GetSystemParameter($conn, "ecpay_query_trade_info_url");
				$G_ECPAY_HASHKEY = GetSystemParameter($conn, "ecpay_hashkey");
				$G_ECPAY_HASHIV = GetSystemParameter($conn, "ecpay_hashiv");
				$G_ECPAY_MERCHANTID = GetSystemParameter($conn, "ecpay_merchant_id");
				$obj = new ECPay_AllInOne();
				//服務參數
				$obj->ServiceURL = $G_ECPAY_SERVER; //服務位置
				$obj->HashKey = $G_ECPAY_HASHKEY;      //測試用Hashkey，請自行帶入ECPay提供的HashKey
				$obj->HashIV = $G_ECPAY_HASHIV;        //測試用HashIV，請自行帶入ECPay提供的HashIV
				$obj->MerchantID = $G_ECPAY_MERCHANTID;

				$obj->Query["MerchantTradeNo"] = $m_dpt_id;
				$obj->Query["TimeStamp"] = time();
				$result_record["result"] = 1;
				$result_record["data_ecpay"] = $obj->QueryTradeInfo();

				/*
						"CustomField1": "",
						"CustomField2": "0905568270",
						"CustomField3": "臺北市",
						"CustomField4": "",
						"HandlingCharge": "0",
						"ItemName": "PLOT561皮卡丘(301) 7944~7946停車媒合服務費 3 元 x 1",
						"MerchantID": "2000132",
						"MerchantTradeNo": "DPTD00000145790002",
						"PaymentDate": "",
						"PaymentType": "BARCODE_BARCODE",
						"PaymentTypeChargeFee": "0",
						"StoreID": "",
						"TradeAmt": "18",
						"TradeDate": "2020/12/28 18:01:42",
						"TradeNo": "2012281801420023",
						"TradeStatus": "0"
				*/
			}
			elseif ($m_dpt_trade_platform == CONST_M_DPT_TRADE_PLATFORM::ALL_PAY) {

				$G_ALLPAY_SERVER = GetSystemParameter($conn, "allpay_query_trade_info_url");
				$G_ALLPAY_HASHKEY = GetSystemParameter($conn, "allpay_hashkey");
				$G_ALLPAY_HASHIV = GetSystemParameter($conn, "allpay_hashiv");
				$G_ALLPAY_MERCHANTID = GetSystemParameter($conn, "allpay_merchant_id");
				$obj = new AllInOne();
				//服務參數
				$obj->ServiceURL = $G_ALLPAY_SERVER; //服務位置
				$obj->HashKey = $G_ALLPAY_HASHKEY;      //測試用Hashkey，請自行帶入ECPay提供的HashKey
				$obj->HashIV = $G_ALLPAY_HASHIV;        //測試用HashIV，請自行帶入ECPay提供的HashIV
				$obj->MerchantID = $G_ALLPAY_MERCHANTID;

				$obj->Query["MerchantTradeNo"] = $m_dpt_id;
				$obj->Query["TimeStamp"] = time();
				$result_record["result"] = 1;
				$result_record["data_opay"] = $obj->QueryTradeInfo();
				/*
						"CustomField1": "",
						"CustomField2": "0905568270",
						"CustomField3": "臺北市",
						"CustomField4": "",
						"HandlingCharge": "0",
						"ItemName": "PLOT561皮卡丘(301) 7944~7946停車媒合服務費 3 元 x 1",
						"MerchantID": "2000132",
						"MerchantTradeNo": "DPTD00000145790002",
						"PaymentDate": "",
						"PaymentType": "BARCODE_BARCODE",
						"PaymentTypeChargeFee": "0",
						"StoreID": "",
						"TradeAmt": "18",
						"TradeDate": "2020/12/28 18:01:42",
						"TradeNo": "2012281801420023",
						"TradeStatus": "0"
				*/
			}
			elseif ($m_dpt_trade_platform == CONST_M_DPT_TRADE_PLATFORM::JKO_PAY) {
				$jkopay = new JkopayHelper();
				$result_record = $jkopay->InquiryRequest([$m_dpt_id]);
				//jko pay的成功result
				if (isset($result_record["result"]) && $result_record["result"] == 1) {
					$result_record["data_jkopay"] = $result_record["data"];
					unset($result_record["data"]);
				}
			}
			elseif ($m_dpt_trade_platform == CONST_M_DPT_TRADE_PLATFORM::GAMA_PAY) {
				$gamapay = new GamapayHelper();
				$result_record = json_decode($gamapay->GetTransOrderFunc("GET TRANS ORDER", $token, $m_dpt_id, $pure_data), true);
				//gama pay的成功result
				if (isset($result_record["result"]) && $result_record["result"] == 1) {
					$result_record["data_gamapay"] = $result_record["data"];
					unset($result_record["data"]);
				}
			}
		}
		else {
			//資料不存在
			rg_activity_log($conn, "", "查詢交易訂單失敗", "無此訂單", $pure_data, "");
			$ans = GetSystemCode("5050036", $language, $conn);
			$result_record = [
				"result" => 0,
				"systemCode" => $ans[0],
				"title" => $ans[1],
				"description" => $ans[2]
			];
		}
		$headers = array('Content-Type' => 'application/json; charset=utf-8');
		return Response()->json($result_record, Response::HTTP_OK, $headers, JSON_UNESCAPED_UNICODE);
	}

	public function testGamapay(Request $request) {
		$activity = $request->input("activity");
		$token = $request->input("token");
		$pure_data = $request->input("pure_data");
		$purchase_list = json_decode(json_encode($request->input("purchase_list")));
		$booking_id = $request->input("booking_id");
		$type = \CONST_GENERATE_CREDIT_ORDER_TYPE::GAMA_PAY;
		$mode = \CONST_GENERATE_CREDIT_ORDER_MODE::PARKING_PAY;
		$gamapay = new GamapayHelper();
		$result_record = json_decode($gamapay->OrderCreateFunc($activity,
			$token,
			$pure_data,
			$purchase_list,
			$booking_id,
			$type,
			$mode),true);
		$headers = array('Content-Type' => 'application/json; charset=utf-8');
		return Response()->json($result_record, Response::HTTP_OK, $headers, JSON_UNESCAPED_UNICODE);
	}
}
