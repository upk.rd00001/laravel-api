<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AnalysisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
	 */
    public function index()
    {
		$headers = array('Content-Type' => 'application/json; charset=utf-8');
		return  Response()->json(["result"=>1], Response::HTTP_OK, $headers, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
	 */
    public function pointAging()
    {
		$ans = DB::select("
			SELECT `year`,
					SUM(CASE 
						WHEN `free` = 1
						THEN `m_pd_origin_point` 
						ELSE 0 
					END) as `free_parking_point`,
					SUM(CASE 
						WHEN `paid` = 1
						THEN `m_pd_origin_point` 
						ELSE 0 
					END) as `paid_parking_point`
			FROM (
				SELECT LEFT(`m_pd_create_datetime`, 4) as `year`,
					`m_pd_origin_point`,
					(CASE WHEN `m_pd_method` IN (1, 2, 13, 19) THEN 1 ELSE 0 END) as `paid`,
					(CASE WHEN `m_pd_method` NOT IN (1, 2, 13, 19) THEN 1 ELSE 0 END) as `free`
				FROM `tb_Member_Paid`
			) a
			GROUP BY `year`
		");
		//dd($ans);
		foreach($ans as $each_ans) {
			$check_exist = DB::table('tb_dashboard_point_aging_analysis')->where('year',$each_ans->year)->count();
			if($check_exist == 0)
				DB::insert('insert into tb_dashboard_point_aging_analysis (year, original_free_parking_point ,original_paid_parking_point) values (?, ?, ?)',[$each_ans->year,$each_ans->free_parking_point,$each_ans->paid_parking_point]);
			else {
				DB::update('update tb_dashboard_point_aging_analysis set original_free_parking_point = '.$each_ans->free_parking_point.', original_paid_parking_point = '.$each_ans->paid_parking_point.' where year = ?', [$each_ans->year]);
			}
		}

		$ans = DB::select("SELECT LEFT(`m_pd_create_datetime`, 4) as `year`, sum(`m_pd_point`)as 'paid_parking_point' FROM `tb_Member_Paid` WHERE `m_pd_method` IN (1, 2, 13, 19) group by year");
		foreach($ans as $each_ans) {
			$check_exist = DB::table('tb_dashboard_point_aging_analysis')->where('year',$each_ans->year)->count();
			if($check_exist == 0)
				DB::insert('insert into tb_dashboard_point_aging_analysis (year, unused_paid_parking_point) values (?, ?)',[$each_ans->year,$each_ans->paid_parking_point]);
			else {
				DB::update('update tb_dashboard_point_aging_analysis set unused_paid_parking_point = '.$each_ans->paid_parking_point.' where year = ?', [$each_ans->year]);
			}
		}
		$ans = DB::select("SELECT LEFT(`m_pd_create_datetime`, 4) as `year`, sum(`m_pd_point`)as 'free_parking_point' FROM `tb_Member_Paid` WHERE `m_pd_method` NOT IN (1, 2, 13, 19) group by year");
		foreach($ans as $each_ans) {
			$check_exist = DB::table('tb_dashboard_point_aging_analysis')->where('year',$each_ans->year)->count();
			if($check_exist == 0)
				DB::insert('insert into tb_dashboard_point_aging_analysis (year, unused_free_parking_point) values (?, ?)',[$each_ans->year,$each_ans->free_parking_point]);
			else {
				DB::update('update tb_dashboard_point_aging_analysis set unused_free_parking_point = '.$each_ans->free_parking_point.' where year = ?', [$each_ans->year]);
			}
		}
		$ans = DB::select("SELECT a.year,count(*) as member_count FROM (SELECT (`m_id`), LEFT(`m_pd_create_datetime`, 4) as `year` FROM `tb_Member_Paid` WHERE `m_pd_method` IN (1, 2, 13, 19) group by m_id,year) as a GROUP BY a.year");
		foreach($ans as $each_ans) {
			$check_exist = DB::table('tb_dashboard_point_aging_analysis')->where('year',$each_ans->year)->count();
			if($check_exist == 0)
				DB::insert('insert into tb_dashboard_point_aging_analysis (year, unused_paid_parking_point_member) values (?, ?)',[$each_ans->year,$each_ans->member_count]);
			else {
				DB::update('update tb_dashboard_point_aging_analysis set unused_paid_parking_point_member = '.$each_ans->member_count.' where year = ?', [$each_ans->year]);
			}
		}
		$ans = DB::select("SELECT a.year,count(*) as member_count FROM (SELECT (`m_id`), LEFT(`m_pd_create_datetime`, 4) as `year` FROM `tb_Member_Paid` WHERE `m_pd_method` NOT IN (1, 2, 13, 19) group by m_id,year) as a GROUP BY a.year");
		foreach($ans as $each_ans) {
			$check_exist = DB::table('tb_dashboard_point_aging_analysis')->where('year',$each_ans->year)->count();
			if($check_exist == 0)
				DB::insert('insert into tb_dashboard_point_aging_analysis (year, unused_free_parking_point_member) values (?, ?)',[$each_ans->year,$each_ans->member_count]);
			else {
				DB::update('update tb_dashboard_point_aging_analysis set unused_free_parking_point_member = '.$each_ans->member_count.' where year = ?', [$each_ans->year]);
			}
		}
		$headers = array('Content-Type' => 'application/json; charset=utf-8');
		return  Response()->json(["result"=>1], Response::HTTP_OK, $headers, JSON_UNESCAPED_UNICODE);
    }
}
