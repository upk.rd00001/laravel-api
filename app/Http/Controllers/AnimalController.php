<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AnimalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
	 */
    public function index()
    {
		$headers = array('Content-Type' => 'application/json; charset=utf-8');
		return  Response()->json(["result"=>1], Response::HTTP_OK, $headers, JSON_UNESCAPED_UNICODE);
		//commmit
    }
}
