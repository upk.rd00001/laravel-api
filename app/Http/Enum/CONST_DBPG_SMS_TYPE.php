<?php

namespace App\Http\Enum;

class CONST_DBPG_SMS_TYPE
{
	const LOGIN = 1;

	const BARCODE = 2;

	const FORGOT_PASSWORD = 3;

	const OUT_NOTIFY = 4;

	const AUTO_RUN_OUT_NOTIFY = 4;//未出場通知簡訊

	const ADMIN_OUT_NOTIFY = 5;//未出場通知簡訊
}