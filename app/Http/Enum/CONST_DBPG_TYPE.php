<?php

namespace App\Http\Enum;

class CONST_DBPG_TYPE
{ //Dashboard Parking
	const INVALID_BOOKING_LOG = 1;//作廢訂單

	const REVERSE_BOOKING_LOG = 2;//還原訂單

	const UPDATE_PLATE_NO = 3;//修改車牌

	const UPDATE_IN_DATETIME = 4;//調整進場時間

	const UPDATE_OUT_DATETIME = 5;//調整出場時間/出場

	const INSERT_UNPAID = 6;//新增繳費單

	const REFUND = 7;//退款

	const INVALID_INVOICE = 8;//作廢發票

	const GENERATE_INVOICE = 9;//開立發票

	const INSERT_DEPOSIT = 10;//輸入繳費紀錄

	const ADD_REMARK = 11;//新增備註

	const LOGIN = 12;//登入

	const LOGOUT = 13;//登出

	const SEND_SMS = 14;//發送簡訊

	const PARKING_300_OUT = 15;//車偵停車出場

	const PARKING_301_IN = 16;//掃碼停車進場

	const PARKING_301_OUT = 17;//掃碼停車出場

	const PARKING_302_PAY = 18;//車辨停車繳費

	const PARKING_302_OUT = 19;//車辨繳費出場

	const PUSH_AUTHORIZED_STORE = 20;//進出場特約店推廣

	const DELETE_FAREKIND = 21;//刪除費率

	const INSERT_FAREKIND = 22;//新增費率

	const ADMIN_OUT = 23;//後台出場

	const ADMIN_OUT_NOTIFY = 24;//後台所有停車頁面提醒出場的簡訊
}