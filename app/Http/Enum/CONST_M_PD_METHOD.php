<?php

namespace App\Http\Enum;

class CONST_M_PD_METHOD
{
	const UNDEFINED = 0;

	const MEMBER_PAID_CREDIT_CARD = 1;    //升等方案信用卡

	const MEMBER_PAID_THIRD_PARTY = 2;    //升等方案-第三方支付

	const GIVE_PAID_BACKDOOR = 3;//停車點數贈送 後台

	const GIVE_PAID_EVERYDAY = 4;//停車點數贈送 每天60(現在無)

	const STORE_TRANSFER = 5;//會員取得店家轉移點數

	const PAID_ACTIVATION = 6;//停車點數激活

	const MEMBER_TRANSFER = 7;//店家取得會員停車點數

	const AGENT_POINT = 8;//推薦會員贈送停車點數 一個會員50點

	const PROFIT_EARN_TRANSFER = 9;//分潤轉換停車點數

	const CANCEL_BOOKING = 10;//取消訂單返回停車點數

	const AUTHORIZED_STORE_AD_BACK = 11;//特約店推廣審核失敗返回的停車點數

	const TRASNFER_PAID_FREE = 12;//會員轉給會員的停車點數(贈送版不開發票)

	const TRASNFER_PAID = 13;//會員轉給會員的停車點數(購買版要開發票)

	const MEMBER_PAID_FREE_POINT = 14;// 購買時 贈送的停車點數

	const FIRST_PAY_GIVE_PARKINT_POINT = 15;// 會員首次交易後送Y%的停車點數

	const USE_APP_PAY_FEEDBACK = 16;// 會員首次交易後送Y%的停車點數

	const INVOICE_GET_PARKING_POINT = 17;// 輸入發票獲得的停車點數

	//消費者在特約店消費100元/點，特約店回饋2%的情境範例
	const AS_TO_CUSTOMER_PAID_BACK = 18;// 會員至特約店消費後，特約店給的回饋點數 (會員+2點)

	const AS_TO_CUSTOMER_REFUND = 19;//因變數名稱有改，所以備份用，理論上CODE裡沒再用

	const AS_TO_CUSTOMER_PARKING_POINT_REFUND = 19;// 特約店退款給會員 (會員+100元/點)

	const AS_TO_CUSTOMER_PAID_BACK_REFUND = 20;// 特約店退款給會員時，會員剩餘點數不夠扣產生的負數 (會員-2點)

	const AS_TO_CUSTOMER_PAID_BACK_AS_REFUND = 21;// 特約店退款給會員時，特約店返回當初回饋的點數 (特約店+2點)

	const REPORT_PARKING_SPACE = 22;// 車位現況檢舉

	const UNDEFINED_NEGATIVE_NUMBER = 999;// 其他未定義導致點數變負數的型態

	public function echoConstant()
	{
		//echo self::SOME_CONSTANT;
	}
}