<?php
namespace App\Http\Enum;

class CONST_M_DPT_TRADE_PLATFORM
{
	const UNDEFINED = 0;

	const NONE = 1;    //無交易平台 (通常是0元用點數付款)

	const ALL_PAY = 2;    //

	const EC_PAY = 3;    //

	const GAMA_PAY = 4;    //

	const LINE_PAY = 5;//

	const GOOGLE_PAY = 6;//

	const APPLE_PAY = 7;//

	const DIRECT_PAY = 10;//

	const DIRECT_PAY_BY_CARD = 11;//

	const WEIXIN_PAY = 12;//

	const JKO_PAY = 13;//

	const CASH_PAID = 14;//

	const EC_PAY_BARCODE = 15;//

	const EC_PAY_BARCODE_FAILED = 415;//

	const AS_PAID_PARKING_POINT = 16;//

	const EASY_WALLET = 17;//

	const EPM_ZERO_CHANG_CHENG_RD = 101;//

	const EPM_ALL_PAY_CHANG_CHENG_RD = 102;//

	const EPM_EC_PAY_CHANG_CHENG_RD = 103;//

	const EPM_GAMA_PAY_CHANG_CHENG_RD = 104;//

	const MI_UNKNOWN = 21;//

	const MI_EASY_CARD = 22;//悠遊卡

	const MI_ONE_PASS_CARD = 23;//一卡通

	const OTHER = 90;//其他 //後台新增繳費紀錄

	public function echoConstant()
	{
		//echo self::SOME_CONSTANT;
	}
}