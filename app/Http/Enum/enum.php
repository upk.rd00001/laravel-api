<?php

class CONST_M_PFE_TYPE {
	const UNDEFINED	 = 0;
	const SHARING_PARKING_SPACE = 1;	//車位共享收入
	const AGENT_POINT = 2;	//會員首次停入300/301贈送分潤點數 一個會員20點
	const TRANSFER_TO_CASH = 1001;	//升等方案-第三方支付
	const TRANSFER_TO_PAID = 1002;//停車點數贈送 後台
	public function echoConstant() {
		//echo self::SOME_CONSTANT;
	}
}
class CONST_M_PDL_REASON {
	const UNDEFINED	 = 0;
	const SHARING_PARKING_SPACE = 9;	//至店家消費點數
	const TRANSFER_TO_PAID = 10;//預約停車消費點數
	const STORE = 11;//店家推廣申請
	const BOOKING = 12;//100%停車點數折抵
	const BOOKING_CANCEL = 13;//車位主30分內取消預約扣點
	const STORE_TO_CUSTOMER = 14;//店家贈送停車點數
	const ROAD_SIDE = 15;//路邊扣繳
	const HANG_NAN = 16;//杭州南路
	const CUSTOMER_TO_STORE = 17;//會員給店家停車點數
	const BACKDOOR = 18;//後台扣除停車點數
	const AUTHORIZED_STORE_AD = 19;//發送推廣扣除停車點數
	const SEND_PAID_TO_CUSTOMER = 20;//會員支付給店家相似於CUSTOMER_TO_STORE
	const OTHER = 21;//其他消費點數補deposit紀錄，補資料管理員用
	const CUSTOMER_TO_AS_PAID = 22;//CUSTOMER_TO_STORE = 17;//會員給店家停車點數 但是存不同table
	const AS_TO_CUSTOMER_PAID_BACK = 23;//會員至特約店消費後，特約店回饋點數
	const AS_TO_CUSTOMER_REFUND = 24;//因變數名稱有改，所以備份用，理論上CODE裡沒再用
	const AS_TO_CUSTOMER_PAID_BACK_REFUND = 24;// 會員至特約店消費後，特約店給的回饋點數不夠返回時，給予該會員負數
	public function echoConstant() {
		//echo self::SOME_CONSTANT;
	}
}
class CONST_M_PYL_TYPE {
	//消費種類(0=位定義 1=取消訂位 2=停車消費 3=特約店消費 4=特約店廣告 5=車位主30分內取消 14=特約店贈送會員停車點數 15=路邊自動扣繳)
	const UNDEFINED	 = 0;
	const BOOKING_CANCELED = 1;	//取消訂位
	const BOOKING_PAID = 2;	//停車消費
	const AUTHORIZED_STORE_SHOP = 3;	//特約店消費
	const AUTHORIZED_STORE_AD_OLD = 4;	//特約店廣告(之前為4現為19)
	const BOOKING_CANCELED_BY_PK_OWNER = 5;	//車位主30分內取消
	const SHARING_PARKING_SPACE = 9;	//至店家消費點數
	const TRANSFER_TO_PAID = 10;//停車消費
	const STORE_ = 11;//店家推廣申請
	const BOOKING = 12;//100%停車點數折抵
	const BOOKING_CANCEL = 13;//車位主30分內取消預約扣點
	const STORE_TO_CUSTOMER = 14;//店家贈送停車點數
	const ROAD_SIDE = 15;//路邊扣繳
	const HANG_NAN = 16;//杭州南路
	const CUSTOMER_TO_STORE = 17;//會員給店家停車點數
	const BACKDOOR = 18;//後台扣除停車點數
	const AUTHORIZED_STORE_AD = 19;//發送推廣扣除停車點數
	const SEND_PAID_TO_CUSTOMER = 20;//給其他會員停車點數
	const DEPOSIT_MANUAL_INSERT = 21;//後台補deposit增加的紀錄
	const CUSTOMER_TO_AS_PAID = 22;//CUSTOMER_TO_STORE = 17;//會員給店家停車點數 但是存不同table
	const AS_TO_CUSTOMER_PAID_BACK = 23;//會員至特約店消費後，特約店回饋點數(特約店扣2點)
	const AS_TO_CUSTOMER_REFUND = 24;//因變數名稱有改，所以備份用，理論上CODE裡沒再用
	const AS_TO_CUSTOMER_PAID_BACK_REFUND = 24;//會員至特約店消費後，特約店退款要扣會員停車點數(消費者扣4點)
	public function echoConstant() {
		//echo self::SOME_CONSTANT;
	}
}

class CONST_DV_TYPE {
	const GROUND_LOCK	 = 0;//地鎖
	const GEOMAGNETIC = 1;	//地磁
	const ACCESS_CONTROLLER = 2;	//門禁控制器
	const GATE = 3;	//閘門
	const GOV_MANUAL_SIGN_UP = 4;	//人工加簽時間
	const INAN_GEOMAGNETIC = 5;//應安杭南停車場地磁進場時間
	const MANUAL_FIX_APPROACH_TIME = 6;//手動校正應安時間
	const LPR_IN_OR_OUT = 7;//車辨進出場
	const APPROACH_301_COIL = 8;//301進場線圈
	const APPEAR_301_COIL = 9;//301出場線圈
	const CONTROLLER_IN_LAST_DATETIME = 10;//10=進場控制器最後一次連線時間,
	const CONTROLLER_OUT_LAST_DATETIME = 11;//11=出場控制器最後一次連線時間,
	const CONTROLLER_IN_OR_OUT_LAST_DATETIME = 12;//12=進出控制器最後連線時間
	const LPR_TOWER = 13;//13=車柱
	const WIN_CHARGE = 14;//14=充電樁
	public function echoConstant() {
		//echo self::SOME_CONSTANT;
	}
}
class CONST_DCP_ONSALE_TYPE {
	const UNDEFINED	 = 0;//
	const FIRST_PAY_FREE_CASH_PERCENT = 1;	//首次付款減免比例%
	const FIRST_PAY_GIVE_POINT_PERCENT = 2;	//會員首次交易後送Y%的停車點數(推播)
	const GOV_IN_X_MINUTES = 3;	//路邊停車前X分鐘 優惠 Y元
	const APP_PAY_FEEDBACK = 4;	//會員繳費後回饋X%
	const INVOICE_GET_PARKING_POINT = 5;	//發票號碼換點數活動
	const CAN_DISCOUNT_M_ID_ARRAY = 11;	//某會員用指定倍率金額收費，或除了某會員以外的會員用指定倍率金額收費
	const PRICE_DURING_PERIOD_TIME = 12;	//停車在某一段時間內用一PRICE計費
	public function echoConstant() {
		//echo self::SOME_CONSTANT;
	}
}
class CONST_DCP_TYPE {
	const UNDEFINED	 = 0;//
	const PARKING_LOT = 9;	//停車場
	const PARKING_SPACE = 6;	//車位
	const GOV_PARKING_SPACE = 100;	//路邊車位
	const INVOICE_GET_PARKING_POINT = 5;	//發票號碼換點數活動
	public function echoConstant() {
		//echo self::SOME_CONSTANT;
	}
}
class CONST_GET_PUSH_TO_FILELOG {
	const BOOKING_LOG = 1;	//預約
	const PRICING_LOG = 2;	//空位時段
	const PARKING_SPACE = 3;	//車位
	const PARKING_LOT = 4;	//停車場
}
class CONST_GENERATE_TIME_COMPONENT {
	const BOOKING_LOG = 1;	//預約
	const PRICING_LOG = 2;	//空位時段
	const WHITE_BLACK_LIST = 3;	//白名單列表
}
class CONST_FL_TYPE {
	//0 = 未定義, 1 = 駕照正面, 2 = 駕照背面, 3 = 車輛, 4 = 原為車輛占不用, 5 = 車位入口, 6 = 停車位, 7 = 停車教學圖片,
	//8 = 臨時註冊車輛照片, 9 = 停車場 10=特約店照片 11=車牌辨識照片,12=特約店推廣照片,13=檢舉照片, 14=客服回報
	//15=後台預約佐證預約用圖' 16=停車場定時抓拍 17=車位定時抓拍 18=車位進場照片 19=車位出場照片 999=預設照片 1000=路邊googlemap圖
	const MEMBER_VEHICLE = 3;
	const MEMBER_VEHICLE_2 = 4;
	const PARKING_SPACE_ENTRANCE = 5;
	const PARKING_SPACE = 6;
	const PARKING_LOG_TUTORIAL = 7;
	//const TMP_MEMBER_VEHICLE = 8; //臨時註冊車輛照片
	const PARKING_LOT = 9;
	const AUTHORIZED_SOTRE = 10;
	const LICENCE_PLATE_RECOGNIZED = 11;
	const AUTHORIZED_SOTRE_AD = 12;
	const COMPLAIN = 13;
	const CUSOTMER_SERVICE = 14;
	const ADMIN_BOOKING_LOG_PROOF = 15;
	const PARKING_LOT_SCHEDULE_LPR_TOWER = 16;
	const PARKING_SPACE_SCHEDULE_LPR_TOWER = 17;
	const PARKING_SPACE_IN = 18;
	const PARKING_SPACE_OUT = 19;
	const DISPLAY_DEFAULT	 = 999;//
}
class CONST_MM_TYPE {
	const UNDEFINED	 = 0;//
	const BOOKING_LOG	 = 1;//應該是有預約的mm_type
	const PARKING_LOT = 9;	//停車場
	const PARKING_SPACE = 6;	//車位
}
//1 = 歐富寶  2 = 綠界  3=gamapay. 4 = line pay 5=google pay  6=apple pay  7=EPM_ecpay 8=EPM_opay=  9=無  10=direct pay 11=directpay by card  12=weixin pay 13=jko pay 14=現金付款cash_paid 15=barcode超商付款
class CONST_GENERATE_CREDIT_ORDER_TYPE {
	const UNDEFINED	 = 0;//
	const O_PAY	 = 1;	//歐富寶
	const EC_PAY = 2;	//綠界
	const GAMA_PAY = 3;	//
	const LINE_PAY = 4;	//
	const GOOGLE_PAY = 5;	//
	const APPLE_PAY = 6;	//
	const EPM_EC_PAY = 7;	//
	const EPM_O_PAY = 8;	//
	const DIRECT_PAY = 10;	//
	const DIRECT_PAY_BY_CARD = 11;	//
	const WEIXIN_PAY = 12;	//
	const JKO_PAY = 13;	//
	const CASH_PAID = 14;	//
	const BARCODE = 15;	//
	const PARKING_POINT = 16;	//
	const EASY_WALLET = 17;	//
}
class CONST_GENERATE_CREDIT_ORDER_MODE {
	const UNDEFINED	 = 0;//
	const PARKING_PAY	 = 1;//停車付款
	const UPGRADE_MEMBER_LEVEL = 2;	//會員升等
	const MEMBER_PAID = 3;	//購買停車點數
	const DONATE = 4;	//捐贈
	const AS_PAID = 5;	//特約店付款
}
class CONST_M_ASPD_TAKEOUT_STATUS {
	const ALL	 = -1;//未定義
	const WAITING_VERIFY	 = 0;//待審核
	const VERIFYING	 = 100;//審核中
	const WAITING_TRANSFER = 200;	//待撥款
	const TRANSFER_COMPLETE = 300;	//撥款完成
	const VERIFY_FAILED = 400;	//審核失敗
	const CUSTOMER_CANCELED = 1000;	//使用者取消
}
class CONST_REMARK_TYPE {
	//3=(3=進出回報,4=我的預約,5=我的被預約),10=特約店,12=特約店推廣 15=政府公開資料停車場
	const UNDEFINED	 = 0;//
	const PARKING_LOT = 9;	//停車場
	const PARKING_SPACE = 6;	//車位
	const IN_OUT_REPORT = 3;	//路邊車位
	const AUTHORIZED_STORE = 10;	//發票號碼換點數活動
	const AUTHORIZED_STORE_AD = 12;	//特約店推廣
	const GOV_OPEN_PARKING_LOG = 15;	//政府公開資料停車場
	const AUTHORIZED_STORE_PAID_CUSTOMER_REMARK = 16;	//特約店收款使用者紀錄
}

class CONST_CPL_TYPE {
	const PARKING_LOG=9; //停車場
	const PARKING_SPACE=6; //車位
	const IN_OUT_REPORT=3; //進出回報
	const BOOKING=4; //我的預約紀錄
	const HAS_BOOKING=4; //我的被預約紀錄
	const AUTHORIZED_STORE=10; //特約店
	const AUTHORIZED_STORE_AD=12; //特約店推廣
	const GOV_PARKING=15; //政府公開資料停車場
	const REPORT_PARKING_SPACE=16; //車位現況檢舉
}
class CONST_CPL_STATUS {
	const UNDEFINED=0; //預設0 PHP7不能使用DEFAULT
	const VERIFYING=1; //審核中
	const SUCCESS=2; //審核成功
	const FAILED=3; //審核失敗
}
?>