<?php

include_once("UPK_GetAuthorizedStorePaidData.php");
include_once("fn_package.php");

class UPK_GetAuthorizedStorePaidResponse
{
	private $id;//call api的會員ID
	private $type; //1=我的紀錄內的付款給特約店紀錄  2=特約店家自行查看收款紀錄 3=後台管理員查看的後台紀錄
	private $m_aspd_takeout_reason;//會員可能要篩選提領狀態
	private $m_aspd_takeout_status;//會員可能要篩選提領狀態

	private $m_id_as_owner;//篩選特約店擁有人ID (後台用)
	private $m_as_id;//篩選特該特約店 (後台用)
	private $is_admin;
	private $start_datetime;
	private $end_datetime;

	private $page;
	private $page_count;
	private $total_page;
	private $total_cnt;

	private $data;
	private $aspd_point;
	private $takeout_point;
	private $total_point;
	//outputs binary
	public $B_ALL;
	public $B_AS_NAME = 1;
	public $B_DATETIME = 2;
	public $B_FEE = 4;
	public $B_REMARK = 8;
	public $B_S4C_HANDLING_FEE = 16;
	public $B_TYPE = 32;
	public $B_TAKEOUT_LOG = 64;
	public $B_APPLY_FEE = 128;
	public $B_ID = 256;
	public $B_STATUS = 512;
	public $B_DESCRIPTION = 1024;
	public $B_LAST_EDIT = 2048;
	public $B_APPLY_DATETIME = 4096;
	public $B_M_ID = 8192;//申請人ID 或收款人ID  不是as_paid裡面的付款人的ID
	public $B_AS_ID = 16384;//特約店ID
	public $B_TRANSFER_FEE = 32768;//轉帳手續費
	public $B_INVOICE = 65536;//發票訊息
	public $B_PAID_BACK = 131072;//回饋點數

	const CONST_TYPE_MEMBER_PAID_TO_AUTHORIZED_STORE = 1;
	const CONST_TYPE_AUTHORIZED_STORE_PAID_AND_TAKE_OUT = 2;
	const CONST_TYPE_BACKEND_ALL_RECORD = 3;

	private $result_array;

	public function __construct()
	{
		$this->data = array();
		$this->total_cnt = 0;
	}

	public function get_result_array()
	{
		return $this->result_array;
	}

	public function get_total_point()
	{
		$this->total_point = $this->aspd_point - $this->takeout_point;
		if ($this->total_point < 0) {
			$this->total_point = 0;
		}
		return $this->total_point;
	}

	public function get_total_page()
	{
		return $this->total_page;
	}

	public function get_total_cnt()
	{
		return $this->total_cnt;
	}

	public function init_as_paid_response($args)
	{
		$this->takeout_point = 0;
		$this->aspd_point = 0;
		if (isset($args["start_datetime"])) {
			$this->start_datetime = $args["start_datetime"];
		} else {
			$this->start_datetime = "";
		}
		if (isset($args["end_datetime"])) {
			$this->end_datetime = $args["end_datetime"];
		} else {
			$this->end_datetime = "";
		}
		if (isset($args["id"])) {
			$this->id = $args["id"];
		} else {
			$this->id = "";
		}
		if (isset($args["type"])) {
			$this->type = $args["type"];
		} else {
			$this->type = 0;
		}

		if (isset($args["m_aspd_takeout_status"])) {
			$this->m_aspd_takeout_status = $args["m_aspd_takeout_status"];
		} else {
			$this->m_aspd_takeout_status = -1;
		}
		if (isset($args["m_aspd_takeout_reason"])) {
			$this->m_aspd_takeout_reason = $args["m_aspd_takeout_reason"];
		} else {
			$this->m_aspd_takeout_reason = 0;
		}

		if (isset($args["m_id_as_owner"])) {
			$this->m_id_as_owner = $args["m_id_as_owner"];
		} else {
			$this->m_id_as_owner = "";
		}
		if (isset($args["m_as_id"])) {
			$this->m_as_id = $args["m_as_id"];
		} else {
			$this->m_as_id = "";
		}
		if (isset($args["is_admin"])) {
			$this->is_admin = $args["is_admin"];
		} else {
			$this->is_admin = 0;
		}

		if (isset($args["page"])) {
			$this->page = $args["page"];
		} else {
			$this->page = 1;
		}
		if (isset($args["page_count"])) {
			$this->page_count = $args["page_count"];
		} else {
			$this->page_count = 50;
		}

		if (isset($args["type"])) {
			$this->type = $args["type"];
		} else {
			$this->type = 0;
		}

		if ($this->type == 0) {
			$this->result_array = array("result" => 0, "title" => "取得提領紀錄錯誤", "description" => "請設定狀態");
			return false;
		}
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$DT_now_m_30d = new Datetime();
		$DT_now_m_30d->modify("-7 day");

		$sql_takeout_filter = "";
		$sql_takeout_log_filter = "";
		$sql_as_paid_filter = "";
		if ($this->m_as_id != "") {
			$sql_takeout_filter .= " AND m_as_id = '".$this->m_as_id."' ";
			$sql_takeout_log_filter .= " AND m_as_id = '".$this->m_as_id."' ";
			$sql_as_paid_filter .= " AND m_as_id = '".$this->m_as_id."' ";
		}
		if ($this->start_datetime != "") {
			$sql_takeout_filter .= " AND m_aspd_takeout_create_datetime > '".$this->start_datetime."' ";
		}
		if ($this->end_datetime != "") {
			$sql_takeout_filter .= " AND m_aspd_takeout_create_datetime < '".$this->end_datetime."' ";
		}
		if ($this->start_datetime != "") {
			$sql_takeout_log_filter .= " AND m_aspd_takeout_create_datetime > '".$this->start_datetime."' ";
		}
		if ($this->end_datetime != "") {
			$sql_takeout_log_filter .= " AND m_aspd_takeout_create_datetime < '".$this->end_datetime."' ";
		}
		if ($this->start_datetime != "") {
			$sql_as_paid_filter .= " AND m_aspd_pay_datetime > '".$this->start_datetime."' ";
		}
		if ($this->end_datetime != "") {
			$sql_as_paid_filter .= " AND m_aspd_pay_datetime < '".$this->end_datetime."' ";
		}
		//search_keyword check
		if (isset($args["search_keyword"]) && $args["search_keyword"] != "") {
			$reg_pattern = "/^(09)\d{8}$/";
			if (preg_match($reg_pattern, $args["search_keyword"])) {
				//電話取M_ID
				$get_phone_user_sql = "SELECT m_id FROM tb_Member WHERE m_cellphone = TRIM(LEADING '0' FROM ".mysql_escape_string($args["search_keyword"]).")";
				$get_phone_user_result = mysql_query($get_phone_user_sql, $conn);
				if (!$get_phone_user_result) {
					$this->result_array = array(
						"result" => 0, "title" => "取得會員ID錯誤", "description" => mysql_error($conn)
					);
					return false;
				} else {
					$num_rows = mysql_num_rows($get_phone_user_result);
					if ($num_rows == 0) {
						$this->result_array = array(
							"result" => 0, "title" => "無此會員ID", "description" => mysql_error($conn)
						);
						return false;
					} else {
						$phone_match_mid_arr = [];
						while ($ans = mysql_fetch_assoc($get_phone_user_result)) {
							array_push($phone_match_mid_arr, $ans["m_id"]);
						}
						$phone_match_mids = array_map('mysql_real_escape_string', array_values($phone_match_mid_arr));
						$sql_as_paid_filter .= " AND m_id IN ('".implode("','", $phone_match_mids)."') ";
						$sql_takeout_filter .= " AND m_id IN ('".implode("','", $phone_match_mids)."') ";
					}
				}
			} else {
				$m_id = $args["search_keyword"];
				Sn2Id("UMID", $m_id);
				$sql_as_paid_filter .= " AND m_id = '".$m_id."' ";
				$sql_takeout_filter .= " AND m_id = '".$m_id."' ";
			}
		}

		if ($this->type == self::CONST_TYPE_MEMBER_PAID_TO_AUTHORIZED_STORE) {
			$sql = "SELECT m_aspd_id FROM tb_Member_As_Paid WHERE m_id = '".$this->id."' AND m_aspd_pay_datetime IS NOT NULL ";
			$sql .= $sql_as_paid_filter;
			$result = mysql_query($sql, $conn);
			if (!$result) {
				$this->result_array = array("result" => 0, "title" => "取得提領紀錄錯誤", "description" => mysql_error($conn));
				return false;
			}
			while ($ans = mysql_fetch_assoc($result)) {
				$m_aspd_id = $ans["m_aspd_id"];
				$this_as_paid_data = new UPK_GetAuthorizedStorePaidData();
				$this_as_paid_data->init_as_paid($m_aspd_id);
				array_push($this->data, $this_as_paid_data);
			}
		} elseif ($this->type == self::CONST_TYPE_AUTHORIZED_STORE_PAID_AND_TAKE_OUT) {
			//(預設) -1 => 不過濾
			//待審核  0 => 影響提領金額運算
			//審核中  100 => 影響提領金額運算
			//待撥款 200 => 影響提領金額運算
			//撥款完成 300 => 影響提領金額運算
			//審核失敗 400
			//使用者取消(只有"待審核"時可以取消) 1000
			if ($this->m_aspd_takeout_reason == 1 || $this->m_aspd_takeout_reason == 0) {
				$sql = "SELECT * FROM tb_Member_As_Paid WHERE m_id_as_owner = '".$this->id."' AND m_aspd_pay_datetime IS NOT NULL ";
				$sql .= $sql_as_paid_filter;
				$result = mysql_query($sql, $conn);
				if (!$result) {
					$this->result_array = array(
						"result" => 0, "title" => "取得提領紀錄錯誤", "description" => mysql_error($conn)
					);
					return false;
				}
				while ($ans = mysql_fetch_assoc($result)) {
					$m_aspd_id = $ans["m_aspd_id"];
					$this_as_paid_data = new UPK_GetAuthorizedStorePaidData();
					$this_as_paid_data->init_as_paid($m_aspd_id);
					$m_aspd_as_fee = $this_as_paid_data->get_apply_fee();
					$DT_m_aspd_create_datetime = new DateTime($this_as_paid_data->get_datetime());
					if ($DT_now_m_30d >= $DT_m_aspd_create_datetime) {
						$this->aspd_point += $m_aspd_as_fee;
					}
					array_push($this->data, $this_as_paid_data);
				}
			}
			if ($this->m_aspd_takeout_reason == 2 || $this->m_aspd_takeout_reason == 0) {
				$sql = "SELECT * FROM tb_Member_As_Paid_Take_Out WHERE m_id = '".$this->id."' AND m_aspd_takeout_delete=0 ";
				$sql .= $sql_takeout_filter;
				$result = mysql_query($sql, $conn);
				if (!$result) {
					$this->result_array = array(
						"result" => 0, "title" => "取得提領紀錄錯誤", "description" => mysql_error($conn)
					);
					return false;
				}
				while ($ans = mysql_fetch_assoc($result)) {
					$m_aspd_takeout_id = $ans["m_aspd_takeout_id"];
					$this_as_paid_takeout_data = new UPK_GetAuthorizedStorePaidData();
					$this_as_paid_takeout_data->init_as_paid_takeout($m_aspd_takeout_id);
					$m_aspd_takeout_status = $this_as_paid_takeout_data->get_status();
					$m_aspd_takeout_apply_money = $this_as_paid_takeout_data->get_fee();
					if ($m_aspd_takeout_status == CONST_M_ASPD_TAKEOUT_STATUS::ALL ||
						$m_aspd_takeout_status == CONST_M_ASPD_TAKEOUT_STATUS::WAITING_VERIFY ||
						$m_aspd_takeout_status == CONST_M_ASPD_TAKEOUT_STATUS::VERIFYING ||
						$m_aspd_takeout_status == CONST_M_ASPD_TAKEOUT_STATUS::WAITING_TRANSFER ||
						$m_aspd_takeout_status == CONST_M_ASPD_TAKEOUT_STATUS::TRANSFER_COMPLETE) {
						$this->takeout_point += $m_aspd_takeout_apply_money;
					}
					array_push($this->data, $this_as_paid_takeout_data);
				}
			}
		} elseif ($this->type == self::CONST_TYPE_BACKEND_ALL_RECORD) {
			if ($this->m_aspd_takeout_reason == 1 || $this->m_aspd_takeout_reason == 0) {
				$sql = "SELECT * FROM tb_Member_As_Paid WHERE m_id_as_owner LIKE '%".$this->m_id_as_owner."%' AND m_aspd_pay_datetime IS NOT NULL ";
				$sql .= $sql_as_paid_filter;
				$result = mysql_query($sql, $conn);
				if (!$result) {
					$this->result_array = array(
						"result" => 0, "title" => "取得提領紀錄錯誤", "description" => mysql_error($conn)
					);
					return false;
				}
				while ($ans = mysql_fetch_assoc($result)) {
					$m_aspd_id = $ans["m_aspd_id"];
					$this_as_paid_data = new UPK_GetAuthorizedStorePaidData();
					$this_as_paid_data->init_as_paid($m_aspd_id);
					array_push($this->data, $this_as_paid_data);
				}
			}
			if ($this->m_aspd_takeout_reason == 2 || $this->m_aspd_takeout_reason == 0) {
				$sql = "SELECT * FROM tb_Member_As_Paid_Take_Out WHERE m_id LIKE '%".$this->m_id_as_owner."%' AND m_aspd_takeout_delete=0 ";
				$sql .= $sql_takeout_filter;
				if ($this->m_aspd_takeout_status != CONST_M_ASPD_TAKEOUT_STATUS::ALL) {
					$sql .= " AND m_aspd_takeout_status='".$this->m_aspd_takeout_status."' ";
				}
				$result = mysql_query($sql, $conn);
				if (!$result) {
					$this->result_array = array(
						"result" => 0, "title" => "取得提領紀錄錯誤", "description" => mysql_error($conn)
					);
					return false;
				}

				while ($ans = mysql_fetch_assoc($result)) {
					$m_aspd_takeout_id = $ans["m_aspd_takeout_id"];
					$this_as_paid_takeout_data = new UPK_GetAuthorizedStorePaidData();
					$this_as_paid_takeout_data->init_as_paid_takeout($m_aspd_takeout_id);
					array_push($this->data, $this_as_paid_takeout_data);
				}
			}
		}
		$this->sort();
		$this->limit();
		return true;
	}

	private function sort()
	{
		for ($i = 0; $i < count($this->data); $i++) {
			for ($j = $i; $j < count($this->data); $j++) {
				if ($this->data[$i]->get_datetime() < $this->data[$j]->get_datetime()) {
					$tmp = $this->data[$i];
					$this->data[$i] = $this->data[$j];
					$this->data[$j] = $tmp;
				}
			}
		}
	}

	private function limit()
	{
		$this->total_cnt = count($this->data);
		if ($this->page_count != 0) {
			$this->total_page = ceil($this->total_cnt / $this->page_count);
		} else {
			$this->total_page = 1;
		}
		if ($this->total_cnt == 0) {
			$this->total_page = 1;
		}
		$tmp_data = array();
		for ($i = ($this->page - 1) * $this->page_count; $i < ($this->page - 1) * $this->page_count + $this->page_count; $i++) {
			if (isset($this->data[$i])) {
				array_push($tmp_data, $this->data[$i]);
			}
		}
		$this->data = $tmp_data;
	}

	public function getAuthorizedStorePaidResponse()
	{
		$B_selector = $this->B_ALL;
		if ($this->type == self::CONST_TYPE_MEMBER_PAID_TO_AUTHORIZED_STORE) {
			$B_selector = $this->B_TYPE | $this->B_AS_NAME | $this->B_DATETIME | $this->B_FEE | $this->B_REMARK | $this->B_PAID_BACK | $this->B_INVOICE;
		} elseif ($this->type == self::CONST_TYPE_AUTHORIZED_STORE_PAID_AND_TAKE_OUT) {
			$B_selector = $this->B_M_ID | $this->B_TYPE | $this->B_ID | $this->B_AS_NAME | $this->B_DATETIME | $this->B_FEE | $this->B_APPLY_FEE | $this->B_S4C_HANDLING_FEE | $this->B_STATUS | $this->B_INVOICE | $this->B_PAID_BACK;
		} elseif ($this->type == self::CONST_TYPE_BACKEND_ALL_RECORD) {
			$B_selector = $this->B_TYPE | $this->B_ID | $this->B_M_ID | $this->B_AS_ID | $this->B_AS_NAME | $this->B_DATETIME | $this->B_FEE | $this->B_APPLY_FEE | $this->B_APPLY_DATETIME | $this->B_REMARK | $this->B_TAKEOUT_LOG | $this->B_STATUS | $this->B_DESCRIPTION | $this->B_LAST_EDIT | $this->B_S4C_HANDLING_FEE | $this->B_TRANSFER_FEE | $this->B_INVOICE | $this->B_PAID_BACK;
		} else {
			//必填欄位未填
			return json_encode(array("result" => 0, "title" => "失敗", "description" => mysql_error()));
		}
		$response_data = array();
		foreach ($this->data as $each_data) {
			array_push($response_data, $each_data->get_array($B_selector));
		}
		return $response_data;
	}
}

?>