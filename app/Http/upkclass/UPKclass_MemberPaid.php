<?php
class UPK_MemberPaid{
	
	private $total_pd_points;
	
	public function __construct($m_id,$sql_logic=""){
		$this->init_m_pd_id($m_id,$sql_logic="");
    }
	public function init_m_pd_id($m_id,$sql_logic=""){
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$language = "zh-tw";
		/*********************************** Put your table name here ***********************************/
		$this->total_pd_points=0;
		$sql2 = "SELECT sum(m_pd_point) FROM tb_Member_Paid WHERE m_id='".$m_id."' and m_pd_limit_datetime>now()";
		$result2 = mysql_query($sql2, $conn);
		if(!$result2)
		{
			return json_encode(array("result"=>0,"title"=>"搜尋停車點數失敗" ,"description"=> mysql_error($conn)));
		}
		$ans2 = mysql_fetch_assoc($result2);
		$this->total_pd_points=(int)$ans2["sum(m_pd_point)"];
		if($this->total_pd_points==null)
			$this->total_pd_points=0;
		
		/**
		* 
		* 2017-10-20 Tiger: 
		* 目前邏輯: 到2018年之前 每天送停車點數60點
		* 判斷如果當天沒送過 0元付款/60點 則insert贈送60點
		* 預計2018之後 變成儲值49，獲得7天每天送60點(未設計)
		* 
		* 
		*/
		//return 1;//2018/01/04 2018不再贈送停車點數 2018/01/06 Kevin說繼續贈送
		//return 1;#if start to give free parking points, mark this line and modify 0 to 60 in# 83
		return 1;//2018/08/01 又不送停車點數了 因為有自動路邊帶扣繳的功能，不能用送的QQ
		//如果當前沒有停車點數才去看有沒有0元免費60點
		if($this->total_pd_points!=0)
			return 1;
		
		$sql = "SELECT * FROM tb_Member_ParkingSpace WHERE (m_id='".$m_id."' OR m_pk_owner_m_id='".$m_id."') AND m_pk_delete='0'";
		$result = mysql_query($sql, $conn);
		if(! $result)
		{
			echo json_encode(array("result"=>0,"title"=>"取得會員資料失敗","description" => mysql_error()));
			return;
		}
		$member_parking_space_count=mysql_num_rows($result);
		//如果沒有車位就不送
		if($member_parking_space_count==0)
			return 1;
			
		$m_pd_method=4;
		$DT_now= new DateTime('now');
		$today= $DT_now->format("Y-m-d")." 00:00:00";
		$sql2 = "SELECT * FROM tb_Member_Paid WHERE m_id='".$m_id."' and m_pd_limit_datetime>'".$today."' AND m_pd_monthlyfee=0 AND m_pd_method='".$m_pd_method."' ";
		
		$result2 = mysql_query($sql2, $conn);
		if(! $result2)
		{
			return json_encode(array("result"=>0,"title"=>"搜尋停車點數失敗" ,"description"=> mysql_error($conn)));
		}
		else if(mysql_num_rows($result2)>0){
			#今日已經送過
		}else{
			$transaction_id = "temp".GenerateRandomString(26, '1234567890abcdefghijklmnopqrstuvwxyz');
			$DT_m_pd_limit_datetime = new DateTime($today);
			$DT_m_pd_limit_datetime->modify('+1 day');
			$date=new DateTime('now');
			$tmp_id = "MBPD".$date->format('YmdHis');
			$sec=microtime();
			$new_id=$tmp_id.$sec[2].$sec[3];//毫秒 $sec[0]=0 $sec[1]=.  $sec[2..3]為毫秒後兩位數  正常顯示為 0.xx (秒)
			$sql = "INSERT INTO tb_Member_Paid (m_pd_id, m_id, m_pd_create_datetime, m_pd_pay_datetime, m_pd_monthlyfee, 
					m_pd_method, m_pd_error_reason, m_pd_request_log, m_pd_transaction_id, m_pd_limit_datetime,m_pd_point,m_pd_origin_point) "
			."SELECT '".$new_id ."', '".$m_id."', now(), now(), 0, '".$m_pd_method."', '無須繳費', '', '".$transaction_id."', '".$DT_m_pd_limit_datetime->format("Y-m-d H:i:s")."','60','60' FROM dual WHERE NOT EXISTS (SELECT 1 FROM tb_Member_Paid WHERE m_id='".$m_id."' and m_pd_limit_datetime>'".$today."' AND m_pd_monthlyfee=0 AND m_pd_method='".$m_pd_method."' ) ";
			if(! mysql_query($sql, $conn))
			{
				return json_encode(array("更新錯誤" => mysql_error()));
			}
			$this->total_pd_points=60;
		}
	}
	function get_total_pd_points()
	{
		return $this->total_pd_points;
	}

}
?>