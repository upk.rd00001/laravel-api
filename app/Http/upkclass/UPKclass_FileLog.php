<?php
class UPK_FileLog
{
	private $fl_sn;
	private $fl_id;
	private $m_id;
	private $fl_upload_time;
	private $fl_file_name;
	private $fl_system_file_name;
	private $fl_system_file_name_thumb;
	private $fl_type;
	private $fl_type_id;
	private $fl_delete;
	private $fl_full_path;
	private $fl_description;
	public $B_fl_sn = 1;
	public $B_fl_id = 2;
	public $B_m_id = 4;
	public $B_fl_upload_time = 8;
	public $B_fl_file_name = 16;
	public $B_fl_system_file_name = 32;
	public $B_fl_system_file_name_thumb = 64;
	public $B_fl_type = 128;
	public $B_fl_type_id = 256;
	public $B_fl_delete = 512;
	public $B_fl_full_path = 1024;
	public $B_fl_description = 2048;
	public $B_ALL;

	public function __construct($fl_id, $sql_logic = "", $action = "select")
	{
		$this->B_ALL = $this->B_fl_sn | $this->B_fl_id | $this->B_m_id | $this->B_fl_upload_time | $this->B_fl_file_name | $this->B_fl_system_file_name | $this->B_fl_system_file_name_thumb | $this->B_fl_type | $this->B_fl_type_id | $this->B_fl_delete | $this->B_fl_full_path | $this->B_fl_description;
		if ($action == "select")
			$this->init_fl_id($fl_id, $sql_logic = "");
		elseif ($action == "insert") {
			//$this->insert_fl_id();
		}
	}

	//如果path為空則試試看name內是否有path
	public function insert($m_id, $fl_type, $fl_type_id, $fl_description, $fl_file_name, $fl_full_path = "", $fl_system_file_name = "", $fl_system_file_name_thumb = "")
	{
		$pure_data = "";
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$tmp_fl_id = 'tmp' . GenerateRandomString(11, '0123456789');
		$sql = "INSERT INTO `tb_File_Log`(`fl_id`, `m_id`, `fl_upload_time`, `fl_file_name`, `fl_system_file_name`, `fl_system_file_name_thumb`, `fl_type`, `fl_type_id`, `fl_delete`, `fl_full_path`, `fl_description`) VALUES ('" . $tmp_fl_id . "','" . $m_id . "',now(),'" . $fl_file_name . "','" . $fl_system_file_name . "','" . $fl_system_file_name_thumb . "','" . $fl_type . "','" . $fl_type_id . "',b'0','" . $fl_full_path . "','" . $fl_description . "')";
		if (!mysql_query($sql, $conn)) {
			//rg_activity_log($conn, "", "上傳照片失敗", "上傳失敗", $pure_data, "");
			$ans = GetSystemCode("20016", $language, $conn);
			rg_activity_log($conn, $m_id, $ans[1], $ans[2], $pure_data, json_encode($ans));
			return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
		}
		else {
			$sql = "SELECT fl_sn FROM tb_File_Log WHERE fl_id='" . $tmp_fl_id . "' and "
				. "m_id='" . $m_id . "' and "
				. "fl_system_file_name='" . $fl_system_file_name . "' ";
			$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
			if (!$ans) {
				//rg_activity_log($conn, "", "上傳照片失敗", "上傳查詢失敗", $pure_data, "");
				$ans = GetSystemCode("20017", $language, $conn);
				rg_activity_log($conn, $m_id, $ans[1], $ans[2], $pure_data, json_encode($ans));
				return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
			$new_id = $ans["fl_sn"];
			Sn2Id("FLID", $new_id);
			$sql = "UPDATE tb_File_Log SET fl_id='" . $new_id . "' WHERE fl_sn='" . $ans["fl_sn"] . "' ";
			if (!mysql_query($sql, $conn)) {
				//rg_activity_log($conn, "", "上傳照片失敗", "上傳更新失敗", $pure_data, "");
				$ans = GetSystemCode("20018", $language, $conn);
				rg_activity_log($conn, $m_id, $ans[1], $ans[2], $pure_data, json_encode($ans));
				return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
		}
		return json_encode(array("result" => 1, "fl_id" => $new_id));
	}

	public function init_fl_id($fl_id, $sql_logic = "")
	{
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$language = "zh-tw";
		/*********************************** Put your table name here ***********************************/
		$sql = "select * from tb_File_Log where fl_id = '" . $fl_id . "' ";

		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			echo json_encode(array("失敗" => mysql_error($conn) . $sql));
			mysql_close($conn);
			return;
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->fl_sn = $ans["fl_sn"];
			$this->fl_id = $ans["fl_id"];
			$this->m_id = $ans["m_id"];
			$this->fl_upload_time = $ans["fl_upload_time"];
			$this->fl_file_name = $ans["fl_file_name"];
			$this->fl_system_file_name = $ans["fl_system_file_name"];
			$this->fl_system_file_name_thumb = $ans["fl_system_file_name_thumb"];
			$this->fl_type = $ans["fl_type"];
			$this->fl_type_id = $ans["fl_type_id"];
			$this->fl_delete = $ans["fl_delete"];
			$this->fl_full_path = $ans["fl_full_path"];
			$this->fl_description = $ans["fl_description"];
		}
	}

	public function get_fl_sn()
	{
		return $this->fl_sn;
	}

	public function get_fl_id()
	{
		return $this->fl_id;
	}

	public function get_m_id()
	{
		return $this->m_id;
	}

	public function get_fl_upload_time()
	{
		return $this->fl_upload_time;
	}

	public function get_fl_file_name()
	{
		return $this->fl_file_name;
	}

	public function get_fl_system_file_name()
	{
		return $this->fl_system_file_name;
	}

	public function get_fl_system_file_name_thumb()
	{
		return $this->fl_system_file_name_thumb;
	}

	public function get_fl_type()
	{
		return $this->fl_type;
	}

	public function get_fl_type_id()
	{
		return $this->fl_type_id;
	}

	public function get_fl_delete()
	{
		return $this->fl_delete;
	}

	public function get_fl_full_path()
	{
		return $this->fl_full_path;
	}

	public function get_fl_description()
	{
		return $this->fl_description;
	}

	public function get_array($select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_fl_sn) != 0) {
			$return_array["fl_sn"] = $this->get_fl_sn();
		}
		if (($select & $this->B_fl_id) != 0) {
			$return_array["fl_id"] = $this->get_fl_id();
		}
		if (($select & $this->B_m_id) != 0) {
			$return_array["m_id"] = $this->get_m_id();
		}
		if (($select & $this->B_fl_upload_time) != 0) {
			$return_array["fl_upload_time"] = $this->get_fl_upload_time();
		}
		if (($select & $this->B_fl_file_name) != 0) {
			$return_array["fl_file_name"] = $this->get_fl_file_name();
		}
		if (($select & $this->B_fl_system_file_name) != 0) {
			$return_array["fl_system_file_name"] = $this->get_fl_system_file_name();
		}
		if (($select & $this->B_fl_system_file_name_thumb) != 0) {
			$return_array["fl_system_file_name_thumb"] = $this->get_fl_system_file_name_thumb();
		}
		if (($select & $this->B_fl_type) != 0) {
			$return_array["fl_type"] = $this->get_fl_type();
		}
		if (($select & $this->B_fl_type_id) != 0) {
			$return_array["fl_type_id"] = $this->get_fl_type_id();
		}
		if (($select & $this->B_fl_delete) != 0) {
			$return_array["fl_delete"] = $this->get_fl_delete();
		}
		if (($select & $this->B_fl_full_path) != 0) {
			$return_array["fl_full_path"] = $this->get_fl_full_path();
		}
		if (($select & $this->B_fl_description) != 0) {
			$return_array["fl_description"] = $this->get_fl_description();
		}
		return $return_array;
	}
}
?>