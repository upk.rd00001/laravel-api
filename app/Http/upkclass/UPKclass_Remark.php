<?php
class UPK_Remark
{
	private $rmk_sn;
	private $rmk_id;
	private $rmk_m_id;
	private $rmk_type;
	private $rmk_type_id;
	private $rmk_title;
	private $rmk_description;
	private $rmk_create_datetime;
	private $rmk_edit_datetime;
	private $rmk_delete;
	public $B_rmk_sn = 1;
	public $B_rmk_id = 2;
	public $B_rmk_m_id = 4;
	public $B_rmk_type = 8;
	public $B_rmk_type_id = 16;
	public $B_rmk_title = 32;
	public $B_rmk_description = 64;
	public $B_rmk_create_datetime = 128;
	public $B_rmk_edit_datetime = 256;
	public $B_rmk_delete = 512;
	public $B_ALL;

	public function __construct($rmk_id, $sql_logic = "")
	{
		$this->B_ALL = $this->B_rmk_sn | $this->B_rmk_id | $this->B_rmk_m_id | $this->B_rmk_type | $this->B_rmk_type_id | $this->B_rmk_title | $this->B_rmk_description | $this->B_rmk_create_datetime | $this->B_rmk_edit_datetime | $this->B_rmk_delete;
		$this->init_rmk_id($rmk_id, $sql_logic = "");
	}

	public function init_rmk_id($rmk_id, $sql_logic = "")
	{
		include("mysql_connect.php");
		if (!$conn) {
			return json_encode(array("result" => 0, "title" => "連線錯誤", "description" => mysql_error()));
		}
#select db
		mysql_query("SET NAMES utf8;", $conn);
		if (!mysql_select_db($dbName, $conn)) {
			return json_encode(array("result" => 0, "title" => "連接db錯誤", "description" => mysql_error()));
		}
		$language = "zh-tw";
		/*********************************** Put your table name here ***********************************/
		$sql = "select * from tb_Remark where rmk_id = '" . $rmk_id . "' ";

		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "失敗", "description" => mysql_error()));
		} else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->rmk_sn = $ans["rmk_sn"];
			$this->rmk_id = $ans["rmk_id"];
			$this->rmk_m_id = $ans["rmk_m_id"];
			$this->rmk_type = $ans["rmk_type"];
			$this->rmk_type_id = $ans["rmk_type_id"];
			$this->rmk_title = $ans["rmk_title"];
			$this->rmk_description = $ans["rmk_description"];
			$this->rmk_create_datetime = $ans["rmk_create_datetime"];
			$this->rmk_edit_datetime = $ans["rmk_edit_datetime"];
			$this->rmk_delete = $ans["rmk_delete"];
		}
	}

	public function get_rmk_sn()
	{
		return $this->rmk_sn;
	}

	public function get_rmk_id()
	{
		return $this->rmk_id;
	}

	public function get_rmk_m_id()
	{
		return $this->rmk_m_id;
	}

	public function get_rmk_type()
	{
		return $this->rmk_type;
	}

	public function get_rmk_type_id()
	{
		return $this->rmk_type_id;
	}

	public function get_rmk_title()
	{
		return $this->rmk_title;
	}

	public function get_rmk_description()
	{
		return $this->rmk_description;
	}

	public function get_rmk_create_datetime()
	{
		return $this->rmk_create_datetime;
	}

	public function get_rmk_edit_datetime()
	{
		return $this->rmk_edit_datetime;
	}

	public function get_rmk_delete()
	{
		return $this->rmk_delete;
	}

	public function get_array($select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_rmk_sn) != 0) {
			$return_array["rmk_sn"] = $this->get_rmk_sn();
		}
		if (($select & $this->B_rmk_id) != 0) {
			$return_array["rmk_id"] = $this->get_rmk_id();
		}
		if (($select & $this->B_rmk_m_id) != 0) {
			$return_array["rmk_m_id"] = $this->get_rmk_m_id();
		}
		if (($select & $this->B_rmk_type) != 0) {
			$return_array["rmk_type"] = $this->get_rmk_type();
		}
		if (($select & $this->B_rmk_type_id) != 0) {
			$return_array["rmk_type_id"] = $this->get_rmk_type_id();
		}
		if (($select & $this->B_rmk_title) != 0) {
			$return_array["rmk_title"] = $this->get_rmk_title();
		}
		if (($select & $this->B_rmk_description) != 0) {
			$return_array["rmk_description"] = $this->get_rmk_description();
		}
		if (($select & $this->B_rmk_create_datetime) != 0) {
			$return_array["rmk_create_datetime"] = $this->get_rmk_create_datetime();
		}
		if (($select & $this->B_rmk_edit_datetime) != 0) {
			$return_array["rmk_edit_datetime"] = $this->get_rmk_edit_datetime();
		}
		if (($select & $this->B_rmk_delete) != 0) {
			$return_array["rmk_delete"] = $this->get_rmk_delete();
		}
		return $return_array;
	}
}
?>