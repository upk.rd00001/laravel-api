<?php
class UPK_PricePackage {
	private $package_array;
	public function __construct($data=array()) {
		//maybe check data is valid
		$this->package_array=$data;
	}
	public function insert($id,$start_date,$start_time,$end_date,$end_time) {
		array_push($this->package_array, array(
			"id" => $id,
			"start_date" => $start_date,
			"end_date" => $end_date,
			"start_time" => $start_time,
			"end_time" => $end_time));
	}
	public function DT_insert($id,$DT_start_datetime,$DT_end_date_time) {
		$start_date=$DT_start_datetime->format("Y-m-d");
		$end_date=$DT_end_date_time->format("Y-m-d");
		$start_time=$DT_start_datetime->format("H:i:s");
		$end_time=$DT_end_date_time->format("H:i:s");
		$this->insert($id,$start_date,$start_time,$end_date,$end_time);
	}
	public function get(){
		return $this->package_array;
	}
}
?>