<?php
class UPK_Member{
	private $m_sn;
	private $m_id;
	private $m_first_name;
	private $m_last_name;
	private $m_nickname;
	private $m_password;
	private $m_sex;
	private $m_birthday;
	private $m_level;
	private $m_email;
	private $m_email_verify;
	private $m_url;
	private $m_phone_country_code;
	private $m_phone_area;
	private $m_phone;
	private $m_cellphone_country_code;
	private $m_cellphone;
	private $m_address_country;
	private $m_address_post_code;
	private $m_address_state;
	private $m_address_city;
	private $m_address_line1;
	private $m_address_line2;
	private $m_address_line3;
	private $m_join_date;
	private $m_validation_code;
	private $m_validation_sms_count;
	private $m_validation_sms_sent_time;
	private $m_enable;
	private $m_enable_date;
	private $m_driver_license_enable;
	private $m_walk_speed;
	private $m_vehicle_speed_level;
	private $m_unit;
	private $m_language;
	private $m_token;
	private $m_token_last_using_time;
	private $m_social_security_numbers;
	private $m_driver_image_filename;
	private $m_driver_image_back_filename;
	private $m_own_point;
	private $m_own_free_point;
	private $m_reference_member_cellphone_country_code;
	private $m_reference_member_cellphone;
	private $m_reference_count;
	private $price;
	private $create_date;
	private $max_management;
	private $promo_code;

	public $vehicle_data;

	public $B_m_sn		=1;
	public $B_m_id		=2;
	public $B_m_first_name		=4;
	public $B_m_last_name		=8;
	public $B_m_nickname		=16;
	public $B_m_password		=32;
	public $B_m_sex		=64;
	public $B_m_birthday		=128;
	//public $B_m_level		=256;		//unused
	public $B_m_email		=512;
	//public $B_m_url		=1024;		//unused
	public $B_m_phone_country_code		=2048;
	//public $B_m_phone_area		=4096;		//unused
	//public $B_m_phone		=8192;		//unused
	public $B_m_cellphone_country_code		=16384;
	public $B_m_cellphone		=32768;
	public $B_m_address		=65536;
	/*
	public $B_m_address_country		=65536;
	public $B_m_address_post_code		=131072;
	public $B_m_address_state		=262144;
	public $B_m_address_city		=524288;
	public $B_m_address_line1		=1048576;
	public $B_m_address_line2		=2097152;
	public $B_m_address_line3		=4194304;
	*/
	public $B_m_join_date		=8388608;
	public $B_m_validation		=4096;
	//public $B_m_validation_code		=16777216;
	//public $B_m_validation_sms_count		=33554432;
	//public $B_m_validation_sms_sent_time		=67108864;
	//public $B_m_enable		=134217728;
	//public $B_m_enable_date		=268435456;		//unused
	//public $B_m_driver_license_enable		=536870912;		//unused
	//public $B_m_walk_speed		=1073741824;		//unused
	//public $B_m_vehicle_speed_level		=131072;		//unused
	public $B_m_unit		=262144;
	public $B_m_language		=524288;
	public $B_m_token		=1048576;
	//public $B_m_token_last_using_time		=8;
	public $B_m_social_security_numbers		=2097152;
	//public $B_m_driver_image_filename		=4194304;		//unused
	//public $B_m_driver_image_back_filename		=16777216;		//unused
	public $B_m_own_point		=33554432;
	public $B_m_own_free_point		=67108864;
	public $B_m_reference_member_cellphone_country_code		=134217728;
	public $B_m_reference_member_cellphone		=1024;
	public $B_m_reference_count		=2048;
	//public $B_price		=4096;		//unused
	//public $B_create_date		=8192;		//unused
	public $B_max_management		=16384;
	//public $B_promo_code		=32768;		//unused

	public $B_unused_field		=8192;

	public $B_ALL;//不顯示一些無關資訊
	public $B_REALLY_ALL;//真的全部丟出來
	public function __construct($m_id,$sql_logic=""){
		$this->B_ALL =$this->B_m_sn | $this->B_m_id | $this->B_m_first_name | $this->B_m_last_name | $this->B_m_nickname | $this->B_m_password | $this->B_m_sex | $this->B_m_birthday  | $this->B_m_email | $this->B_m_address | $this->B_m_join_date | $this->B_m_unit | $this->B_m_language | $this->B_m_token | $this->B_m_social_security_numbers |  $this->B_m_own_point | $this->B_m_own_free_point | $this->B_m_reference_member_cellphone_country_code | $this->B_m_reference_member_cellphone | $this->B_m_reference_count | $this->B_max_management | $this->B_unused_field | $this->B_m_validation;
		$this->init_m_id($m_id,$sql_logic="");
	}
	public function init_m_cellphone($cellphone,$cellphone_country_code="+886") {
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$sql = "SELECT m_id, m_enable, m_token_last_using_time, m_validation_sms_count, m_cellphone_country_code, m_cellphone, m_token FROM tb_Member WHERE " . sql_cellphone_compare("m_cellphone", $cellphone) . " AND m_cellphone_country_code='" . $cellphone_country_code . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "搜尋帳號失敗", "description" => mysql_error()));
		}
		else if (mysql_num_rows($result) == 0) {
			#無帳號須註冊
			include_once("/../main_api/RegisterFunc.php");
			$register_args["activity"] = "REGISTER";
			$register_args["cellphone"] = $cellphone;
			$register_args["cellphone_country_code"] = $cellphone_country_code;
			$password = GenerateRandomString(10, '0123456789');
			$register_args["password"] = $password;
			$register_args["is_send_sms"] = 0;//不傳送sms
			$tans = json_decode(RegisterFunc($register_args), true);
			if ($tans["result"] == 1) {
				$m_token = $tans["token"];
				$m_id = $tans["member_id"];
				//後台更新驗證狀態
				$ignore_is_admin = 1;
				include_once("/../admin_api/UpdateValidationStatusFunc.php");
				$ans = UpdateValidateionStatusFunc("UPDATE VALIDATION STATUS", $m_token, $m_id, $ignore_is_admin);
				$tans = json_decode($ans, true);
				if ($tans["result"] == 0) {
					return json_encode($tans);
				}
				//成功就繼續做下去
			}
			else {
				return json_encode($tans);
			}
		}
		else {
			$ans = mysql_fetch_assoc($result);
			$m_id = $ans["m_id"];
		}
		$this->init_m_id($m_id);
		return json_encode(array("reuslt"=>1));
	}
	public function init_m_id($m_id,$sql_logic=""){
		global $conn,$dbName;
		$language = "zh-tw";
		/*********************************** Put your table name here ***********************************/
		$sql="select * from tb_Member where m_id = '".$m_id."' ";

		$sql.=$sql_logic;
		$result = mysql_query($sql, $conn);
		if(! $result){
			return json_encode(array("失敗" => mysql_error($conn).$sql));
		}
		else if(mysql_num_rows($result) == 1) {
			$ans=mysql_fetch_assoc($result);
			$this->m_sn=$ans["m_sn"];
			$this->m_id=$ans["m_id"];
			$this->m_first_name=$ans["m_first_name"];
			$this->m_last_name=$ans["m_last_name"];
			$this->m_nickname=$ans["m_nickname"];
			$this->m_password=$ans["m_password"];
			$this->m_sex=$ans["m_sex"];
			$this->m_birthday=$ans["m_birthday"];
			$this->m_level=$ans["m_level"];
			$this->m_email=$ans["m_email"];
			$this->m_email_verify=$ans["m_email_verify"];
			$this->m_url=$ans["m_url"];
			$this->m_phone_country_code=$ans["m_phone_country_code"];
			$this->m_phone_area=$ans["m_phone_area"];
			$this->m_phone=$ans["m_phone"];
			$this->m_cellphone_country_code=$ans["m_cellphone_country_code"];
			$this->m_cellphone=$ans["m_cellphone"];
			$this->m_address_country=$ans["m_address_country"];
			$this->m_address_post_code=$ans["m_address_post_code"];
			$this->m_address_state=$ans["m_address_state"];
			$this->m_address_city=$ans["m_address_city"];
			$this->m_address_line1=$ans["m_address_line1"];
			$this->m_address_line2=$ans["m_address_line2"];
			$this->m_address_line3=$ans["m_address_line3"];
			$this->m_join_date=$ans["m_join_date"];
			$this->m_validation_code=$ans["m_validation_code"];
			$this->m_validation_sms_count=$ans["m_validation_sms_count"];
			$this->m_validation_sms_sent_time=$ans["m_validation_sms_sent_time"];
			$this->m_enable=$ans["m_enable"];
			$this->m_enable_date=$ans["m_enable_date"];
			$this->m_driver_license_enable=$ans["m_driver_license_enable"];
			$this->m_walk_speed=$ans["m_walk_speed"];
			$this->m_vehicle_speed_level=$ans["m_vehicle_speed_level"];
			$this->m_unit=$ans["m_unit"];
			$this->m_language=$ans["m_language"];
			$this->m_token=$ans["m_token"];
			$this->m_token_last_using_time=$ans["m_token_last_using_time"];
			$this->m_social_security_numbers=$ans["m_social_security_numbers"];
			$this->m_driver_image_filename=$ans["m_driver_image_filename"];
			$this->m_driver_image_back_filename=$ans["m_driver_image_back_filename"];
			$this->m_own_point=$ans["m_own_point"];
			$this->m_own_free_point=$ans["m_own_free_point"];
			$this->m_reference_member_cellphone_country_code=$ans["m_reference_member_cellphone_country_code"];
			$this->m_reference_member_cellphone=$ans["m_reference_member_cellphone"];
			$this->m_reference_count=$ans["m_reference_count"];
			$this->price=$ans["price"];
			$this->create_date=$ans["create_date"];
			$this->max_management=$ans["max_management"];
			$this->promo_code=$ans["promo_code"];
			$this->m_has_parking_space=$ans["m_has_parking_space"];
		}
	}
	public function getMemberVehicle() {
		global $conn,$dbName;
		$sql = "SELECT m_ve_id FROM tb_Member_Vehicle WHERE m_id='".$this->m_id."' AND m_ve_delete='0' ";
		$result = mysql_query($sql,$conn);
		if(!$result){
		}
		else{
			$this->vehicle_data=array();
			while($ans = mysql_fetch_assoc($result)){
				$m_ve_id = $ans["m_ve_id"];
				$this_vehicle_data = new UPK_MemberVehicle($m_ve_id);
				array_push($this->vehicle_data,$this_vehicle_data);
			}
		}
	}
	public function searchVehicleByLicencePlate($licence_plate_no){
		if($this->vehicle_data == null)
			$this->getMemberVehicle();
		foreach($this->vehicle_data as $each_vehicle_data) {
			//完全相同的車牌只有一個
			//要考慮 有-或沒-
			//考慮大小寫
			if($licence_plate_no==$each_vehicle_data->get_m_ve_plate_no())
				return $each_vehicle_data;
			if(strtoupper(str_replace("-","",$licence_plate_no)) ==
				strtoupper(str_replace("-","",$each_vehicle_data->get_m_ve_plate_no())))
				return $each_vehicle_data;
		}
		return null;
	}
public function get_m_sn(){
	return $this->m_sn;
}
public function get_m_id(){
	return $this->m_id;
}
public function get_m_first_name(){
	return $this->m_first_name;
}
public function get_m_last_name(){
	return $this->m_last_name;
}
public function get_m_nickname(){
	return $this->m_nickname;
}
public function get_m_password(){
	return $this->m_password;
}
public function get_m_sex(){
	return $this->m_sex;
}
public function get_m_birthday(){
	return $this->m_birthday;
}
public function get_m_level(){
	return $this->m_level;
}
public function get_m_email(){
	return $this->m_email;
}
public function get_m_email_verify(){
	return $this->m_email_verify;
}
public function get_m_url(){
	return $this->m_url;
}
public function get_m_phone_country_code(){
	return $this->m_phone_country_code;
}
public function get_m_phone_area(){
	return $this->m_phone_area;
}
public function get_m_phone(){
	return $this->m_phone;
}
public function get_m_cellphone_country_code(){
	return $this->m_cellphone_country_code;
}
public function get_m_cellphone(){
	return $this->m_cellphone;
}
public function get_m_address_country(){
	return $this->m_address_country;
}
public function get_m_address_post_code(){
	return $this->m_address_post_code;
}
public function get_m_address_state(){
	return $this->m_address_state;
}
public function get_m_address_city(){
	return $this->m_address_city;
}
public function get_m_address_line1(){
	return $this->m_address_line1;
}
public function get_m_address_line2(){
	return $this->m_address_line2;
}
public function get_m_address_line3(){
	return $this->m_address_line3;
}
public function get_m_join_date(){
	return $this->m_join_date;
}
public function get_m_validation_code(){
	return $this->m_validation_code;
}
public function get_m_validation_sms_count(){
	return $this->m_validation_sms_count;
}
public function get_m_validation_sms_sent_time(){
	return $this->m_validation_sms_sent_time;
}
public function get_m_enable(){
	return $this->m_enable;
}
public function get_m_enable_date(){
	return $this->m_enable_date;
}
public function get_m_driver_license_enable(){
	return $this->m_driver_license_enable;
}
public function get_m_walk_speed(){
	return $this->m_walk_speed;
}
public function get_m_vehicle_speed_level(){
	return $this->m_vehicle_speed_level;
}
public function get_m_unit(){
	return $this->m_unit;
}
public function get_m_language(){
	return $this->m_language;
}
public function get_m_token(){
	return $this->m_token;
}
public function get_m_token_last_using_time(){
	return $this->m_token_last_using_time;
}
public function get_m_social_security_numbers(){
	return $this->m_social_security_numbers;
}
public function get_m_driver_image_filename(){
	return $this->m_driver_image_filename;
}
public function get_m_driver_image_back_filename(){
	return $this->m_driver_image_back_filename;
}
public function get_m_own_point(){
	return $this->m_own_point;
}
public function get_m_own_free_point(){
	return $this->m_own_free_point;
}
public function get_m_reference_member_cellphone_country_code(){
	return $this->m_reference_member_cellphone_country_code;
}
public function get_m_reference_member_cellphone(){
	return $this->m_reference_member_cellphone;
}
public function get_m_reference_count(){
	return $this->m_reference_count;
}
public function get_price(){
	return $this->price;
}
public function get_create_date(){
	return $this->create_date;
}
public function get_max_management(){
	return $this->max_management;
}
public function get_promo_code(){
	return $this->promo_code;
}
public function get_array($select=""){
	if($select=="")
		$select=$this->B_ALL;
	$return_array=array();
	if(($select & $this->B_m_sn) != 0 ){
		$return_array["m_sn"]=$this->get_m_sn();
	}
	if(($select & $this->B_m_id) != 0 ){
		$return_array["m_id"]=$this->get_m_id();
	}
	if(($select & $this->B_m_first_name) != 0 ){
		$return_array["m_first_name"]=$this->get_m_first_name();
	}
	if(($select & $this->B_m_last_name) != 0 ){
		$return_array["m_last_name"]=$this->get_m_last_name();
	}
	if(($select & $this->B_m_nickname) != 0 ){
		$return_array["m_nickname"]=$this->get_m_nickname();
	}
	if(($select & $this->B_m_password) != 0 ){
		$return_array["m_password"]=$this->get_m_password();
	}
	if(($select & $this->B_m_sex) != 0 ){
		$return_array["m_sex"]=$this->get_m_sex();
	}
	if(($select & $this->B_m_birthday) != 0 ){
		$return_array["m_birthday"]=$this->get_m_birthday();
	}
	if(($select & $this->B_m_email) != 0 ){
		$return_array["m_email"]=$this->get_m_email();
		$return_array["m_email_verify"]=$this->get_m_email_verify();
	}
	if(($select & $this->B_m_phone_country_code) != 0 ){
		$return_array["m_phone_country_code"]=$this->get_m_phone_country_code();
	}
	if(($select & $this->B_m_cellphone_country_code) != 0 ){
		$return_array["m_cellphone_country_code"]=$this->get_m_cellphone_country_code();
	}
	if(($select & $this->B_m_cellphone) != 0 ){
		$return_array["m_cellphone"]=$this->get_m_cellphone();
	}
	if(($select & $this->B_m_address) != 0 ){
		$return_array["m_address_country"]=$this->get_m_address_country();
		$return_array["m_address_post_code"]=$this->get_m_address_post_code();
		$return_array["m_address_state"]=$this->get_m_address_state();
		$return_array["m_address_city"]=$this->get_m_address_city();
		$return_array["m_address_line1"]=$this->get_m_address_line1();
		$return_array["m_address_line2"]=$this->get_m_address_line2();
		$return_array["m_address_line3"]=$this->get_m_address_line3();
	}
	if(($select & $this->B_m_join_date) != 0 ){
		$return_array["m_join_date"]=$this->get_m_join_date();
	}
	if(($select & $this->B_m_validation) != 0 ){
		$return_array["m_validation_code"]=$this->get_m_validation_code();
		$return_array["m_validation_sms_count"]=$this->get_m_validation_sms_count();
		$return_array["m_validation_sms_sent_time"]=$this->get_m_validation_sms_sent_time();
		$return_array["m_enable"]=$this->get_m_enable();
		$return_array["m_enable_date"]=$this->get_m_enable_date();
	}
	if(($select & $this->B_m_unit) != 0 ){
		$return_array["m_unit"]=$this->get_m_unit();
	}
	if(($select & $this->B_m_language) != 0 ){
		$return_array["m_language"]=$this->get_m_language();
	}
	if(($select & $this->B_m_token) != 0 ){
		$return_array["m_token"]=$this->get_m_token();
	}/*
	if(($select & $this->B_m_token_last_using_time) != 0 ){
		$return_array["m_token_last_using_time"]=$this->get_m_token_last_using_time();
	}*/
	if(($select & $this->B_m_social_security_numbers) != 0 ){
		$return_array["m_social_security_numbers"]=$this->get_m_social_security_numbers();
	}
	if(($select & $this->B_m_own_point) != 0 ){
		$return_array["m_own_point"]=$this->get_m_own_point();
	}
	if(($select & $this->B_m_own_free_point) != 0 ){
		$return_array["m_own_free_point"]=$this->get_m_own_free_point();
	}
	if(($select & $this->B_m_reference_member_cellphone_country_code) != 0 ){
		$return_array["m_reference_member_cellphone_country_code"]=$this->get_m_reference_member_cellphone_country_code();
	}
	if(($select & $this->B_m_reference_member_cellphone) != 0 ){
		$return_array["m_reference_member_cellphone"]=$this->get_m_reference_member_cellphone();
	}
	if(($select & $this->B_m_reference_count) != 0 ){
		$return_array["m_reference_count"]=$this->get_m_reference_count();
	}
	if(($select & $this->B_max_management) != 0 ){
		$return_array["max_management"]=$this->get_max_management();
	}
	if(($select & $this->B_unused_field) != 0 ){
		$return_array["m_level"]=$this->get_m_level();
		$return_array["m_url"]=$this->get_m_url();
		$return_array["m_phone_area"]=$this->get_m_phone_area();
		$return_array["m_phone"]=$this->get_m_phone();
		$return_array["m_driver_license_enable"]=$this->get_m_driver_license_enable();
		$return_array["m_walk_speed"]=$this->get_m_walk_speed();
		$return_array["m_vehicle_speed_level"]=$this->get_m_vehicle_speed_level();
		$return_array["m_driver_image_filename"]=$this->get_m_driver_image_filename();
		$return_array["m_driver_image_back_filename"]=$this->get_m_driver_image_back_filename();
		$return_array["price"]=$this->get_price();
		$return_array["create_date"]=$this->get_create_date();
		$return_array["promo_code"]=$this->get_promo_code();
	}
	return $return_array;
}
}

?>