<?php
class UPK_MemberDeposit{
	private $m_dpt_sn;
	private $m_dpt_id;
	private $m_id;
	private $m_bkl_id;
	private $m_dpt_create_datetime;
	private $m_dpt_pay_datetime;
	private $m_dpt_amount;
	private $m_dpt_point;
	private $m_dpt_method;
	private $m_dpt_handling_charge;
	private $m_dpt_error_reason;
	private $m_dpt_payment_info_log; //barcode用
//	private $m_dpt_request_log;
//	private $m_dpt_response_log;
	private $m_transaction_id;
	private $m_dpt_trade_platform;
	private $m_dpt_origin_point;
	private $m_dpt_update_point;
	private $m_ppl_promotion_code;
	private $invoice_number;
	private $invoice_amount;
	private $m_dpt_refund_point;

	//custom var
	private $invoices;
	private $barcode_url;

	public $B_m_dpt_sn		=1;
	public $B_m_dpt_id		=2;
	public $B_m_id		=4;
	public $B_m_bkl_id		=8;
	public $B_m_dpt_create_datetime		=16;
	public $B_m_dpt_pay_datetime		=32;
	public $B_m_dpt_amount		=64;
	public $B_m_dpt_point		=128;
	public $B_m_dpt_method		=256;
	public $B_m_dpt_handling_charge		=512;
	public $B_m_dpt_error_reason		=1024;
	public $B_m_dpt_payment_info_log		=2097152;
//	public $B_m_dpt_request_log		=2048;
//	public $B_m_dpt_response_log		=4096;
	public $B_m_transaction_id		=8192;
	public $B_m_dpt_trade_platform		=16384;
	public $B_m_dpt_origin_point		=32768;
	public $B_m_dpt_update_point		=65536;
	public $B_m_ppl_promotion_code		=131072;
	public $B_invoice		=262144;
//	public $B_invoice_number		=262144;
//	public $B_invoice_amount		=524288;
	public $B_m_dpt_refund_point		=1048576;
	public $B_ALL;
	public function __construct($m_dpt_id,$sql_logic=""){
		$this->B_ALL =$this->B_m_dpt_sn | $this->B_m_dpt_id | $this->B_m_id | $this->B_m_bkl_id | $this->B_m_dpt_create_datetime | $this->B_m_dpt_pay_datetime | $this->B_m_dpt_amount | $this->B_m_dpt_point | $this->B_m_dpt_method | $this->B_m_dpt_handling_charge | $this->B_m_dpt_error_reason | $this->B_m_transaction_id | $this->B_m_dpt_trade_platform | $this->B_m_dpt_origin_point | $this->B_m_dpt_update_point | $this->B_m_ppl_promotion_code | $this->B_invoice | $this->B_m_dpt_refund_point | $this->B_m_dpt_payment_info_log;
		$this->invoices=array();
		$this->barcode_url="";
		$this->init_m_dpt_id($m_dpt_id,$sql_logic="");
	}
	public function generate_barcode_url() {
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$m_dpt_payment_info_array = json_decode($this->m_dpt_payment_info_log,true);
		if(isset($m_dpt_payment_info_array["Barcode1"]) &&
			isset($m_dpt_payment_info_array["Barcode2"]) &&
			isset($m_dpt_payment_info_array["Barcode3"])) {
			$Barcode1 = $m_dpt_payment_info_array["Barcode1"];
			$Barcode2 = $m_dpt_payment_info_array["Barcode2"];
			$Barcode3 = $m_dpt_payment_info_array["Barcode3"];
			$exdate="";
			$expired="0";
			if(isset($m_dpt_payment_info_array["ExpireDate"])) {
				$exdate = $m_dpt_payment_info_array["ExpireDate"];
				$DT_exdate = new DateTime($exdate);
				$DT_now = new DateTime();
				if($DT_exdate >= $DT_now) {
					$expired="0";
				}
				else {
					$expired="1";
				}
			}
			$msg="";
			if(isset($m_dpt_payment_info_array["TradeAmt"]))
				$msg="繳費金額: ".$m_dpt_payment_info_array["TradeAmt"]."元";
			$G_MEMBER_SPACE4CAR_BARCODE_URL = GetSystemParameter($conn, "member_space4car_barcode_url");
			$this->barcode_url = $G_MEMBER_SPACE4CAR_BARCODE_URL . "?barcode1=".$Barcode1."&barcode2=".$Barcode2."&barcode3=".$Barcode3."&exdate=".$exdate."&msg=".$msg."&expired=".$expired;
		}

	}
	public function init_m_dpt_id($m_dpt_id,$sql_logic="") {
		if($m_dpt_id==""){
			#沒有付款紀錄時的資料
			$this->m_dpt_sn=0;
			$this->m_dpt_id="";
			$this->m_id="";//
			$this->m_bkl_id="";//
			$this->m_dpt_create_datetime=null;
			$this->m_dpt_pay_datetime=null;
			$this->m_dpt_amount=0;
			$this->m_dpt_point=0;
			$this->m_dpt_method=0;
			$this->m_dpt_handling_charge=0;
			$this->m_dpt_error_reason="尚未產生訂單";
			$this->m_dpt_payment_info_log="";
//			$this->m_dpt_request_log=$ans["m_dpt_request_log"];
//			$this->m_dpt_response_log=$ans["m_dpt_response_log"];
			$this->m_transaction_id="";
			$this->m_dpt_trade_platform=0;
			$this->m_dpt_origin_point=0;
			$this->m_dpt_update_point=0;
			$this->m_ppl_promotion_code="";
			$this->invoice_number="";
			$this->invoice_amount=0;
			$this->m_dpt_refund_point=0;
			return true;
		}
		global $conn, $dbName;
		check_conn($conn,$dbName);
		/*********************************** Put your table name here ***********************************/
		$sql="select * from tb_Member_Deposit where m_dpt_id = '".$m_dpt_id."' ";

		$sql.=$sql_logic;
		$result = mysql_query($sql, $conn);
		if(! $result){
			return json_encode(array("result"=>0, "title"=>"失敗", "description"=> mysql_error()));
		}
		else if(mysql_num_rows($result) == 1) {
			$ans=mysql_fetch_assoc($result);
			$this->m_dpt_sn=$ans["m_dpt_sn"];
			$this->m_dpt_id=$ans["m_dpt_id"];
			$this->m_id=$ans["m_id"];
			$this->m_bkl_id=$ans["m_bkl_id"];
			$this->m_dpt_create_datetime=$ans["m_dpt_create_datetime"];
			$this->m_dpt_pay_datetime=$ans["m_dpt_pay_datetime"];
			$this->m_dpt_amount=$ans["m_dpt_amount"];
			$this->m_dpt_point=$ans["m_dpt_point"];
			$this->m_dpt_method=$ans["m_dpt_method"];
			$this->m_dpt_handling_charge=$ans["m_dpt_handling_charge"];
			$this->m_dpt_error_reason=$ans["m_dpt_error_reason"];
			$this->m_dpt_payment_info_log=$ans["m_dpt_payment_info_log"];
			if($this->m_dpt_payment_info_log != "")
				$this->generate_barcode_url();
//			$this->m_dpt_request_log=$ans["m_dpt_request_log"];
//			$this->m_dpt_response_log=$ans["m_dpt_response_log"];
			$this->m_transaction_id=$ans["m_transaction_id"];
			$this->m_dpt_trade_platform=$ans["m_dpt_trade_platform"];
			$this->m_dpt_origin_point=$ans["m_dpt_origin_point"];
			$this->m_dpt_update_point=$ans["m_dpt_update_point"];
			$this->m_ppl_promotion_code=$ans["m_ppl_promotion_code"];
			$this->invoice_number=$ans["invoice_number"];
			$this->invoice_amount=$ans["invoice_amount"];
			$this->GetInvoice();
			$this->m_dpt_refund_point=$ans["m_dpt_refund_point"];
			if($this->m_dpt_trade_platform=="21" || $this->m_dpt_trade_platform=="22" || $this->m_dpt_trade_platform=="23"){
				//如果是悠遊卡就去讀booking的point去複寫他的dpt_amount
				$this_booking_log=new UPK_BookingLog($this->m_bkl_id);
				$this->m_dpt_point=$this_booking_log->get_m_bkl_estimate_point();
				$this->m_dpt_amount=$this_booking_log->get_m_bkl_estimate_point();
			}
		}
		return true;
	}
	public function GetInvoice()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);

		$sql = "SELECT inv_id FROM tb_Invoice WHERE m_dpt_id = '" . $this->m_dpt_id . "' AND inv_delete='0' AND inv_enable='1' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
		}
		else {
			while ($ans = mysql_fetch_assoc($result)) {
				$this_invoice = new UPK_Invoice($ans["inv_id"]);
				$this->invoice_number = $this_invoice->get_inv_number();
				//$this->invoice_amount = $this_invoice->get_inv_amount();
				array_push($this->invoices, $this_invoice);
			}
		}
	}
	public function get_m_dpt_sn(){
		return $this->m_dpt_sn;
	}
	public function get_m_dpt_id(){
		return $this->m_dpt_id;
	}
	public function get_m_id(){
		return $this->m_id;
	}
	public function get_m_bkl_id(){
		return $this->m_bkl_id;
	}
	public function get_m_dpt_create_datetime(){
		return $this->m_dpt_create_datetime;
	}
	public function get_m_dpt_pay_datetime(){
		return $this->m_dpt_pay_datetime;
	}
	public function get_m_dpt_amount(){
		return $this->m_dpt_amount;
	}
	public function get_m_dpt_point(){
		return $this->m_dpt_point;
	}
	public function get_m_dpt_method(){
		return $this->m_dpt_method;
	}
	public function get_m_dpt_handling_charge(){
		return $this->m_dpt_handling_charge;
	}
	public function get_m_dpt_error_reason(){
		return $this->m_dpt_error_reason;
	}
	public function get_m_dpt_payment_info_log(){
		return $this->m_dpt_payment_info_log;
	}
	public function get_barcode_url(){
		return $this->barcode_url;
	}
//	public function get_m_dpt_request_log(){
//		return $this->m_dpt_request_log;
//	}
//	public function get_m_dpt_response_log(){
//		return $this->m_dpt_response_log;
//	}
	public function get_m_transaction_id(){
		return $this->m_transaction_id;
	}
	public function get_m_dpt_trade_platform(){
		return $this->m_dpt_trade_platform;
	}
	public function get_m_dpt_origin_point(){
		return $this->m_dpt_origin_point;
	}
	public function get_m_dpt_update_point(){
		return $this->m_dpt_update_point;
	}
	public function get_m_ppl_promotion_code(){
		return $this->m_ppl_promotion_code;
	}
	public function get_invoice_number(){
		return $this->invoice_number;
	}
	public function get_invoice_amount(){
		return (int)$this->invoice_amount;
	}
	public function get_m_dpt_refund_point(){
		return (int)$this->m_dpt_refund_point;
	}
	public function get_array($select=""){
		if($select=="")
			$select=$this->B_ALL;
		$return_array=array();
		if(($select & $this->B_m_dpt_sn) != 0 ){
			$return_array["m_dpt_sn"]=$this->get_m_dpt_sn();
		}
		if(($select & $this->B_m_dpt_id) != 0 ){
			$return_array["m_dpt_id"]=$this->get_m_dpt_id();
		}
		if(($select & $this->B_m_id) != 0 ){
			$return_array["m_id"]=$this->get_m_id();
		}
		if(($select & $this->B_m_bkl_id) != 0 ){
			$return_array["m_bkl_id"]=$this->get_m_bkl_id();
		}
		if(($select & $this->B_m_dpt_create_datetime) != 0 ){
			$return_array["m_dpt_create_datetime"]=$this->get_m_dpt_create_datetime();
		}
		if(($select & $this->B_m_dpt_pay_datetime) != 0 ){
			$return_array["m_dpt_pay_datetime"]=$this->get_m_dpt_pay_datetime();
		}
		if(($select & $this->B_m_dpt_amount) != 0 ){
			$return_array["m_dpt_amount"]=$this->get_m_dpt_amount();
		}
		if(($select & $this->B_m_dpt_point) != 0 ){
			$return_array["m_dpt_point"]=$this->get_m_dpt_point();
		}
		if(($select & $this->B_m_dpt_method) != 0 ){
			$return_array["m_dpt_method"]=$this->get_m_dpt_method();
		}
		if(($select & $this->B_m_dpt_handling_charge) != 0 ){
			$return_array["m_dpt_handling_charge"]=$this->get_m_dpt_handling_charge();
		}
		if(($select & $this->B_m_dpt_error_reason) != 0 ){
			$return_array["m_dpt_error_reason"]=$this->get_m_dpt_error_reason();
		}
		if(($select & $this->B_m_dpt_payment_info_log) != 0 ){
			$return_array["m_dpt_payment_info_log"]=$this->get_m_dpt_payment_info_log();
			$return_array["barcode_url"]=$this->get_barcode_url();
		}
//		if(($select & $this->B_m_dpt_request_log) != 0 ){
//			$return_array["m_dpt_request_log"]=$this->get_m_dpt_request_log();
//		}
//		if(($select & $this->B_m_dpt_response_log) != 0 ){
//			$return_array["m_dpt_response_log"]=$this->get_m_dpt_response_log();
//		}
		if(($select & $this->B_m_transaction_id) != 0 ){
			$return_array["m_transaction_id"]=$this->get_m_transaction_id();
		}
		if(($select & $this->B_m_dpt_trade_platform) != 0 ){
			$return_array["m_dpt_trade_platform"]=$this->get_m_dpt_trade_platform();
		}
		if(($select & $this->B_m_dpt_origin_point) != 0 ){
			$return_array["m_dpt_origin_point"]=$this->get_m_dpt_origin_point();
		}
		if(($select & $this->B_m_dpt_update_point) != 0 ){
			$return_array["m_dpt_update_point"]=$this->get_m_dpt_update_point();
		}
		if(($select & $this->B_m_ppl_promotion_code) != 0 ){
			$return_array["m_ppl_promotion_code"]=$this->get_m_ppl_promotion_code();
		}
		if(($select & $this->B_invoice) != 0 ){
			$return_array["invoice_number"]=$this->get_invoice_number();
			$return_array["invoice_amount"]=$this->get_invoice_amount();
			$tmp_array = array();
			foreach((array)$this->invoices as $each_invoice){
				array_push($tmp_array,$each_invoice->get_array());
			}
			$return_array["invoice_array"]=$tmp_array;
		}
		if(($select & $this->B_m_dpt_refund_point) != 0 ){
			$return_array["m_dpt_refund_point"]=$this->get_m_dpt_refund_point();
		}
		return $return_array;
	}
}
