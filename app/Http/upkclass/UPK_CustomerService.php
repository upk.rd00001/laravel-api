<?php

class UPK_Customer_Service
{
	private $is_log;
	private $m_cs_sn;
	private $m_csl_sn;
	private $m_cs_id;
	private $m_admin_id;
	private $m_id;
	private $m_cs_created_datetime;
	private $m_cs_contact_datetime;
	private $m_cs_contact_type;
	private $m_cs_is_delete;
	private $m_cs_status;
	private $m_cs_service_type;
	private $m_cs_service_description;
	private $m_plots_id;
	private $m_pk_id;
	private $m_ve_id;
	private $m_bkl_start_datetime;
	private $m_bkl_end_datetime;
	private $m_bkl_id;
	private $m_admin_id_assigned;
	public $B_m_cs_sn = 1;
	public $B_m_cs_id = 2;
	public $B_m_admin_id = 4;
	public $B_m_id = 8;
	public $B_m_cs_created_datetime = 16;
	public $B_m_cs_contact_datetime = 32;
	public $B_m_cs_contact_type = 64;
	public $B_m_cs_is_delete = 128;
	public $B_m_cs_status = 256;
	public $B_m_cs_service_type = 512;
	public $B_m_cs_service_description = 1024;
	public $B_m_plots_id = 2048;
	public $B_m_pk_id = 4096;
	public $B_m_ve_id = 8192;
	public $B_m_bkl_start_datetime = 16384;
	public $B_m_bkl_end_datetime = 32768;
	public $B_m_bkl_id = 65536;
	public $B_m_admin_id_assigned = 131072;
	public $B_m_cs_file_logs = 262144;
	public $B_ALL;

	public $customer_service_logs;

	public $this_admin_member;
	public $this_assigned_member;
	public $this_member;
	public $this_booking_log;
	public $this_parking_space;
	public $this_parking_lot;
	public $this_vehicle_data;
	public $this_file_logs;

	public function __construct($m_cs_id, $is_log = false, $sql_logic = "")
	{
		$this->B_ALL = $this->B_m_cs_sn | $this->B_m_cs_id | $this->B_m_admin_id | $this->B_m_id | $this->B_m_cs_created_datetime | $this->B_m_cs_contact_datetime | $this->B_m_cs_contact_type | $this->B_m_cs_is_delete | $this->B_m_cs_status | $this->B_m_cs_service_type | $this->B_m_cs_service_description | $this->B_m_plots_id | $this->B_m_pk_id | $this->B_m_ve_id | $this->B_m_bkl_start_datetime | $this->B_m_bkl_end_datetime | $this->B_m_bkl_id | $this->B_m_admin_id_assigned | $this->B_m_cs_file_logs;
		if($is_log)
			$this->init_m_csl_sn($m_cs_id, $sql_logic = "");
		else
			$this->init_m_cs_id($m_cs_id, $sql_logic = "");

		//就算id是空也要有class列別攘變數撈出空值
		$this->this_booking_log = new UPK_BookingLog($this->m_bkl_id);
		$this->this_parking_space = new UPK_ParkingSpace($this->m_pk_id);
		$this->this_parking_lot = new UPK_ParkingLot($this->m_plots_id);
		$this->this_vehicle_data = new UPK_MemberVehicle($this->m_ve_id);
		$this->this_member = new UPK_Member($this->m_id);
		$this->this_assigned_member = new UPK_Member($this->m_admin_id_assigned);
	}

	public function init_m_cs_id($m_cs_id, $sql_logic = "")
	{
		$this->is_log = false;
		global $conn, $dbName;
		check_conn($conn, $dbName);
		/*********************************** Put your table name here ***********************************/
		$sql = "select * from tb_Member_Customer_Service where m_cs_id = '" . $m_cs_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "失敗", "description" => mysql_error()));
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->m_cs_sn = $ans["m_cs_sn"];
			$this->m_cs_id = $ans["m_cs_id"];
			$this->m_admin_id = $ans["m_admin_id"];
			$this->m_id = $ans["m_id"];
			$this->m_cs_created_datetime = $ans["m_cs_created_datetime"];
			$this->m_cs_contact_datetime = $ans["m_cs_contact_datetime"];
			$this->m_cs_contact_type = $ans["m_cs_contact_type"];
			$this->m_cs_is_delete = $ans["m_cs_is_delete"];
			$this->m_cs_status = $ans["m_cs_status"];
			$this->m_cs_service_type = $ans["m_cs_service_type"];
			$this->m_cs_service_description = $ans["m_cs_service_description"];
			$this->m_plots_id = $ans["m_plots_id"];
			$this->m_pk_id = $ans["m_pk_id"];
			$this->m_ve_id = $ans["m_ve_id"];
			$this->m_bkl_start_datetime = $ans["m_bkl_start_datetime"];
			$this->m_bkl_end_datetime = $ans["m_bkl_end_datetime"];
			$this->m_bkl_id = $ans["m_bkl_id"];
			$this->m_admin_id_assigned = $ans["m_admin_id_assigned"];
			$this->this_file_logs = new UPK_File_Logs(14,$this->m_cs_id);
		}
	}

	public function init_m_csl_sn($m_csl_sn, $sql_logic = "")
	{
		$this->is_log = true;
		global $conn, $dbName;
		check_conn($conn, $dbName);
		/*********************************** Put your table name here ***********************************/
		$sql = "select * from tb_Member_Customer_Service_Log where m_csl_sn = '" . $m_csl_sn . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "失敗", "description" => mysql_error()));
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->m_csl_sn = $ans["m_csl_sn"];
			$this->m_cs_id = $ans["m_cs_id"];
			$this->m_admin_id = $ans["m_admin_id"];
			$this->m_id = $ans["m_id"];
			$this->m_cs_created_datetime = $ans["m_cs_created_datetime"];
			$this->m_cs_contact_datetime = $ans["m_cs_contact_datetime"];
			$this->m_cs_contact_type = $ans["m_cs_contact_type"];
			$this->m_cs_is_delete = $ans["m_cs_is_delete"];
			$this->m_cs_status = $ans["m_cs_status"];
			$this->m_cs_service_type = $ans["m_cs_service_type"];
			$this->m_cs_service_description = $ans["m_cs_service_description"];
			$this->m_plots_id = $ans["m_plots_id"];
			$this->m_pk_id = $ans["m_pk_id"];
			$this->m_ve_id = $ans["m_ve_id"];
			$this->m_bkl_start_datetime = $ans["m_bkl_start_datetime"];
			$this->m_bkl_end_datetime = $ans["m_bkl_end_datetime"];
			$this->m_bkl_id = $ans["m_bkl_id"];
			$this->m_admin_id_assigned = $ans["m_admin_id_assigned"];
			$this->this_file_logs = new UPK_File_Logs(14,"");//init
		}
	}

	public function getCustomerServiceLog()
	{
		//log不可以搜尋自己
		if ($this->is_log)
			return false;
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT m_csl_sn FROM tb_Member_Customer_Service_Log WHERE m_cs_id='" . $this->m_cs_id . "' ORDER BY m_csl_sn DESC";
		$result_log = mysql_query($sql, $conn);
		if (!$result_log) {
			return false;
		}
		$this->customer_service_logs = array();
		$is_first=true;
		while ($ans_log = mysql_fetch_assoc($result_log)) {
			if($is_first) { //第一筆不要搜尋
				$is_first=false;
				continue;
			}
			$this_cs_log = new UPK_Customer_Service($ans_log["m_csl_sn"],true);
			array_push($this->customer_service_logs,$this_cs_log);
		}
		return true;
	}

	public function get_m_cs_sn()
	{
		return $this->m_cs_sn;
	}

	public function get_m_csl_sn()
	{
		return $this->m_csl_sn;
	}

	public function get_m_cs_id()
	{
		return $this->m_cs_id;
	}

	public function get_m_admin_id()
	{
		return $this->m_admin_id;
	}

	public function get_m_id()
	{
		return $this->m_id;
	}

	public function get_m_cs_created_datetime()
	{
		return $this->m_cs_created_datetime;
	}

	public function get_m_cs_contact_datetime()
	{
		return $this->m_cs_contact_datetime;
	}

	public function get_m_cs_contact_type()
	{
		return $this->m_cs_contact_type;
	}

	public function get_m_cs_is_delete()
	{
		return $this->m_cs_is_delete;
	}

	public function get_m_cs_status()
	{
		return $this->m_cs_status;
	}

	public function get_m_cs_service_type()
	{
		return $this->m_cs_service_type;
	}

	public function get_m_cs_service_description()
	{
		return $this->m_cs_service_description;
	}

	public function get_m_plots_id()
	{
		return $this->m_plots_id;
	}

	public function get_m_pk_id()
	{
		return $this->m_pk_id;
	}

	public function get_m_ve_id()
	{
		return $this->m_ve_id;
	}

	public function get_m_bkl_start_datetime()
	{
		return $this->m_bkl_start_datetime;
	}

	public function get_m_bkl_end_datetime()
	{
		return $this->m_bkl_end_datetime;
	}

	public function get_m_bkl_id()
	{
		return $this->m_bkl_id;
	}

	public function get_m_admin_id_assigned()
	{
		return $this->m_admin_id_assigned;
	}

	public function get_array($select = "")
	{
		if ($select == "") $select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_m_cs_sn) != 0) {
			if($this->is_log)
				$return_array["m_csl_sn"] = $this->get_m_csl_sn();
			else
				$return_array["m_cs_sn"] = $this->get_m_cs_sn();
		}
		if (($select & $this->B_m_cs_id) != 0) {
			$return_array["m_cs_id"] = $this->get_m_cs_id();
		}
		if (($select & $this->B_m_admin_id) != 0) {
			$return_array["m_admin_id"] = $this->get_m_admin_id();
		}
		if (($select & $this->B_m_id) != 0) {
			$return_array["m_id"] = $this->get_m_id();
			$return_array["member_data"] = $this->this_member->get_array($this->this_member->B_m_cellphone);
		}
		if (($select & $this->B_m_cs_created_datetime) != 0) {
			$return_array["m_cs_created_datetime"] = $this->get_m_cs_created_datetime();
		}
		if (($select & $this->B_m_cs_contact_datetime) != 0) {
			$return_array["m_cs_contact_datetime"] = $this->get_m_cs_contact_datetime();
		}
		if (($select & $this->B_m_cs_contact_type) != 0) {
			$return_array["m_cs_contact_type"] = $this->get_m_cs_contact_type();
		}
		if (($select & $this->B_m_cs_is_delete) != 0) {
			$return_array["m_cs_is_delete"] = $this->get_m_cs_is_delete();
		}
		if (($select & $this->B_m_cs_status) != 0) {
			$return_array["m_cs_status"] = $this->get_m_cs_status();
		}
		if (($select & $this->B_m_cs_service_type) != 0) {
			$return_array["m_cs_service_type"] = $this->get_m_cs_service_type();
		}
		if (($select & $this->B_m_cs_service_description) != 0) {
			$return_array["m_cs_service_description"] = $this->get_m_cs_service_description();
		}
		if (($select & $this->B_m_plots_id) != 0) {
			$return_array["m_plots_id"] = $this->get_m_plots_id();
			$return_array["parking_lots"] = $this->this_parking_lot->get_array($this->this_parking_lot->B_m_plots_name);
		}
		if (($select & $this->B_m_pk_id) != 0) {
			$return_array["m_pk_id"] = $this->get_m_pk_id();
			$return_array["parking_space"] = $this->this_parking_space->get_array($this->this_parking_space->B_m_pk_number
				| $this->this_parking_space->B_m_pk_floor);
		}
		if (($select & $this->B_m_ve_id) != 0) {
			$return_array["m_ve_id"] = $this->get_m_ve_id();
			$return_array["vehicle_data"] = $this->this_vehicle_data->get_array($this->this_vehicle_data->B_m_ve_plate_no);
		}
		if (($select & $this->B_m_bkl_start_datetime) != 0) {
			$return_array["m_bkl_start_datetime"] = $this->get_m_bkl_start_datetime();
		}
		if (($select & $this->B_m_bkl_end_datetime) != 0) {
			$return_array["m_bkl_end_datetime"] = $this->get_m_bkl_end_datetime();
		}
		if (($select & $this->B_m_bkl_id) != 0) {
			if($this->this_booking_log->get_m_bkl_cancel()=="1")
				$return_array["m_bkl_id"] = "";
			else
				$return_array["m_bkl_id"] = $this->get_m_bkl_id();
		}
		if (($select & $this->B_m_admin_id_assigned) != 0) {
			$return_array["m_admin_id_assigned"] = $this->get_m_admin_id_assigned();
			$return_array["m_admin_assigned"] = $this->this_assigned_member->get_array($this->this_assigned_member->B_m_cellphone);
		}
		if (($select & $this->B_m_cs_file_logs) != 0) {
			$return_array["file_logs"] = null;
			$return_array["file_logs_array"] = $this->this_file_logs->get_array();
		}
		return $return_array;
	}
}

?>