<?php
class UPK_Url {
	private $url_array=array();

	public function __construct($url_array=array()){
		if(!is_array($url_array)){
			$url_array = str_replace ("\n","\\n",$url_array);
			$url_array = (array) json_decode($url_array,true);
		}
		foreach($url_array as $each_url) {
			if(isset($each_url->url)) {
				$this_url = new UPK_EachUrl($each_url->url);
				$tmp_each_url = json_decode(json_encode($each_url),true);
				$this_url->mapping($tmp_each_url);
				array_push($this->url_array, $this_url);
			}
			elseif(isset($each_url["url"])) {
				$this_url = new UPK_EachUrl($each_url["url"]);
				$this_url->mapping($each_url);
				array_push($this->url_array, $this_url);
			}
		}
	}
	public function add_url($url){
		$this_url = new UPK_EachUrl($url);
		array_push($this->url_array,$this_url);
	}
	public  function mapping($data)
	{
		$data = str_replace ("\n","\\n",$data);
		$data_array = (array)json_decode($data,true);
		foreach ($data_array as $each_data) {
			$tmp_each_url = new UPK_EachUrl();
			$tmp_each_url->mapping($each_data);
			array_push($this->url_array, $tmp_each_url);
		}
	}
	public function get_array() {
		$tmp_array=array();
		foreach($this->url_array as $each_url){
			array_push($tmp_array,$each_url->get_array());
		}
		return $tmp_array;
	}
}
class UPK_EachUrl {
	private $url="";
	private $title="";
	private $description="";

	public function __construct($url=""){
		$this->url = $url;
	}
	public  function mapping(array $data)
	{
		foreach ($data as $key => $val) {
			if (property_exists(__CLASS__, $key)) {
				$this->$key = $val;
			}
		}
	}
	public function get_array(){
		$tmp_array=array();
		$tmp_array["url"]=$this->url;
		$tmp_array["description"]=$this->description;
		$tmp_array["title"]=$this->title;
		return $tmp_array;

	}
}
?>