<?php
class UPK_WhiteBlackList{
	private $m_wbl_sn;
	private $m_wbl_id;
	private $m_wbl_delete;
	private $m_wbl_white_or_black;
	private $m_wbl_type_id;
	private $m_wbl_type;
	private $m_wbl_pass_id;
	private $m_wbl_pass_type;
	private $m_wbl_start_date;
	private $m_wbl_end_date;
	private $m_wbl_start_time;
	private $m_wbl_end_time;
	private $m_wl_schedule_type;
	private $m_wl_schedule_weekday;
	private $m_wbl_create_datetime;
	private $m_wbl_create_m_id;

	public $B_m_wbl_sn		=1;
	public $B_m_wbl_id		=2;
	public $B_m_wbl_delete		=4;
	public $B_m_wbl_white_or_black		=8;
	public $B_m_wbl_type_id		=16;
	public $B_m_wbl_type		=32;
	public $B_m_wbl_pass_id		=64;
	public $B_m_wbl_pass_type		=128;
	public $B_m_wbl_start_date		=256;
	public $B_m_wbl_end_date		=512;
	public $B_m_wbl_start_time		=1024;
	public $B_m_wbl_end_time		=2048;
	public $B_m_wl_schedule_type		=4096;
	public $B_m_wl_schedule_weekday		=8192;
	public $B_m_wbl_create_datetime		=16384;
	public $B_m_wbl_create_m_id		=32768;
	public $B_ALL;
	public function __construct($m_wbl_id,$sql_logic=""){
		$this->B_ALL =$this->B_m_wbl_sn | $this->B_m_wbl_id | $this->B_m_wbl_delete | $this->B_m_wbl_white_or_black | $this->B_m_wbl_type_id | $this->B_m_wbl_type | $this->B_m_wbl_pass_id | $this->B_m_wbl_pass_type | $this->B_m_wbl_start_date | $this->B_m_wbl_end_date | $this->B_m_wbl_start_time | $this->B_m_wbl_end_time | $this->B_m_wl_schedule_type | $this->B_m_wl_schedule_weekday | $this->B_m_wbl_create_datetime | $this->B_m_wbl_create_m_id;
		$this->init_m_wbl_id($m_wbl_id,$sql_logic="");
	}
	public function init_m_wbl_id($m_wbl_id,$sql_logic=""){
		global $conn, $dbName;
		check_conn($conn,$dbName);
		/*********************************** Put your table name here ***********************************/
		$sql="select * from tb_Member_White_Black_List where m_wbl_id = '".$m_wbl_id."' ";

		$sql.=$sql_logic;
		$result = mysql_query($sql, $conn);
		if(! $result){
			return json_encode(array("result"=>0, "title"=>"失敗", "description"=> mysql_error()));
		}
		else if(mysql_num_rows($result) == 1){
			$ans=mysql_fetch_assoc($result);
			$this->m_wbl_sn=$ans["m_wbl_sn"];
			$this->m_wbl_id=$ans["m_wbl_id"];
			$this->m_wbl_delete=$ans["m_wbl_delete"];
			$this->m_wbl_white_or_black=$ans["m_wbl_white_or_black"];
			$this->m_wbl_type_id=$ans["m_wbl_type_id"];
			$this->m_wbl_type=$ans["m_wbl_type"];
			$this->m_wbl_pass_id=$ans["m_wbl_pass_id"];
			$this->m_wbl_pass_type=$ans["m_wbl_pass_type"];
			$this->m_wbl_start_date=$ans["m_wbl_start_date"];
			$this->m_wbl_end_date=$ans["m_wbl_end_date"];
			$this->m_wbl_start_time=$ans["m_wbl_start_time"];
			$this->m_wbl_end_time=$ans["m_wbl_end_time"];
			if(substr($this->m_wbl_end_time,0,5)=="23:59"){
				$this->m_wbl_end_time="24:00:00";//因為DB無法給24:00:00 ，給0又不對
			}
			$this->m_wl_schedule_type=$ans["m_wl_schedule_type"];
			$this->m_wl_schedule_weekday=$ans["m_wl_schedule_weekday"];
			$this->m_wbl_create_datetime=$ans["m_wbl_create_datetime"];
			$this->m_wbl_create_m_id=$ans["m_wbl_create_m_id"];
		}
	}
	public function isTimeAvailable($DT_datetime)
	{
		if ($this->m_wl_schedule_type == "1") {
			$DT_start_date = new DateTime($this->m_wbl_start_date);
			$DT_end_date = new DateTime($this->m_wbl_end_date);
			$DT_date = new DateTime($DT_datetime->format("Y-m-d"));
			$DT_time = new DateTime($DT_datetime->format("H:i:s"));
			$week_j = $DT_datetime->format('w');
			$DT_start_time = new DateTime($this->m_wbl_start_time);
			$DT_end_time = new DateTime($this->m_wbl_end_time);
			if ($DT_start_date <= $DT_date && $DT_date <= $DT_end_date) {
				//日期在範圍內
			}
			else {
				//日期在範圍外
				return false;
			}
			//星期 資料庫是 日一二三四五六 假設星期五則是1111101 但 禮拜五是pow^5=32 所以要strrev
			$b_j = base_convert(strrev($this->m_wl_schedule_weekday), 2, 10) & pow(2, $week_j);
			if ($b_j == 0) {
				//星期在範圍外
				return false;
			}
			else {
				//星期在範圍內
			}
			if ($DT_start_time <= $DT_time && $DT_time <= $DT_end_time) {
				//時間在範圍內
			}
			else {
				//時間在範圍外
				return false;
			}
			return true;
		}
		else {
			//臨時
			$DT_wbl_start_datetime = new DateTime($this->m_wbl_start_date . " " . $this->m_wbl_start_time);
			$DT_wbl_end_datetime = new DateTime($this->m_wbl_end_date . " " . $this->m_wbl_end_time);
			if ($DT_wbl_start_datetime <= $DT_datetime && $DT_datetime < $DT_wbl_end_datetime)
				return true;
			else return false;
		}
	}
	public function whiteBlackDateCalculate($date,$DT_start_datetime,$DT_end_datetime) {
		$DT_start_date = new DateTime($this->m_wbl_start_date);
		$DT_end_date = new DateTime($this->m_wbl_end_date);
		$DT_now = new DateTime($date);
		$week_j = $DT_now->format('w');
		if ($this->m_wl_schedule_type == "1") {
			if($DT_start_date<=$DT_now && $DT_now<=$DT_end_date){
				//日期在範圍內 要切
			} else {
				//不用切
				return array(array("start_datetime"=>$DT_start_datetime,"end_datetime"=>$DT_end_datetime));
			}
			//星期 資料庫是 日一二三四五六 假設星期五則是1111101 但 禮拜五是pow^5=32 所以要strrev
			$b_j = base_convert(strrev($this->m_wl_schedule_weekday), 2, 10) & pow(2, $week_j);
			if ($b_j == 0) {
				//不用切
				return array(array("start_datetime"=>$DT_start_datetime,"end_datetime"=>$DT_end_datetime));
			}
			else {
				$DT_wbl_start_datetime = new DateTime($date . " " . $this->m_wbl_start_time);
				$DT_wbl_end_datetime = new DateTime($date . " " . $this->m_wbl_end_time);
			}
		}
		else {
			//臨時
			$DT_wbl_start_datetime = new DateTime($this->m_wbl_start_date . " " . $this->m_wbl_start_time);
			$DT_wbl_end_datetime = new DateTime($this->m_wbl_end_date . " " . $this->m_wbl_end_time);
		}
		$datetime_data = DateTimeBoolean($DT_start_datetime, $DT_end_datetime, $DT_wbl_start_datetime, $DT_wbl_end_datetime, "SUB");
		$datetime_array = $datetime_data["datetime_array"];
		return $datetime_array;
	}
	
	//遞迴
	//停車時段陣列，透過此function切出非白名單的時段出來，然後return 兩個時段
	//然後外面會有迴圈去call各個白名單
	//範例 白1切完有2個停車時段，之後切給白2變成3個停車時段，以此類推
	//-------------------------------
	//-------------------白11111-----
	//---------白222-----白11111-----
	public function whiteBlackPricePackage($not_count_price_package)
	{
		if(count($not_count_price_package)>1) {
			$tmp_data=array();
			foreach($not_count_price_package as $each_not_count_price_package) {
				$tmp_package[0]=$each_not_count_price_package;
				$return_package_data = $this->whiteBlackPricePackage($tmp_package);
				foreach($return_package_data as $each_return_data) {
					array_push($tmp_data, $each_return_data);
				}
			}
			return $tmp_data;
		}
		
		$today_package_data=array();
		foreach($not_count_price_package as $each_not_count_price_package) {
			$each_id = $each_not_count_price_package["id"];
			$DT_start_date = new DateTime($each_not_count_price_package["start_date"]);
			$DT_end_date = new DateTime($each_not_count_price_package["end_date"]);
			$DT_start_datetime = new DateTime($each_not_count_price_package["start_date"]." ".$each_not_count_price_package["start_time"]);
			$DT_end_datetime = new DateTime($each_not_count_price_package["end_date"]." ".$each_not_count_price_package["end_time"]);

			$today_package_data = array(); //今天的切完之後在把這個丟到下一天
			array_push($today_package_data,array("start_date"=>$DT_start_datetime->format("Y-m-d"),"end_date"=>$DT_end_datetime->format("Y-m-d"),"start_time"=>$DT_start_datetime->format("H:i:s"),"end_time"=>$DT_end_datetime->format("H:i:s"),"id"=>$each_id));
			for ($j = new DateTime($DT_start_date->format("Y-m-d")); $j <= $DT_end_date; $j->modify("+1 day")) {
				$next_day_package_data=array();
				foreach($today_package_data as $each_today_data) {
					$DT_each_each_start_datetime = new DateTime($each_today_data["start_date"]." ".$each_today_data["start_time"]);
					$DT_each_each_end_datetime = new DateTime($each_today_data["end_date"]." ".$each_today_data["end_time"]);
					$datetime_array = $this->whiteBlackDateCalculate($j->format("Y-m-d"), $DT_each_each_start_datetime, $DT_each_each_end_datetime);
					foreach ((array)$datetime_array as $each_datetime) {
						$DT_each_start_datetime = $each_datetime["start_datetime"];
						$DT_each_end_datetime = $each_datetime["end_datetime"];
						array_push($next_day_package_data,array("start_date"=>$DT_each_start_datetime->format("Y-m-d"),"end_date"=>$DT_each_end_datetime->format("Y-m-d"),"start_time"=>$DT_each_start_datetime->format("H:i:s"),"end_time"=>$DT_each_end_datetime->format("H:i:s"),"id"=>$each_id));
					}
				}
				$today_package_data=array();
				foreach ((array)$next_day_package_data as $each_next_day_data) {
					$each_next_day_data["id"]=$each_id;
					array_push($today_package_data, $each_next_day_data);
				}

			}
		}
		return $today_package_data;
	}
	public function get_m_wbl_sn(){
		return $this->m_wbl_sn;
	}
	public function get_m_wbl_id(){
		return $this->m_wbl_id;
	}
	public function get_m_wbl_delete(){
		return $this->m_wbl_delete;
	}
	public function get_m_wbl_white_or_black(){
		return $this->m_wbl_white_or_black;
	}
	public function get_m_wbl_type_id(){
		return $this->m_wbl_type_id;
	}
	public function get_m_wbl_type(){
		return $this->m_wbl_type;
	}
	public function get_m_wbl_pass_id(){
		return $this->m_wbl_pass_id;
	}
	public function get_m_wbl_pass_type(){
		return $this->m_wbl_pass_type;
	}
	public function get_m_wbl_start_date(){
		return $this->m_wbl_start_date;
	}
	public function get_m_wbl_end_date(){
		return $this->m_wbl_end_date;
	}
	public function get_m_wbl_start_time(){
		return $this->m_wbl_start_time;
	}
	public function get_m_wbl_end_time(){
		return $this->m_wbl_end_time;
	}
	public function get_m_wl_schedule_type(){
		return $this->m_wl_schedule_type;
	}
	public function get_m_wl_schedule_weekday(){
		return $this->m_wl_schedule_weekday;
	}
	public function get_m_wbl_create_datetime(){
		return $this->m_wbl_create_datetime;
	}
	public function get_m_wbl_create_m_id(){
		return $this->m_wbl_create_m_id;
	}
	public function get_array($select="") {
		if($select=="")
			$select=$this->B_ALL;
		$return_array=array();
		if(($select & $this->B_m_wbl_sn) != 0 ){
			$return_array["m_wbl_sn"]=$this->get_m_wbl_sn();
		}
		if(($select & $this->B_m_wbl_id) != 0 ){
			$return_array["m_wbl_id"]=$this->get_m_wbl_id();
		}
		if(($select & $this->B_m_wbl_delete) != 0 ){
			$return_array["m_wbl_delete"]=$this->get_m_wbl_delete();
		}
		if(($select & $this->B_m_wbl_white_or_black) != 0 ){
			$return_array["m_wbl_white_or_black"]=$this->get_m_wbl_white_or_black();
		}
		if(($select & $this->B_m_wbl_type_id) != 0 ){
			$return_array["m_wbl_type_id"]=$this->get_m_wbl_type_id();
		}
		if(($select & $this->B_m_wbl_type) != 0 ){
			$return_array["m_wbl_type"]=$this->get_m_wbl_type();
		}
		if(($select & $this->B_m_wbl_pass_id) != 0 ){
			$return_array["m_wbl_pass_id"]=$this->get_m_wbl_pass_id();
		}
		if(($select & $this->B_m_wbl_pass_type) != 0 ){
			$return_array["m_wbl_pass_type"]=$this->get_m_wbl_pass_type();
		}
		if(($select & $this->B_m_wbl_start_date) != 0 ){
			$return_array["m_wbl_start_date"]=$this->get_m_wbl_start_date();
		}
		if(($select & $this->B_m_wbl_end_date) != 0 ){
			$return_array["m_wbl_end_date"]=$this->get_m_wbl_end_date();
		}
		if(($select & $this->B_m_wbl_start_time) != 0 ){
			$return_array["m_wbl_start_time"]=$this->get_m_wbl_start_time();
		}
		if(($select & $this->B_m_wbl_end_time) != 0 ){
			$return_array["m_wbl_end_time"]=$this->get_m_wbl_end_time();
		}
		if(($select & $this->B_m_wl_schedule_type) != 0 ){
			$return_array["m_wl_schedule_type"]=$this->get_m_wl_schedule_type();
		}
		if(($select & $this->B_m_wl_schedule_weekday) != 0 ){
			$return_array["m_wl_schedule_weekday"]=$this->get_m_wl_schedule_weekday();
		}
		if(($select & $this->B_m_wbl_create_datetime) != 0 ){
			$return_array["m_wbl_create_datetime"]=$this->get_m_wbl_create_datetime();
		}
		if(($select & $this->B_m_wbl_create_m_id) != 0 ){
			$return_array["m_wbl_create_m_id"]=$this->get_m_wbl_create_m_id();
		}
		return $return_array;
	}
}
?>