<?php
/*Charging Procedure:
1. To get charger status (use GET request) and make sure it is at 'Available' status before sending charging request. ('Available for charging')
2. To start charging: use POST 'start_charging' request
3. To stop charging: use POST 'stop_charging' request
4. Within 60 seconds, you should be able to see charging state with charging current value.

NOTES: MUST READ
1. After sending POST 'start_charging' request, DO NOT SEND ANOTHER POST 'start_charging' request.
2. If you must send another POST 'start_charging' request, please wait for at least 60 seconds (timeout) before sending next request. (some charging process may take up to 60 seconds)

## 取得充電樁狀態 (python example)
import requests
requests.get('https://charging.wincharge.net/park_charging/vendor/space4car', json={"evse_id" :"wincharge_httpv16_40F5202C5CDF"}, verify=False, headers={'API-Key' : 's4cfnROVcjk-BrND8CSYjMPdsOAx33WTeXxMNpkCJH_dEo'}).status_code

requests.get('https://charging.wincharge.net/park_charging/vendor/space4car', json={"evse_id" :"wincharge_httpv16_40F5202C5CDF"}, verify=False, headers={'API-Key' : 's4cfnROVcjk-BrND8CSYjMPdsOAx33WTeXxMNpkCJH_dEo'}).text

requests.get('https://charging.wincharge.net/park_charging/vendor/space4car', json={"evse_id" :"wincharge_httpv16_40F5202C5CDF", "transaction_id":"1602423595"}, verify=False, headers={'API-Key' : 's4cfnROVcjk-BrND8CSYjMPdsOAx33WTeXxMNpkCJH_dEo'}).text

Response examples:

When status is 'AVAILABLE'
------------------------------------------------------------------
'{  
"accumulated_charge": 28496,                                    --> 充電樁累積充電量
"error_code": "NoError",                                        --> NoError 正常
"evse_id": "wincharge_httpv16_40F5202C5CDF",   
"first_time": "2020-07-03T08:36:24Z",                           --> 樁第一次回報時間 (UTC0)    !!!跟充電無關!!!
"last_time": "2020-07-10T11:16:46Z",                            --> 樁最後一次回報時間 (UTC0)  !!!跟充電無關!!!
"name": "space4car-\\u5065\\u5eb7\\u8def",                      --> 充電樁名稱 
"status": "Available"
}'

When status is 'CHARGING'
------------------------------------------------------------------
'{  
"accumulated_charge": 2106,   
"current_ampere": 32.0,                                       --> 目前充電電流
"current_voltage": 220.0,                                     --> 目前充電電壓
"energy_charged_value": 234,                                  --> 到目前為止的充電量
"error_code": "NoError",   
"evse_id": "wincharge_httpv16_40F5202C5CDF",                  --> 充電樁序號
"first_time": "2020-06-13T05:14:14Z",   
"last_time": "2020-07-10T13:49:33Z",   
"name": "\\u6e2c\\u8a66\\u6a01",                              --> 充電樁名稱 (網頁設定)
"start_time": "2020-07-10T13:49:17Z",                         --> 開始充電時間 (UTC0)
"status": "Charging",   
}'

When "transaction_id" is in GET
------------------------------------------------------------------
'{  
"accumulated_charge": 1755,   
"charging_stop_time": "2020-07-10T13:41:04Z",                 --> 充飽時間 (UTC0)
"end_time": "2020-07-10T13:41:18Z",                           --> 結束時間 (UTC0) (遠端停止, 拔槍, 刷卡停止)
"energy_charged_value": 351,   
"error_code": "NoError",   
"evse_id": "wincharge_ocppv16_WC12345670",   
"first_time": "2020-06-13T05:14:14Z",   
"last_time": "2020-07-10T13:41:18Z",   
"name": "\\u6e2c\\u8a66\\u6a01",   
"start_time": "2020-07-10T13:40:23Z",   
"status": "Available",                                        --> 發出 http GET request 當時樁的狀態
"transaction_id": 1594388422
}'

====================================================================================================
7.7. ChargePointStatus in ocpp16
Available                                                       --> 樁現在有空, 可以提供充電服務
Charging                                                        --> 充電中
Finishing                                                       --> 車子不拉電 (不一定是充飽)
Disconnected (more than 10 minutes from last heartbeat)         --> 樁已經離線
Faulted                                                         --> 樁有問題
Preparing                                                       --> 槍已插上, 或樁準備好充電  (請先要求車主先插槍, 確認後發出開始充電(remote start charging), 比較不會有樁timeout問題)
SuspendedEVSE                                                   (不理會)
SuspendedEV                                                     (不理會)
Reserved                                                        (不理會)
Unavailable                                                     (不理會) --> 停止服務

7.6. ChargePointErrorCode in ocpp16
NoError
HighTemperature
GroundFailure
OverCurrentFailure
OverVoltage
PowerMeterFailure
PowerSwitchFailure
ReaderFailure
ResetFailure
UnderVoltage
ConnectorLockFailure
EVCommunicationError
LocalListConflict
OtherError
WeakSignal

- 200 Response
{ 
 "status": "Available",                         # 看這個  7.7. ChargePointStatus in ocpp16
 "error_code": "NoError",                       # 看這個  7.6. ChargePointErrorCode in ocpp16
 "accumulated_charge": 104779,                  # 此充電樁的總輸出電量 (Wh)
 "current_voltage": 220.7,                      # 目前充電電壓 if 'status' : 'charging'
 "current_ampere": 0.0,                         # 目前充電電流 if 'status' : 'charging'    
 "energy_value": 1130.0,                        # 目前充電電量 (Wh) if 'status' : 'charging'    

 "start_time": "2020-06-17T01:52:56Z",          #UTC+0 開始充電 time format: ISO8601
 "charging_stop_time": "2020-06-17T01:52:56Z",  #UTC+0 停止充電 (continue updating if 'status' is 'charging')
 "end_time": "2020-06-17T01:52:56Z"             #UTC+0 結束充電 (continue updating if 'status' is 'charging')   
}

- 400+ Response
{ 
  "status": "invalid"

## 控制充電
requests.post('https://charging.wincharge.net/park_charging/vendor/space4car', json={"evse_id":"wincharge_httpv16_40F5202C5CDF", "command":"start_charging"}, verify=False, headers={'API-Key' : 's4cfnROVcjk-BrND8CSYjMPdsOAx33WTeXxMNpkCJH_dEo'}).text

requests.post('http://charging.wincharge.net/park_charging/vendor/space4car', json={"evse_id":"wincharge_httpv16_40F5202C5CDF", "command":"stop_charging"}, verify=False, headers={'API-Key' : 's4cfnROVcjk-BrND8CSYjMPdsOAx33WTeXxMNpkCJH_dEo'}).text

`POST`
{ 
  "evse_id": "wincharge_httpv16_40F5202C5CDF"
  "command" : "start_charging" or "stop_charging"
}

- 2xx Response
{ 
  "status": "accepted"
}

- 400+ Response
{ 
  "status": "invalid"
}*/
class UPK_Wincharge
{
	private $evse_id;
	private $m_id;
	const STATUS = 1;
	const START_CHARGE = 2;
	const STOP_CHARGE = 3;
	private $result;
	public function __construct($evse_id,$m_id)
	{
		$this->evse_id = $evse_id;
		$this->m_id = $m_id;
	}
	public function async_force_start_charging($token){
		if(IsDebug())
		$url = 'https://sandbox.api.space4car.com/sandbox_api/template/async_wincharge.php';
	elseif(IsPreview())
		$url = 'https://api.space4car.com/preview_api/template/async_wincharge.php';
	else
		$url = 'https://api.space4car.com/api/template/async_wincharge.php';

		$arr["activity"]="ASYNC WINCHARGE";
		$arr["token"]=$token;
		$arr["evse_id"]=$this->evse_id;
		$arr["mode"]=1;
		$headers = array('Content-Type: application/json');
		$json=json_encode($arr);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
		curl_setopt($curl, CURLOPT_TIMEOUT, 1);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
		$result = curl_exec($curl);
		curl_close($curl);
	}
	public function force_start_charging()
	{
		$response_arr = array();
		$response_start_charge = $this->send(UPK_Wincharge::START_CHARGE);
		array_push($response_arr,$response_start_charge);
		sleep(65);//休息60秒再呼叫一次
		$response_start_charge = $this->send(UPK_Wincharge::START_CHARGE);
		array_push($response_arr,$response_start_charge);
		return $response_arr;
	}
	public function async_force_stop_charging($token){
		if(IsDebug())
			$url = 'https://sandbox.api.space4car.com/sandbox_api/template/async_wincharge.php';
		elseif(IsPreview())
			$url = 'https://api.space4car.com/preview_api/template/async_wincharge.php';
		else
			$url = 'https://api.space4car.com/api/template/async_wincharge.php';

		$arr["activity"]="ASYNC WINCHARGE";
		$arr["token"]=$token;
		$arr["evse_id"]=$this->evse_id;
		$arr["mode"]=3;
		$headers = array('Content-Type: application/json');
		$json=json_encode($arr);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
		curl_setopt($curl, CURLOPT_TIMEOUT, 1);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
		$result = curl_exec($curl);
		curl_close($curl);
	}
	public function force_stop_charging()
	{
		$response_arr = array();
		$response_stop_charge = $this->send(UPK_Wincharge::STOP_CHARGE);
		array_push($response_arr,$response_stop_charge);
		sleep(65);//休息60秒再呼叫一次
		$response_stop_charge = $this->send(UPK_Wincharge::STOP_CHARGE);
		array_push($response_arr,$response_stop_charge);
		return $response_arr;
	}
	public function start_charging()
	{
		$response_start_charge = $this->send(UPK_Wincharge::START_CHARGE);
		if ($response_start_charge["status"] == "accepted") {
			$this->result = array("result"=>1);
		} else {
			$this->result = array("result"=>0,"title"=>"充電失敗","description"=>"","data"=>$response_start_charge);
		}
		return $this->result;
	}
	public function stop_charging()
	{
		$response_stop_charge = $this->send(UPK_Wincharge::STOP_CHARGE);
		if ($response_stop_charge["status"] == "accepted") {
			$this->result = array("result"=>1);
		} else {
			$this->result = array("result"=>0,"title"=>"停止充電失敗","description"=>"","data"=>$response_stop_charge);
		}
		return $this->result;
	}
	public function get_status()
	{
		$response_status = $this->send(UPK_Wincharge::STATUS);
		$status = $response_status["status"];
		if ($status == "Available") {
			$this->result = array("result"=>1,"data"=>$response_status);
		} elseif ($status == "Charging") {
			$this->result = array("result"=>2,"data"=>$response_status);
		} elseif ($status == "Finishing") {//車子不拉電 (不一定是充飽)
			$this->result = array("result"=>0,"title"=>"充電失敗","description"=>"已經充滿","data"=>$response_status);
		} elseif ($status == "Disconnected") {//樁已經離線
			$this->result = array("result"=>0,"title"=>"充電失敗","description"=>"無法連接裝置，請洽客服","data"=>$response_status);
		} elseif ($status == "Faulted") {//樁有問題
			$this->result = array("result"=>0,"title"=>"充電失敗","description"=>"設備異常，請洽客服","data"=>$response_status);
		} elseif ($status == "Preparing") {
			//槍已插上, 或樁準備好充電  (請先要求車主先插槍, 確認後發出開始充電(remote start charging), 比較不會有樁timeout問題)
			$this->result = array("result"=>0,"title"=>"充電失敗","description"=>"請確認充電樁是否連接正確","data"=>$response_status);
		} elseif ($status == "invalid") {
			$this->result = array("result"=>0,"title"=>"充電失敗","description"=>"請在試一次","data"=>$response_status);
		} else {
			$this->result = array("result"=>0,"title"=>"充電失敗","description"=>"網路異常","data"=>$response_status);
		}
		return $this->result;
	}
	public function get_result()
	{
		return ReturnJsonModule($this->result);
	}
	private function send($command)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://charging.wincharge.net/park_charging/vendor/space4car",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "unf-8", //""
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 10, //0
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_HTTPHEADER => array(
				"API-Key: s4cfnROVcjk-BrND8CSYjMPdsOAx33WTeXxMNpkCJH_dEo",
				"Content-Type: application/json"
			),
		));
		global $conn,$dbName;
		check_conn($conn,$dbName);
		//CURLOPT_POSTFIELDS => "{\r\n    \"evse_id\": \"" . $this->evse_id . "\",\r\n    \"command\": \"stop_charging\"\r\n}",
		if ($command == UPK_Wincharge::START_CHARGE) {
			curl_setopt($curl, CURLOPT_POSTFIELDS, "{\r\n    \"evse_id\": \"" . $this->evse_id . "\",\r\n    \"command\": \"start_charging\"\r\n}");
		} elseif ($command == UPK_Wincharge::STOP_CHARGE) {
			curl_setopt($curl, CURLOPT_POSTFIELDS, "{\r\n    \"evse_id\": \"" . $this->evse_id . "\",\r\n    \"command\": \"stop_charging\"\r\n}");
		} elseif ($command == UPK_Wincharge::STATUS) {
			curl_setopt($curl, CURLOPT_POSTFIELDS, "{\"evse_id\": \"" . $this->evse_id . "\",\"transaction_id\":\"1602423595\"}");
		} else {
			$ans["status"] = "invalid";
			$ans["description"] = "invalid command";//除非參數下錯不然不會來這邊
			return $ans;
		}
		$response = curl_exec($curl);
		curl_close($curl);
		rg_activity_log($conn,$this->m_id,"呼叫充電樁",$this->evse_id,"command=".$command,$response);
		$response_arr = json_decode($response, true);
		if (!isset($response_arr["status"]))
			$response_arr["status"] = "invalid";
		return $response_arr;
	}
}
