<?php
class UPK_MemberVehicle
{
	private $m_ve_sn;
	private $m_ve_id;
	private $m_id;
	private $m_ve_plate_no;
	private $m_ve_create_datetime;
	private $m_ve_color;
	private $m_ve_color_description;
	private $ves_id;
	private $m_ve_license_enable;
	private $m_ve_can_planepk;
	private $m_ve_can_machinepk;
	private $m_ve_can_undergoundpk;
	private $m_ve_can_threedpk;
	private $m_ve_can_privatepk;
	private $m_ve_can_publicpk;
	private $m_ve_can_streetpk;
	private $m_ve_delete;
	private $m_ve_share_code_create_datetime;
	private $m_ve_share_code;
	private $m_ve_length;
	private $m_ve_width;
	private $m_ve_height;
	private $m_ve_weight;
	private $m_ve_special_plate_no;
	private $m_ve_special_level;
	private $m_ve_special_issuing_unit;
	private $m_ve_special_limit_datetime;
	private $m_ve_special_number;
	private $m_ve_special_auth_status;
	private $m_ve_special_auth_datetime;
	private $m_ve_special_auth_m_id;
	private $m_ve_special_address;
	private $m_ve_agreed_gov;
	private $m_ve_is_auto_pay_gov_parking_fee;
	private $m_ve_power_type;
	private $ves_array;//要改成class

	private $is_over_24_hours;
	private $is_admin;//編輯用的

	public $json_result;

	public $B_m_ve_sn = 1;
	public $B_m_ve_id = 2;
	public $B_m_id = 4;
	public $B_m_ve_plate_no = 8;
	public $B_m_ve_create_datetime = 16;
	public $B_m_ve_color = 32;
	public $B_m_ve_color_description = 64;
	public $B_ves_id = 128;
	public $B_m_ve_license_enable = 256;
	public $B_m_ve_can_pk = 512;
	public $B_is_over_24_hours = 1024;
	/*
	public $B_m_ve_can_planepk		=512;
	public $B_m_ve_can_machinepk		=1024;
	public $B_m_ve_can_undergoundpk		=2048;
	public $B_m_ve_can_threedpk		=4096;
	public $B_m_ve_can_privatepk		=8192;
	public $B_m_ve_can_publicpk		=16384;
	public $B_m_ve_can_streetpk		=32768;
	*/
	public $B_m_ve_delete = 65536;
	public $B_m_ve_share_code_create_datetime = 131072;
	public $B_m_ve_share_code = 262144;
	public $B_m_ve_length = 524288;
	public $B_m_ve_width = 1048576;
	public $B_m_ve_height = 2097152;
	public $B_m_ve_weight = 4194304;
	public $B_m_ve_special = 8388608;
	/*
	public $B_m_ve_special_plate_no		=8388608;
	public $B_m_ve_special_level		=16777216;
	public $B_m_ve_special_issuing_unit		=33554432;
	public $B_m_ve_special_limit_datetime		=67108864;
	public $B_m_ve_special_number		=134217728;
	public $B_m_ve_special_auth_status		=268435456;
	public $B_m_ve_special_auth_datetime		=536870912;
	public $B_m_ve_special_auth_m_id		=1073741824;
	public $B_m_ve_special_address		=2147483648;
	*/
	//public $B_m_ve_agreed_gov		=4294967296;
	public $B_m_ve_agreed_gov = 16777216;
	public $B_m_ve_is_auto_pay_gov_parking_fee = 33554432;
	public $B_m_ve_power_type = 67108864;
	public $B_ALL;

	public function __construct($m_ve_id, $sql_logic = "")
	{
		$this->B_ALL = $this->B_m_ve_sn | $this->B_m_ve_id | $this->B_m_id | $this->B_m_ve_plate_no | $this->B_m_ve_create_datetime | $this->B_m_ve_color | $this->B_m_ve_color_description | $this->B_ves_id | $this->B_m_ve_license_enable | $this->B_m_ve_can_pk | $this->B_is_over_24_hours | $this->B_m_ve_delete | $this->B_m_ve_share_code_create_datetime | $this->B_m_ve_share_code | $this->B_m_ve_length | $this->B_m_ve_width | $this->B_m_ve_height | $this->B_m_ve_weight | $this->B_m_ve_special | $this->B_m_ve_agreed_gov | $this->B_m_ve_is_auto_pay_gov_parking_fee | $this->B_m_ve_power_type;
		$this->is_admin = 0;
		$this->init_m_ve_id($m_ve_id, $sql_logic = "");
	}
	public function init_m_ve_plate_no($plate_number,$id) {
		$this_member = new UPK_Member($id);
		$token = $this_member->get_m_token();
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$sql = "SELECT m_ve_id FROM tb_Member_Vehicle WHERE m_ve_delete='0' AND m_id='" . $id . "' AND m_ve_plate_no='" . $plate_number . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "查詢車輛失敗", "description" => mysql_error($conn)));
		}
		else if (mysql_num_rows($result) == 0) {
			#無此紀錄
			include_once("/../main_api/RegisterVehicleFunc.php");
			//{"activity":"REGISTER VEHICLE","token":"0afcd43d-13d6-e71f-412c-468212d74bc6","vehicle_plate_number":"TEST-0427","ves_id":"VEID0000000000","trademark":"","model":"","length":"0","width":"0","height":"0","weight":"0","color":"000000","can_planepk":"1","can_machinepk":"1","can_undergoundpk":"1","can_threedpk":"1","can_privatepk":"1","can_publicpk":"1","can_streetpk":"1"}
			$vehicle_plate_number = $plate_number;
			$ves_id = "VEID0000000000";
			$trademark = "";
			$model = "";
			$length = "0";
			$width = "0";
			$height = "0";
			$weight = "0";
			$color = "000000";
			$can_planepk = "1";
			$can_machinepk = "1";
			$can_undergoundpk = "1";
			$can_threedpk = "1";
			$can_privatepk = "1";
			$can_publicpk = "1";
			$can_streetpk = "1";
			$ttans = RegisterVehicleFunc("", "REGISTER VEHICLE", $token, $vehicle_plate_number, $color, $can_planepk, $can_machinepk, $can_undergoundpk, $can_threedpk, $can_privatepk, $can_publicpk, $can_streetpk, $trademark, $model, $length, $width, $height, $weight, $ves_id, "0", "1", "1", "1");
			$tans = json_decode($ttans, true);
			if ($tans["result"] == 0) {
				return json_encode($tans);
			}
			else {
				$vehicle_id = $tans["vehicle_id"];
			}
		}
		else {
			$ans = mysql_fetch_array($result);
			$vehicle_id = $ans["m_ve_id"];
		}
		$this->init_m_ve_id($vehicle_id);
		return json_encode(array("reuslt"=>1));
	}
	public function set_ves_id($ves_id = null, $trademark = "", $model = "")
	{
		$pure_data = "ves_id=" . $ves_id . ",trademark=" . $trademark . ",model=" . $model;
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		if($ves_id==null && $trademark=="" && $model==""){
			$ves_id="VEID0000000000";
		}
		if ($ves_id == null) {
			$sql = "SELECT ves_id FROM tb_Vehicles WHERE ves_trademark='" . $trademark . "' and ves_model='" . $model . "'";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return ReturnJsonModule(array("result" => 0, "title" => "搜尋車種失敗", "description" => mysql_error()));
			}
			else if (mysql_num_rows($result) == 0) {
				#查無此車種
				rg_activity_log($conn, "", "搜尋車種失敗", "無此車種", $pure_data, "");
				//$trademark = "";
				//$model = "";
				$ans = GetSystemCode("2020007", $language, $conn);
				return ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
			else {
				$ans2 = mysql_fetch_assoc($result);
				$this->ves_id = $ans2["ves_id"];
			}
		}
		if ($ves_id != null) {
			$sql = "SELECT * FROM tb_Vehicles WHERE ves_id='" . $ves_id . "'";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return ReturnJsonModule(array("result" => 0, "title" => "搜尋車種失敗", "description" => mysql_error()));
			}
			else if (mysql_num_rows($result) == 0) {    #無此車種
				if ($ves_id == "VEID0000000000") {
					$sql = "INSERT INTO `tb_Vehicles` (`ves_sn`, `m_id`, `ves_id`, `ves_trademark`, `ves_model`, `ves_length`, `ves_width`, `ves_height`, `ves_weight`, `ves_create_date`) VALUES ('0', 'UMID0000000000', 'VEID0000000000', '-請選擇廠牌型號', '', '0', '0', '0', '0', '2017-01-03 00:00:00')";
					$result = mysql_query($sql, $conn);
					if (!$result) {
						return ReturnJsonModule(array("result" => 0, "title" => "新增預設車款失敗", "description" => mysql_error()));
					}
					//$trademark = "-請選擇廠牌型號";
					//$model = "";
					$this->ves_id = "VEID0000000000";
					$this->m_ve_length = "3";
					$this->m_ve_width = "3";
					$this->m_ve_height = "3";
					$this->m_ve_weight = "3";
				}
				else {
					rg_activity_log($conn, "", "搜尋車種失敗", "無此車種", $pure_data, "");
					//$trademark = "";
					//$model = "";
					$ans = GetSystemCode("2020007", $language, $conn);
					return ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
				}
			}
			else {
				$ans2 = mysql_fetch_assoc($result);
				//$trademark = $ans2["ves_trademark"];
				//$model = $ans2["ves_model"];
				$this->ves_id = $ans2["ves_id"];
				$this->m_ve_length = $ans2["ves_length"];
				$this->m_ve_width = $ans2["ves_width"];
				$this->m_ve_height = $ans2["ves_height"];
				$this->m_ve_weight = $ans2["ves_weight"];
			}
		}
	}
	public function insert($token)
	{
		$pure_data = "token=" . $token;
		if ($this->ves_id==null) $this->set_ves_id("VEID0000000000");
		if ($this->m_ve_can_planepk == null) $this->m_ve_can_planepk = 0;
		if ($this->m_ve_can_machinepk == null) $this->m_ve_can_machinepk = 0;
		if ($this->m_ve_can_undergoundpk == null) $this->m_ve_can_undergoundpk = 0;
		if ($this->m_ve_can_threedpk == null) $this->m_ve_can_threedpk = 0;
		if ($this->m_ve_can_privatepk == null) $this->m_ve_can_privatepk = 0;
		if ($this->m_ve_can_publicpk == null) $this->m_ve_can_publicpk = 0;
		if ($this->m_ve_can_streetpk == null) $this->m_ve_can_streetpk = 0;

		if ($this->m_ve_agreed_gov == null) $this->m_ve_agreed_gov = 0;
		if ($this->m_ve_special_plate_no == null) $this->m_ve_special_plate_no = 1;//小自客
		if ($this->m_ve_special_auth_status == null) $this->m_ve_special_auth_status = 1;//審核中

		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";

		rg_activity_log($conn, "", "新增車輛開始", "", $pure_data, "");
		$G_SYSTEM_ERROR_RECEIVE_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
		$G_NEW_VEHICLE_FREE_POINTS = GetSystemParameter($conn, "new_vehicle_free_points");
		$G_MEMBER_SPACE4CAR_ROUTE = GetSystemParameter($conn, "memberSpace4carRoute");
		$G_NEW_VEHICLE_FREE_POINTS_DEAD = GetSystemParameter($conn, "new_vehicle_free_points_dead");
		if ($token == null) {
			rg_activity_log($conn, "", "新增車輛失敗", "必填欄位未填", $pure_data, "");
			$ans = GetSystemCode("2020005", $language, $conn);
			$this->json_result = ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			return false;
		}
		#確認使用者
		$sql = "SELECT m_id, m_language FROM tb_Member WHERE m_token='" . $token . "' AND m_id<>'UMID0000000000'";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			$this->json_result = ReturnJsonModule(array("result" => 0, "title" => "取得token錯誤", "description" => mysql_error()));
			return false;
		}
		else if (mysql_num_rows($result) == 0) {
			#token失效
			rg_activity_log($conn, "", "取得資訊失敗", "token失效", $pure_data, "");
			$ans = GetSystemCode("9", $language, $conn);
			$this->json_result = ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			return false;
		}
		if ($this->m_ve_plate_no == null || $this->ves_id == null) {
			rg_activity_log($conn, "", "新增車輛失敗", "必填欄位未填", $pure_data, "");
			$ans = GetSystemCode("2020005", $language, $conn);
			$this->json_result = ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			return false;
		}
		$ans = mysql_fetch_assoc($result);
		$id = $ans["m_id"];
		$language = $ans["m_language"];

		$sql = "SELECT m_id, m_ve_plate_no,m_ve_id FROM tb_Member_Vehicle WHERE m_ve_delete=0 and m_id='" . $id . "'";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			$this->json_result = ReturnJsonModule(array("result" => 0, "title" => "搜尋車輛失敗", "description" => mysql_error()));
			return false;
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$m_ve_id = $ans["m_ve_id"];
			rg_activity_log($conn, "", "新增車輛開始LOG", $this->m_ve_plate_no."  ".$m_ve_id, $pure_data, $sql);
			if ($ans["m_ve_plate_no"] == "車號未輸入") {
				$ans = updateVehiclePlateNo($m_ve_id, $this->m_ve_plate_no, $id, $pure_data);
				$tans = json_decode($ans, true);
				if ($tans["result"] == 1) {
					//要保證這隻API都匯回傳vehicle id  fn_package的GetMemberVehicle會用到
					$tans["vehicle_id"] = $m_ve_id;
					$this->m_ve_id=$m_ve_id;
					$this->json_result = ReturnJsonModule($tans);
					return true;
				}
			}
		}
		$sql = "SELECT m_id, m_ve_license_enable FROM tb_Member_Vehicle WHERE m_ve_plate_no='" . $this->m_ve_plate_no . "' and m_ve_delete=0 and m_id='" . $id . "'";
		rg_activity_log($conn, "", "新增車輛開始LOG", "select", $pure_data, $sql);
		$result = mysql_query($sql, $conn);
		if (!$result) {
			$this->json_result = ReturnJsonModule(array("result" => 0, "title" => "搜尋車輛失敗", "description" => mysql_error()));
			return false;
		}
		else if (mysql_num_rows($result) == 0) {
			#無重複，可新增(該會員沒有重複的車輛)
			$tmp_ve_id = "tmp" . GenerateRandomString(11, '0123456789');
			$sql = "INSERT INTO tb_Member_Vehicle (m_ve_id, m_id, m_ve_plate_no, m_ve_color,ves_id, m_ve_can_planepk, 
						m_ve_can_machinepk, m_ve_can_undergoundpk, m_ve_can_threedpk, m_ve_can_privatepk, m_ve_can_publicpk,
						m_ve_can_streetpk, m_ve_delete, m_ve_length, m_ve_width, m_ve_height, m_ve_weight, m_ve_agreed_gov, m_ve_special_plate_no, m_ve_special_auth_status) VALUES ('" . $tmp_ve_id . "', '" . $id . "', '" . $this->m_ve_plate_no . "', '" . $this->m_ve_color . "', '" . $this->ves_id . "', b'" . $this->m_ve_can_planepk . "', b'" . $this->m_ve_can_machinepk . "', b'" . $this->m_ve_can_undergoundpk . "', b'" . $this->m_ve_can_threedpk . "', b'" . $this->m_ve_can_privatepk . "', b'" . $this->m_ve_can_publicpk . "', b'" . $this->m_ve_can_streetpk . "', b'0', '" . $this->m_ve_length . "', '" . $this->m_ve_width . "', '" . $this->m_ve_height . "', '" . $this->m_ve_weight . "', '" . $this->m_ve_agreed_gov . "', '" . $this->m_ve_special_plate_no . "', '" . $this->m_ve_special_auth_status . "')";
			rg_activity_log($conn, "", "新增車輛開始LOG", "insert", $pure_data, $sql);
			if (!mysql_query($sql, $conn)) {
				$this->json_result = ReturnJsonModule(array("result" => 0, "title" => "新增失敗", "description" => mysql_error()));
				return false;
			}
			$sql = "SELECT m_ve_sn FROM tb_Member_Vehicle WHERE m_id='" . $id . "' and m_ve_plate_no='" . $this->m_ve_plate_no . "' and m_ve_delete=0 and m_ve_id='" . $tmp_ve_id . "'";
			$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
			if (!$ans) {
				$this->json_result = ReturnJsonModule(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error()));
				return false;
			}
			$new_id = $ans["m_ve_sn"];
			Sn2Id("MVID", $new_id);
			$sql = "UPDATE tb_Member_Vehicle SET m_ve_id='" . $new_id . "', m_ve_license_enable=0 WHERE m_id='" . $id . "' and m_ve_plate_no='" . $this->m_ve_plate_no . "' and m_ve_delete=0 and m_ve_id='" . $tmp_ve_id . "'";
			rg_activity_log($conn, "", "新增車輛開始LOG", "update", $pure_data, $sql);
			if (!mysql_query($sql, $conn)) {
				$this->json_result = ReturnJsonModule(array("result" => 0, "title" => "更新失敗", "description" => mysql_error()));
				return false;
			}
			else {
				$this->m_ve_id = $new_id;
				rg_activity_log($conn, $id, "車輛新增成功", "", $pure_data, "");
				if ($this->m_ve_plate_no != "車號未輸入") {
					//砍車輛之前要判斷有沒有預約
					#如果後來自己新增車輛的話，如果之前有自動建立過車輛為輸入則將他砍掉
					$sql = "SELECT * FROM tb_Member_Vehicle WHERE m_id='" . $id . "' and m_ve_plate_no='車號未輸入' and m_ve_delete=0 ";
					$result = mysql_query($sql, $conn);
					if (!$result) {
						$this->json_result = ReturnJsonModule(array("result" => 0, "title" => "更新失敗", "description" => mysql_error()));
						return false;
					}
					elseif (mysql_num_rows($result) == 1) {
						$ans = mysql_fetch_array($result);
						include_once("/../main_api/DelMemberVehicleFunc.php");
						DelMemberVehicle("DEL MEMBER VEHICLE", $token, $ans["m_ve_id"]);//直接忽略回傳數值
					}
				}
				//判斷是否為第一次新增
				$sql = "select m_id from tb_Member_Vehicle where m_id='" . $id . "'";    //不能判斷is_delete
				$result = mysql_query($sql, $conn);
				if (!$result) {
					$this->json_result = ReturnJsonModule(array("result" => 0, "title" => "更新失敗", "description" => mysql_error()));
					return false;
				}
				else {
					if (mysql_num_rows($result) == 1) {
						//是第一台車
						include_once("/../main_api/DepositFreePoint_func.php");
						$ttans = DepositFreePointfunc("DEPOSIT FREE POINT", $id, $G_NEW_VEHICLE_FREE_POINTS, "4", "", $G_NEW_VEHICLE_FREE_POINTS_DEAD);
						$ttans = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $ttans), true);
						if ($ttans["result"] == 1) {
							rg_activity_log($conn, $id, "贈送新車輛免費點數成功", "", $sql, "");
						}
						else rg_activity_log($conn, $id, "贈送新車輛免費點數失敗", "", $sql, "");
					}

					include_once("/../template/email_template.php");
				/*	EmailTemplate($conn, "易停網管理員", "upk.rd00001@gmail.com",
						"易停網管理員-車輛審核通知信",
						"ID: " . $new_id . "<br>內容: " . $this->m_ve_plate_no .
						"<br><a href='" . $G_MEMBER_SPACE4CAR_ROUTE . "vehicle/details/" . $new_id . "/1'>點我進行審核</a>");*/
					$this->m_ve_id=$new_id;
					$this->json_result = ReturnJsonModule(array("result" => 1, "vehicle_id" => $new_id));
					return true;
				}
			}
		}
		else {
			//該會員有重複的車輛
			$ans = GetSystemCode("19", $language, $conn);
			$this->json_result = ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			return false;
		}
	}

	public function init_m_ve_id($m_ve_id, $sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		/*********************************** Put your table name here ***********************************/
		$sql = "select * from tb_Member_Vehicle where m_ve_id = '" . $m_ve_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			echo ReturnJsonModule(array("失敗" => mysql_error($conn) . $sql));
			mysql_close($conn);
			return;
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->m_ve_sn = $ans["m_ve_sn"];
			$this->m_ve_id = $ans["m_ve_id"];
			$this->m_id = $ans["m_id"];
			if ($ans["m_ve_plate_no"] == "車號未輸入")
				$this->m_ve_plate_no = "預設車輛";
			else $this->m_ve_plate_no = $ans["m_ve_plate_no"];
			$this->m_ve_create_datetime = $ans["m_ve_create_datetime"];
			$this->m_ve_color = $ans["m_ve_color"];
			$this->m_ve_color_description = $ans["m_ve_color_description"];
			$this->ves_id = $ans["ves_id"];
			$this->m_ve_license_enable = $ans["m_ve_license_enable"];
			$this->m_ve_can_planepk = $ans["m_ve_can_planepk"];
			$this->m_ve_can_machinepk = $ans["m_ve_can_machinepk"];
			$this->m_ve_can_undergoundpk = $ans["m_ve_can_undergoundpk"];
			$this->m_ve_can_threedpk = $ans["m_ve_can_threedpk"];
			$this->m_ve_can_privatepk = $ans["m_ve_can_privatepk"];
			$this->m_ve_can_publicpk = $ans["m_ve_can_publicpk"];
			$this->m_ve_can_streetpk = $ans["m_ve_can_streetpk"];
			$this->m_ve_delete = $ans["m_ve_delete"];
			$this->m_ve_share_code_create_datetime = $ans["m_ve_share_code_create_datetime"];
			$this->m_ve_share_code = $ans["m_ve_share_code"];
			$this->m_ve_length = $ans["m_ve_length"];
			$this->m_ve_width = $ans["m_ve_width"];
			$this->m_ve_height = $ans["m_ve_height"];
			$this->m_ve_weight = $ans["m_ve_weight"];
			$this->m_ve_special_plate_no = $ans["m_ve_special_plate_no"];
			$this->m_ve_special_level = $ans["m_ve_special_level"];
			$this->m_ve_special_issuing_unit = $ans["m_ve_special_issuing_unit"];
			//$this->m_ve_special_limit_datetime=$ans["m_ve_special_limit_datetime"];
			$DT_special_limit_datetime = new DateTime($ans["m_ve_special_limit_datetime"]);
			$this->m_ve_special_limit_datetime = $DT_special_limit_datetime->format("Y-m-d");
			$this->m_ve_special_number = $ans["m_ve_special_number"];
			$this->m_ve_special_auth_status = $ans["m_ve_special_auth_status"];
			$this->m_ve_special_auth_datetime = $ans["m_ve_special_auth_datetime"];
			$this->m_ve_special_auth_m_id = $ans["m_ve_special_auth_m_id"];
			$this->m_ve_special_address = $ans["m_ve_special_address"];
			$this->m_ve_agreed_gov = $ans["m_ve_agreed_gov"];
			$this->m_ve_is_auto_pay_gov_parking_fee = $ans["m_ve_is_auto_pay_gov_parking_fee"];
			$this->m_ve_power_type = $ans["m_ve_power_type"];
			$DT_create_datetime = new DateTime($this->m_ve_create_datetime);
			$DT_now = new DateTime('now');
			$DT_create_datetime->modify('+1 day');
			if ($DT_now > $DT_create_datetime)
				$this->is_over_24_hours = 1;
			else
				$this->is_over_24_hours = 0;
			if ($this->ves_id == "")
				$this->ves_id = "VEID0000000000";
			$sql = "SELECT ves_sn,ves_id,ves_trademark,ves_model,ves_length,ves_width,ves_height,ves_weight,ves_create_date,ves_source FROM tb_Vehicles WHERE ves_id='" . $this->ves_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				echo ReturnJsonModule(array("失敗" => mysql_error($conn) . $sql));
				mysql_close($conn);
				return;
			}
			else if (mysql_num_rows($result) == 1) {
				$ans = mysql_fetch_assoc($result);
				$this->ves_array = $ans;
				unset($this->ves_array["m_id"]);//車款的m_id不要帶進去
			}
		}
	}

	public function GetFileLog($token)
	{
		include_once("/../select_api/GetFileLogFunc.php");
		$ttans = json_decode(GetFileLogFunc("GET FILE LOG", $token, "3", $this->m_ve_id), true);
		//$tmp["file_logs"]=$ttans;
		if ($ttans["result"] == 0)
			return 0;
		$this->file_logs = $ttans["data"];
		return 1;
	}

	public function get_m_ve_sn()
	{
		return $this->m_ve_sn;
	}

	public function get_m_ve_id()
	{
		return $this->m_ve_id;
	}

	public function get_m_id()
	{
		return $this->m_id;
	}

	public function get_m_ve_plate_no()
	{
		return $this->m_ve_plate_no;
	}

	public function get_m_ve_create_datetime()
	{
		return $this->m_ve_create_datetime;
	}

	public function get_m_ve_color()
	{
		return $this->m_ve_color;
	}

	public function get_m_ve_color_description()
	{
		return $this->m_ve_color_description;
	}

	public function get_ves_id()
	{
		return $this->ves_id;
	}

	public function get_m_ve_license_enable()
	{
		return $this->m_ve_license_enable;
	}

	public function get_m_ve_can_planepk()
	{
		return $this->m_ve_can_planepk;
	}

	public function get_m_ve_can_machinepk()
	{
		return $this->m_ve_can_machinepk;
	}

	public function get_m_ve_can_undergoundpk()
	{
		return $this->m_ve_can_undergoundpk;
	}

	public function get_m_ve_can_threedpk()
	{
		return $this->m_ve_can_threedpk;
	}

	public function get_m_ve_can_privatepk()
	{
		return $this->m_ve_can_privatepk;
	}

	public function get_m_ve_can_publicpk()
	{
		return $this->m_ve_can_publicpk;
	}

	public function get_m_ve_can_streetpk()
	{
		return $this->m_ve_can_streetpk;
	}

	public function get_is_over_24_hours()
	{
		return $this->is_over_24_hours;
	}

	public function get_m_ve_delete()
	{
		return $this->m_ve_delete;
	}

	public function get_m_ve_share_code_create_datetime()
	{
		return $this->m_ve_share_code_create_datetime;
	}

	public function get_m_ve_share_code()
	{
		return $this->m_ve_share_code;
	}

	public function get_m_ve_length()
	{
		return $this->m_ve_length;
	}

	public function get_m_ve_width()
	{
		return $this->m_ve_width;
	}

	public function get_m_ve_height()
	{
		return $this->m_ve_height;
	}

	public function get_m_ve_weight()
	{
		return $this->m_ve_weight;
	}

	public function get_m_ve_special_plate_no()
	{
		return $this->m_ve_special_plate_no;
	}

	public function get_m_ve_special_level()
	{
		return $this->m_ve_special_level;
	}

	public function get_true_special_level()
	{
		//加上審核與日期判斷的是否身障
		$DT_m_ve_special_limit_datetime = new DateTime($this->get_m_ve_special_limit_datetime());
		$DT_now = new DateTime();
		//如果已經過期就當一般車
		if ($DT_now > $DT_m_ve_special_limit_datetime) {
			return "0";
		}
		//如果沒審核就當一班車
		if ($this->get_m_ve_special_auth_status() != "2") {
			return "0";
		}
		return $this->m_ve_special_level;
	}

	public function get_m_ve_special_issuing_unit()
	{
		return $this->m_ve_special_issuing_unit;
	}

	public function get_m_ve_special_limit_datetime()
	{
		return $this->m_ve_special_limit_datetime;
	}

	public function get_m_ve_special_number()
	{
		return $this->m_ve_special_number;
	}

	public function get_m_ve_special_auth_status()
	{
		return $this->m_ve_special_auth_status;
	}

	public function get_m_ve_special_auth_datetime()
	{
		return $this->m_ve_special_auth_datetime;
	}

	public function get_m_ve_special_auth_m_id()
	{
		return $this->m_ve_special_auth_m_id;
	}

	public function get_m_ve_special_address()
	{
		return $this->m_ve_special_address;
	}

	public function get_m_ve_agreed_gov()
	{
		return $this->m_ve_agreed_gov;
	}

	public function get_m_ve_is_auto_pay_gov_parking_fee()
	{
		return $this->m_ve_is_auto_pay_gov_parking_fee;
	}

	public function get_m_ve_power_type()
	{
		return $this->m_ve_power_type;
	}

	public function get_array($select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_m_ve_sn) != 0) {
			$return_array["m_ve_sn"] = $this->get_m_ve_sn();
		}
		if (($select & $this->B_m_ve_id) != 0) {
			$return_array["m_ve_id"] = $this->get_m_ve_id();
		}
		if (($select & $this->B_m_id) != 0) {
			$return_array["m_id"] = $this->get_m_id();
		}
		if (($select & $this->B_m_ve_plate_no) != 0) {
			$return_array["m_ve_plate_no"] = $this->get_m_ve_plate_no();
		}
		if (($select & $this->B_m_ve_create_datetime) != 0) {
			$return_array["m_ve_create_datetime"] = $this->get_m_ve_create_datetime();
		}
		if (($select & $this->B_m_ve_color) != 0) {
			$return_array["m_ve_color"] = $this->get_m_ve_color();
		}
		if (($select & $this->B_m_ve_color_description) != 0) {
			$return_array["m_ve_color_description"] = $this->get_m_ve_color_description();
		}
		if (($select & $this->B_ves_id) != 0) {
			$return_array["ves_id"] = $this->get_ves_id();
		}
		if (($select & $this->B_m_ve_license_enable) != 0) {
			$return_array["m_ve_license_enable"] = $this->get_m_ve_license_enable();
		}
		if (($select & $this->B_m_ve_can_pk) != 0) {
			$return_array["m_ve_can_planepk"] = $this->get_m_ve_can_planepk();
			$return_array["m_ve_can_machinepk"] = $this->get_m_ve_can_machinepk();
			$return_array["m_ve_can_undergoundpk"] = $this->get_m_ve_can_undergoundpk();
			$return_array["m_ve_can_threedpk"] = $this->get_m_ve_can_threedpk();
			$return_array["m_ve_can_privatepk"] = $this->get_m_ve_can_privatepk();
			$return_array["m_ve_can_publicpk"] = $this->get_m_ve_can_publicpk();
			$return_array["m_ve_can_streetpk"] = $this->get_m_ve_can_streetpk();
		}
		if (($select & $this->B_is_over_24_hours) != 0) {
			$return_array["is_over_24_hours"] = $this->get_is_over_24_hours();
		}
		if (($select & $this->B_m_ve_delete) != 0) {
			$return_array["m_ve_delete"] = $this->get_m_ve_delete();
		}
		if (($select & $this->B_m_ve_share_code_create_datetime) != 0) {
			$return_array["m_ve_share_code_create_datetime"] = $this->get_m_ve_share_code_create_datetime();
		}
		if (($select & $this->B_m_ve_share_code) != 0) {
			$return_array["m_ve_share_code"] = $this->get_m_ve_share_code();
		}
		if (($select & $this->B_m_ve_length) != 0) {
			$return_array["m_ve_length"] = $this->get_m_ve_length();
		}
		if (($select & $this->B_m_ve_width) != 0) {
			$return_array["m_ve_width"] = $this->get_m_ve_width();
		}
		if (($select & $this->B_m_ve_height) != 0) {
			$return_array["m_ve_height"] = $this->get_m_ve_height();
		}
		if (($select & $this->B_m_ve_weight) != 0) {
			$return_array["m_ve_weight"] = $this->get_m_ve_weight();
		}
		if (($select & $this->B_m_ve_special) != 0) {
			$return_array["m_ve_special_plate_no"] = $this->get_m_ve_special_plate_no();
			$return_array["m_ve_special_level"] = $this->get_m_ve_special_level();
			$return_array["m_ve_special_issuing_unit"] = $this->get_m_ve_special_issuing_unit();
			$return_array["m_ve_special_limit_datetime"] = $this->get_m_ve_special_limit_datetime();
			$return_array["m_ve_special_number"] = $this->get_m_ve_special_number();
			$return_array["m_ve_special_auth_status"] = $this->get_m_ve_special_auth_status();
			$return_array["m_ve_special_auth_datetime"] = $this->get_m_ve_special_auth_datetime();
			$return_array["m_ve_special_auth_m_id"] = $this->get_m_ve_special_auth_m_id();
			$return_array["m_ve_special_address"] = $this->get_m_ve_special_address();
		}
		if (($select & $this->B_m_ve_agreed_gov) != 0) {
			$return_array["m_ve_agreed_gov"] = $this->get_m_ve_agreed_gov();
		}
		if (($select & $this->B_m_ve_is_auto_pay_gov_parking_fee) != 0) {
			$return_array["m_ve_is_auto_pay_gov_parking_fee"] = $this->get_m_ve_is_auto_pay_gov_parking_fee();
		}
		if (($select & $this->B_m_ve_power_type) != 0) {
			$return_array["m_ve_power_type"] = $this->get_m_ve_power_type();
		}
		if (is_array($this->ves_array) && ($select & $this->B_ves_id) != 0)
			$return_array = array_merge($return_array, $this->ves_array);
		return $return_array;
	}

	public function set_m_ve_color_description($m_ve_color_description)
	{
		if ($m_ve_color_description !== "")
			$this->m_ve_color_description = $m_ve_color_description;
	}

	public function set_m_ve_plate_no($m_ve_plate_no, $is_special_plate_no = "0")
	{
		global $conn,$dbName;
		check_conn($conn,$dbName);
		rg_activity_log($conn, "", "新增車輛LOG", $m_ve_plate_no." ".$is_special_plate_no, "", "");
		if (!is_ValidPlateNo($m_ve_plate_no) && $is_special_plate_no == "0") {
			return false;
		}
		//之前是車號未輸入或者是新insert的時候為空白才可以變更
		if ($m_ve_plate_no !== "" && ($this->m_ve_plate_no == "車號未輸入" || $this->m_ve_plate_no == null || $this->m_ve_plate_no == "")) {
			if($m_ve_plate_no=="車號未輸入")
				$this->m_ve_plate_no = $m_ve_plate_no;
			else
				$this->m_ve_plate_no = strtoupper($m_ve_plate_no);

		}
		return true;
	}

	public function set_m_ve_can_planepk($m_ve_can_planepk)
	{
		if ($m_ve_can_planepk !== "")
			$this->m_ve_can_planepk = $m_ve_can_planepk;
	}

	public function set_m_ve_can_machinepk($m_ve_can_machinepk)
	{
		if ($m_ve_can_machinepk !== "")
			$this->m_ve_can_machinepk = $m_ve_can_machinepk;
	}

	public function set_m_ve_can_undergoundpk($m_ve_can_undergoundpk)
	{
		if ($m_ve_can_undergoundpk !== "")
			$this->m_ve_can_undergoundpk = $m_ve_can_undergoundpk;
	}

	public function set_m_ve_can_threedpk($m_ve_can_threedpk)
	{
		if ($m_ve_can_threedpk !== "")
			$this->m_ve_can_threedpk = $m_ve_can_threedpk;
	}

	public function set_m_ve_can_privatepk($m_ve_can_privatepk)
	{
		if ($m_ve_can_privatepk !== "")
			$this->m_ve_can_privatepk = $m_ve_can_privatepk;
	}

	public function set_m_ve_can_publicpk($m_ve_can_publicpk)
	{
		if ($m_ve_can_publicpk !== "")
			$this->m_ve_can_publicpk = $m_ve_can_publicpk;
	}

	public function set_m_ve_can_streetpk($m_ve_can_streetpk)
	{
		if ($m_ve_can_streetpk !== "")
			$this->m_ve_can_streetpk = $m_ve_can_streetpk;
	}

	public function set_m_ve_length($m_ve_length)
	{
		if ($m_ve_length !== "")
			$this->m_ve_length = $m_ve_length;
	}

	public function set_m_ve_width($m_ve_width)
	{
		if ($m_ve_width !== "")
			$this->m_ve_width = $m_ve_width;
	}

	public function set_m_ve_height($m_ve_height)
	{
		if ($m_ve_height !== "")
			$this->m_ve_height = $m_ve_height;
	}

	public function set_m_ve_weight($m_ve_weight)
	{
		if ($m_ve_weight !== "")
			$this->m_ve_weight = $m_ve_weight;
	}

	public function set_m_ve_special_plate_no($m_ve_special_plate_no)
	{
		if ($m_ve_special_plate_no !== "")
			$this->m_ve_weight = $m_ve_special_plate_no;
	}

	public function set_m_ve_special_level($m_ve_special_level)
	{
		//如果有從一班改為身障 如果非管理人 則預設審核中
		if ($this->m_ve_special_level != $m_ve_special_level && $m_ve_special_level != "0" && $this->is_admin == 0)
			$this->m_ve_special_auth_status = 1;
		if ($m_ve_special_level !== "")
			$this->m_ve_special_level = $m_ve_special_level;
	}

	public function set_m_ve_special_limit_datetime($m_ve_special_limit_datetime)
	{
		//如果有從一班改為身障 如果非管理人 則預設審核中
		if ($this->m_ve_special_limit_datetime != $m_ve_special_limit_datetime && $this->is_admin == 0)
			$this->m_ve_special_auth_status = 1;
		if ($m_ve_special_limit_datetime !== "")
			$this->m_ve_special_limit_datetime = $m_ve_special_limit_datetime;
	}

	public function set_m_ve_special_number($m_ve_special_number)
	{
		//如果有從一班改為身障 如果非管理人 則預設審核中
		if ($this->m_ve_special_number != $m_ve_special_number && $this->is_admin == 0)
			$this->m_ve_special_auth_status = 1;
		if ($m_ve_special_number !== "")
			$this->m_ve_special_number = $m_ve_special_number;
	}

	public function set_m_ve_special_issuing_units($m_ve_special_issuing_unit)
	{
		//如果有從一班改為身障 如果非管理人 則預設審核中
		if ($this->m_ve_special_issuing_unit != $m_ve_special_issuing_unit && $this->is_admin == 0)
			$this->m_ve_special_auth_status = 1;
		if ($m_ve_special_issuing_unit !== "")
			$this->m_ve_special_issuing_unit = $m_ve_special_issuing_unit;
	}

	public function set_m_ve_special_auth_statuss($m_ve_special_auth_status)
	{
		if ($this->m_ve_special_issuing_unit != $m_ve_special_auth_status && $this->is_admin == 0)
			$this->m_ve_special_auth_status = 1;
		if ($m_ve_special_auth_status !== "")
			$this->m_ve_special_auth_status = $m_ve_special_auth_status;
	}

	public function set_m_ve_special_address($m_ve_special_address)
	{
		if ($m_ve_special_address !== "")
			$this->m_ve_special_address = $m_ve_special_address;
	}

	public function set_m_ve_agreed_gov($m_ve_agreed_gov)
	{
		if ($m_ve_agreed_gov !== "")
			$this->m_ve_agreed_gov = $m_ve_agreed_gov;
	}

	public function set_m_ve_is_auto_pay_gov_parking_fee($m_ve_is_auto_pay_gov_parking_fee)
	{
		if ($m_ve_is_auto_pay_gov_parking_fee !== "")
			$this->m_ve_is_auto_pay_gov_parking_fee = $m_ve_is_auto_pay_gov_parking_fee;
	}

	public function set_m_ve_power_type($m_ve_power_type)
	{
		if ($m_ve_power_type !== "")
			$this->m_ve_power_type = $m_ve_power_type;
	}
}
?>