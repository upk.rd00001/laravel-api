<?php

use App\Http\Enum\CONST_M_PD_METHOD;

include_once("UPK_DiscountPlan_args.php");

class UPK_DiscountPlan
{
	private $dcp_sn;
	private $dcp_id;
	private $dcp_type;
	private $dcp_type_id;
	private $dcp_onsale_type;
	private $dcp_parameters;
	private $dcp_parameters_array;
	private $dcp_create_datetime;
	private $dcp_delete;
	public $B_dcp_sn = 1;
	public $B_dcp_id = 2;
	public $B_dcp_type = 4;
	public $B_dcp_type_id = 8;
	public $B_dcp_onsale_type = 16;
	public $B_dcp_parameters = 32;
	public $B_dcp_create_datetime = 64;
	public $B_dcp_delete = 128;
	public $B_ALL;
	public $c_dcp_args;
	public $dcp_result;

	private $tiger_check;

	public function __construct($dcp_id, $sql_logic = "", $c_dcp_args = null)
	{
		$this->B_ALL = $this->B_dcp_sn | $this->B_dcp_id | $this->B_dcp_type | $this->B_dcp_type_id | $this->B_dcp_onsale_type | $this->B_dcp_parameters | $this->B_dcp_create_datetime | $this->B_dcp_delete;
		$this->init_dcp_id($dcp_id, $sql_logic = "");
		$this->c_dcp_args = $c_dcp_args;
		$this->tiger_check = array();
	}

	public function init_dcp_id($dcp_id, $sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		/*********************************** Put your table name here ***********************************/
		$sql = "select * from tb_DiscountPlan where dcp_id = '" . $dcp_id . "' ";

		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "失敗", "description" => mysql_error()));
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->dcp_sn = $ans["dcp_sn"];
			$this->dcp_id = $ans["dcp_id"];
			$this->dcp_type = $ans["dcp_type"];
			$this->dcp_type_id = $ans["dcp_type_id"];
			$this->dcp_onsale_type = $ans["dcp_onsale_type"];
			$this->dcp_parameters = $ans["dcp_parameters"];
			$this->dcp_parameters_array = json_decode($ans["dcp_parameters"], true);
			$this->dcp_create_datetime = $ans["dcp_create_datetime"];
			$this->dcp_delete = $ans["dcp_delete"];
		}
	}

	public function is_available_discount_plan($m_id)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		if ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::FIRST_PAY_FREE_CASH_PERCENT) {
			if ($this->is_first_pay($m_id)) {
				return true;
			}
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::FIRST_PAY_GIVE_POINT_PERCENT) {
			if ($this->is_first_pay($m_id)) {
				return true;
			}
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::GOV_IN_X_MINUTES) {
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			$this_booking_log = $this->c_dcp_args->get_this_booking_log();
			if ($this_booking_log == null)
				return false;
			$this_booking_log->GetMemberVehicle();
			$this_vehicle_data = $this_booking_log->vehicle_data;
			$this_parking_space = $this->c_dcp_args->get_this_parking_space();
			$m_ve_id = $this->c_dcp_args->get_m_ve_id();
			$this_vehicle_data = $this->c_dcp_args->get_this_vehicle_data();
			$m_plots_id = $this->c_dcp_args->get_m_plots_id();
			$data = $this->c_dcp_args->get_price_package();
			$token = $this->c_dcp_args->get_token();
			$m_ve_special_level = $this->c_dcp_args->get_m_ve_special_level();
			if ($this->dcp_type_id == "新北市自助計費") {
				$this_parking_lot = new UPK_ParkingLot($m_plots_id);
				if ($this_parking_lot->get_m_plots_type() != "100" ||
					strpos($this_parking_lot->get_m_plots_address_city(), "新北市") === false)
					return false;
			}
			//不知道是什麼文字，反正就不要執行該費率除非有定義
			else {
				return false;
			}
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			$now_fare_kind = null;
			$can_use_this_dcp = true;//必須全符合條件才可以用，所以其中一個不符合就令他false
			$this_parking_space->GetFareKindParkingSpace();
			$pk_fare_kind_array = $this_parking_space->gov_farekind_parkingspaces;
			$DT_start_datetime = new DateTime($this_booking_log->get_m_bkl_start_date() . " " . $this_booking_log->get_m_bkl_start_time());
			foreach ($pk_fare_kind_array as $each_pk_fare_kind) {
				if ($each_pk_fare_kind->is_charge($DT_start_datetime) == true) {
					$now_fare_kind = $each_pk_fare_kind;
					break;
				}
			}
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			// 貨車及身障都沒有這個優惠
			$road_id = "";
			$sql_gov_parkingspace_area = "SELECT * FROM tb_Gov_ParkingSpace_Area WHERE m_plots_id='" . $this_parking_lot->get_m_plots_id() . "' ";
			$result_gov_parkingspace_area = mysql_query($sql_gov_parkingspace_area, $conn);
			if (!$result_gov_parkingspace_area) {
				$can_use_this_dcp = false;
			}
			elseif (mysql_num_rows($result_gov_parkingspace_area) == 1) {
				$ans_gov_parkingspace_area = mysql_fetch_assoc($result_gov_parkingspace_area);
				$road_id = $ans_gov_parkingspace_area["road_id"];
			}
			else {
				$can_use_this_dcp = false;
			}
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			if ($m_ve_id == "") {
				$can_use_this_dcp = false;
			}
			else {
				include_once("/../upkclass/ntpclass/NTPclass_WebService.php");
				$this_ntp_service = new NTPclass_WebService($m_id);
				if ($this_ntp_service->OwnerTicketCheck($this_vehicle_data->get_m_ve_plate_no(), $this_booking_log->get_DT_m_bkl_start_datetime(), $road_id, $this_booking_log->get_m_bkl_gov_id())) {
					array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0),"tiger_check_ntp_service"=>$this_ntp_service->get_tiger_check()));
					//當天沒有開過單才能夠優惠
					if (isTextContain($this_ntp_service->getLastResponse(), "FALSE")) {
						//可以用優惠
					}
					else {
						$can_use_this_dcp = false;
					}
					// 貨車及身障都沒有這個優惠
				}
			}
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			//今天沒有費率就不用優惠
			if ($now_fare_kind == null) {
				$can_use_this_dcp = false;
			}
			elseif ($this_parking_lot->get_m_plots_type() == "100" &&
				($now_fare_kind->get_fare_id() == "1" || $now_fare_kind->get_fare_id() == "3" || $now_fare_kind->get_fare_id() == "5")) {
				//只有計次才要判斷前X分鐘以分計費，超過X分鐘用原價計
				if ($now_fare_kind->get_fare_id() == "3") {
					$fare_parameters_array = $now_fare_kind->get_fare_parameters_array();
					array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
					foreach ($fare_parameters_array["part"] as $fare_para) {
						$fare_start_time = new DateTime($data[0]["start_date"] . " " . $fare_para["start_time"]);
						$fare_end_time = new DateTime($data[0]["end_date"] . " " . $fare_para["end_time"]);
						$DT_start_datetime = new DateTime($data[0]["start_date"] . " " . $data[0]["start_time"]);
						$DT_end_datetime = new DateTime($data[0]["end_date"] . " " . $data[0]["end_time"]);
						$logiced_time = DateTimeBoolean($DT_start_datetime, $DT_end_datetime, $fare_start_time, $fare_end_time, "AND");
						if (!isset($fare_para["timing_in_count"]))
							$fare_para["timing_in_count"] = array();//計次都預設給他要前X分鐘以分計費
						if (isset($fare_para["timing_in_count"])) {
							//一些預設參數
							if (!isset($fare_para["timing_in_count"]["price"]))
								$fare_para["timing_in_count"]["price"] = 15;//感覺還是比照用30分鐘的單位
							if (!isset($fare_para["timing_in_count"]["in_x_time"]))
								//	$fare_para["timing_in_count"]["in_x_time"]=120;//2小時內可用此邏輯，外面的min_unit應需大於他
								$fare_para["timing_in_count"]["in_x_time"] = ($fare_para["price"] / $fare_para["timing_in_count"]["price"]) * 30;//2小時內可用此邏輯，外面的min_unit應需大於他
							if (!isset($fare_para["timing_in_count"]["min_unit"]))
								$fare_para["timing_in_count"]["min_unit"] = 1;//裡面最小單位時間 預設1分鐘
							//外面的min_unit的是代表每次幾分鐘
							//所以他不是時間調整的單位

							//如果是在時間內就以分計費 $logiced_time["time"]單位是秒 in_x_time 要*60
							if ($fare_para["timing_in_count"]["in_x_time"] * 60 >= $logiced_time["time"]) {
								//要以分計費
							}
							else {
								//不以分計費，用原價計算不能有折抵
								$can_use_this_dcp = false;
							}
						}
					}
				}
			}
			else {
				$can_use_this_dcp = false;
			}
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			return $can_use_this_dcp;
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::APP_PAY_FEEDBACK) {
			return true;
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::INVOICE_GET_PARKING_POINT) {
			//可以設定使用天數
			//可以設定統編
			//可以設定停車點數X點
			if ($this->c_dcp_args->get_business_identifier() == $this->dcp_parameters_array["business_identifier"]) {
				if (isset($this->dcp_parameters_array["allow_days_ago"])) {
					$DT_invoice_datetime = new DateTime($this->c_dcp_args->get_invoice_datetime());
					$DT_now = new DateTime();
					$sub_sec = DateTimeSub($DT_now, $DT_invoice_datetime);
					$sub_day = $sub_sec / (60 * 60 * 24);
					if ($this->dcp_parameters_array["allow_days_ago"] >= $sub_day)
						return true;
					else {
						$ans = GetSystemCode("5040065", $language, $conn);
						$this->dcp_result = ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => sprintf($ans[2], $this->dcp_parameters_array["allow_days_ago"])));
						return false;
					}
				}
				return true;
			}
			//不知道是什麼文字，反正就不要執行該費率除非有定義
			else {
				return false;
			}
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::CAN_DISCOUNT_M_ID_ARRAY) {
			if (isset($this->dcp_parameters_array["cannot_discount_member"])) {
				foreach ($this->dcp_parameters_array["cannot_discount_member"] as $each_m_id) {
					if ($each_m_id == $m_id)
						return false;
				}
			}
			else return true;
			if (isset($this->dcp_parameters_array["can_discount_member"])) {
				if (count($this->dcp_parameters_array["can_discount_member"]) == 0) {
					return true;
				}
				foreach ($this->dcp_parameters_array["can_discount_member"] as $each_m_id) {
					if ($each_m_id == $m_id)
						return true;
				}
			}
			else return true;
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::PRICE_DURING_PERIOD_TIME) {
			$DT_start_datetime = new DateTime($this->c_dcp_args->get_start_date() . " " . $this->c_dcp_args->get_start_time());
			$DT_end_datetime = new DateTime($this->c_dcp_args->get_end_date() . " " . $this->c_dcp_args->get_end_time());
			$period_time = DateTimeSub($DT_end_datetime, $DT_start_datetime)/60; // second/60 = min
			if (isset($this->dcp_parameters_array["valid_after_time"])) {
				$DT_valid_after_time = new DateTime($this->dcp_parameters_array["valid_after_time"]);
				$DT_start_time = new DateTime($this->c_dcp_args->get_start_time());
				if($DT_valid_after_time<=$DT_start_time){
					//valid
				}
				else
					return false;
			}
			if ($this->dcp_parameters_array["min_during_period_time"] <= $period_time && $period_time <= $this->dcp_parameters_array["max_during_period_time"]) {
				return true;
			}
			else return false;

		}
		//不知道是什麼文字，反正就不要執行該費率除非有定義
		else {
			return false;
		}
		return false;
	}

	//產出折價後需付現金與
	//基本上是一個point去算出要折多少，未來如果要分停車點數的話就代參數吧
	//回傳就是一個point，根據type決定不同意義
	public function calc_discount_plan($m_id, $point, $args = [])
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		if ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::FIRST_PAY_FREE_CASH_PERCENT) {
			//還是先確認參數存在再優惠
			if (isset($this->dcp_parameters_array["first_pay_free_cash_percent"])) {
				//$discount_point = $point * (1 - $this->dcp_parameters_array["first_pay_free_cash_percent"]);
				//$discount = $this->dcp_parameters_array["first_pay_free_cash_percent"];
				$point = $point * $this->dcp_parameters_array["first_pay_free_cash_percent"];
			}
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::FIRST_PAY_GIVE_POINT_PERCENT) {
			if (isset($this->dcp_parameters_array["first_pay_give_point_percent"])) {
				$first_pay_give_point_percent = round($point * $this->dcp_parameters_array["first_pay_give_point_percent"]);

				include_once("/../pay_api/InsertPaidFunc.php");
				$tans = json_decode(InsertPaidFunc($m_id, CONST_M_PD_METHOD::FIRST_PAY_GIVE_PARKINT_POINT,
					"首次交易贈送停車點數",
					$first_pay_give_point_percent), true);
				$this->dcp_result = ReturnJsonModule($tans);
				if ($tans["result"] == 1) {
					PushTo($conn, $m_id, "完成於易停網的首次交易", "恭喜您獲得" . $first_pay_give_point_percent . "點停車點數", "");
				}
				else {
				}
			}
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::GOV_IN_X_MINUTES) {
			$original_total_point = $point;
			$this_parking_space = $this->c_dcp_args->get_this_parking_space();
			$m_ve_id = $this->c_dcp_args->get_m_ve_id();
			$this_vehicle_data = $this->c_dcp_args->get_this_vehicle_data();
			$this_booking_log = $this->c_dcp_args->get_this_booking_log();
			$m_plots_id = $this->c_dcp_args->get_m_plots_id();
			$data = $this->c_dcp_args->get_price_package();
			$token = $this->c_dcp_args->get_token();
			$m_ve_special_level = $this->c_dcp_args->get_m_ve_special_level();
			//新北市路邊停車的前30分鐘1元，因為車位格數與停車場數量很多所以決定使用plots_type=100來判斷路邊是要用這種費率計費
			//之前只有前X分鐘免費，但是沒有錢X分鐘幾元的設定
			//新北市要改優惠邏輯，前30分只算1元 改完的話要把這段刪掉
			$DT_start_datetime = new DateTime($data[0]["start_date"] . " " . $data[0]["start_time"]);
			$DT_end_datetime = new DateTime($data[0]["end_date"] . " " . $data[0]["end_time"]);
			$dcp_parameters = json_decode($this->dcp_parameters, true);
			if (isset($dcp_parameters["in_x_minutes"]))
				$in_x_minutes = $dcp_parameters["in_x_minutes"];
			else $in_x_minutes = 30;
			if (isset($dcp_parameters["price"]))
				$dcp_price = $dcp_parameters["price"];
			else $dcp_price = 30;
			$DT_start_datetime->modify("+" . $in_x_minutes . " min");
			//如果少於30分鐘 就令total_point=
			if ($DT_start_datetime >= $DT_end_datetime) {
				$point = 1;
			}
			else {
				$is_receive = false;
				$ignore_unstopable = false;
				/*
								//雖然original_total_point之前算過，不過不想跟外面計算扯到關係所以進來在算一次
								$tmp = json_decode(GovPriceCalculateFunc2("GOV PRICE CALCULATE", $token, $data, "", $m_ve_special_level, $ignore_unstopable, "", $is_receive), true);//先算一下原價
								if ($tmp["result"] == 1) {
									$original_total_point = $tmp["data"][0]["total"];
								}
								else {
									//可能該日無收費
									$original_total_point = 0;
								}*/
				//如果大於30分鐘就算一下30分鐘多少錢 然後有優惠的金額=無優惠的金額-30分鐘金額+30分鐘優惠
				$data[0]["end_date"] = $DT_start_datetime->format("Y-m-d");
				$data[0]["end_time"] = $DT_start_datetime->format("H:i:s");
				$tans = json_decode(GovPriceCalculateFunc2("GOV PRICE CALCULATE", $token, $data, "", $m_ve_special_level, $ignore_unstopable, $m_ve_id, $is_receive), true);
				$this->dcp_result = ReturnJsonModule($tans);
				if ($tans["result"] == 1)
					$point = $original_total_point - $tans["data"][0]["total"] + $dcp_price;
				else {
					$point = $original_total_point;
				}
			}
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::APP_PAY_FEEDBACK) {
			if (isset($this->dcp_parameters_array["use_app_pay_feedback"])) {
				$point = ceil($this->dcp_parameters_array["use_app_pay_feedback"] * $point);
				include_once("/../pay_api/InsertPaidFunc.php");
				$tans = json_decode(InsertPaidFunc($m_id, CONST_M_PD_METHOD::USE_APP_PAY_FEEDBACK,
					"使用易停網消費回饋" . ($this->dcp_parameters_array["use_app_pay_feedback"] * 100) . "%停車點數", $point), true);
				$this->dcp_result = ReturnJsonModule($tans);
				if ($tans["result"] == 1) {
					rg_activity_log($conn, $m_id, "贈送回饋金成功", "恭喜您獲得" . $point . "點停車點數", "", "");
					PushTo($conn, $m_id, "易停網消費回饋金", "恭喜您獲得" . $point . "點停車點數", "");
				}
				else {
				}
			}
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::INVOICE_GET_PARKING_POINT) {
			if (isset($this->dcp_parameters_array["invoice_get_parking_percent"])
				&& $this->dcp_parameters_array["invoice_get_parking_percent"] != 0) {
				$point = round($point * $this->dcp_parameters_array["invoice_get_parking_percent"]);
			}
			elseif (isset($this->dcp_parameters_array["invoice_get_parking_point"])) {
				$point = $this->dcp_parameters_array["invoice_get_parking_point"];
			}
			if ($point != 0) {
				if (isset($this->dcp_parameters_array["expire_date"]) && $this->dcp_parameters_array["expire_date"] != 0) {
					$now_p_expire_date = new DateTime();
					$now_p_expire_date->modify("+ " . $this->dcp_parameters_array["expire_date"] . " day");
					$expire_date = $now_p_expire_date->format("Y-m-d H:i:s");
				}
				else $expire_date = "";
				include_once("/../pay_api/InsertPaidFunc.php");
				$tans = json_decode(InsertPaidFunc($m_id, CONST_M_PD_METHOD::INVOICE_GET_PARKING_POINT,
					"發票號碼兌換點數活動-" . $this->c_dcp_args->get_invoice_number(), $point, $expire_date), true);

				$this->dcp_result = ReturnJsonModule($tans);
				if ($tans["result"] == 1) {
					rg_activity_log($conn, $m_id, "發票號碼兌換點數成功", $this->c_dcp_args->get_invoice_number(), json_encode($tans), "");
					PushTo($conn, $m_id, "發票號碼兌換點數", "恭喜您獲得" . $point . "點停車點數", "");
				}
				else {
				}
			}
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::CAN_DISCOUNT_M_ID_ARRAY) {
			if (isset($this->dcp_parameters_array["discount_percent"])) {
				$point = round($point * $this->dcp_parameters_array["discount_percent"]);
			}
		}
		elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::PRICE_DURING_PERIOD_TIME) {
			if (isset($this->dcp_parameters_array["price"])) {
				$point = $this->dcp_parameters_array["price"];
			}
		}
		return $point;
	}

	public function is_first_pay($m_id)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$is_first_pay = false;
		$sql_dpt = "SELECT m_dpt_id,m_bkl_id FROM tb_Member_Deposit WHERE m_id='" . $m_id . "' AND m_dpt_pay_datetime IS NOT NULL";
		$result_dpt = mysql_query($sql_dpt, $conn);
		if (!$result_dpt) {
			//當作沒有優惠
		}
		//之前未曾付過款就代表有優惠
		elseif (mysql_num_rows($result_dpt) == 0) {
			$is_first_pay = true;
		}
		//判斷 301 302 的第一次付款是假付款
		elseif (mysql_num_rows($result_dpt) == 1) {
			if ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::FIRST_PAY_GIVE_POINT_PERCENT) {
				$is_first_pay = true;
			}
			elseif ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::FIRST_PAY_FREE_CASH_PERCENT) {
				$ans_dpt = mysql_fetch_assoc($result_dpt);
				$m_bkl_id = $ans_dpt["m_bkl_id"];
				$this_booking_log = new UPK_BookingLog($m_bkl_id);
				if ($this_booking_log->get_m_bkl_plots_type() == "301" ||
					$this_booking_log->get_m_bkl_plots_type() == "302") {
					$is_first_pay = true;
				}
				elseif ($this_booking_log->get_m_bkl_plots_type() == "100") {
					$is_first_pay = false;
				}
				elseif ($this_booking_log->get_m_bkl_plots_type() == "300") {
					$is_first_pay = false;
				}
				elseif ($this_booking_log->get_m_bkl_plots_type() == "0") {
					$is_first_pay = false;
				}
			}
		}
		elseif (mysql_num_rows($result_dpt) == 2) {
			if ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::FIRST_PAY_FREE_CASH_PERCENT) {
				$is_first_pay = false;
			}
			if ($this->dcp_onsale_type == CONST_DCP_ONSALE_TYPE::FIRST_PAY_GIVE_POINT_PERCENT) {
				$ans_dpt = mysql_fetch_assoc($result_dpt);
				$m_bkl_id = $ans_dpt["m_bkl_id"];
				$this_booking_log = new UPK_BookingLog($m_bkl_id);
				if ($this_booking_log->get_m_bkl_plots_type() == "301" ||
					$this_booking_log->get_m_bkl_plots_type() == "302") {
					$is_first_pay = true;
				}
				elseif ($this_booking_log->get_m_bkl_plots_type() == "100") {
					$is_first_pay = false;
				}
				elseif ($this_booking_log->get_m_bkl_plots_type() == "300") {
					$is_first_pay = false;
				}
				elseif ($this_booking_log->get_m_bkl_plots_type() == "0") {
					$is_first_pay = false;
				}
			}
		}
		else {
			//超過1筆付款紀錄都是有付款
		}
		return $is_first_pay;
	}

	public function get_dcp_sn()
	{
		return $this->dcp_sn;
	}

	public function get_dcp_id()
	{
		return $this->dcp_id;
	}

	public function get_dcp_type()
	{
		return $this->dcp_type;
	}

	public function get_dcp_type_id()
	{
		return $this->dcp_type_id;
	}

	public function get_dcp_onsale_type()
	{
		return $this->dcp_onsale_type;
	}

	public function get_tiger_check()
	{
		return $this->tiger_check;
	}

	public function get_dcp_onsale_type_description()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT * FROM tb_DiscountPlan_OnsaleType WHERE dcp_onsale_type='" . $this->dcp_onsale_type . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return "優惠方案";
		}
		elseif (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$tmp_text = $ans["dcp_onsale_type_description"];
			if ($this->dcp_onsale_type == 1) {
				if (isset($this->dcp_parameters_array["first_pay_free_cash_percent"]))
					$tmp_text = $ans["dcp_onsale_type_description"] . $this->dcp_parameters_array["first_pay_free_cash_percent"] . "%現金";
			}
			return $tmp_text;
		}
		return "優惠方案";
	}

	public function get_dcp_parameters()
	{
		return $this->dcp_parameters;
	}

	public function get_dcp_parameters_array()
	{
		return $this->dcp_parameters_array;
	}

	public function get_dcp_create_datetime()
	{
		return $this->dcp_create_datetime;
	}

	public function get_dcp_delete()
	{
		return $this->dcp_delete;
	}

	public function get_array($select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_dcp_sn) != 0) {
			$return_array["dcp_sn"] = $this->get_dcp_sn();
		}
		if (($select & $this->B_dcp_id) != 0) {
			$return_array["dcp_id"] = $this->get_dcp_id();
		}
		if (($select & $this->B_dcp_type) != 0) {
			$return_array["dcp_type"] = $this->get_dcp_type();
		}
		if (($select & $this->B_dcp_type_id) != 0) {
			$return_array["dcp_type_id"] = $this->get_dcp_type_id();
		}
		if (($select & $this->B_dcp_onsale_type) != 0) {
			$return_array["dcp_onsale_type"] = $this->get_dcp_onsale_type();
			$return_array["dcp_onsale_type_description"] = $this->get_dcp_onsale_type_description();
		}
		if (($select & $this->B_dcp_parameters) != 0) {
			$return_array["dcp_parameters"] = $this->get_dcp_parameters();
		}
		if (($select & $this->B_dcp_create_datetime) != 0) {
			$return_array["dcp_create_datetime"] = $this->get_dcp_create_datetime();
		}
		if (($select & $this->B_dcp_delete) != 0) {
			$return_array["dcp_delete"] = $this->get_dcp_delete();
		}
		return $return_array;
	}
}

?>