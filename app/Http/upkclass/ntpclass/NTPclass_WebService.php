<?php
//namespace Space4car_API\upkclass\ntpclass\NTPclass_WebService;
use Space4car_API\upkclass\ntpclass\NTPclass_WebService as NTP;
include_once ("OwnerStartTicket.php");
include_once ("OwnerEndTicket.php");
include_once ("OwnerTicketCheck.php");
include_once("/../../fn_package.php");

class NTPclass_WebService
{
	private $wsdl_url;
	private $client;
	private $last_response;
	private $last_request;
	private $id;
	private $tiger_check;

	public function __construct($id="") {
		$this->tiger_check = array();
		$this->id=$id;
		$this->client=null;
	}

	public function init_webservice_connect() {
		if($this->client != null)
			return;
		global $conn, $dbName;
		check_conn($conn,$dbName);
		$G_NTP_SH_WEBSERVICE_URL = GetSystemParameter($conn, "Ntp_Sh_WebService_url");
		$G_NTP_SH_WEBSERVICE_URL_BAK = GetSystemParameter($conn, "Ntp_Sh_WebService_url_bak");
		rg_activity_log($conn,$this->id,"新北市WebService","連線開始",$this->wsdl_url,"");
		ini_set("soap.wsdl_cache_enabled", "0");
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		for($i=1;$i<7;$i++) {
			try {
				if($i<=3) {
					//前三圈用正式
					$this->wsdl_url=$G_NTP_SH_WEBSERVICE_URL;
					$this->client = new \SoapClient($this->wsdl_url);//要用uparking setting
				}
				else {
					//後三圈用備援
					$this->wsdl_url=$G_NTP_SH_WEBSERVICE_URL_BAK;
					$this->client = new \SoapClient($this->wsdl_url);//要用uparking setting
				}
				break;
			} catch (Exception $e) {
				//next
				rg_activity_log($conn,$this->id,"新北市WebService","連線失敗-".$i,$this->wsdl_url,"");
			}
		}
		rg_activity_log($conn,$this->id,"新北市WebService","連線結束",$this->wsdl_url,"");
		
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
	}
	public function OwnerStartTicket($m_bkl_gov_id, $road_id, $m_pk_number, $m_ve_plate_no, $date, $time
		, $DefaultType = "001", $CarType = "10", $TicketType = "1") {
		$this->init_webservice_connect();
		if($this->client==null)
			return false;
		global $conn,$dbName;
		check_conn($conn, $dbName);
		//if (IsDebug())//測試版不要傳到新北市停管處，免得發生問題
		//	return true;
		$cOwnerStartTicket = new NTP\OwnerStartTicket($m_bkl_gov_id, $road_id, $m_pk_number, $m_ve_plate_no, $date, $time
			, $DefaultType, $CarType, $TicketType);
		$this->last_request = print_r($cOwnerStartTicket, true);
		rg_activity_log($conn, $this->id, "新北市WebService", "OwnerStartTicket", $this->wsdl_url . " " . json_encode($cOwnerStartTicket), "");
		for ($i = 1; $i <= 3; $i++) {
			try {
				$response = $this->client->OwnerStartTicket($cOwnerStartTicket);
				break;
			} catch (Exception $e) {
				rg_activity_log($conn,$this->id,"新北市WebService","OwnerStartTicket 連線失敗-".$i,$this->wsdl_url." ".json_encode($cOwnerStartTicket),"");
			}
		}
		
		if(isset($response) && isset($response->OwnerStartTicketResult)) {
		}
		else return false;
			
		$this->last_response=$response->OwnerStartTicketResult;
		//include("mysql_connect");
		rg_activity_log($conn,$this->id,"新北市WebService","OwnerStartTicket",$this->wsdl_url." ".json_encode($cOwnerStartTicket),json_encode($response));
		if(\isTextContain($this->last_response,"OK") || \isTextContain($this->last_response,"REPEAT")){
			return true;
		}
		return false;//NOTFOUND
	}
	public function OwnerEndTicket($m_bkl_gov_id, $date, $time, $price) {
		$this->init_webservice_connect();
		if($this->client==null)
			return false;
		global $conn,$dbName;
		check_conn($conn,$dbName);
		//if(IsDebug())//測試版不要傳到新北市停管處，免得發生問題
		//	return true;
		$cOwnerEndTicket = new NTP\OwnerEndTicket($m_bkl_gov_id, $date, $time, $price);
		$this->last_request=print_r($cOwnerEndTicket,true);
		rg_activity_log($conn,$this->id,"新北市WebService","OwnerEndTicket",$this->wsdl_url." ".json_encode($cOwnerEndTicket),"");
		for ($i = 1; $i <= 3; $i++) {
			try {
				$response = $this->client->OwnerEndTicket ($cOwnerEndTicket);
				break;
			} catch (Exception $e) {
				rg_activity_log($conn,$this->id,"新北市WebService","OwnerEndTicket 連線失敗-".$i,$this->wsdl_url." ".json_encode($cOwnerEndTicket),"");
			}
		}
		$this->last_response=$response->OwnerEndTicketResult;
		rg_activity_log($conn,$this->id,"新北市WebService","OwnerEndTicket",$this->wsdl_url." ".json_encode($cOwnerEndTicket),json_encode($response));
		if(\isTextContain($this->last_response,"OK") || \isTextContain($this->last_response,"REPEAT")){
			return true;
		}
		return false;
	}
	public function OwnerTicketCheck($m_ve_plate_no,$DT_start_datetime,$road_id,$m_bkl_gov_id) {
		global $conn,$dbName;
		check_conn($conn,$dbName);
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		$sql="SELECT m_bkl_car_parked_roadid_today FROM tb_Member_ParkingSpace_Booking_Log WHERE m_bkl_gov_id='".$m_bkl_gov_id."' ";
		$result=mysql_query($sql,$conn);
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		if(!$result){
			//執行失敗，去問web service吧
		}
		elseif(mysql_num_rows($result)==1) {
			$ans=mysql_fetch_assoc($result);
			if($ans["m_bkl_car_parked_roadid_today"]=="0") {
				$this->last_response="OK_FALSE";
				return true;
			}
			elseif($ans["m_bkl_car_parked_roadid_today"]=="1") {
				$this->last_response="OK_TRUE";
				return true;
			}
			//m_bkl_car_parked_roadid_today = -1
			else {
				//沒問過，就先問web service吧
			}
		}
		$this->init_webservice_connect();
		if($response = $this->client==null)
			return false;
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		//if(IsDebug())//測試版不要傳到新北市停管處，免得發生問題
		//	return true;
		$cOwnerTicketCheck = new NTP\OwnerTicketCheck($m_ve_plate_no, $DT_start_datetime, $road_id, $m_bkl_gov_id);
		$this->last_request=print_r($cOwnerTicketCheck,true);
		rg_activity_log($conn,$this->id,"新北市WebService","OwnerTicketCheck",$this->wsdl_url." ".json_encode($cOwnerTicketCheck),"");
		for ($i = 1; $i <= 3; $i++) {
			try {
				$response = $this->client->OwnerTicketCheck ($cOwnerTicketCheck);
				break;
			} catch (Exception $e) {
				rg_activity_log($conn,$this->id,"新北市WebService","OwnerTicketCheck 連線失敗-".$i,$this->wsdl_url." ".json_encode($cOwnerTicketCheck),"");
			}
		}

		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		$this->last_response=$response->OwnerTicketCheckResult;
		rg_activity_log($conn,$this->id,"新北市WebService","OwnerTicketCheck",$this->wsdl_url." ".json_encode($cOwnerTicketCheck),json_encode($response));
		if(\isTextContain($this->last_response,"OK") || \isTextContain($this->last_response,"REPEAT")){

			if (isTextContain($this->last_response, "FALSE")) {
				$m_bkl_car_parked_roadid_today=0;
			}
			elseif (isTextContain($this->last_response, "TRUE")) {
				$m_bkl_car_parked_roadid_today=1;
			}

			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			$sql="UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_car_parked_roadid_today='".$m_bkl_car_parked_roadid_today."' WHERE m_bkl_gov_id='".$m_bkl_gov_id."' ";
			$result=mysql_query($sql,$conn);

			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			return true;
		}
		return false;
	}
	public function getLastRequest() {
		return $this->last_request;
	}
	public function getLastResponse() {
		return $this->last_response;
	}
	public function get_tiger_check() {
		return $this->tiger_check;
	}
}
/*class HelloWord
{
	public $sParkno;
	public $sCheck;
	public function __construct()
	{
		$this->sCheck="sCheck string";
		$this->sParkno="sParkno string";
	}
}*/
?>