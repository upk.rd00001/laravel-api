<?php
	header("Content-type: application/json; charset=utf-8");
	mb_internal_encoding('utf-8');
?>
<?php
	include_once("fn_package.php");
	$pure_data = file_get_contents('php://input');
	$data_back = json_decode($pure_data);
{
	if (isset($data_back->{"activity"})) {
		$activity = InputRegularExpression($data_back->{"activity"}, "text");
	}
	else $activity = "";
	if (isset($data_back->{"token"})) {
		$token = InputRegularExpression($data_back->{"token"}, "text");
	}
	else $token = "";
	if (isset($data_back->{"m_bkl_gov_id"})) {
		$m_bkl_gov_id = InputRegularExpression($data_back->{"m_bkl_gov_id"}, "text");
	}
	else $m_bkl_gov_id = "";
//date 格式範例 1071224
	if (isset($data_back->{"date"})) {
		$date = InputRegularExpression($data_back->{"date"}, "text");
	}
	else $date = "";
//time 格式範例 155900
	if (isset($data_back->{"time"})) {
		$time = InputRegularExpression($data_back->{"time"}, "text");
	}
	else $time = "";
	if (isset($data_back->{"price"})) {
		$price = InputRegularExpression($data_back->{"price"}, "text");
	}
	else $price = "";


	if ($activity == null || $token == null) {
		rg_activity_log($conn, "", "出場回報失敗", "必填欄位未填", $pure_data, "");
		$ans = GetSystemCode("3020036", $language, $conn);
		return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
	}
	else if ($activity != "OWNER TICKET CHECK") {
		rg_activity_log($conn, "", "出場回報失敗", "activity錯誤", $pure_data, "");
		$ans = GetSystemCode("3020035", $language, $conn);
		return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
	}
	$sql = "SELECT * FROM tb_Member WHERE m_token='" . $token . "'";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return json_encode(array("result" => 0, "title" => "離場回報失敗", "description" => mysql_error($conn)));
	}
	else if (mysql_num_rows($result) == 0) {
		#token失效
		rg_activity_log($conn, "", "出場回報失敗", "token失效", $pure_data, "");
		$ans = GetSystemCode("9", $language, $conn);
		return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
	}
	$ans = mysql_fetch_assoc($result);
	$id = $ans["m_id"];
	$language = $ans["m_language"];
	include_once("/../upkclass/ntpclass/NTPclass_WebService.php");
	$this_ntp_service = new NTPclass_WebService($id);
//新北市路邊停車是因為先付款之後出場後要上傳經過這裡要重算 所以已經出場的要重算　所以要用price
	if ($this_ntp_service->OwnerEndTicket($m_bkl_gov_id, $date, $time, $price)) {
		//可以結單
		return json_encode(array("result" => 1));
	}
	return json_encode(array("result" => 0));
}
?>