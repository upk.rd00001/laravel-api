<?php
/*class UPK_DiscountPan_args {
	private $args;
	public function set_args($key,$value) {
		$this->args[$key]=$value;
	}
	public function get_args() {
		return $this->args;
	}
}*/
class UPK_DiscountPan_PRICE_DURING_PERIOD_TIME {
	private $args;
	private $start_date;
	private $start_time;
	private $end_date;
	private $end_time;
	public function set_args($key,$value) {
		$this->args[$key]=$value;
	}
	public function set_start_date($start_date) {
		$this->start_date=$start_date;
	}
	public function set_start_time($start_time) {
		$this->start_time=$start_time;
	}
	public function set_end_date($end_date) {
		$this->end_date=$end_date;
	}
	public function set_end_time($end_time) {
		$this->end_time=$end_time;
	}
	public function get_start_date() {
		return $this->start_date;
	}
	public function get_start_time() {
		return $this->start_time;
	}
	public function get_end_date() {
		return $this->end_date;
	}
	public function get_end_time() {
		return $this->end_time;
	}
	public function get_args() {
		return $this->args;
	}
}
class UPK_DiscountPan_INVOICE_GET_PARKING_POINT {
	private $args;
	private $business_identifier;
	private $invoice_number;
	private $invoice_datetime;
	public function set_args($key,$value) {

	}
	public function set_business_identifier($business_identifier){
		$this->business_identifier=$business_identifier;
	}
	public function set_invoice_datetime($invoice_datetime){
		$this->invoice_datetime=$invoice_datetime;
	}
	public function get_business_identifier(){
		return $this->business_identifier;
	}
	public function set_invoice_number($invoice_number){
		$this->invoice_number=$invoice_number;
	}
	public function get_invoice_number(){
		return $this->invoice_number;
	}
	public function get_invoice_datetime(){
		return $this->invoice_datetime;
	}
	public function get_args() {
		return $this->args;
	}
}
class UPK_DiscountPan_GOV_IN_X_MINUTES {
	private $args;
	private $this_booking_log;
	private $data;
	private $token;
	private $this_vehicle_data;
	private $this_parking_space;
	private $m_ve_id;
	private $m_plots_id;
	private $dcp_onsale_type;
	public function set_args($key,$value) {
		$this->args[$key]=$value;
	}
	public function set_this_booking_log($this_booking_log) {
		$this->this_booking_log=$this_booking_log;
	}
	public function set_this_parking_space($this_parking_space) {
		$this->this_parking_space=$this_parking_space;
	}
	public function set_price_package($price_package) {
		$this->data=$price_package;
	}
	public function set_token($token) {
		$this->token=$token;
	}
	public function set_this_vehicle_data($this_vehicle_data) {
		$this->this_vehicle_data=$this_vehicle_data;
		//$this->m_ve_id=$this_vehicle_data->get_m_ve_id();
	}
	public function set_m_ve_id($m_ve_id) {
		$this->m_ve_id=$m_ve_id;
	}
	public function set_m_plots_id($m_plots_id) {
		$this->m_plots_id=$m_plots_id;
	}
	public function get_this_booking_log() {
		return $this->this_booking_log;
	}
	public function get_price_package() {
		return $this->data;
	}
	public function get_token() {
		return $this->token;
	}
	public function get_this_vehicle_data() {
		return $this->this_vehicle_data;
	}
	public function get_this_parking_space() {
		return $this->this_parking_space;
	}
	public function get_m_ve_id() {
		return $this->m_ve_id;
	}
	public function get_m_ve_special_level() {
		return $this->this_vehicle_data->get_m_ve_special_level();
	}
	public function get_m_plots_id() {
		return $this->m_plots_id;
	}
	public function _construct()	{
		$this->dcp_onsale_type=CONST_DCP_ONSALE_TYPE::GOV_IN_X_MINUTES;
	}
	public function get_args() {
		return $this->args;
	}
}