<?php

class UPK_File_Logs
{
	private $file_logs;

	/** 根據ID決定是否回傳預設圖片
	 * @param $m_id
	 * @return bool
	 */
	static public function isDisplayDefaultId($m_id){
		if($m_id == "UMID0000023481" || //tester 03 0999999903
			$m_id == "UMID0000023480" || //tester 02 0999999902
			$m_id == "UMID0000023477" || //tester 01 0999999901
			$m_id == "UMID0000023460") {
			return true;
		}
		else return false;
	}
	public function __construct($fl_type, $fl_type_id,$m_id="")
	{
		$this->file_logs = array();
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		if(UPK_File_Logs::isDisplayDefaultId($m_id)) {
			$sql = "SELECT fl_id FROM tb_File_Log WHERE fl_delete=0  AND fl_type='" . CONST_FL_TYPE::DISPLAY_DEFAULT . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				rg_activity_log($conn, "", "取得照片失敗", "系統錯誤", "", "");
				$ans = GetSystemCode("20013", $language, $conn);
				return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
			while ($ans = mysql_fetch_assoc($result)) {
				$this_file_log = new UPK_FileLog($ans["fl_id"]);
				array_push($this->file_logs, $this_file_log);
			}
		}
		else {
			$sql = "SELECT fl_id FROM tb_File_Log WHERE fl_delete=0  AND fl_type='".$fl_type."'  AND fl_type_id='".$fl_type_id."' ";
			$result = mysql_query($sql, $conn);
			if (! $result) {
				rg_activity_log($conn, "", "取得照片失敗", "系統錯誤", "", "");
				$ans = GetSystemCode("20013", $language, $conn);
				return json_encode([
					"result" => 0,
					"systemCode" => $ans[0],
					"title" => $ans[1],
					"description" => $ans[2]
				]);
			}
			while ($ans = mysql_fetch_assoc($result)) {
				$this_file_log = new UPK_FileLog($ans["fl_id"]);
				array_push($this->file_logs, $this_file_log);
			}
		}
	}

	public function get_array()
	{
		$ans_array = array();
		foreach ($this->file_logs as $each_file_log) {
			array_push($ans_array, $each_file_log->get_array());
		}
		return $ans_array;
	}

	public function get_result()
	{
		$ans_array = array();
		foreach ($this->file_logs as $each_file_log) {
			array_push($each_file_log, $each_file_log->get_array);
		}
		return array("result" => 1, "data" => $ans_array);
	}

} ?>