<?php
class UPK_Invoice
{
	private $inv_sn;
	private $inv_id;
	private $m_dpt_id;
	private $inv_number;
	private $inv_amount;
	private $inv_enable;
	private $inv_arg;
	private $inv_create_datetime;
	private $inv_delete;
	private $invoice_desc;
	public $B_inv_sn = 1;
	public $B_inv_id = 2;
	public $B_m_dpt_id = 4;
	public $B_inv_number = 8;
	public $B_inv_amount = 16;
	public $B_inv_enable = 32;
	public $B_inv_arg = 64;
	public $B_inv_create_datetime = 128;
	public $B_inv_delete = 256;
	public $B_invoice_desc = 512;
	public $B_ALL;

	public function __construct($inv_id, $sql_logic = "")
	{
		$this->B_ALL = $this->B_inv_sn | $this->B_inv_id | $this->B_m_dpt_id | $this->B_inv_number | $this->B_inv_amount | $this->B_inv_enable | $this->B_inv_arg | $this->B_inv_create_datetime | $this->B_inv_delete;
		$this->init_inv_id($inv_id, $sql_logic = "");
		$this->invoice_desc = "";
	}

	public function init_inv_id($inv_id, $sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		/*********************************** Put your table name here ***********************************/
		$sql = "select * from tb_Invoice where inv_id = '" . $inv_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "失敗", "description" => mysql_error()));
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->inv_sn = $ans["inv_sn"];
			$this->inv_id = $ans["inv_id"];
			$this->m_dpt_id = $ans["m_dpt_id"];
			$this->inv_number = $ans["inv_number"];
			$this->inv_amount = $ans["inv_amount"];
			$this->inv_enable = $ans["inv_enable"];
			$this->inv_arg = $ans["inv_arg"];
			$this->inv_create_datetime = $ans["inv_create_datetime"];
			$this->inv_delete = $ans["inv_delete"];
		}
	}

	public function get_inv_sn()
	{
		return $this->inv_sn;
	}

	public function get_inv_id()
	{
		return $this->inv_id;
	}

	public function get_m_dpt_id()
	{
		return $this->m_dpt_id;
	}

	public function get_inv_number()
	{
		return $this->inv_number;
	}

	public function get_inv_amount()
	{
		return $this->inv_amount;
	}

	public function get_inv_enable()
	{
		return $this->inv_enable;
	}

	public function get_inv_arg()
	{
		return $this->inv_arg;
	}

	public function get_inv_create_datetime()
	{
		return $this->inv_create_datetime;
	}

	public function get_inv_delete()
	{
		return $this->inv_delete;
	}
	public function get_invoice_desc()
	{
		return $this->invoice_desc;
	}

	public function get_array($select = "")
	{
		if ($select == "") $select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_inv_sn) != 0) {
			$return_array["inv_sn"] = $this->get_inv_sn();
		}
		if (($select & $this->B_inv_id) != 0) {
			$return_array["inv_id"] = $this->get_inv_id();
		}
		if (($select & $this->B_m_dpt_id) != 0) {
			$return_array["m_dpt_id"] = $this->get_m_dpt_id();
		}
		if (($select & $this->B_inv_number) != 0) {
			$return_array["inv_number"] = $this->get_inv_number();
		}
		if (($select & $this->B_inv_amount) != 0) {
			$return_array["inv_amount"] = $this->get_inv_amount();
		}
		if (($select & $this->B_inv_enable) != 0) {
			$return_array["inv_enable"] = $this->get_inv_enable();
		}
		if (($select & $this->B_inv_arg) != 0) {
			if($this->inv_sn<16114) {
				global $conn,$dbName;
				check_conn($conn,$dbName);
				$sql="SELECT invoice_arg FROM tb_Member_Deposit WHERE m_dpt_id='".$this->m_dpt_id."' ";
				$result = mysql_query($sql,$conn);
				$ans= mysql_fetch_assoc($result);
				$return_array["inv_arg"] = json_decode($ans["invoice_arg"]);
			}
			else
				$return_array["inv_arg"] = json_decode($this->get_inv_arg());
		}
		if (($select & $this->B_inv_create_datetime) != 0) {
			$return_array["inv_create_datetime"] = $this->get_inv_create_datetime();
		}
		if (($select & $this->B_inv_delete) != 0) {
			$return_array["inv_delete"] = $this->get_inv_delete();
		}
		if (($select & $this->B_invoice_desc) != 0) {
			$inv_arg_array = json_decode($this->get_inv_arg(),true);
			$CustomerIdentifier = "";
			if(isset($inv_arg_array["CustomerIdentifier"]))
				$CustomerIdentifier = $inv_arg_array["CustomerIdentifier"];
			$Donation = "";
			if(isset($inv_arg_array["Donation"]))
				$Donation = $inv_arg_array["Donation"];
			$LoveCode = "";
			if(isset($inv_arg_array["LoveCode"]))
				$LoveCode = $inv_arg_array["LoveCode"];
			$extra_desc = "";
			if($Donation==1) { //2 = 無捐贈
				$extra_desc = "捐贈";
				if($LoveCode!="") {
					$extra_desc .= "(".$LoveCode.")";
				}
			}
			if($CustomerIdentifier!="") {
				$extra_desc = "統一編號(".$CustomerIdentifier.")";
			}
			$this->invoice_desc = $this->inv_number.$extra_desc;
			$return_array["invoice_desc"] = $this->get_invoice_desc();
		}
		return $return_array;
	}
}
?>