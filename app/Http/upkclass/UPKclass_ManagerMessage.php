<?php

class UPK_Manager_Message
{
	private $mm_sn;
	private $mm_id;
	private $mm_type;
	private $mm_type_id;
	private $mm_msg_type;
	private $mm_create_datetime;
	private $m_id;
	private $mm_title;
	private $mm_description;
	public $B_mm_sn = 1;
	public $B_mm_id = 2;
	public $B_mm_type = 4;
	public $B_mm_type_id = 8;
	public $B_mm_msg_type = 16;
	public $B_mm_create_datetime = 32;
	public $B_m_id = 64;
	public $B_mm_title = 128;
	public $B_mm_description = 256;
	public $B_ALL;

	public function __construct($mm_id, $sql_logic = "")
	{
		$this->B_ALL = $this->B_mm_sn | $this->B_mm_id | $this->B_mm_type | $this->B_mm_type_id | $this->B_mm_msg_type | $this->B_mm_create_datetime | $this->B_m_id | $this->B_mm_title | $this->B_mm_description;
		$this->init_mm_id($mm_id, $sql_logic = "");
	}

	public function init_mm_id($mm_id, $sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		/*********************************** Put your table name here ***********************************/
		$sql = "select * from tb_Manager_Message where mm_id = '" . $mm_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "失敗", "description" => mysql_error()));
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->mm_sn = $ans["mm_sn"];
			$this->mm_id = $ans["mm_id"];
			$this->mm_type = $ans["mm_type"];
			$this->mm_type_id = $ans["mm_type_id"];
			$this->mm_msg_type = $ans["mm_msg_type"];
			$this->mm_create_datetime = $ans["mm_create_datetime"];
			$this->m_id = $ans["m_id"];
			$this->mm_title = $ans["mm_title"];
			$this->mm_description = $ans["mm_description"];
		}
	}

	public function get_mm_sn()
	{
		return $this->mm_sn;
	}

	public function get_mm_id()
	{
		return $this->mm_id;
	}

	public function get_mm_type()
	{
		return $this->mm_type;
	}

	public function get_mm_type_id()
	{
		return $this->mm_type_id;
	}

	public function get_mm_msg_type()
	{
		return $this->mm_msg_type;
	}

	public function get_mm_create_datetime()
	{
		return $this->mm_create_datetime;
	}

	public function get_m_id()
	{
		return $this->m_id;
	}

	public function get_mm_title()
	{
		return $this->mm_title;
	}

	public function get_mm_description()
	{
		return $this->mm_description;
	}

	public function get_array($select = "")
	{
		if ($select == "") $select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_mm_sn) != 0) {
			$return_array["mm_sn"] = $this->get_mm_sn();
		}
		if (($select & $this->B_mm_id) != 0) {
			$return_array["mm_id"] = $this->get_mm_id();
		}
		if (($select & $this->B_mm_type) != 0) {
			$return_array["mm_type"] = $this->get_mm_type();
		}
		if (($select & $this->B_mm_type_id) != 0) {
			$return_array["mm_type_id"] = $this->get_mm_type_id();
		}
		if (($select & $this->B_mm_msg_type) != 0) {
			$return_array["mm_msg_type"] = $this->get_mm_msg_type();
		}
		if (($select & $this->B_mm_create_datetime) != 0) {
			$return_array["mm_create_datetime"] = $this->get_mm_create_datetime();
		}
		if (($select & $this->B_m_id) != 0) {
			$return_array["m_id"] = $this->get_m_id();
		}
		if (($select & $this->B_mm_title) != 0) {
			$return_array["mm_title"] = $this->get_mm_title();
		}
		if (($select & $this->B_mm_description) != 0) {
			$return_array["mm_description"] = $this->get_mm_description();
		}
		return $return_array;
	}
}

?>