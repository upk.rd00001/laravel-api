<?php
class UPK_GetAuthorizedStorePaidData
{	//outputs
	private $type;
	private $id;
	private $as_name;
	private $datetime;
	private $fee;
	private $apply_fee;
	private $apply_datetime;
	private $remark;
	private $handling_fee;
	private $transfer_fee;
	private $status;
	private $description;
	private $last_edit_datetime;
	private $last_edit_m_id;
	private $m_id;
	private $as_id;
	private $paid_back;

	//outputs binary
	public $B_ALL;

	public $B_AS_NAME=1;
	public $B_DATETIME=2;
	public $B_FEE=4;
	public $B_REMARK=8;
	public $B_S4C_HANDLING_FEE=16;
	public $B_TYPE=32;
	public $B_TAKEOUT_LOG=64;
	public $B_APPLY_FEE = 128;
	public $B_ID = 256;//takeout_id or as_paid_id
	public $B_STATUS = 512;
	public $B_DESCRIPTION = 1024;
	public $B_LAST_EDIT = 2048;
	public $B_APPLY_DATETIME = 4096;
	public $B_M_ID = 8192;//申請人ID 或收款人ID  不是as_paid裡面的付款人的ID
	public $B_AS_ID = 16384;//特約店ID
	public $B_TRANSFER_FEE = 32768;//轉帳手續費
	public $B_INVOICE = 65536;//發票訊息
	public $B_PAID_BACK = 131072;//回饋點數

	private $as_paid_takeout_logs;
	private $invoice_data;

	public function __construct()
	{
			$this->as_paid_takeout_logs=array();
			$this->invoice_data=array();
	}

	public function init_as_paid_takeout_log($m_aspd_take_out_log_sn)
	{
		//tb_Member_As_Paid_Take_Out_Log
		global $conn, $dbName;
		check_conn($conn, $dbName);

		$sql = "SELECT * FROM tb_Member_As_Paid_Take_Out_Log LEFT JOIN tb_Member_Authorized_Store ON tb_Member_As_Paid_Take_Out_Log.m_as_id = tb_Member_Authorized_Store.m_as_id WHERE m_aspd_takeout_log_sn = '" . $m_aspd_take_out_log_sn . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return false;
		}
		elseif (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->type = 3;
			$this->id = $ans["m_aspd_takeout_id"];
			$this->as_id = $ans ["m_as_id"];
			$this->as_name = $ans ["m_as_name"];
			$this->m_id = $ans ["m_id"];
			$this->datetime = $ans ["m_aspd_takeout_create_datetime"];
			$this->fee = $ans ["m_aspd_takeout_apply_money"];
			$this->apply_fee = $ans ["m_aspd_takeout_apply_money"];
			$this->apply_datetime = $ans ["m_aspd_takeout_datetime"];
			$this->remark = array();
			$this->handling_fee = $ans ["m_aspd_takeout_handling_fee"];
			$this->transfer_fee = $ans ["m_aspd_takeout_transfer_fee"];
			$this->status = $ans ["m_aspd_takeout_status"];
			$this->description = $ans ["m_aspd_takeout_description"];
			$this->last_edit_datetime = $ans ["m_aspd_takeout_last_edit_datetime"];
			$this->last_edit_m_id = $ans ["m_aspd_takeout_last_edit_m_id"];
			return true;
		}
		return false;
	}
	public function init_as_paid_takeout($m_aspd_takeout_id)
	{
		//tb_Member_As_Paid_Take_Out
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$this->id = $m_aspd_takeout_id;
		//$sql = "SELECT * FROM tb_Member_As_Paid_Take_Out LEFT JOIN tb_Member_Authorized_Store ON tb_Member_As_Paid_Take_Out.m_as_id = tb_Member_Authorized_Store.m_as_id WHERE m_aspd_takeout_id = '" . $m_aspd_take_out . "' ";
		$sql = "SELECT * FROM tb_Member_As_Paid_Take_Out LEFT JOIN tb_Member_Authorized_Store ON tb_Member_As_Paid_Take_Out.m_as_id = tb_Member_Authorized_Store.m_as_id WHERE m_aspd_takeout_id = '" . $m_aspd_takeout_id . "' ";

		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "失敗", "description" => mysql_error()));
		}
		elseif(mysql_num_rows($result)==1) {
			$ans = mysql_fetch_assoc($result);
			$this->type = 2;
			$this->id = $ans["m_aspd_takeout_id"];
			$this->m_id = $ans ["m_id"];
			$this->as_id = $ans ["m_as_id"];
			$this->as_name = $ans ["m_as_name"];
			$this->datetime = $ans ["m_aspd_takeout_create_datetime"];
			$this->fee = $ans ["m_aspd_takeout_apply_money"];
			$this->apply_fee = $ans ["m_aspd_takeout_money"];
			$this->apply_datetime = $ans ["m_aspd_takeout_datetime"];
			$this->remark = array();
			$this->handling_fee = $ans ["m_aspd_takeout_handling_fee"];
			$this->transfer_fee = $ans ["m_aspd_takeout_transfer_fee"];
			$this->status = $ans ["m_aspd_takeout_status"];
			$this->description = $ans ["m_aspd_takeout_description"];
			$this->last_edit_datetime = $ans ["m_aspd_takeout_last_edit_datetime"];
			$this->last_edit_m_id = $ans ["m_aspd_takeout_last_edit_m_id"];
			$sql_inv = "SELECT inv_id FROM tb_Invoice WHERE m_dpt_id = '".$m_aspd_takeout_id."' AND inv_delete='0' AND inv_enable='1' ";
			$result_inv = mysql_query($sql_inv, $conn);
			while($ans_inv= mysql_fetch_assoc($result_inv)) {
				$each_invoice_data = new UPK_Invoice($ans_inv["inv_id"]);
				array_push($this->invoice_data, $each_invoice_data->get_array($each_invoice_data->B_inv_number | $each_invoice_data->B_m_dpt_id | $each_invoice_data->B_inv_create_datetime | $each_invoice_data->B_inv_id | $each_invoice_data->B_inv_amount | $each_invoice_data->B_invoice_desc));
			}
			return true;
		}
		return false;
	}
	public function init_as_paid($m_aspd_id)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);

		$this->id = $m_aspd_id;
		$sql = "SELECT tb_Member_As_Paid.*,tb_Member_Authorized_Store.m_as_name as m_as_name FROM tb_Member_As_Paid LEFT JOIN tb_Member_Authorized_Store ON tb_Member_As_Paid.m_as_id = tb_Member_Authorized_Store.m_as_id WHERE m_aspd_id = '" . $m_aspd_id . "' ";

		$result = mysql_query($sql, $conn);
		if (!$result) {
			return false;
		}
		elseif(mysql_num_rows($result)==1) {
			$ans = mysql_fetch_assoc($result);
			$this->type = 1;
			$this->id = $ans["m_aspd_id"];
			$this->as_id = $ans ["m_as_id"];
			$this->m_id = $ans ["m_id"]; //2020-10-27 因後台顯示要改成付款人 (原為收款人)
			$this->as_name = $ans ["m_as_name"];
			$this->datetime = $ans ["m_aspd_pay_datetime"];
			$this->transfer_fee = 0;
			$this->fee = $ans ["m_aspd_fee"];
			$this->apply_fee = $ans ["m_aspd_as_fee"];
			$this->paid_back = $ans ["m_aspd_paid_back"];
			$this->remark = array();
			$sql_remark = "SELECT * FROM tb_Remark WHERE rmk_type_id = '" . $this->id . "' AND rmk_delete='0' ";
			$result_remark  = mysql_query($sql_remark , $conn);
			if (!$result_remark) {
				return json_encode(array("result" => 0, "title" => "失敗", "description" => mysql_error()));
			}
			while($ans_remark  = mysql_fetch_assoc($result_remark)) {
				$rmk_id = $ans_remark["rmk_id"];
				$this_remark = new UPK_Remark($rmk_id);
				array_push($this->remark, $this_remark);
			}
			$this->handling_fee = $ans ["m_aspd_space4car_handling_fee"];
			$this->status = -1;

			$sql_inv = "SELECT inv_id FROM tb_Invoice WHERE m_dpt_id = '".$m_aspd_id."' AND inv_delete='0' AND inv_enable='1' ";
			$result_inv = mysql_query($sql_inv, $conn);
			while($ans_inv= mysql_fetch_assoc($result_inv)) {
				$each_invoice_data = new UPK_Invoice($ans_inv["inv_id"]);
				array_push($this->invoice_data, $each_invoice_data->get_array($each_invoice_data->B_inv_number | $each_invoice_data->B_m_dpt_id | $each_invoice_data->B_inv_create_datetime | $each_invoice_data->B_inv_id | $each_invoice_data->B_inv_amount | $each_invoice_data->B_invoice_desc));
			}
			return true;
		}
		return false;
	}

	public function get_type() {
		return (int)$this->type;
	}
	public function get_id() {
		return $this->id;
	}
	public function get_m_id() {
		return $this->m_id;
	}
	public function get_as_id() {
		return $this->as_id;
	}
	public function get_as_name() {
		return $this->as_name;
	}
	public function get_datetime() {
		return $this->datetime;
	}
	public function get_fee() {
		return (int)$this->fee;
	}
	public function get_apply_fee() {
		return (float)$this->apply_fee;
	}
	public function get_transfer_fee() {
		return (int)$this->transfer_fee;
	}
	public function get_apply_datetime() {
		return $this->apply_datetime;
	}
	public function get_paid_back() {
		return (int)$this->paid_back;
	}
	public function get_remark()
	{
		$tmp_arr = array();
		foreach ($this->remark as $each_remark) {
			$selector=$each_remark->B_rmk_title | $each_remark->B_rmk_description | $each_remark->B_rmk_create_datetime;
			array_push($tmp_arr, $each_remark->get_array($selector));
		}
		return $tmp_arr;
	}
	public function get_handling_fee() {
		return (float)$this->handling_fee;
	}
	public function get_status() {
		return (int)$this->status;
	}
	public function get_description() {
		return $this->description;
	}
	public function get_last_edit_datetime() {
		return $this->last_edit_datetime;
	}
	public function get_last_edit_m_id() {
		return $this->last_edit_m_id;
	}
	public function get_invoice_data() {
		return $this->invoice_data;
	}
	public function get_as_paid_takeout_log() {
		if($this->type == 2) {
			global $conn, $dbName;
			check_conn($conn, $dbName);
			$sql = "SELECT * FROM tb_Member_As_Paid_Take_Out_Log WHERE m_aspd_takeout_id = '" . $this->id . "'  AND m_aspd_takeout_delete=0 ORDER BY m_aspd_takeout_log_sn DESC ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return false;
			}
			$is_first=true;
			while($ans = mysql_fetch_assoc($result)) {
				if($is_first) {
					$is_first = false;
					continue;
				}
				$m_aspd_takeout_log_sn = $ans["m_aspd_takeout_log_sn"];
				$this_as_paid_takeout_log = new UPK_GetAuthorizedStorePaidData();
				$this_as_paid_takeout_log->init_as_paid_takeout_log($m_aspd_takeout_log_sn);
				array_push($this->as_paid_takeout_logs,$this_as_paid_takeout_log);
			}
			return true;
		}
		return false;
	}
	public function get_takeout_log($select = "") {
		if ($select == "")
			$select = $this->B_ALL;
		if($this->get_as_paid_takeout_log()){
			//成功繼續做
		}
		else {
			//false
		}
		$return_array = array();
		foreach($this->as_paid_takeout_logs as $each_as_takeout_log) {
			array_push($return_array,$each_as_takeout_log->get_array($select));
		}
		return $return_array;
	}
	public function get_array($select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_TYPE) != 0) {
			$return_array["type"] = $this->get_type();
		}
		if (($select & $this->B_ID) != 0) {
			$return_array["id"] = $this->get_id();
		}
		if (($select & $this->B_M_ID) != 0) {
			$return_array["m_id"] = $this->get_m_id();
		}
		if (($select & $this->B_AS_ID) != 0) {
			$return_array["as_id"] = $this->get_as_id();
		}
		if (($select & $this->B_AS_NAME) != 0) {
			$return_array["as_name"] = $this->get_as_name();
		}
		if (($select & $this->B_DATETIME) != 0) {
			$return_array["datetime"] = $this->get_datetime();
		}
		if (($select & $this->B_FEE) != 0) {
			$return_array["fee"] = $this->get_fee();
		}
		if (($select & $this->B_APPLY_FEE) != 0) {
			$return_array["apply_fee"] = $this->get_apply_fee();
		}
		if (($select & $this->B_APPLY_DATETIME) != 0) {
			$return_array["apply_datetime"] = $this->get_apply_datetime();
		}
		if (($select & $this->B_REMARK) != 0) {
			$return_array["remark"] = $this->get_remark();
		}
		if (($select & $this->B_S4C_HANDLING_FEE) != 0) {
			$return_array["handling_fee"] = $this->get_handling_fee();
		}
		if (($select & $this->B_TRANSFER_FEE) != 0) {
			$return_array["transfer_fee"] = $this->get_transfer_fee();
		}
		if (($select & $this->B_TAKEOUT_LOG) != 0) {
			$return_array["takeout_log"] = $this->get_takeout_log($select);
		}
		if (($select & $this->B_STATUS) != 0) {
			$return_array["status"] = $this->get_status();
		}
		if (($select & $this->B_DESCRIPTION) != 0) {
			$return_array["description"] = $this->get_description();
		}
		if (($select & $this->B_LAST_EDIT) != 0) {
			$return_array["last_edit_datetime"] = $this->get_last_edit_datetime();
			$return_array["last_edit_m_id"] = $this->get_last_edit_m_id();
		}
		if (($select & $this->B_INVOICE) != 0) {
			$return_array["invoice_data"] = $this->get_invoice_data();
		}
		if (($select & $this->B_PAID_BACK) != 0) {
			$return_array["paid_back"] = $this->get_paid_back();
		}
		return $return_array;
	}
}

?>