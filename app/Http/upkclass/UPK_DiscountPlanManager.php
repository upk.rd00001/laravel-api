<?php
include_once ("UPK_DiscountPlan.php");
include_once ("UPK_DiscountPlan_args.php");
class UPK_DiscountPlanManager
{
	private $c_dcp_array; //目前一組但要保留彈性
	private $point;
	private $parking_point;
	public $c_dcp_gov;
	public $c_dcp_invoice;
	public $dcp_result;

	private $tiger_check;
	public function __construct()
	{
		$this->c_dcp_array = array();
		$this->point = 0;
		$this->parking_point = 0;
		$this->tiger_check = array();
		$this->c_dcp_gov = new UPK_DiscountPan_GOV_IN_X_MINUTES();
		$this->c_dcp_inv = new UPK_DiscountPan_INVOICE_GET_PARKING_POINT();
		$this->dcp_result = ReturnJsonModule(array("result" => 0, "title" => "", "description" => ""));
	}
	function select_discount_plan($m_id,$dcp_onsale_type,$dcp_type,$dcp_type_id,$dcp_args=null) {
		$this->tiger_check = array();
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$dcp_m_plots_id_sql="";
		$dcp_parameter_sql="";
		$dcp_type_id_sql=" AND dcp_type_id='".$dcp_type_id."' ";
		if($dcp_type==CONST_DCP_TYPE::PARKING_SPACE) {
			$m_pk_id=$dcp_type_id;
			$this_parking_space = new UPK_ParkingSpace($m_pk_id);
			$this_parking_space->GetParkingLot();
			$this_parking_lot=$this_parking_space->parking_lot;
			$m_plots_id=$this_parking_lot->get_m_plots_id();
			$dcp_m_plots_id_sql=" OR (dcp_type='9' AND dcp_type_id='" . $m_plots_id . "') ";
		}
		if($dcp_type==CONST_DCP_TYPE::GOV_PARKING_SPACE) {
			$dcp_type_id_sql="";//路邊不管ID是什麼
		}
		if($dcp_type==CONST_DCP_TYPE::INVOICE_GET_PARKING_POINT) {
			$dcp_parameter_sql=" AND dcp_parameters LIKE  '%".$dcp_args->get_business_identifier()."%' ";//路邊不管ID是什麼
		}
		//過濾 停車場或車位或符合 $dcp_type_array 者，且有在 $dcp_onsale_type_array 裡面
		$sql = "SELECT dcp_id,dcp_onsale_type FROM tb_DiscountPlan WHERE ((dcp_type='".$dcp_type."' ".$dcp_type_id_sql.")".$dcp_m_plots_id_sql.") AND dcp_onsale_type='".$dcp_onsale_type." ' ".$dcp_parameter_sql." AND dcp_delete='0' ORDER BY dcp_sn DESC ";
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		$result = mysql_query($sql, $conn);
		if (!$result) {
			//當作沒有優惠
		}
		$has_error_result = false;
		while ($ans = mysql_fetch_assoc($result)) {
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			$this_discount_plan = new UPK_DiscountPlan($ans["dcp_id"],"",$dcp_args);
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			if($this_discount_plan->is_available_discount_plan($m_id)) {
				array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0),"tiger_check_dcp_manager"=>$this_discount_plan->get_tiger_check()));
				array_push($this->c_dcp_array, $this_discount_plan);
			}
			else {
				$this->dcp_result = $this_discount_plan->dcp_result;
				$has_error_result = true;
			}
		}
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		if(count($this->c_dcp_array)==0 && $has_error_result==false) {
			if($dcp_type==CONST_DCP_TYPE::INVOICE_GET_PARKING_POINT) {
				$language = "zh-tw";
				$pure_data = "";
				rg_activity_log($conn, $m_id, "輸入發票獲取停車點數失敗", "此發票非易停網合作店家，請換一張發票再試一次", $pure_data, $ans);
				$ans = GetSystemCode("5040064", $language, $conn);
				$this->dcp_result = ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
			return false;
		}
		return true;
	}
	//產出折價後獲得的折扣或者是折價後的金額
	//基本上是一個point去算出要折多少，未來如果要分停車點數的話就代參數吧
	//回傳就是一個point，根據type決定不同意義
	//為了未來可能要多筆的相容性，所以暫且製作成array
	//計算金額時，折價只能有一個，但一些優惠送點可以多個(暫)
	function execute_discount_plan_array($m_id,$point,$args=[])
	{
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$tmp_point=0;
		$is_success=false;
		foreach($this->c_dcp_array as $this_discount_plan) {
			if($this_discount_plan->is_available_discount_plan($m_id)) {
				//根據不同type決定不同算法
				if ($this_discount_plan->get_dcp_onsale_type() == CONST_DCP_ONSALE_TYPE::FIRST_PAY_FREE_CASH_PERCENT) {
					$point = $this_discount_plan->calc_discount_plan($m_id,$point);
					$tmp_point=$point;
					$is_success=true;
				}
				elseif ($this_discount_plan->get_dcp_onsale_type() == CONST_DCP_ONSALE_TYPE::FIRST_PAY_GIVE_POINT_PERCENT) {
					$tmp_point += $this_discount_plan->calc_discount_plan($m_id,$point);
					$is_success=true;
				}
				elseif ($this_discount_plan->get_dcp_onsale_type() == CONST_DCP_ONSALE_TYPE::GOV_IN_X_MINUTES) {
					//路邊邏輯
					$tmp_point += $this_discount_plan->calc_discount_plan($m_id,$point,$this->c_dcp_gov);
					$this->dcp_result = $this_discount_plan->dcp_result;
					$is_success = true;
				}
				elseif ($this_discount_plan->get_dcp_onsale_type() == CONST_DCP_ONSALE_TYPE::APP_PAY_FEEDBACK) {
					$tmp_point += $this_discount_plan->calc_discount_plan($m_id,$point);
					$is_success=true;
				}
				elseif ($this_discount_plan->get_dcp_onsale_type() == CONST_DCP_ONSALE_TYPE::INVOICE_GET_PARKING_POINT) {
					$tmp_point += $this_discount_plan->calc_discount_plan($m_id,$point,$this->c_dcp_inv);
					$this->dcp_result = $this_discount_plan->dcp_result;
					$tans = json_decode($this_discount_plan->dcp_result,true);
					if($tans["result"]==1) {
						$is_success = true;
					}
				}
				elseif ($this_discount_plan->get_dcp_onsale_type() == CONST_DCP_ONSALE_TYPE::CAN_DISCOUNT_M_ID_ARRAY) {
					$tmp_point += $this_discount_plan->calc_discount_plan($m_id,$point);
					$is_success = true;
				}
				elseif ($this_discount_plan->get_dcp_onsale_type() == CONST_DCP_ONSALE_TYPE::PRICE_DURING_PERIOD_TIME) {
					$tmp_point += $this_discount_plan->calc_discount_plan($m_id,$point);
					$is_success = true;
				}
			}
		}
		if($is_success) {
			return json_encode(array("result" => 1, "point" => $tmp_point));
		}
		else {
			return $this->dcp_result;
		}
	}
	public function get_c_dcp_array() {
		return $this->c_dcp_array;
	}
	public function get_tiger_check() {
		return $this->tiger_check;
	}
}