<?php
class UPK_BookingLogManager
{
	private $m_bkl_sn;
	private $m_bkl_id;
	private $m_bkl_create_datetime;
	private $m_pk_id;
	private $m_ppl_id;
	private $m_ve_id;
	private $m_bkl_creator_m_id;
	private $m_ve_m_id;
//	private $m_plots_id;
	private $m_bkl_book_type;
//	private $m_bkl_start_date;
//	private $m_bkl_end_date;
//	private $m_bkl_start_time;
//	private $m_bkl_end_time;
	private $m_bkl_estimate_point;
	private $m_bkl_estimate_free_point;
//	private $m_bkl_estimate_point_int;
//	private $m_bkl_estimate_free_point_int;
//	private $m_bkl_estimate_total_point;
//	private $m_bkl_estimate_total_point_int;
	private $m_bkl_earlybird_offer;
	private $m_ppl_promotion_code;
	private $m_bkl_visit_description;
//	private $m_bkl_cancel;
//	private $m_id_cancel;
	private $m_bkl_group_id;
	private $m_bkl_sendpush;
	private $m_bkl_gov_id;
//	private $m_bkl_is_paid;
	private $m_bkl_is_pd;
//	private $m_bkl_transfer_m_id;//預約轉移的m_id
//	private $m_bkl_transfer_cellphone;
//	private $m_bkl_is_over_night; //是否要隔夜自動開單
	private $m_bkl_plots_type;
//	private $m_bkl_is_end;
	private $m_bkl_due_point;
	private $m_bkl_whitelist_point;
	private $m_bkl_discount_point;

	private $DT_start_datetime;
	private $DT_end_datetime;
	private $m_id_appear;
	private $this_parking_space;
	private $this_parking_lot;
	private $dcp_id;
	public $price_package;
	private $token;
	private $m_id;
	private $m_ve_special_level;
	private $m_bkl_is_customize;
	private $m_bkl_customize_price;
	private $m_bkl_is_fleeing_fee;
	private $m_bkl_fleeing_fee;
	private $m_bkl_handling_fee;

	public function __construct()
	{
		$this->DT_start_datetime = new DateTime();
		$this->DT_start_datetime = new DateTime();
		$this->price_package = new UPK_PricePackage();
		$this->m_bkl_earlybird_offer = 0;//早鳥優惠 沒再用
		$this->m_ppl_promotion_code = "";//優惠代碼 也沒再用
		$this->m_bkl_visit_description = "";//到訪戶號
		$this->m_bkl_gov_id = "";//路邊單號
		$this->m_bkl_sendpush = "1";//反正一定要送推播
		$this->m_bkl_group_id = "";//預約群組，只有續費延時或加簽用
		$this->m_bkl_book_type = "1";//預約type
		$this->m_bkl_is_customize = 0;//是否逃費 (預設 非逃費 = 0)
		$this->m_bkl_customize_price = 0;//是否逃費 (預設 非逃費 = 0)
		$this->m_bkl_is_fleeing_fee = 0;//是否逃費 (預設 非逃費 = 0)
		$this->m_bkl_fleeing_fee = 0;//逃費金額 (預設  0元)
		$this->m_bkl_handling_fee = 0;//手續費 (預設  0元)
		$this->m_bkl_creator_m_id = "";//預約建立人 主要記錄後台建立用 (預設 空)
	}

	public function set_token($token)
	{
		$this->token = $token;
	}
	public function set_m_bkl_book_type($m_bkl_book_type)
	{
		$this->m_bkl_book_type = $m_bkl_book_type;
	}

	public function set_m_bkl_is_customize($m_bkl_is_customize)
	{
		$this->m_bkl_is_customize = $m_bkl_is_customize;
	}

	public function set_m_bkl_customize_price($m_bkl_customize_price)
	{
		$this->m_bkl_customize_price = $m_bkl_customize_price;
	}

	public function set_m_bkl_is_fleeing_fee($m_bkl_is_fleeing_fee)
	{
		$this->m_bkl_is_fleeing_fee = $m_bkl_is_fleeing_fee;
	}

	public function set_m_bkl_fleeing_fee($m_bkl_fleeing_fee)
	{
		$this->m_bkl_fleeing_fee = $m_bkl_fleeing_fee;
	}

	public function set_handling_fee($m_bkl_handling_fee)
	{
		$this->m_bkl_handling_fee = $m_bkl_handling_fee;
	}

	public function set_m_bkl_creator_m_id($m_bkl_creator_m_id)
	{
		$this->m_bkl_creator_m_id = $m_bkl_creator_m_id;
	}


	//ID可以是MPID或者是PPLD可能要看m_plots_type決定
	public function set_price_package($id = "")
	{
		if ($id == "") {
			if ($this->m_bkl_plots_type == "300" || $this->m_bkl_plots_type == "301" || $this->m_bkl_plots_type == "302") {
				$id = $this->m_pk_id;
			}
			else {
				//..ppld
			}
		}
		$this->price_package = new UPK_PricePackage();
		$this->price_package->DT_insert($id, $this->DT_start_datetime, $this->DT_end_datetime);
	}

	public function set_parking($m_plots_id,$m_pk_id)
	{
		$this->set_parking_lot($m_plots_id);
		if($m_pk_id!=""){
			$this->set_parking_space($m_pk_id);
		}
	}

	public function set_parking_lot($m_plots_id)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		//$this->this_parking_lot=new UPK_ParkingLot($m_plots_id);
		$sql = "SELECT m_pk_id FROM tb_Member_ParkingSpace WHERE m_plots_id='" . $m_plots_id . "' AND m_pk_delete='0' ";
		$result = mysql_query($sql, $conn);
		$ans = mysql_fetch_assoc($result);
		$this->set_parking_space($ans["m_pk_id"]);

	}

	public function set_parking_space($m_pk_id)
	{
		$this->this_parking_space = new UPK_ParkingSpace($m_pk_id);
		$this->this_parking_space->GetParkingLot();
		$this->this_parking_lot = $this->this_parking_space->parking_lot;
		$this->m_bkl_plots_type = $this->this_parking_lot->get_m_plots_type();
		$this->m_pk_id = $m_pk_id;
	}

	public function set_m_id($m_id)
	{
		$this->m_id = $m_id;
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT m_token FROM tb_Member WHERE m_id='" . $m_id . "' ";
		$result = mysql_query($sql, $conn);
		$ans = mysql_fetch_assoc($result);
		$this->token = $ans["m_token"];

		$sql = "SELECT m_ve_id FROM tb_Member_Vehicle WHERE m_id='" . $m_id . "' AND m_ve_delete='0' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {

		}
		else if (mysql_num_rows($result) == 0) {
			include_once("/../main_api/RegisterVehicleFunc.php");
			//{"activity":"REGISTER VEHICLE","token":"0afcd43d-13d6-e71f-412c-468212d74bc6","vehicle_plate_number":"TEST-0427","ves_id":"VEID0000000000","trademark":"","model":"","length":"0","width":"0","height":"0","weight":"0","color":"000000","can_planepk":"1","can_machinepk":"1","can_undergoundpk":"1","can_threedpk":"1","can_privatepk":"1","can_publicpk":"1","can_streetpk":"1"}
			$vehicle_plate_number = "車號未輸入";
			$ves_id = "VEID0000000000";
			$trademark = "";
			$model = "";
			$length = "0";
			$width = "0";
			$height = "0";
			$weight = "0";
			$color = "000000";
			$can_planepk = "1";
			$can_machinepk = "1";
			$can_undergoundpk = "1";
			$can_threedpk = "1";
			$can_privatepk = "1";
			$can_publicpk = "1";
			$can_streetpk = "1";
			$pure_data = file_get_contents('php://input');
			$token = $this->token;
			$ttans = RegisterVehicleFunc($pure_data, "REGISTER VEHICLE", $token, $vehicle_plate_number, $color, $can_planepk, $can_machinepk, $can_undergoundpk, $can_threedpk, $can_privatepk, $can_publicpk, $can_streetpk, $trademark, $model, $length, $width, $height, $weight, $ves_id, "0", "1", "1", "1");
			$tans = json_decode($ttans, true);
			if ($tans["result"] == 0) {
				return json_encode($tans);
			}
			else {
				$vehicle_id = $tans["vehicle_id"];
				$this->set_vehicle($vehicle_id);
			}

		}
		else {
			$ans = mysql_fetch_assoc($result);
			$this->set_vehicle($ans["m_ve_id"]);
		}
	}

	public function set_vehicle($vehicle_id)
	{
		$this->m_ve_id = $vehicle_id;
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql = "SELECT * FROM tb_Member_Vehicle WHERE m_ve_delete='0' AND m_ve_id='" . $vehicle_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "搜尋車輛失敗", "description" => mysql_error($conn)));
		}
		else if (mysql_num_rows($result) == 0) {
			return json_encode(array("result" => 0, "title" => "無此車輛", "description" => mysql_error($conn)));
		}
		$ans = mysql_fetch_assoc($result);
		$m_ve_plate_no = $ans["m_ve_plate_no"];//身障等級 0=無 1=一般 2=中度 3=C及肢障
		$this->m_ve_special_level = $ans["m_ve_special_level"];//身障等級 0=無 1=一般 2=中度 3=C及肢障
		$this->m_ve_m_id = $ans["m_id"];
		//動力類別 動力形式 0=永動機 1=汽油 2=柴油 3=電動 4=油電混合 5=混合動力
		//如果動力是電動的話則不能自助計費(新北市)
		$m_ve_power_type = $ans["m_ve_power_type"];
		if ($m_ve_power_type == "3") {//  && strpos($this_parking_lot,"新北市")!==false不分縣市
			rg_activity_log($conn, $this->m_id, "自助開單失敗", "電動車不可開單", "", "");
			$ans = GetSystemCode("3030035", $language, $conn);
			return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
		}

		$sql = "SELECT * FROM tb_Gov_Special_Vehicle WHERE sv_plate_no='" . $m_ve_plate_no . "' AND sv_start_datetime<now() AND now()<sv_end_datetime";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "搜尋車輛失敗", "description" => mysql_error($conn)));
		}
		$sv_special_level = mysql_num_rows($result);//如果沒有就是0，有則是非0(通常為會是1)

		if ($this->m_ve_special_level == '3' || $this->m_ve_special_level == '2' || $this->m_ve_special_level == '1') {
			//身障車
			//自己有直不做事
		}
		elseif ($sv_special_level != 0 && $this->m_ve_special_level == 0) {
			//如果該會員未填寫身障車但是卻已經被登入在身障車內則採用身障車資料
			$this->m_ve_special_level = 1;
		}
		else {
			//如果不是身障車 或許也不用做這一行
			$this->m_ve_special_level = 0;
		}

		//$this->m_ve_special_level;
	}

	public function set_datetime($DT_start_datetime, $DT_end_datetime)
	{
		$this->DT_start_datetime = new DateTime($DT_start_datetime->format("Y-m-d H:i:s"));
		$this->DT_end_datetime = new DateTime($DT_end_datetime->format("Y-m-d H:i:s"));
	}
	public function set_m_id_appear($m_id_appear)
	{
		$this->m_id_appear = $m_id_appear;
	}

	public function calc_price($cost = -1)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		if ($cost >= 0) {
			$this->m_bkl_estimate_point = $cost;
			$this->m_bkl_estimate_free_point = 0;
			$this->m_bkl_is_pd = 1;
			$this->m_bkl_due_point = $cost;
			$this->m_bkl_whitelist_point = 0;
			$this->m_bkl_discount_point = 0;
			$this->dcp_id = "";
			return $cost;
		}
		else {
			//cost==-1要算金額
			if (($this->m_bkl_plots_type == "300" || $this->m_bkl_plots_type == "301" || $this->m_bkl_plots_type == "302")) {
				include_once("/../main_api/GovPriceCalculateFunc_v2.php");
				$ttans = GovPriceCalculateFunc("GOV PRICE CALCULATE", $this->token, $this->price_package->get(), $this->m_bkl_group_id, $this->m_ve_special_level);
			}
			else {
				include_once("/../main_api/ParkingPriceCalculate_func.php");
				$ttans = ParkingPriceCalcuate("PARKING PRICE CALCULATE", $this->token, $this->price_package->get(), $this->m_bkl_group_id, $this->m_ve_special_level);
			}
			$ttans = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $ttans), true);
			if (isset($ttans["data"][0]["due_point"]))
				$m_bkl_due_point = $ttans["data"][0]["due_point"];//未優惠前的總金額
			else $m_bkl_due_point = 0;
		}
		if ($ttans["result"] == 0) {
			#計價失敗
			return json_encode($ttans);
		}
		if (isset($ttans["m_bkl_is_pd"])) {
			$this->m_bkl_is_pd = $ttans["m_bkl_is_pd"];
		}
		for ($i = 0; $i < count($ttans["data"]); ++$i) {
			if ($this->this_parking_space->get_m_pk_only_same_community() == '1') {
				if (strlen(trim($this->m_bkl_visit_description)) == 0) {
					//理論上進不來之前有檔過
					$ans = GetSystemCode("3030030", $language, $conn);
					rg_activity_log($conn, $this->m_id, $ans[1], $ans[2], "", json_encode($ans));
					return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
				}
			}
			//$tmp_id = $ttans["data"][$i]["id"];
			$this->m_bkl_estimate_point = $ttans["data"][$i]["point"];
			$this->m_bkl_estimate_free_point = $ttans["data"][$i]["free_point"];
			$this->m_bkl_is_pd = 1;
			$this->m_bkl_due_point = $m_bkl_due_point;
			$this->m_bkl_whitelist_point = $ttans["data"][$i]["whitelist_point"];
			$this->m_bkl_discount_point = $ttans["data"][$i]["discount_point"];
			$this->dcp_id = $ttans["data"][$i]["dcp_id"];
		}
	}

	public function insert()
	{
		//屬於沒有空位時段要增加控衛時段的才要增加空位時段，如果是預約則原本就有空位時段不用增加空位時段
		if ($this->m_bkl_plots_type == "300" || $this->m_bkl_plots_type == "301" || $this->m_bkl_plots_type == "302")
			$this->insert_pricing_log();

		$this->insert_booking_log();
		$this->insert_parking_log();
	}

	private function insert_pricing_log()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$m_pk_id = $this->m_pk_id;
		$start_date = $this->DT_start_datetime->format("Y-m-d");
		$start_time = $this->DT_start_datetime->format("H:i:s");
		$end_date = $this->DT_end_datetime->format("Y-m-d");
		$end_time = $this->DT_end_datetime->format("H:i:s");
		#路邊停車OR車位請求要新增空位時段
		#自動新增的空位時段不計較有沒有重疊
		#2018-07-31 Tiger: 新北大流程要判斷該車位有沒有被占用

		if ($this->m_bkl_plots_type == "300")
			$description = "應安停車空位時段";
		elseif ($this->m_bkl_plots_type == "301")
			$description = "北大停車空位時段";
		elseif ($this->m_bkl_plots_type == "302")
			$description = "台灣大車位車辨空位時段";
		else//if($this->m_bkl_plots_type==100//目前剩下就是路邊停車的狀況
			$description = "路邊停車空位時段";

		$tmp_m_ppl_id = 'tmp' . GenerateRandomString(11, '0123456789');
		$sql = "INSERT INTO tb_Member_ParkingSpace_Pricing_Log (m_ppl_id, m_ppl_group_id, m_pk_id, m_ppl_create_datetime, m_ppl_price_points, m_ppl_price_type, m_ppl_price_member_description, m_ppl_start_date, m_ppl_end_date, m_ppl_start_time, m_ppl_end_time,m_ppl_daily_max_price,m_id_create)
		 VALUES ('" . $tmp_m_ppl_id . "', 'tmp', '" . $m_pk_id . "', now(), '0', '" . $this->m_bkl_book_type . "', '" . $description . "', '" . $start_date . "', '" . $end_date . "', '" . $start_time . "', '" . $end_time . "', '0', '')";
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "新增空位時段錯誤", "description" => mysql_error($conn)));
		}
		$sql = "SELECT m_ppl_sn FROM tb_Member_ParkingSpace_Pricing_Log WHERE m_pk_id='" . $m_pk_id . "' and m_ppl_start_date='" . $start_date . "' and m_ppl_end_date='" . $end_date . "' and m_ppl_start_time='" . $start_time . "' and m_ppl_end_time='" . $end_time . "' AND m_ppl_id='" . $tmp_m_ppl_id . "'";
		$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
		if (!$ans) {
			return json_encode(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error($conn)));
		}
		$new_ppld = $ans["m_ppl_sn"];
		$new_gid = $ans["m_ppl_sn"];
		Sn2Id("PPLD", $new_ppld);
		Sn2Id("PPLD", $new_gid);
		///  $start_date, $end_date, $start_time, $end_time
		$sql = "UPDATE tb_Member_ParkingSpace_Pricing_Log SET m_ppl_id='" . $new_ppld . "', m_ppl_group_id='" . $new_gid . "' WHERE m_pk_id='" . $m_pk_id . "' and m_ppl_start_date='" . $start_date . "' and m_ppl_end_date='" . $end_date . "' and m_ppl_start_time='" . $start_time . "' and m_ppl_end_time='" . $end_time . "' AND m_ppl_id='" . $tmp_m_ppl_id . "'";
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "更新空位時段失敗", "description" => mysql_error($conn)));
		}
		$this->m_ppl_id = $new_ppld;
		MemcacheSetPricingLog('_UPK_PricingLog:', $new_ppld);
	}

	private function insert_booking_log()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$start_date = $this->DT_start_datetime->format("Y-m-d");
		$start_time = $this->DT_start_datetime->format("H:i:s");
		$end_date = $this->DT_end_datetime->format("Y-m-d");
		$end_time = $this->DT_end_datetime->format("H:i:s");
//		$this_tmp_booking_log = new UPK_BookingLog($this->m_bkl_group_id);
		$this->this_parking_space->GetFareKindParkingSpace();
		$m_bkl_farekind_array = array();
		foreach ((array)$this->this_parking_space->gov_farekind_parkingspaces as $each_fare_kind) {
			array_push($m_bkl_farekind_array, $each_fare_kind->get_sn());
		}
		$sql_m_bkl_farekind_array = json_encode($m_bkl_farekind_array);
//		$m_bkl_transfer_m_id = $this_tmp_booking_log->get_m_bkl_transfer_m_id();
		$tmp_m_bkl_id = 'tmp' . GenerateRandomString(11, '0123456789');
		$m_bkl_transfer_m_id = "";

		$no_dup1 = "(CONCAT(m_bkl_end_date, ' ', m_bkl_end_time)<=CONCAT('" . $start_date . "', ' ', '" . $start_time . "') and CONCAT(m_bkl_end_date, ' ', m_bkl_end_time)<=CONCAT('" . $end_date . "', ' ', '" . $end_time . "'))";
		$no_dup2 = "(CONCAT(m_bkl_start_date, ' ', m_bkl_start_time)>=CONCAT('" . $start_date . "', ' ', '" . $start_time . "') and CONCAT(m_bkl_start_date, ' ', m_bkl_start_time)>=CONCAT('" . $end_date . "', ' ', '" . $end_time . "'))";
		if ($this->m_bkl_plots_type == "0") {
			$generate_sql_bkl = new TimeComponent("", "", "", "", "");
			$generate_sql_bkl->sql_field_name_init("m_bkl_start_date", "m_bkl_end_date", "m_bkl_start_time", "m_bkl_end_time");
			$sql_bkl_is_inside = $generate_sql_bkl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "inside");
			$sql_bkl_is_outside = $generate_sql_bkl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "outside");
			$sql_bkl_is_cross = $generate_sql_bkl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "cross");
			$sql_bkl_is_include = $generate_sql_bkl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "include");
			$dup_sql = "SELECT 1 FROM tb_Member_ParkingSpace_Booking_Log where m_pk_id='" . $this->m_pk_id . "' AND 0";
			//AND 0 先不精確檔重複
		}
		else {
			//預約的時候要判斷時間重複(預約時要判斷預約時段不可重疊)，
			//路邊、應安杭南300、北大301、車辨302 的預約時段可以重疊
			$dup_sql = "SELECT 1 FROM tb_Member_ParkingSpace_Booking_Log as tb_bkl 
							LEFT JOIN tb_Member_Parking_Log as tb_pl ON tb_bkl.m_bkl_id=tb_pl.m_bkl_id 
						WHERE tb_bkl.m_pk_id='" . $this->m_pk_id . "' AND m_bkl_cancel=0 AND tb_bkl.m_bkl_group_id='' 
							AND tb_pl.m_pl_end_time IS NULL  and !(" . $no_dup1 . " or " . $no_dup2 . ") ";
			$dup_sql = "SELECT 1 FROM tb_Member_ParkingSpace_Booking_Log where m_pk_id='" . $this->m_pk_id . "' AND 0";
		}
		if ($this->m_bkl_plots_type == '0' || $this->m_bkl_plots_type == '300') {
			$tmp_point = $this->m_bkl_estimate_point;
			$tmp_free_point = $this->m_bkl_estimate_free_point;
		}
		else {
			$tmp_point = 0;
			$tmp_free_point = 0;
		}
		if($this->m_bkl_creator_m_id == "") {
			$this->m_bkl_creator_m_id = $this->m_ve_m_id;
		}
		$sql = "INSERT INTO tb_Member_ParkingSpace_Booking_Log (
				m_bkl_id, m_bkl_create_datetime, m_pk_id, m_ppl_id, m_ve_id, m_bkl_creator_m_id, m_ve_m_id, m_plots_id, m_bkl_book_type,
				m_bkl_start_date, m_bkl_end_date, m_bkl_start_time, m_bkl_end_time,
				m_bkl_estimate_point, m_bkl_estimate_free_point,
				m_bkl_earlybird_offer, m_ppl_promotion_code,m_bkl_visit_description,
				m_bkl_group_id,m_bkl_sendpush,m_bkl_gov_id,m_bkl_is_pd,
				m_bkl_transfer_m_id,m_bkl_due_point,m_bkl_whitelist_point,m_bkl_farekind_array,m_bkl_is_paid,m_bkl_is_customize,m_bkl_customize_price,m_bkl_is_fleeing_fee,m_bkl_fleeing_fee,m_bkl_handling_fee)
				 select '" . $tmp_m_bkl_id . "', now(), '" . $this->m_pk_id . "', '" . $this->m_ppl_id . "', '" . $this->m_ve_id . "', '" . $this->m_bkl_creator_m_id . "', '" . $this->m_ve_m_id . "', '" . $this->this_parking_space->get_m_plots_id() . "', '" . $this->m_bkl_book_type . "',
					'" . $start_date . "', '" . $end_date . "', '" . $start_time . "', '" . $end_time . "',
					'" . $tmp_point . "', '" . $tmp_free_point . "',
					'" . $this->m_bkl_earlybird_offer . "', '" . $this->m_ppl_promotion_code . "', '" . $this->m_bkl_visit_description . "',
					'" . $this->m_bkl_group_id . "', '" . $this->m_bkl_sendpush . "', '" . $this->m_bkl_gov_id . "', '" . $this->m_bkl_is_pd . "',
					'" . $m_bkl_transfer_m_id . "' , '" . $this->m_bkl_due_point . "', '" . $this->m_bkl_whitelist_point . "', '" . $sql_m_bkl_farekind_array . "' , '0', '".$this->m_bkl_is_customize."', '".$this->m_bkl_customize_price."', '".$this->m_bkl_is_fleeing_fee."', '".$this->m_bkl_fleeing_fee."', '".$this->m_bkl_handling_fee."' from dual 
					WHERE NOT EXISTS (" . $dup_sql . ")";
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "新增失敗", "description" => mysql_error($conn)));
		}
		$sql = "SELECT m_bkl_sn FROM tb_Member_ParkingSpace_Booking_Log WHERE m_pk_id='" . $this->m_pk_id . "' and m_ppl_id='" . $this->m_ppl_id . "' and m_ve_id='" . $this->m_ve_id . "' and m_bkl_book_type='" . $this->m_bkl_book_type . "' and m_bkl_start_date='" . $start_date . "' and m_bkl_end_date='" . $end_date . "' and m_bkl_start_time='" . $start_time . "' and m_bkl_end_time='" . $end_time . "' and m_bkl_id='" . $tmp_m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		$ans = mysql_fetch_assoc($result);
		$new_id = $ans["m_bkl_sn"];
		Sn2Id("BKLD", $new_id);
		//採用bkl_sn最後3馬當做開單編號
		$m_plots_address_city = $this->this_parking_lot->get_m_plots_address_city();
		$m_plots_type = $this->this_parking_lot->get_m_plots_type();

		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_id='" . $new_id . "',m_bkl_gov_id='" . $this->m_bkl_gov_id . "', m_plots_address_city='" . $m_plots_address_city . "', m_bkl_plots_type='" . $m_plots_type . "' , m_bkl_disp_unpaid_page='1' WHERE m_bkl_sn=" . $ans["m_bkl_sn"];
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "更新失敗", "description" => mysql_error($conn)));
		}
		$this->m_bkl_id = $new_id;

		if ($this->m_bkl_book_type == 1) {
			$this_booking_log = new UPK_BookingLog($this->m_bkl_id);
			$this_booking_log->UpdateBookingLogView($this->m_id,
				$this->m_bkl_estimate_point,$this->m_bkl_estimate_free_point,
				 $this->m_bkl_whitelist_point,$this->m_bkl_discount_point,
				$this->m_bkl_due_point,$this->dcp_id);
		}

		if ($this->m_bkl_plots_type == '300') {
			$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_is_paid='0' WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
			if (!mysql_query($sql, $conn)) {
				return json_encode(array("result" => 0, "title" => "更新失敗", "description" => mysql_error($conn)));
			}
		}
		else {
			//300不用假的零元付款，因為掃出場才需付款，但是其他的需要
			include_once("/../pay_api/GenerateCreditOrdersZeroFunc.php");
			GenerateCreditOrdersZeroFunc("GENERATE CREDIT ORDERS", $this->token, $new_id, "", true);
		}
	}
	//外部近來的需檢察參數有無下
	public function insertParkingLog() {
		if($this->m_bkl_id=="") {
			return json_encode(array("result" => 0, "title" => "必填欄位未填", "description" => "預約未填寫"));
		}
		if($this->m_pk_id=="") {
			return json_encode(array("result" => 0, "title" => "必填欄位未填", "description" => "車位未填寫"));
		}
		if($this->m_ve_id=="") {
			return json_encode(array("result" => 0, "title" => "必填欄位未填", "description" => "車輛未填寫"));
		}
		return $this->insert_parking_log();
	}
	private function insert_parking_log()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$tmp_m_pl_id = 'tmp' . GenerateRandomString(11, '0123456789');
		$start_date = $this->DT_start_datetime->format("Y-m-d");
		$start_time = $this->DT_start_datetime->format("H:i:s");
		$end_date = $this->DT_end_datetime->format("Y-m-d");
		$end_time = $this->DT_end_datetime->format("H:i:s");
		$sql = "SELECT m_pl_id,m_pl_end_time FROM tb_Member_Parking_Log WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
		}
		else if (mysql_num_rows($result) == 0) {
			$sql = "INSERT INTO tb_Member_Parking_Log (m_pl_id, m_bkl_id, m_pk_id, m_ve_id, m_pl_start_time, m_pl_end_time, m_id_appear) VALUES ('" . $tmp_m_pl_id . "', '" . $this->m_bkl_id . "', '" . $this->m_pk_id . "', '" . $this->m_ve_id . "', '" . $start_date . " " . $start_time . "', '" . $end_date . " " . $end_time . "', '".$this->m_id_appear."')";
			$result = mysql_query($sql, $conn);
			if (!$result) {
			}
			$sql = "SELECT m_pl_sn FROM tb_Member_Parking_Log WHERE m_pl_id='" . $tmp_m_pl_id . "' and "
				. "m_bkl_id='" . $this->m_bkl_id . "' and "
				. "m_pk_id='" . $this->m_pk_id . "' ";
			$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
			if (!$ans) {
			}
			$new_id = $ans["m_pl_sn"];
			Sn2Id("PLID", $new_id);
			$sql = "UPDATE tb_Member_Parking_Log SET m_pl_id='" . $new_id . "'  WHERE m_pl_sn='" . $ans["m_pl_sn"] . "' ";
			if (!mysql_query($sql, $conn)) {
				return json_encode(array("result" => 0, "title" => "update faild"));
			}
			MemcacheSetBookingLog('_UPK_BookingLog:', $this->m_bkl_id);
			return json_encode(array("result" => 1));
		}
		//如果零元付款的時候已經自動進場
		else {
			$ans = mysql_fetch_assoc($result);
			$sql = "UPDATE tb_Member_Parking_Log SET  m_pl_end_time='" . $end_date . " " . $end_time . "', m_id_appear = '".$this->m_id_appear."'  WHERE m_pl_id='" . $ans["m_pl_id"] . "' ";
			if (!mysql_query($sql, $conn)) {
				return json_encode(array("result" => 0, "title" => "update faild"));
			}
			MemcacheSetBookingLog('_UPK_BookingLog:', $this->m_bkl_id);
			return json_encode(array("result" => 1));
		}
	}

	public function get_m_bkl_id()
	{
		return $this->m_bkl_id;
	}
	public function set_m_bkl_id($m_bkl_id)
	{
		$this->m_bkl_id = $m_bkl_id;
	}
}
?>