<?php

class UPK_AuthorizedStore
{
	private $m_as_sn;
	private $m_as_id;
	private $m_id;
	private $m_as_create_datetime;
	private $m_as_name;
	private $m_as_keyword;
	private $m_as_opening_time;
	private $m_as_phone_number;
	private $m_as_address_country;
	private $m_as_address_post_code;
	private $m_as_address_state;
	private $m_as_address_city;
	private $m_as_address_line1;
	private $m_as_address_line2;
	private $m_as_address_line3;
	private $m_as_address_description;
	private $m_as_lng;
	private $m_as_lat;
	private $m_as_alt;
	private $m_as_last_edit_time;
	private $m_as_last_edit_m_id;
	private $m_as_html_body;
	private $m_as_url;
	private $m_is_close;
	private $m_is_close_datetime;
	private $m_as_delete;
	private $m_as_right_enable;
	private $m_as_sale_description;
	private $m_as_is_c2as_parking_point;
	private $m_as_s4c_handling_fee;
	private $m_as_s4c_parking_point_back;
	private $m_as_invoice;
	private $is_space4car_create_invoice;

	public $result_array;

	public $B_m_as_sn = 1;
	public $B_m_as_id = 2;
	public $B_m_id = 4;
	public $B_m_as_create_datetime = 8;
	public $B_m_as_name = 16;
	public $B_m_as_keyword = 32;
	public $B_m_as_opening_time = 64;
	public $B_m_as_phone_number = 128;
	public $B_m_as_address = 256;
	/*public $B_m_as_address_country = 256;
	public $B_m_as_address_post_code = 512;
	public $B_m_as_address_state = 1024;
	public $B_m_as_address_city = 2048;
	public $B_m_as_address_line1 = 4096;
	public $B_m_as_address_line2 = 8192;
	public $B_m_as_address_line3 = 16384;
	public $B_m_as_address_description = 32768;
	public $B_m_as_lng = 65536;
	public $B_m_as_lat = 131072;
	public $B_m_as_alt = 262144;*/
	public $B_m_as_last_edit_time = 524288;
	public $B_m_as_last_edit_m_id = 1048576;
	public $B_m_as_html_body = 2097152;
	public $B_m_as_url = 4194304;
	public $B_m_is_close = 8388608;
	public $B_m_is_close_datetime = 16777216;
	public $B_m_as_delete = 33554432;
	public $B_m_as_right_enable = 67108864;
	public $B_m_as_sale_description = 134217728;
	public $B_m_as_is_c2as_parking_point = 268435456;
	public $B_m_as_s4c_handling_fee = 536870912;
	public $B_m_as_s4c_parking_point_back = 1073741824;
	public $B_m_as_invoice = 512;
	public $B_is_space4car_create_invoice = 1024;
	public $B_ALL;

	public function __construct($m_as_id, $sql_logic = "")
	{
		$this->B_ALL = $this->B_m_as_sn | $this->B_m_as_id | $this->B_m_id | $this->B_m_as_create_datetime | $this->B_m_as_name | $this->B_m_as_keyword | $this->B_m_as_opening_time | $this->B_m_as_phone_number | $this->B_m_as_address | $this->B_m_as_last_edit_time | $this->B_m_as_last_edit_m_id | $this->B_m_as_html_body | $this->B_m_as_url | $this->B_m_is_close | $this->B_m_is_close_datetime | $this->B_m_as_delete | $this->B_m_as_right_enable | $this->B_m_as_sale_description | $this->B_m_as_is_c2as_parking_point | $this->B_m_as_s4c_handling_fee | $this->B_m_as_s4c_parking_point_back | $this->B_m_as_invoice | $this->B_is_space4car_create_invoice;
		$this->init_m_as_id($m_as_id, $sql_logic = "");
	}

	public function init_m_as_id($m_as_id, $sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		/*********************************** Put your table name here ***********************************/
		$sql = "select * from tb_Member_Authorized_Store where m_as_id = '" . $m_as_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			$this->result_array =array("result" => 0, "title" => "失敗", "description" => mysql_error());
			return false;
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->m_as_sn = $ans["m_as_sn"];
			$this->m_as_id = $ans["m_as_id"];
			$this->m_id = $ans["m_id"];
			$this->m_as_create_datetime = $ans["m_as_create_datetime"];
			$this->m_as_name = $ans["m_as_name"];
			$this->m_as_keyword = $ans["m_as_keyword"];
			$this->m_as_opening_time = $ans["m_as_opening_time"];
			$this->m_as_phone_number = $ans["m_as_phone_number"];
			$this->m_as_address_country = $ans["m_as_address_country"];
			$this->m_as_address_post_code = $ans["m_as_address_post_code"];
			$this->m_as_address_state = $ans["m_as_address_state"];
			$this->m_as_address_city = $ans["m_as_address_city"];
			$this->m_as_address_line1 = $ans["m_as_address_line1"];
			$this->m_as_address_line2 = $ans["m_as_address_line2"];
			$this->m_as_address_line3 = $ans["m_as_address_line3"];
			$this->m_as_address_description = $ans["m_as_address_description"];
			$this->m_as_lng = $ans["m_as_lng"];
			$this->m_as_lat = $ans["m_as_lat"];
			$this->m_as_alt = $ans["m_as_alt"];
			$this->m_as_last_edit_time = $ans["m_as_last_edit_time"];
			$this->m_as_last_edit_m_id = $ans["m_as_last_edit_m_id"];
			$this->m_as_html_body = $ans["m_as_html_body"];
			$this->m_as_url = $ans["m_as_url"];
			$this->m_is_close = $ans["m_is_close"];
			$this->m_is_close_datetime = $ans["m_is_close_datetime"];
			$this->m_as_delete = $ans["m_as_delete"];
			$this->m_as_right_enable = $ans["m_as_right_enable"];
			$this->m_as_sale_description = $ans["m_as_sale_description"];
			$this->m_as_is_c2as_parking_point = $ans["m_as_is_c2as_parking_point"];
			$this->m_as_s4c_handling_fee = $ans["m_as_s4c_handling_fee"];
			$this->m_as_s4c_parking_point_back = $ans["m_as_s4c_parking_point_back"];
			$this->m_as_invoice = json_decode($ans["m_as_invoice_json"]);
			$this->is_space4car_create_invoice = $ans["is_space4car_create_invoice"];

		}
	}

	public function update_m_as_s4c_handling_fee($m_as_s4c_handling_fee) {
		global $conn,$dbName;
		check_conn($conn,$dbName);
		if($this->m_as_id != null) {
			$sql = "UPDATE tb_Member_Authorized_Store SET m_as_s4c_handling_fee='" . $m_as_s4c_handling_fee . "' WHERE m_as_id='" . $this->m_as_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				$this->result_array = array("result" => 0, "title" => "修改特約店失敗", "description" => mysql_error());
				return false;
			}
			return true;
		}
		return false;
	}
	public function update_m_as_s4c_parking_point_back($m_as_s4c_parking_point_back) {
		global $conn,$dbName;
		check_conn($conn,$dbName);
		if($this->m_as_id != null) {
			$sql = "UPDATE tb_Member_Authorized_Store SET m_as_s4c_parking_point_back='" . $m_as_s4c_parking_point_back . "' WHERE m_as_id='" . $this->m_as_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				$this->result_array = array("result" => 0, "title" => "修改特約店失敗", "description" => mysql_error());
				return false;
			}
			return true;
		}
		return false;
	}
	public function update_m_as_invoice($invoice_title,$invoice_business_number,$invoice_email) {
		$m_as_invoice_json = json_encode(array("invoice_title"=>$invoice_title,"invoice_business_number"=>$invoice_business_number,"invoice_email"=>$invoice_email),JSON_UNESCAPED_UNICODE);
		global $conn,$dbName;
		check_conn($conn,$dbName);
		if($this->m_as_id != null) {
			$sql = "UPDATE tb_Member_Authorized_Store SET m_as_invoice_json='" . $m_as_invoice_json . "' WHERE m_as_id='" . $this->m_as_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				$this->result_array = array("result" => 0, "title" => "修改特約店失敗", "description" => mysql_error());
				return false;
			}
			return true;
		}
		return false;
	}
	public function update_is_space4car_create_invoice($is_space4car_create_invoice) {
		global $conn,$dbName;
		check_conn($conn,$dbName);
		if($this->m_as_id != null) {
			$sql = "UPDATE tb_Member_Authorized_Store SET is_space4car_create_invoice='" . $is_space4car_create_invoice . "' WHERE m_as_id='" . $this->m_as_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				$this->result_array = array("result" => 0, "title" => "修改特約店失敗", "description" => mysql_error());
				return false;
			}
			return true;
		}
		return false;
	}
	public function get_m_as_sn()
	{
		return $this->m_as_sn;
	}

	public function get_m_as_id()
	{
		return $this->m_as_id;
	}

	public function get_m_id()
	{
		return $this->m_id;
	}

	public function get_m_as_create_datetime()
	{
		return $this->m_as_create_datetime;
	}

	public function get_m_as_name()
	{
		return $this->m_as_name;
	}

	public function get_m_as_keyword()
	{
		return $this->m_as_keyword;
	}

	public function get_m_as_opening_time()
	{
		return $this->m_as_opening_time;
	}

	public function get_m_as_phone_number()
	{
		return $this->m_as_phone_number;
	}

	public function get_m_as_address_country()
	{
		return $this->m_as_address_country;
	}

	public function get_m_as_address_post_code()
	{
		return $this->m_as_address_post_code;
	}

	public function get_m_as_address_state()
	{
		return $this->m_as_address_state;
	}

	public function get_m_as_address_city()
	{
		return $this->m_as_address_city;
	}

	public function get_m_as_address_line1()
	{
		return $this->m_as_address_line1;
	}

	public function get_m_as_address_line2()
	{
		return $this->m_as_address_line2;
	}

	public function get_m_as_address_line3()
	{
		return $this->m_as_address_line3;
	}

	public function get_m_as_address_description()
	{
		return $this->m_as_address_description;
	}

	public function get_m_as_lng()
	{
		return $this->m_as_lng;
	}

	public function get_m_as_lat()
	{
		return $this->m_as_lat;
	}

	public function get_m_as_alt()
	{
		return $this->m_as_alt;
	}

	public function get_m_as_last_edit_time()
	{
		return $this->m_as_last_edit_time;
	}

	public function get_m_as_last_edit_m_id()
	{
		return $this->m_as_last_edit_m_id;
	}

	public function get_m_as_html_body()
	{
		return $this->m_as_html_body;
	}

	public function get_m_as_url()
	{
		return $this->m_as_url;
	}

	public function get_m_is_close()
	{
		return $this->m_is_close;
	}

	public function get_m_is_close_datetime()
	{
		return $this->m_is_close_datetime;
	}

	public function get_m_as_delete()
	{
		return $this->m_as_delete;
	}

	public function get_m_as_right_enable()
	{
		return $this->m_as_right_enable;
	}

	public function get_m_as_sale_description()
	{
		return $this->m_as_sale_description;
	}

	public function get_m_as_is_c2as_parking_point()
	{
		return $this->m_as_is_c2as_parking_point;
	}
	public function get_m_as_s4c_handling_fee()
	{
		return round((float)$this->m_as_s4c_handling_fee,4);//基本上不會小於0.01
	}
	public function get_m_as_s4c_parking_point_back()
	{
		return round((float)$this->m_as_s4c_parking_point_back,4);//基本上不會小於0.01
	}
	public function get_m_as_invoice()
	{
		return $this->m_as_invoice;
	}
	public function get_is_space4car_create_invoice()
	{
		return $this->is_space4car_create_invoice;
	}

	public function get_file_logs()
	{
		$file_logs_result=array("result"=>1,"data"=>$this->get_file_logs_array());
		return $file_logs_result;
	}
	public function get_file_logs_array($m_id="")
	{
		global $conn,$dbName;

		$this_file_logs = new UPK_File_Logs(CONST_FL_TYPE::AUTHORIZED_SOTRE,$this->m_as_id,$m_id);
		$file_logs_array = $this_file_logs->get_array();
		return $file_logs_array;
	}

	public function get_array($select = "")
	{
		if ($select == "") $select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_m_as_sn) != 0) {
			$return_array["m_as_sn"] = $this->get_m_as_sn();
		}
		if (($select & $this->B_m_as_id) != 0) {
			$return_array["m_as_id"] = $this->get_m_as_id();
		}
		if (($select & $this->B_m_id) != 0) {
			$return_array["m_id"] = $this->get_m_id();
		}
		if (($select & $this->B_m_as_create_datetime) != 0) {
			$return_array["m_as_create_datetime"] = $this->get_m_as_create_datetime();
		}
		if (($select & $this->B_m_as_name) != 0) {
			$return_array["m_as_name"] = $this->get_m_as_name();
		}
		if (($select & $this->B_m_as_keyword) != 0) {
			$return_array["m_as_keyword"] = $this->get_m_as_keyword();
		}
		if (($select & $this->B_m_as_opening_time) != 0) {
			$return_array["m_as_opening_time"] = $this->get_m_as_opening_time();
		}
		if (($select & $this->B_m_as_phone_number) != 0) {
			$return_array["m_as_phone_number"] = $this->get_m_as_phone_number();
		}
		if (($select & $this->B_m_as_address) != 0) {
			$return_array["m_as_address_country"] = $this->get_m_as_address_country();
			$return_array["m_as_address_post_code"] = $this->get_m_as_address_post_code();
			$return_array["m_as_address_state"] = $this->get_m_as_address_state();
			$return_array["m_as_address_city"] = $this->get_m_as_address_city();
			$return_array["m_as_address_line1"] = $this->get_m_as_address_line1();
			$return_array["m_as_address_line2"] = $this->get_m_as_address_line2();
			$return_array["m_as_address_line3"] = $this->get_m_as_address_line3();
			$return_array["m_as_address_description"] = $this->get_m_as_address_description();
			$return_array["m_as_lng"] = $this->get_m_as_lng();
			$return_array["m_as_lat"] = $this->get_m_as_lat();
			$return_array["m_as_alt"] = $this->get_m_as_alt();
		}
		if (($select & $this->B_m_as_last_edit_time) != 0) {
			$return_array["m_as_last_edit_time"] = $this->get_m_as_last_edit_time();
		}
		if (($select & $this->B_m_as_last_edit_m_id) != 0) {
			$return_array["m_as_last_edit_m_id"] = $this->get_m_as_last_edit_m_id();
		}
		if (($select & $this->B_m_as_html_body) != 0) {
			$return_array["m_as_html_body"] = $this->get_m_as_html_body();
		}
		if (($select & $this->B_m_as_url) != 0) {
			$return_array["m_as_url"] = $this->get_m_as_url();
		}
		if (($select & $this->B_m_is_close) != 0) {
			$return_array["m_is_close"] = $this->get_m_is_close();
		}
		if (($select & $this->B_m_is_close_datetime) != 0) {
			$return_array["m_is_close_datetime"] = $this->get_m_is_close_datetime();
		}
		if (($select & $this->B_m_as_delete) != 0) {
			$return_array["m_as_delete"] = $this->get_m_as_delete();
		}
		if (($select & $this->B_m_as_right_enable) != 0) {
			$return_array["m_as_right_enable"] = $this->get_m_as_right_enable();
		}
		if (($select & $this->B_m_as_sale_description) != 0) {
			$return_array["m_as_sale_description"] = $this->get_m_as_sale_description();
		}
		if (($select & $this->B_m_as_is_c2as_parking_point) != 0) {
			$return_array["m_as_is_c2as_parking_point"] = $this->get_m_as_is_c2as_parking_point();
		}
		if (($select & $this->B_m_as_s4c_handling_fee) != 0) {
			$return_array["m_as_s4c_handling_fee"] = $this->get_m_as_s4c_handling_fee();
		}
		if (($select & $this->B_m_as_s4c_parking_point_back) != 0) {
			$return_array["m_as_s4c_parking_point_back"] = $this->get_m_as_s4c_parking_point_back();
		}
		if (($select & $this->B_m_as_invoice) != 0) {
			$return_array["m_as_invoice"] = $this->get_m_as_invoice();
		}
		if (($select & $this->B_is_space4car_create_invoice) != 0) {
			$return_array["is_space4car_create_invoice"] = $this->get_is_space4car_create_invoice();
		}
		return $return_array;
	}
}