<?php
class UPK_EvaluationLog {
	private $m_pel_sn;
	public $m_pel_id;
	private $m_pl_id;
	private $m_bkl_id;
	private $m_pyl_id;
	private $m_pel_score;
	private $m_pel_evaluation;
	private $m_pel_suggestion;
	private $m_pel_evaluate_To;
	private $m_pel_is_appeal;
	private $m_pel_is_contactus;
	private $m_pel_appeal_content;
	private $m_pel_type;
	private $m_pel_create_datetime;
	
	public $B_m_pel_sn					=1;
	public $B_m_pel_id					=2;
	public $B_m_pl_id					=4;
	public $B_m_bkl_id					=8;
	public $B_m_pyl_id					=16;
	public $B_m_pel_score				=32;
	public $B_m_pel_evaluation			=64;
	public $B_m_pel_suggestion			=128;
	public $B_m_pel_evaluate_To			=256;
	public $B_m_pel_is_appeal			=512;
	public $B_m_pel_is_contactus		=1024;
	public $B_m_pel_appeal_content		=2048;
	public $B_m_pel_type				=4096;
	public $B_m_pel_create_datetime		=8192;
	
	public $B_ID;
	public $B_BODY;
	public $B_ALL;
	
	public function __construct($m_pel_id,$sql_logic=""){
		$this->B_BODY=$this->B_m_pel_score | $this->B_m_pel_evaluation | $this->B_m_pel_suggestion | $this->B_m_pel_evaluate_To | $this->B_m_pel_is_appeal | $this->B_m_pel_is_contactus | $this->B_m_pel_appeal_content | $this->B_m_pel_type | $this->B_m_pel_create_datetime;
		$this->B_ID = $this->B_m_pl_id | $this->B_m_pyl_id | $this->B_m_bkl_id | $this->B_m_pel_id;
		$this->B_ALL = $this->B_m_pel_sn | $this->B_BODY | $this->B_ID;
		$this->init_pel_id($m_pel_id,$sql_logic="");
    }
    
    public function init_pel_id($m_pel_id,$sql_logic=""){

		global $conn,$dbName;
		check_conn($conn,$dbName);
		$language = "zh-tw";
		$sql="select * from tb_Member_Parking_Evaluation_Log where m_pel_id = '".$m_pel_id."' ";
		$sql.=$sql_logic;
		$result = mysql_query($sql, $conn);
		if(! $result){
			return json_encode(array("失敗" => mysql_error($conn).$sql));
		}else if(mysql_num_rows($result) == 1)
		{
			$ans=mysql_fetch_assoc($result);
			$this->m_pel_sn=$ans["m_pel_sn"];
			$this->m_pel_id=$ans["m_pel_id"];
			$this->m_pl_id=$ans["m_pl_id"];
			$this->m_bkl_id=$ans["m_bkl_id"];
			$this->m_pyl_id=$ans["m_pyl_id"];
			$this->m_pel_score=$ans["m_pel_score"];
			$this->m_pel_evaluation=$ans["m_pel_evaluation"];
			$this->m_pel_suggestion=$ans["m_pel_suggestion"];
			$this->m_pel_evaluate_To=$ans["m_pel_evaluate_To"];
			$this->m_pel_is_appeal=$ans["m_pel_is_appeal"];
			$this->m_pel_is_contactus=$ans["m_pel_is_contactus"];
			$this->m_pel_appeal_content=$ans["m_pel_appeal_content"];
			$this->m_pel_type=$ans["m_pel_type"];
			$this->m_pel_create_datetime=$ans["m_pel_create_datetime"];
		}
		else{
		//多個相同ID
			return json_encode(array("有多個相同付費紀錄ID" => mysql_error($conn).$sql));
		}
    }
    
    public function get_m_pel_sn(){
		return $this->m_pel_sn;
    }
    public function get_m_pel_id(){
		return $this->m_pel_id;
    }
    public function get_m_pl_id(){
		return $this->m_pl_id;
    }
    public function get_m_bkl_id(){
		return $this->m_bkl_id;
    }
    public function get_m_pel_score(){
		return (int)$this->m_pel_score;
    }
    public function get_m_pel_evaluation(){
		return $this->m_pel_evaluation;
    }
    public function get_m_pel_suggestion(){
		return $this->m_pel_suggestion;
    }
    public function get_m_pel_evaluate_To(){
		return $this->m_pel_evaluate_To;
    }
	public function get_m_pel_is_appeal(){
		return $this->m_pel_is_appeal;
	}
	public function get_m_pel_is_contactus(){
		return $this->m_pel_is_contactus;
	}
	public function get_m_pel_appeal_content(){
		return $this->m_pel_appeal_content;
	}
	public function get_m_pel_type(){
		return $this->m_pel_type;
	}
	public function get_m_pel_create_datetime(){
		return $this->m_pel_create_datetime;
	}
	public function get_array($select=""){
		if($select=="")
			$select=$this->B_ALL;
    	$return_array=array();
		if(($select&$this->B_m_pel_sn)!=0){
			$return_array["m_pel_sn"]=$this->get_m_pel_sn();
		}
		if(($select&$this->B_m_pel_id)!=0){
			$return_array["m_pel_id"]=$this->get_m_pel_id();
		}
		if(($select&$this->B_m_pl_id)!=0){
			$return_array["m_pl_id"]=$this->get_m_pl_id();
		}
		if(($select&$this->B_m_bkl_id)!=0){
			$return_array["m_bkl_id"]=$this->get_m_bkl_id();
		}
		if(($select&$this->B_m_pel_score)!=0){
			$return_array["m_pel_score"]=$this->get_m_pel_score();
		}
		if(($select&$this->B_m_pel_evaluation)!=0){
			$return_array["m_pel_evaluation"]=$this->get_m_pel_evaluation();
		}
		if(($select&$this->B_m_pel_suggestion)!=0){
			$return_array["m_pel_suggestion"]=$this->get_m_pel_suggestion();
		}
		if(($select&$this->B_m_pel_evaluate_To)!=0){
			$return_array["m_pel_evaluate_To"]=$this->get_m_pel_evaluate_To();
		}
		if(($select&$this->B_m_pel_is_appeal)!=0){
			$return_array["m_pel_is_appeal"]=$this->get_m_pel_is_appeal();
		}
		if(($select&$this->B_m_pel_is_contactus)!=0){
			$return_array["m_pel_is_contactus"]=$this->get_m_pel_is_contactus();
		}
		if(($select&$this->B_m_pel_type)!=0){
			$return_array["m_pel_type"]=$this->get_m_pel_type();
		}
		if(($select&$this->B_m_pel_create_datetime)!=0){
			$return_array["m_pel_create_datetime"]=$this->get_m_pel_create_datetime();
		}
		return $return_array;
	}
}
?>