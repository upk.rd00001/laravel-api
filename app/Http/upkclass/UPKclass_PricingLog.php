<?php
include_once("UPKclass_ParkingSpace.php");
include_once("UPKclass_BookingLog.php");
include_once("UPKclass_TimeComponet.php");
class UPK_PricingLog extends TimeComponent
{// extends TimeComponent
	private $m_ppl_sn;
	private $m_ppl_id;
	private $m_ppl_group_id;
	private $m_pk_id;
	private $m_ppl_create_datetime;
	private $m_ppl_price_points;
	private $m_ppl_price_points_free;
	private $m_ppl_price_type;
	private $m_ppl_price_member_description;
	private $m_ppl_start_date;
	private $m_ppl_end_date;
	private $m_ppl_start_time;
	private $m_ppl_end_time;
	private $m_ppl_delete;
	private $m_ppl_daily_max_price;
	private $m_id_create;
	public $B_m_ppl_sn = 1;
	public $B_m_ppl_id = 2;
	public $B_m_ppl_group_id = 4;
	public $B_m_pk_id = 8;
	public $B_m_ppl_create_datetime = 16;
	public $B_m_ppl_price_points = 32;
	public $B_m_ppl_price_points_free = 64;
	public $B_m_ppl_price_type = 128;
	public $B_m_ppl_price_member_description = 256;
	public $B_m_ppl_start_date = 512;
	public $B_m_ppl_end_date = 1024;
	public $B_m_ppl_start_time = 2048;
	public $B_m_ppl_end_time = 4096;
	public $B_m_ppl_delete = 8192;
	public $B_m_ppl_daily_max_price = 16384;
	public $B_m_id_create = 32768;
	public $B_ALL;

	private $bkl_total_page;
	private $bkl_total_cnt;
	public $booking_logs;
	public $parking_space;

	public function __construct($m_ppl_id, $sql_logic = "")
	{
		$this->B_ALL = $this->B_m_ppl_sn | $this->B_m_ppl_id | $this->B_m_ppl_group_id | $this->B_m_pk_id | $this->B_m_ppl_create_datetime | $this->B_m_ppl_price_points | $this->B_m_ppl_price_points_free | $this->B_m_ppl_price_type | $this->B_m_ppl_price_member_description | $this->B_m_ppl_start_date | $this->B_m_ppl_end_date | $this->B_m_ppl_start_time | $this->B_m_ppl_end_time | $this->B_m_ppl_delete | $this->B_m_ppl_daily_max_price | $this->B_m_id_create;
		$this->init_ppl_id($m_ppl_id);
		$this->bkl_total_cnt = 0;
	}

	public function init_ppl_id($m_ppl_id, $sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql = "select * from tb_Member_ParkingSpace_Pricing_Log where m_ppl_id = '" . $m_ppl_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->m_ppl_sn = $ans["m_ppl_sn"];
			$this->m_ppl_id = $ans["m_ppl_id"];
			$this->m_ppl_group_id = $ans["m_ppl_group_id"];
			$this->m_pk_id = $ans["m_pk_id"];
			$this->m_ppl_create_datetime = $ans["m_ppl_create_datetime"];
			$this->m_ppl_price_points = $ans["m_ppl_price_points"];
			$this->m_ppl_price_points_free = $ans["m_ppl_price_points_free"];
			$this->m_ppl_price_type = $ans["m_ppl_price_type"];
			$this->m_ppl_price_member_description = $ans["m_ppl_price_member_description"];
			$this->m_ppl_start_date = $ans["m_ppl_start_date"];
			$this->m_ppl_end_date = $ans["m_ppl_end_date"];
			$this->m_ppl_start_time = $ans["m_ppl_start_time"];
			$this->m_ppl_end_time = $ans["m_ppl_end_time"];
			$this->m_ppl_delete = $ans["m_ppl_delete"];
			$this->m_ppl_daily_max_price = $ans["m_ppl_daily_max_price"];
			$this->m_id_create = $ans["m_id_create"];
			parent::__construct($this->m_ppl_start_date, $this->m_ppl_end_date, $this->m_ppl_start_time, $this->m_ppl_end_time);
		}
		else if (mysql_num_rows($result) == 0) {
			return json_encode(array("result" => 0, "無此空位時段ID" => $m_ppl_id));
		}
		else {
			//多個相同ID
			return json_encode(array("result" => 0, "有多個相同空位時段ID" => $m_ppl_id));
		}
	}

	public function GetBookingLog($sql_logic = " AND tb_Member_Deposit.m_dpt_pay_datetime IS NOT NULL ", $sql_join = "", $args = array())
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";

		$this->ppl_total_page = 1;
		$args["sql_logic"]=$sql_logic;
		$args["sql_join"]=$sql_join;
		$sql = $this->GetBookingLogSql($args);
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return false;
		}
		else if (mysql_num_rows($result) == 0) {
			$this->booking_logs = array();
			return true;
		}
		$temp_booking_logs_array = array();
		$memcache = get_memcache();
		while ($ans = mysql_fetch_assoc($result)) {
			$tmp_booking_log = false;
			if ($memcache) {
				$tmp_booking_log = $memcache->get($dbName . '_UPK_BookingLog:' . $ans["unq_m_bkl_group_id"]);
			}
			else {
				#記憶體快取不可用
			}
			if (!$tmp_booking_log) {
				$tmp_booking_log = MemcacheSetBookingLog('_UPK_BookingLog:', $ans["unq_m_bkl_group_id"], $memcache);
			}

			array_push($temp_booking_logs_array, $tmp_booking_log);
		}
		$this->booking_logs = $temp_booking_logs_array;
		return true;
	}

	private function GetBookingLogSql($args = array()) {
		$is_already_out=2;
		if(isset($args["is_already_out"]))
			$is_already_out=$args["is_already_out"];
		$sql_logic="";
		if(isset($args["sql_logic"]))
			$sql_logic=$args["sql_logic"];
		$sql_join="";
		if(isset($args["sql_join"]))
			$sql_join=$args["sql_join"];
		if(isset($args["is_count"]))
			$is_count=$args["is_count"];
		else $is_count = false;
		$limit_sql = "";
		if (isset($args["page"]) && isset($args["page_count"])) {
			$page = $args["page"];
			$page_count = $args["page_count"];
			$limit_sql = " LIMIT " . (($page - 1) * $page_count) . "," . $page_count . " ";
			$this->get_bkl_total_cnt($args);
			$this->bkl_total_page = ceil($this->bkl_total_cnt / $page_count);
		}
		$sql_selector = " IF((m_bkl_group_id!='' AND m_bkl_is_paid='1'),m_bkl_group_id ,tb_Member_ParkingSpace_Booking_Log.m_bkl_id) AS unq_m_bkl_group_id ";
		if($is_count){
			$sql_selector=" count(DISTINCT(IF((m_bkl_group_id!='' AND m_bkl_is_paid='1'),m_bkl_group_id ,tb_Member_ParkingSpace_Booking_Log.m_bkl_id))) as cnt";
		}
		$sql = "SELECT ".$sql_selector." FROM tb_Member_ParkingSpace_Booking_Log LEFT JOIN tb_Member_Deposit ON tb_Member_Deposit.m_bkl_id=tb_Member_ParkingSpace_Booking_Log.m_bkl_id ";
		$sql .= $sql_join . " ";
		if($is_already_out===0 || $is_already_out===1)
			$sql .= " LEFT JOIN tb_Member_Parking_Log ON tb_Member_Parking_Log.m_bkl_id=IF((m_bkl_group_id!='' AND m_bkl_is_paid='1'),m_bkl_group_id ,tb_Member_ParkingSpace_Booking_Log.m_bkl_id) ";
		$sql .= " WHERE tb_Member_ParkingSpace_Booking_Log.m_ppl_id = '" . $this->m_ppl_id . "' ";
		$sql .= $sql_logic;
		if($is_already_out===0)
			$sql .= "  AND tb_Member_Parking_Log.m_pl_end_time IS NULL  ";
		elseif($is_already_out===1)
			$sql .= "  AND tb_Member_Parking_Log.m_pl_end_time IS NOT NULL  ";
		$sql .= " AND tb_Member_Deposit.m_dpt_pay_datetime IS NOT NULL ";
		if($is_count){
			//不能group by因為在selector的時候用了DISTINCT 當然不用limit
		}
		else {
			$sql .= "  GROUP BY unq_m_bkl_group_id ";
			$sql .= $limit_sql;
		}
		return $sql;
	}
	public function GetParkingSpace($sql_logic = "", $memcache = false)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$tmp_parking_space = null;
		$mem_conn_flag = false;
		if (!$memcache) {
			include("memcache_connect.php");
		}
		if ($memcache) {
			$tmp_parking_space = $memcache->get($dbName . '_UPK_ParkingSpace:' . $this->m_pk_id);
		}
		else {
			#記憶體快取不可用 先不做事反正都要重撈
		}
		if (!$tmp_parking_space) {
			$tmp_parking_space = MemcacheSetParkingSpace('_UPK_ParkingSpace:', $this->m_pk_id, $memcache);
		}
		if ($memcache && $mem_conn_flag)//不是從輸入進來的
			memcache_close($memcache);
		$this->parking_space = $tmp_parking_space;
		return (array("result" => 1));
	}

	public function text()//__toString()
	{
		return array(
			"m_ppl_id" => $this->m_ppl_id,
			"m_ppl_group_id" => $this->m_ppl_group_id,
			"m_ppl_start_date" => $this->m_ppl_start_date,
			"m_ppl_end_date" => $this->m_ppl_end_date,
			"m_ppl_start_time" => substr($this->m_ppl_start_time, 0, -3),
			"m_ppl_end_time" => substr($this->m_ppl_end_time, 0, -3),
			"m_ppl_price_type" => $this->m_ppl_price_type,
			"m_ppl_delete" => $this->m_ppl_delete,
			"m_ppl_price_points" => $this->m_ppl_price_points,
			"m_ppl_price_points_free" => $this->m_ppl_price_points_free,
			"m_ppl_daily_max_price" => $this->m_ppl_daily_max_price,
			"m_ppl_price_member_description" => $this->m_ppl_price_member_description
		);
	}

	public function get_m_ppl_sn()
	{
		return $this->m_ppl_sn;
	}

	public function get_m_ppl_id()
	{
		return $this->m_ppl_id;
	}

	public function get_m_ppl_group_id()
	{
		return $this->m_ppl_group_id;
	}

	public function get_m_pk_id()
	{
		return $this->m_pk_id;
	}

	public function get_m_ppl_create_datetime()
	{
		return substr($this->m_ppl_create_datetime, 0, -3);
		//return $this->m_ppl_create_datetime;
	}

	public function get_m_ppl_price_points()
	{
		return $this->m_ppl_price_points;
	}

	public function get_m_ppl_price_points_free()
	{
		return $this->m_ppl_price_points_free;
	}

	public function get_m_ppl_price_type()
	{
		return $this->m_ppl_price_type;
	}

	public function get_m_ppl_price_member_description()
	{
		return $this->m_ppl_price_member_description;
	}

	public function get_m_ppl_start_date()
	{
		return $this->m_ppl_start_date;
	}

	public function get_m_ppl_end_date()
	{
		return $this->m_ppl_end_date;
	}

	public function get_m_ppl_start_time()
	{
		return substr($this->m_ppl_start_time, 0, -3);
		//return $this->m_ppl_start_time;
	}

	public function get_m_ppl_end_time()
	{
		return substr($this->m_ppl_end_time, 0, -3);
		//return $this->m_ppl_end_time;
	}

	public function get_m_ppl_delete()
	{
		return $this->m_ppl_delete;
	}

	public function get_m_ppl_daily_max_price()
	{
		return $this->m_ppl_daily_max_price;
	}

	public function get_m_id_create()
	{
		return $this->m_id_create;
	}

	public function get_bkl_total_page()
	{
		return $this->bkl_total_page;
	}

	public function set_bkl_total_cnt($args)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		unset($args["page"]);
		unset($args["page_count"]);
		$args["is_count"] = true;
		$sql = $this->GetBookingLogSql($args);
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return false;
		}
		$ans = mysql_fetch_assoc($result);
		$this->bkl_total_cnt = $ans["cnt"];
		return true;
	}

	public function get_bkl_total_cnt($args=array())
	{
		$this->set_bkl_total_cnt($args);
		return $this->bkl_total_cnt;
	}

	public function DisplayParkingSpace()
	{
		if (isset($this->parking_spaces))
			return $this->parking_spaces->get_array($this->parking_spaces->B_ALL);
		return null;
	}

	public function get_array($select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_m_ppl_sn) != 0) {
			$return_array["m_ppl_sn"] = $this->get_m_ppl_sn();
		}
		if (($select & $this->B_m_ppl_id) != 0) {
			$return_array["m_ppl_id"] = $this->get_m_ppl_id();
		}
		if (($select & $this->B_m_ppl_group_id) != 0) {
			$return_array["m_ppl_group_id"] = $this->get_m_ppl_group_id();
		}
		if (($select & $this->B_m_pk_id) != 0) {
			$return_array["m_pk_id"] = $this->get_m_pk_id();
		}
		if (($select & $this->B_m_ppl_create_datetime) != 0) {
			$return_array["m_ppl_create_datetime"] = $this->get_m_ppl_create_datetime();
		}
		if (($select & $this->B_m_ppl_price_points) != 0) {
			$return_array["m_ppl_price_points"] = $this->get_m_ppl_price_points();
		}
		if (($select & $this->B_m_ppl_price_points_free) != 0) {
			$return_array["m_ppl_price_points_free"] = $this->get_m_ppl_price_points_free();
		}
		if (($select & $this->B_m_ppl_price_type) != 0) {
			$return_array["m_ppl_price_type"] = $this->get_m_ppl_price_type();
		}
		if (($select & $this->B_m_ppl_price_member_description) != 0) {
			$return_array["m_ppl_price_member_description"] = $this->get_m_ppl_price_member_description();
		}
		if (($select & $this->B_m_ppl_start_date) != 0) {
			$return_array["m_ppl_start_date"] = $this->get_m_ppl_start_date();
		}
		if (($select & $this->B_m_ppl_end_date) != 0) {
			$return_array["m_ppl_end_date"] = $this->get_m_ppl_end_date();
		}
		if (($select & $this->B_m_ppl_start_time) != 0) {
			$return_array["m_ppl_start_time"] = $this->get_m_ppl_start_time();
		}
		if (($select & $this->B_m_ppl_end_time) != 0) {
			$return_array["m_ppl_end_time"] = $this->get_m_ppl_end_time();
		}
		if (($select & $this->B_m_ppl_delete) != 0) {
			$return_array["m_ppl_delete"] = $this->get_m_ppl_delete();
		}
		if (($select & $this->B_m_ppl_daily_max_price) != 0) {
			$return_array["m_ppl_daily_max_price"] = $this->get_m_ppl_daily_max_price();
		}
		if (($select & $this->B_m_id_create) != 0) {
			$return_array["m_id_create"] = $this->get_m_id_create();
		}
		return $return_array;
	}
}

?>
