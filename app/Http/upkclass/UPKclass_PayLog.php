<?php


class UPK_PayLog {
	private $m_pyl_sn;
	public $m_pyl_id;
	private $m_bkl_id;
	private $m_pk_id;
	private $m_ppl_id;
	private $m_ppl_promotion_code;
	private $m_ve_id;
	private $m_id;
	public $m_pyl_point;
	public $m_pyl_point_free;
	private $m_pyl_origin_amount;
	private $m_pyl_update_amount;
	private $m_pyl_origin_amount_free;
	private $m_pyl_update_amount_free;
	private $m_pyl_type;
	private $m_pyl_datetime;
	public function __construct($m_pyl_id,$sql_logic=""){
		if($m_pyl_id==" "||$m_pyl_id=="")return;
		$this->init_pyl_id($m_pyl_id,$sql_logic="");
		$this->B_ID =$this->B_m_pyl_id | $this->B_m_bkl_id | $this->B_m_pk_id | $this->B_m_ppl_id | $this->B_m_ppl_promotion_code | $this->B_m_ve_id | $this->B_m_id; 
		$this->B_BODY =$this->B_m_pyl_point | $this->B_m_pyl_point_free | $this->B_m_pyl_origin_amount | $this->B_m_pyl_update_amount | $this->B_m_pyl_origin_amount_free | $this->B_m_pyl_update_amount_free | $this->B_m_pyl_type | $this->B_m_pyl_datetime; 
		$this->B_ALL =$this->B_m_pyl_sn | $this->B_ID | $this->B_BODY; 
		$this->init_pyl_id($m_pyl_id,$sql_logic="");
    }
    
    public function init_pyl_id($m_pyl_id,$sql_logic=""){

		global $conn,$dbName;
		check_conn($conn,$dbName);
		$language = "zh-tw";
		$sql="select * from tb_Member_Pay_Log where m_pyl_id = '".$m_pyl_id."' ";
		$sql.=$sql_logic;
		$result = mysql_query($sql, $conn);
		if(! $result){
			return json_encode(array("result"=>0,"失敗" => mysql_error($conn).$sql));
		}else if(mysql_num_rows($result) == 1)
		{
			$ans=mysql_fetch_assoc($result);
			$this->m_pyl_sn=$ans["m_pyl_sn"];
			$this->m_pyl_id=$ans["m_pyl_id"];
			$this->m_bkl_id=$ans["m_bkl_id"];
			$this->m_pk_id=$ans["m_pk_id"];
			$this->m_ppl_id=$ans["m_ppl_id"];
			$this->m_ppl_promotion_code=$ans["m_ppl_promotion_code"];
			$this->m_ve_id=$ans["m_ve_id"];
			$this->m_id=$ans["m_id"];
			$this->m_pyl_point=$ans["m_pyl_point"];
			$this->m_pyl_point_free=$ans["m_pyl_point_free"];
			$this->m_pyl_origin_amount=$ans["m_pyl_origin_amount"];
			$this->m_pyl_update_amount=$ans["m_pyl_update_amount"];
			$this->m_pyl_origin_amount_free=$ans["m_pyl_origin_amount_free"];
			$this->m_pyl_update_amount_free=$ans["m_pyl_update_amount_free"];
			$this->m_pyl_type=$ans["m_pyl_type"];
			$this->m_pyl_datetime=$ans["m_pyl_datetime"];
		}
		else{
		//多個相同ID
			return json_encode(array("result"=>0,"有多個相同付費紀錄ID" => mysql_error($conn).$sql));
		}
    }
public $B_m_pyl_sn		=1;
public $B_m_pyl_id		=2;
public $B_m_bkl_id		=4;
public $B_m_pk_id		=8;
public $B_m_ppl_id		=16;
public $B_m_ppl_promotion_code		=32;
public $B_m_ve_id		=64;
public $B_m_id		=128;
public $B_m_pyl_point		=256;
public $B_m_pyl_point_free		=512;
public $B_m_pyl_origin_amount		=1024;
public $B_m_pyl_update_amount		=2048;
public $B_m_pyl_origin_amount_free		=4096;
public $B_m_pyl_update_amount_free		=8192;
public $B_m_pyl_type		=16384;
public $B_m_pyl_datetime		=32768;
public $B_ALL;
public $B_ID;
public $B_BODY;


public function get_m_pyl_sn(){
	return $this->m_pyl_sn;
}
public function get_m_pyl_id(){
	return $this->m_pyl_id;
}
public function get_m_bkl_id(){
	return $this->m_bkl_id;
}
public function get_m_pk_id(){
	return $this->m_pk_id;
}
public function get_m_ppl_id(){
	return $this->m_ppl_id;
}
public function get_m_ppl_promotion_code(){
	return $this->m_ppl_promotion_code;
}
public function get_m_ve_id(){
	return $this->m_ve_id;
}
public function get_m_id(){
	return $this->m_id;
}
public function get_m_pyl_point(){
	return $this->m_pyl_point;
}
public function get_m_pyl_point_free(){
	return $this->m_pyl_point_free;
}
public function get_m_pyl_origin_amount(){
	return $this->m_pyl_origin_amount;
}
public function get_m_pyl_update_amount(){
	return $this->m_pyl_update_amount;
}
public function get_m_pyl_origin_amount_free(){
	return $this->m_pyl_origin_amount_free;
}
public function get_m_pyl_update_amount_free(){
	return $this->m_pyl_update_amount_free;
}
public function get_m_pyl_type(){
	return $this->m_pyl_type;
}
public function get_m_pyl_datetime(){
	return $this->m_pyl_datetime;
}
public function get_array($select=""){
	if($select=="")
		$select=$this->B_ALL;
	$return_array=array();
	if(($select & $this->B_m_pyl_sn) != 0 ){
		$return_array["m_pyl_sn"]=$this->get_m_pyl_sn();
	}
	if(($select & $this->B_m_pyl_id) != 0 ){
		$return_array["m_pyl_id"]=$this->get_m_pyl_id();
	}
	if(($select & $this->B_m_bkl_id) != 0 ){
		$return_array["m_bkl_id"]=$this->get_m_bkl_id();
	}
	if(($select & $this->B_m_pk_id) != 0 ){
		$return_array["m_pk_id"]=$this->get_m_pk_id();
	}
	if(($select & $this->B_m_ppl_id) != 0 ){
		$return_array["m_ppl_id"]=$this->get_m_ppl_id();
	}
	if(($select & $this->B_m_ppl_promotion_code) != 0 ){
		$return_array["m_ppl_promotion_code"]=$this->get_m_ppl_promotion_code();
	}
	if(($select & $this->B_m_ve_id) != 0 ){
		$return_array["m_ve_id"]=$this->get_m_ve_id();
	}
	if(($select & $this->B_m_id) != 0 ){
		$return_array["m_id"]=$this->get_m_id();
	}
	if(($select & $this->B_m_pyl_point) != 0 ){
		$return_array["m_pyl_point"]=$this->get_m_pyl_point();
	}
	if(($select & $this->B_m_pyl_point_free) != 0 ){
		$return_array["m_pyl_point_free"]=$this->get_m_pyl_point_free();
	}
	if(($select & $this->B_m_pyl_origin_amount) != 0 ){
		$return_array["m_pyl_origin_amount"]=$this->get_m_pyl_origin_amount();
	}
	if(($select & $this->B_m_pyl_update_amount) != 0 ){
		$return_array["m_pyl_update_amount"]=$this->get_m_pyl_update_amount();
	}
	if(($select & $this->B_m_pyl_origin_amount_free) != 0 ){
		$return_array["m_pyl_origin_amount_free"]=$this->get_m_pyl_origin_amount_free();
	}
	if(($select & $this->B_m_pyl_update_amount_free) != 0 ){
		$return_array["m_pyl_update_amount_free"]=$this->get_m_pyl_update_amount_free();
	}
	if(($select & $this->B_m_pyl_type) != 0 ){
		$return_array["m_pyl_type"]=$this->get_m_pyl_type();
	}
	if(($select & $this->B_m_pyl_datetime) != 0 ){
		$return_array["m_pyl_datetime"]=$this->get_m_pyl_datetime();
	}
	return $return_array;
}
}
?>