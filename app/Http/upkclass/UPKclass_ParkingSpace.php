<?php

include_once("UPKclass_BookingLog.php");
include_once("UPKclass_PricingLog.php");
include_once("UPKclass_GovFareKindParkingSpace.php");

class UPK_ParkingSpace
{//extends UPK_PricinigLog
	private $m_pk_sn;
	private $m_pk_id;
	private $m_plots_id;
	private $m_id;
	private $pkt_id;
	private $m_pk_place_type;
	private $m_pk_delete;
	private $m_pk_address_country;
	private $m_pk_address_post_code;
	private $m_pk_address_state;
	private $m_pk_address_city;
	private $m_pk_address_line1;
	private $m_pk_address_line2;
	private $m_pk_address_line3;
	private $m_pk_floor;
	private $m_pk_number;
	private $m_pk_community_name;
	private $m_pk_only_same_community;
	private $m_pk_lng;
	private $m_pk_lat;
	private $m_pk_alt;
	private $m_pk_right_enable;
	private $m_pk_inout_way;
	private $m_pk_owner;
	private $m_pk_inout_side;
	private $m_pk_length;
	private $m_pk_width;
	private $m_pk_height;
	private $m_pk_weight;
	private $m_pk_agent_m_id;
	private $m_pk_agent_number;
	private $m_pk_owner_m_id;
	private $m_pk_owner_number;
	private $m_pk_developer_m_id;
	private $m_pk_max_discount;
	private $m_pk_max_free_discount;
	private $m_is_close;
	private $m_notification_enable;
	private $m_pk_create_datetime;
	private $m_pk_last_edit_datetime;
	private $m_pk_free_time;
	private $m_pk_disabled_free_time;
	private $m_pk_severe_disabled_free_time;
	//自訂義參數
	private $m_pk_rating;
	private $m_pk_gov_pricing_desc;
	private $ppl_total_page;
	private $ppl_total_cnt;

	public $B_m_pk_sn = 1;
	public $B_m_pk_id = 2;
	public $B_m_plots_id = 4;
	public $B_m_id = 8;
	public $B_pkt_id = 16;
	public $B_m_pk_place_type = 32;
	public $B_m_pk_delete = 64;
	/* //數字不夠用 合併address
	public $B_m_pk_address_country		=128;
	public $B_m_pk_address_post_code		=256;
	public $B_m_pk_address_state		=512;
	public $B_m_pk_address_city		=1024;
	public $B_m_pk_address_line1		=2048;
	public $B_m_pk_address_line2		=4096;
	public $B_m_pk_address_line3		=8192;
	*/
	public $B_m_pk_address = 128;
//	public $B_m_pk_agent_m_id		=256;
//	public $B_m_pk_owner_m_id		=512;
	public $B_m_pk_agent = 256;
	public $B_m_pk_owner = 512;
	public $B_m_pk_developer_m_id = 1024;
	public $B_m_is_close = 2048;
	public $B_m_notification_enable = 4096;

	public $B_m_pk_rating = 8192;

	public $B_m_pk_floor = 16384;
	public $B_m_pk_number = 32768;
	public $B_m_pk_community_name = 65536;
	public $B_m_pk_only_same_community = 131072;
	public $B_m_pk_lng = 262144;
	public $B_m_pk_lat = 524288;
	public $B_m_pk_alt = 1048576;
	public $B_m_pk_right_enable = 2097152;
	public $B_m_pk_inout_way = 4194304;
	//public $B_m_pk_owner		=8388608;
	public $B_m_pk_inout_side = 16777216;
	public $B_m_pk_size = 33554432;
	public $B_m_pk_edit_datetime = 134217728;
	//public $B_m_pk_length		=33554432;
	//public $B_m_pk_width		=67108864;
	//public $B_m_pk_height		=134217728;
	//public $B_m_pk_weight		=268435456;

	//public $B_m_pk_agent_number		=536870912;
	//public $B_m_pk_owner_number		=1073741824;
	public $B_m_pk_max_discount = 536870912;
	public $B_m_pk_max_free_discount = 1073741824;
	public $B_m_pk_file_logs = 8388608;

	/*//溢位部分捕到砍掉的Address裡面
		public $B_m_pk_agent_m_id		=536870912;
		public $B_m_pk_owner_m_id		=1073741824;
		public $B_m_pk_developer_m_id		=-2147483648;
		public $B_m_is_close		=1;
	*/
	public $B_m_pk_x_free_time = 67108864;
	/*	public $B_m_pk_free_time = 67108864;
		public $B_m_pk_disabled_free_time = 134217728;
		public $B_m_pk_severe_disabled_free_time = 268435456;
	*/
//const
	public $C_PARKING_SPACE_DETAIL = 1; //get_ppl_cnt

	public $B_ALL;
	private $S_m_pk_sn = "m_pk_sn";
	private $S_m_pk_id = "m_pk_id";
	private $S_m_plots_id = "m_plots_id";
	private $S_m_id = "m_id";
	private $S_pkt_id = "pkt_id";
	private $S_m_pk_place_type = "m_pk_place_type";
	private $S_m_pk_delete = "m_pk_delete";
	private $S_m_pk_address_country = "m_pk_address_country";
	private $S_m_pk_address_post_code = "m_pk_address_post_code";
	private $S_m_pk_address_state = "m_pk_address_state";
	private $S_m_pk_address_city = "m_pk_address_city";
	private $S_m_pk_address_line1 = "m_pk_address_line1";
	private $S_m_pk_address_line2 = "m_pk_address_line2";
	private $S_m_pk_address_line3 = "m_pk_address_line3";
	private $S_m_pk_floor = "m_pk_floor";
	private $S_m_pk_number = "m_pk_number";
	private $S_m_pk_community_name = "m_pk_community_name";
	private $S_m_pk_only_same_community = "m_pk_only_same_community";
	private $S_m_pk_lng = "m_pk_lng";
	private $S_m_pk_lat = "m_pk_lat";
	private $S_m_pk_alt = "m_pk_alt";
	private $S_m_pk_right_enable = "m_pk_right_enable";
	private $S_m_pk_inout_way = "m_pk_inout_way";
	private $S_m_pk_owner = "m_pk_owner";
	private $S_m_pk_inout_side = "m_pk_inout_side";
	private $S_m_pk_length = "m_pk_length";
	private $S_m_pk_width = "m_pk_width";
	private $S_m_pk_height = "m_pk_height";
	private $S_m_pk_weight = "m_pk_weight";
	private $S_m_pk_agent_m_id = "m_pk_agent_m_id";
	private $S_m_pk_agent_number = "m_pk_agent_number";
	private $S_m_pk_owner_m_id = "m_pk_owner_m_id";
	private $S_m_pk_owner_number = "m_pk_owner_number";
	private $S_m_pk_developer_m_id = "m_pk_developer_m_id";
	private $S_m_pk_max_discount = "m_pk_max_discount";
	private $S_m_pk_file_logs = "m_pk_file_logs";
	private $S_m_pk_max_free_discount = "m_pk_max_free_discount";
	private $S_m_is_close = "m_is_close";
	private $S_m_notification_enable = "m_notification_enable";
	private $S_m_pk_create_datetime = "m_pk_create_datetime";
	private $S_m_pk_last_edit_datetime = "m_pk_last_edit_datetime";
	private $S_m_pk_free_time = "m_pk_free_time";
	private $S_m_pk_disabled_free_time = "m_pk_disabled_free_time";
	private $S_m_pk_severe_disabled_free_time = "m_pk_severe_disabled_free_time";

	/* @var $parking_lot UPK_ParkingLot */
	public $parking_lot;
	public $pricing_logs;
	public $booking_logs;
	public $parking_logs;
	public $file_logs;
	public $gov_farekind_parkingspaces;
	public $gov_farekind_now;

	public function __construct($m_pk_id, $sql_logic = "", $select = "")
	{
		$this->B_ALL = $this->B_m_pk_sn | $this->B_m_pk_id | $this->B_m_plots_id | $this->B_m_id | $this->B_pkt_id | $this->B_m_pk_place_type | $this->B_m_pk_delete | $this->B_m_pk_address | $this->B_m_pk_floor | $this->B_m_pk_number | $this->B_m_pk_community_name | $this->B_m_pk_only_same_community | $this->B_m_pk_lng | $this->B_m_pk_lat | $this->B_m_pk_alt | $this->B_m_pk_right_enable | $this->B_m_pk_inout_way | $this->B_m_pk_owner | $this->B_m_pk_inout_side | $this->B_m_pk_size | $this->B_m_pk_agent | $this->B_m_pk_owner | $this->B_m_pk_developer_m_id | $this->B_m_is_close | $this->B_m_notification_enable | $this->B_m_pk_rating | $this->B_m_pk_max_discount | $this->B_m_pk_max_free_discount | $this->B_m_pk_x_free_time | $this->B_m_pk_edit_datetime | $this->B_m_pk_file_logs;
		$this->init_pk_id($m_pk_id, $sql_logic = "", $select);
		$this->ppl_total_cnt = 0;
	}

	public function init_pk_id($m_pk_id, $sql_logic = "", $select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		$select = $select | $this->B_m_pk_id;//一定要有pk_id後面要判斷是否路邊要自己去撈路段資料
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql_select = "";
		$sql_select = $this->generate_sql_string($select);
		$sql = "select " . $sql_select . " from tb_Member_ParkingSpace where m_pk_id = '" . $m_pk_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->put_data_to_class($ans, $select);

		}
		else if (mysql_num_rows($result) == 0) {
			return json_encode(array("result" => 0, "無此車位ID" => $m_pk_id));
		}
		else {
			//多個相同ID
			return json_encode(array("result" => 0, "有多個相同停車位ID" => $m_pk_id));
		}
		return 1;
	}


	public function GetFileLog($token)
	{
		/*include_once("/../select_api/GetFileLogFunc.php");
		$ttans=json_decode(GetFileLogFunc("GET FILE LOG",$token,"6",$this->m_pk_id),true);
		//$tmp["file_logs"]=$ttans;
		if($ttans["result"]==0)
			return 0;
		$this->file_logs = $ttans["data"];*/
		return 1;
	}

	//memcache 代表是否需要重新連線, false = 要重新連線, true = 不要重新連線
	public function GetParkingLot($sql_logic = "", $memcache = false)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$tmp_parking_lot = null;
		$memcache = get_memcache();
		if ($memcache) {
			//TODO:
			//		$tmp_parking_lot=$memcache->get($dbName.'_UPK_ParkingLot:'.$this->m_plots_id);  //Luke註解後似乎正常
		}
		else {
			#記憶體快取不可用 先不做事反正都要重撈
		}
		if (!$tmp_parking_lot) {
			$tmp_parking_lot = MemcacheSetParkingLot('_UPK_ParkingLot:', $this->m_plots_id, $memcache);
		}
		$this->parking_lot = $tmp_parking_lot;
		return (array("result" => 1));
	}

	public function GetFareKindParkingSpace($sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql = "select * from tb_Gov_FareKind_ParkingSpace where m_pk_id = '" . $this->m_pk_id . "' AND is_disable='0' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) == 0) {
			//若無車位的費率則使用停車場的費率
			$sql = "select * from tb_Gov_FareKind_ParkingSpace where m_plots_id = '" . $this->m_plots_id . "' AND is_disable='0' ";
			$sql .= $sql_logic;
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "失敗" => mysql_error($conn) . $sql));
			}
			else if (mysql_num_rows($result) == 0) {
				$this->gov_farekind_parkingspace = array();
				return 0;
			}
		}
		$temp_gov_farekind_parkingspace_array = array();
		$sn = "";
		$this->m_pk_gov_pricing_desc = "";
		while ($ans = mysql_fetch_assoc($result)) {
			$sn = $ans["sn"];
			$tmp_gov_farekind_parkingspace = new UPK_GovFareKindParkingSpace($ans["sn"]);
			array_push($temp_gov_farekind_parkingspace_array, $tmp_gov_farekind_parkingspace);
			$this->m_pk_gov_pricing_desc .= $tmp_gov_farekind_parkingspace->get_fare_desc() . " ; ";
			if ($tmp_gov_farekind_parkingspace->is_charge(new DateTime()) == true) {
				$this->gov_farekind_now = $tmp_gov_farekind_parkingspace;
			}
		}
		$this->m_pk_gov_pricing_desc = substr($this->m_pk_gov_pricing_desc, 0, -3);
		$this->gov_farekind_parkingspaces = $temp_gov_farekind_parkingspace_array;
		return 1;
	}

	public function GetPricingLog($sql_logic = "", $sql_join = "", $args = array())
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$limit_sql = "";
		$orderby_sql = "";
		$this->ppl_total_page = 1;
		if (isset($args["orderby"])) {
			$orderby_sql = " ORDER BY m_ppl_sn DESC ";
		}
		if (isset($args["page"]) && isset($args["page_count"])) {
			$page = $args["page"];
			$page_count = $args["page_count"];
			$limit_sql = " LIMIT " . (($page - 1) * $page_count) . "," . $page_count . " ";
			$this->get_ppl_total_cnt();
			$this->ppl_total_page = ceil($this->ppl_total_cnt / $page_count);
		}
		$sql = "SELECT * FROM tb_Member_ParkingSpace_Pricing_Log as tb_ppl";
		$sql .= $sql_join;
		$sql .= " WHERE tb_ppl.m_pk_id = '" . $this->m_pk_id . "' AND tb_ppl.m_ppl_delete='0' ";
		$sql .= $sql_logic;
		$sql .= $orderby_sql;
		$sql .= $limit_sql;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return false;
		}
		else if (mysql_num_rows($result) == 0) {
			$this->pricing_logs = array();
			return true;
		}
		$temp_pricing_logs_array = array();
		$old_pl_id = "";
		while ($ans = mysql_fetch_assoc($result)) {
			//if($old_pl_id!=$ans["m_ppl_id"])
			{
				$old_pl_id = $ans["m_ppl_id"];
				$tmp_pricing_log = new UPK_PricingLog($ans["m_ppl_id"]);

				array_push($temp_pricing_logs_array, $tmp_pricing_log);
			}
		}
		$this->pricing_logs = $temp_pricing_logs_array;
		return true;
	}

	//下了時間篩選的pricing log
	public function GetPricingLog_v2($start_date, $end_date, $start_time, $end_time, $book_type = 1)
	{
		if ($this->parking_lot == null) {
			$this->GetParkingLot();
		}
		$generate_sql_ppl = new TimeComponent("", "", "", "", "");
		$generate_sql_ppl->sql_field_name_init("m_ppl_start_date", "m_ppl_end_date", "m_ppl_start_time", "m_ppl_end_time");
		if ($this->parking_lot->get_m_plots_type() == 100) {
			#路邊車位雖然沒有開放空位時段，但如果有人預約則會有，判斷該路邊車是否已經有人預約
			$this->GetPricingLog(" AND (" . $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "cross") . " OR " . $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "inside") . ") ");//搜尋該停車位所有的空位時段
		}
		elseif ($this->parking_lot->get_m_plots_type() == 200) {//可預約的路邊停車 // 2018-01-25 先讓兩個type都搜尋
			$this->GetPricingLog(" AND ((m_ppl_price_type='3000' OR m_ppl_price_type='1')  AND (" . $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "inside") . " OR !" . $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "outside") . ")) ");//搜尋該停車位所有的空位時段
		}
		else {
			//原來的邏輯
			$tmp_price_type = "";
			if ($book_type == "") {
				$tmp_price_type = "1";
			}
			else if ($book_type == "1") {
				$tmp_price_type = " m_ppl_price_type='" . $book_type . "' ";
			}
			else {
				//有可能要找月租的預約時段
				//$tmp_price_type = " (m_ppl_price_type='" . $book_type . "' OR m_ppl_price_type='1') ";
				//2019-01-19 Tiger: 因為在月租地圖業面時，會顯示30分的費率的數字 但是會寫每月10元，最後算出來是10*2*24*30的金額，所以月租地圖頁面時不顯示預約費率
				//未來再將這個if else拔掉
				$tmp_price_type = " m_ppl_price_type='" . $book_type . "' ";
			}

			$this->GetPricingLog(" AND (" . $tmp_price_type . " AND (" . $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "inside") . " OR !" . $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "outside") . ")) ");//搜尋該停車位所有的空位時段
		}
	}

	public function GetParkingLog($sql_logic = "", $sql_join = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql = "SELECT * FROM tb_Member_Parking_Log as tb_pl";
		$sql .= $sql_join;
		$sql .= " WHERE tb_pl.m_pk_id = '" . $this->m_pk_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) == 0) {
			$this->parking_logs = null;
			return 0;
		}
		$temp_parking_logs_array = array();
		while ($ans = mysql_fetch_assoc($result)) {
			$tmp_parking_log = new UPK_ParkingLog($ans["m_pl_id"]);
			array_push($temp_parking_logs_array, $tmp_parking_log);
		}
		$this->parking_logs = $temp_parking_logs_array;
		return 1;
	}

	public function GetBookingLog($sql_logic = "", $value = 0, $args = array())
	{
		$sql_orderby = "";
		$sql_limit = "";
		$sql_is_out = "";
		if (isset($args["orderby"]))
			$sql_orderby = " ORDER BY " . $args["orderby"] . " DESC";
		if (isset($args["limit"]))
			$sql_limit = $args["limit"];
		if (isset($args["m_bkl_cancel"])) {
			if ($args["m_bkl_cancel"] === 0) {
				$sql_logic .= " AND m_bkl_cancel='0' ";
			}
			elseif ($args["m_bkl_is_cancel"] === 1) {
				$sql_logic .= " AND m_bkl_cancel='1' ";
			}
		}
		if (isset($args["is_out"])) {
			if ($args["is_out"] === 0) {
				$sql_is_out = " AND m_pl_end_time IS NULL";
			}
			elseif ($args["is_out"] === 1) {
				$sql_is_out = " AND m_pl_end_time IS NOT NULL";
			}
		}

		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql = "SELECT u.* FROM (SELECT IF((m_bkl_group_id!='' AND m_bkl_is_paid='1'),m_bkl_group_id ,m_bkl_id) AS unq_m_bkl_group_id 
			FROM tb_Member_ParkingSpace_Booking_Log WHERE m_pk_id = '" . $this->m_pk_id . "' " . $sql_logic . " ) as u 
			LEFT JOIN tb_Member_Parking_Log ON u.unq_m_bkl_group_id=tb_Member_Parking_Log.m_bkl_id WHERE 1 " . $sql_is_out . " ";
		$sql .= $sql_orderby;
		$sql .= $sql_limit;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) == 0) {
			$this->booking_logs = null;
			return 0;
		}
		$temp_booking_logs_array = array();
		$old_bkl_id = "";
		$total_score = 0;
		$score_cnt = 0;
		while ($ans = mysql_fetch_assoc($result)) {
			//if($old_pl_id!=$ans["m_ppl_id"])
			{
				$unq_m_bkl_group_id = $ans["unq_m_bkl_group_id"];
				//計算此停車場的分數，並不影響預約結果輸出
				$sql_score = "select * from tb_Member_Parking_Evaluation_Log where m_bkl_id='" . $unq_m_bkl_group_id . "'";
				$result_score = mysql_query($sql_score, $conn);
				if (!$result_score) {
					return json_encode(array("result" => 0, "失敗" => mysql_error($conn) . $sql));
				}
				else if (mysql_num_rows($result_score) == 0) {
				}
				else {
					$ans = mysql_fetch_assoc($result_score);
					if ($ans["m_pel_score"] == 0) continue;
					$total_score += $ans["m_pel_score"];
					$score_cnt++;
				}
				$tmp_booking_log = new UPK_BookingLog($unq_m_bkl_group_id);
				array_push($temp_booking_logs_array, $tmp_booking_log);
			}
		}
		if ($score_cnt != 0)
			$this->m_pk_rating = round($total_score / $score_cnt, 1);

		$this->booking_logs = $temp_booking_logs_array;
		return 1;
	}

	public function text()//__toString()
	{
		return array(
			"m_pk_sn" => $this->m_pk_sn,
			"m_pk_id" => $this->m_pk_id,
			"m_plots_id" => $this->m_plots_id,
			"m_id" => $this->m_id,
			"pkt_id" => $this->pkt_id,
			"m_pk_length" => $this->m_pk_length,
			"m_pk_width" => $this->m_pk_width,
			"m_pk_height" => $this->m_pk_height,
			"m_pk_weight" => $this->m_pk_weight,
			"m_pk_owner_m_id" => $this->m_pk_owner_m_id,
			"m_pk_agent_m_id" => $this->m_pk_agent_m_id,
			"m_pk_inout_side" => $this->m_pk_inout_side,
			"m_pk_inout_way" => $this->m_pk_inout_way,
			"m_pk_floor" => $this->m_pk_floor,
			"m_pk_number" => $this->m_pk_number,
			"m_pk_community_name" => $this->m_pk_community_name,
			"m_pk_only_same_community" => $this->m_pk_only_same_community
		);
	}

	public function getSameCommunityDisplay($conn, $m_id)
	{

		if ($this->get_m_pk_only_same_community() == 1) {
			$sql = "SELECT * FROM tb_Member_Only_Same_Community WHERE m_osc_is_delete=0 AND
						m_osc_type=6 AND m_osc_type_id='" . $this->get_m_pk_id() . "'
						AND m_osc_m_id='" . $m_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return 0;
			}
			else if (mysql_num_rows($result) == 0) {
				#不再訪客名單內不顯示
				$pk_same_display = 0;
			}
			else {
				#否則就顯示
				$pk_same_display = 1;
			}
			$sql = "SELECT * FROM tb_Member_Only_Same_Community WHERE m_osc_is_delete=0 AND
						m_osc_type=6 AND m_osc_type_id='" . $this->get_m_pk_id() . "'";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return 0;
			}
			else if (mysql_num_rows($result) == 0) {
				#如果名單內空無一人就顯示
				$pk_same_display = 1;
			}
		}
		else {
			#不限鄰居訪客
			$pk_same_display = 1;
		}
		if ($this->get_m_pk_owner_m_id() == $m_id || $this->get_m_pk_agent_m_id() == $m_id) {
			#如果是車位主或代管人就顯示此車位
			$pk_same_display = 1;
		}
		else {
			if ($this->parking_lot == null)
				$this->GetParkingLot();
			$this_parking_lot = $this->parking_lot;
			$plots_same_display = $this_parking_lot->getSameCommunityDisplay($conn, $m_id);
			#否則近來判斷鄰居
			if ($plots_same_display == 0 && $pk_same_display == 0) {
				$pk_same_display = 0;
//				continue;
			}
			elseif ($plots_same_display == 0 && $pk_same_display == 1) {
				if ($this->get_m_pk_only_same_community() == 1) {
					//如果該會員有被人加入住戶
					$pk_same_display = 1;
				}
				else {
					//沒有則看停車場
					$pk_same_display = 0; // 因為停車場是0
//					continue;
				}
			}
			elseif ($plots_same_display == 1 && $pk_same_display == 0) {
				$pk_same_display = 0;
//				continue;
			}
			elseif ($plots_same_display == 1 && $pk_same_display == 1) {
				$pk_same_display = 1;
			}
		}
		return $pk_same_display;
	}

	public function getPurchaseName()
	{
		$purchase_name = "停車媒合服務費";
		if ($this->parking_lot->get_m_plots_type() == "0") {
			$purchase_name = $this->parking_lot->get_m_plots_name() . " " . $this->get_m_pk_floor(true) . " " . $this->get_m_pk_number() . "號";
			$purchase_name .= "停車媒合服務費";
		}
		else if ($this->parking_lot->get_m_plots_type() == "100") {
			//路邊要蓋掉描述
			$purchase_name = "易停網 新北市自助計費代收 " . $this->parking_lot->get_m_plots_name() . " 格號: " . $this->get_m_pk_number() . "號";
		}
		else if ($this->parking_lot->get_m_plots_type() == "300") {
			$purchase_name = $this->parking_lot->get_m_plots_name() . " 格號: " . $this->get_m_pk_number() . "號";
			$purchase_name .= "停車媒合服務費";
		}
		else if ($this->parking_lot->get_m_plots_type() == "301") {
			$purchase_name = $this->parking_lot->get_m_plots_name();
			$purchase_name .= "停車媒合服務費";
		}
		else if ($this->parking_lot->get_m_plots_type() == "302") {
			$purchase_name = $this->parking_lot->get_m_plots_name();
			$purchase_name .= "停車媒合服務費";
		}
		else {
			//保持原樣
		}
		return $purchase_name;
	}

	public function get_m_pk_sn()
	{
		return $this->m_pk_sn;
	}

	public function get_m_pk_id()
	{
		return $this->m_pk_id;
	}

	public function get_m_plots_id()
	{
		return $this->m_plots_id;
	}

	public function get_m_id()
	{
		return $this->m_id;
	}

	public function get_pkt_id()
	{
		return $this->pkt_id;
	}

	public function get_m_pk_place_type()
	{
		return $this->m_pk_place_type;
	}

	public function get_m_pk_delete()
	{
		return $this->m_pk_delete;
	}

	public function get_m_pk_address_country()
	{
		return $this->m_pk_address_country;
	}

	public function get_m_pk_address_post_code()
	{
		return $this->m_pk_address_post_code;
	}

	public function get_m_pk_address_state()
	{
		return $this->m_pk_address_state;
	}

	public function get_m_pk_address_city()
	{
		return $this->m_pk_address_city;
	}

	public function get_m_pk_address_line1()
	{
		return $this->m_pk_address_line1;
	}

	public function get_m_pk_address_line2()
	{
		return $this->m_pk_address_line2;
	}

	public function get_m_pk_address_line3()
	{
		return $this->m_pk_address_line3;
	}

	public function get_m_pk_floor($disp = false)
	{

		if ($disp == true) {
			if ($this->m_pk_floor != "") {
				if (substr($this->m_pk_floor, 0, 1) == "B")
					return $this->m_pk_floor;
				else
					return $this->m_pk_floor . "F";

			}
			else {
				return "";
			}
		}

		return $this->m_pk_floor;
	}

	public function get_m_pk_number()
	{
		return $this->m_pk_number;
	}

	public function get_m_pk_community_name()
	{
		return $this->m_pk_community_name;
	}

	public function get_m_pk_only_same_community()
	{
		return $this->m_pk_only_same_community;
	}

	public function get_m_pk_lng()
	{
		return $this->m_pk_lng;
	}

	public function get_m_pk_lat()
	{
		return $this->m_pk_lat;
	}

	public function get_m_pk_alt()
	{
		return $this->m_pk_alt;
	}

	public function get_m_pk_right_enable()
	{
		return $this->m_pk_right_enable;
	}

	public function get_m_pk_inout_way()
	{
		return $this->m_pk_inout_way;
	}

	public function get_m_pk_owner()
	{
		return $this->m_pk_owner;
	}

	public function get_m_pk_inout_side()
	{
		return $this->m_pk_inout_side;
	}

	public function get_m_pk_length()
	{
		return $this->m_pk_length;
	}

	public function get_m_pk_width()
	{
		return $this->m_pk_width;
	}

	public function get_m_pk_height()
	{
		return $this->m_pk_height;
	}

	public function get_m_pk_weight()
	{
		return $this->m_pk_weight;
	}

	public function get_m_pk_agent_m_id()
	{
		return $this->m_pk_agent_m_id;
	}

	public function get_m_pk_agent_number()
	{
		return $this->m_pk_agent_number;
	}

	public function get_m_pk_owner_m_id()
	{
		return $this->m_pk_owner_m_id;
	}

	public function get_m_pk_owner_number()
	{
		return $this->m_pk_owner_number;
	}

	public function get_m_pk_developer_m_id()
	{
		return $this->m_pk_developer_m_id;
	}

	public function get_m_is_close()
	{
		return $this->m_is_close;
	}

	public function get_m_notification_enable()
	{
		return $this->m_notification_enable;
	}

	public function get_m_pk_create_datetime()
	{
		return $this->m_pk_create_datetime;
	}

	public function get_m_pk_last_edit_datetime()
	{
		return $this->m_pk_last_edit_datetime;
	}

	public function get_m_pk_rating()
	{
		return $this->m_pk_rating;
	}

	public function get_m_pk_gov_pricing_desc()
	{
		return $this->m_pk_gov_pricing_desc;
	}

	public function get_m_pk_max_discount()
	{
		return (float)$this->m_pk_max_discount;
	}

	public function get_m_pk_max_free_discount()
	{
		return (float)$this->m_pk_max_free_discount;
	}

	public function get_m_pk_free_time()
	{
		return $this->m_pk_free_time;
	}

	public function get_m_pk_disabled_free_time()
	{
		return $this->m_pk_disabled_free_time;
	}

	public function get_m_pk_severe_disabled_free_time()
	{
		return $this->m_pk_severe_disabled_free_time;
	}

	public function get_file_logs_array($m_id = "")
	{
		if(UPK_File_Logs::isDisplayDefaultId($m_id)) {
			$this_file_log = new UPK_File_Logs(CONST_FL_TYPE::DISPLAY_DEFAULT,"",$m_id);
			return $this_file_log->get_array();
		}
		else {
			if (isset($this->file_logs->data)) {
				return $this->file_logs->data;
			}
			else {
				return [];
			}
		}
	}

	public function get_ppl_total_page()
	{
		return $this->ppl_total_page;
	}

	public function set_ppl_total_cnt($mode = 1)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql_logic = "";
		if ($mode == $this->C_PARKING_SPACE_DETAIL) {
			$DT_start_datetime = new DateTime();
			$DT_end_datetime = new DateTime("2199-12-31 23:59:59");
			$start_date = $DT_start_datetime->format("Y-m-d");
			$end_date = $DT_end_datetime->format("Y-m-d");
			$start_time = $DT_start_datetime->format("H:i:s");
			$end_time = $DT_end_datetime->format("H:i:s");
			$generate_sql_ppl = generateTimeComponent(CONST_GENERATE_TIME_COMPONENT::PRICING_LOG);
			$sql_ppl_is_inside = $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "inside");
			$sql_ppl_is_outside = $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "outside");
			$sql_ppl_is_cross = $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "cross");
			$sql_ppl_is_include = $generate_sql_ppl->SqlTimeComparison($start_date, $end_date, $start_time, $end_time, "include");
			$sql_logic .= " AND !(" . $sql_ppl_is_outside . ")";//在outside就不要
		}

		$sql = "SELECT count(*) as cnt FROM tb_Member_ParkingSpace_Pricing_Log as tb_ppl";
		$sql .= " WHERE tb_ppl.m_pk_id = '" . $this->m_pk_id . "' AND tb_ppl.m_ppl_delete='0' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return false;
		}
		$ans = mysql_fetch_assoc($result);
		$this->ppl_total_cnt = $ans["cnt"];
		return true;
	}

	public function get_ppl_total_cnt($mode = 1)
	{
		$this->set_ppl_total_cnt($mode);
		return $this->ppl_total_cnt;
	}

	public function get_array($select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_m_pk_sn) != 0) {
			$return_array["m_pk_sn"] = $this->get_m_pk_sn();
		}
		if (($select & $this->B_m_pk_id) != 0) {
			$return_array["m_pk_id"] = $this->get_m_pk_id();
		}
		if (($select & $this->B_m_plots_id) != 0) {
			$return_array["m_plots_id"] = $this->get_m_plots_id();
		}
		if (($select & $this->B_m_id) != 0) {
			$return_array["m_id"] = $this->get_m_id();
		}
		if (($select & $this->B_pkt_id) != 0) {
			$return_array["pkt_id"] = $this->get_pkt_id();
		}
		if (($select & $this->B_m_pk_place_type) != 0) {
			$return_array["m_pk_place_type"] = $this->get_m_pk_place_type();
		}
		if (($select & $this->B_m_pk_delete) != 0) {
			$return_array["m_pk_delete"] = $this->get_m_pk_delete();
		}

		if (($select & $this->B_m_pk_address) != 0) {
			$return_array["m_pk_address_country"] = $this->get_m_pk_address_country();
			$return_array["m_pk_address_post_code"] = $this->get_m_pk_address_post_code();
			$return_array["m_pk_address_state"] = $this->get_m_pk_address_state();
			$return_array["m_pk_address_city"] = $this->get_m_pk_address_city();
			$return_array["m_pk_address_line1"] = $this->get_m_pk_address_line1();
			$return_array["m_pk_address_line2"] = $this->get_m_pk_address_line2();
			$return_array["m_pk_address_line3"] = $this->get_m_pk_address_line3();
		}
		if (($select & $this->B_m_pk_floor) != 0) {
			$return_array["m_pk_floor"] = $this->get_m_pk_floor();
		}
		if (($select & $this->B_m_pk_number) != 0) {
			$return_array["m_pk_number"] = $this->get_m_pk_number();
		}
		if (($select & $this->B_m_pk_community_name) != 0) {
			$return_array["m_pk_community_name"] = $this->get_m_pk_community_name();
		}
		if (($select & $this->B_m_pk_only_same_community) != 0) {
			$return_array["m_pk_only_same_community"] = $this->get_m_pk_only_same_community();
		}
		if (($select & $this->B_m_pk_lng) != 0) {
			$return_array["m_pk_lng"] = $this->get_m_pk_lng();
		}
		if (($select & $this->B_m_pk_lat) != 0) {
			$return_array["m_pk_lat"] = $this->get_m_pk_lat();
		}
		if (($select & $this->B_m_pk_alt) != 0) {
			$return_array["m_pk_alt"] = $this->get_m_pk_alt();
		}
		if (($select & $this->B_m_pk_right_enable) != 0) {
			$return_array["m_pk_right_enable"] = $this->get_m_pk_right_enable();
		}
		if (($select & $this->B_m_pk_inout_way) != 0) {
			$return_array["m_pk_inout_way"] = $this->get_m_pk_inout_way();
		}
		if (($select & $this->B_m_pk_owner) != 0) {
			$return_array["m_pk_owner"] = $this->get_m_pk_owner();
		}
		if (($select & $this->B_m_pk_inout_side) != 0) {
			$return_array["m_pk_inout_side"] = $this->get_m_pk_inout_side();
		}
		if (($select & $this->B_m_pk_size) != 0) {
			$return_array["m_pk_length"] = $this->get_m_pk_length();
			$return_array["m_pk_width"] = $this->get_m_pk_width();
			$return_array["m_pk_height"] = $this->get_m_pk_height();
			$return_array["m_pk_weight"] = $this->get_m_pk_weight();
		}
		if (($select & $this->B_m_pk_agent) != 0) {
			$return_array["m_pk_agent_m_id"] = $this->get_m_pk_agent_m_id();
			$return_array["m_pk_agent_number"] = $this->get_m_pk_agent_number();
		}
		if (($select & $this->B_m_pk_owner) != 0) {
			$return_array["m_pk_owner_m_id"] = $this->get_m_pk_owner_m_id();
			$return_array["m_pk_owner_number"] = $this->get_m_pk_owner_number();
		}
		if (($select & $this->B_m_pk_developer_m_id) != 0) {
			$return_array["m_pk_developer_m_id"] = $this->get_m_pk_developer_m_id();
		}
		if (($select & $this->B_m_is_close) != 0) {
			$return_array["m_is_close"] = $this->get_m_is_close();
		}
		if (($select & $this->B_m_notification_enable) != 0) {
			$return_array["m_notification_enable"] = $this->get_m_notification_enable();
		}
		if (($select & $this->B_m_pk_rating) != 0) {
			$return_array["m_pk_rating"] = $this->get_m_pk_rating();
		}
		if (($select & $this->B_m_pk_max_discount) != 0) {
			$return_array["m_pk_max_discount"] = $this->get_m_pk_max_discount();
		}
		if (($select & $this->B_m_pk_max_free_discount) != 0) {
			$return_array["m_pk_max_free_discount"] = $this->get_m_pk_max_free_discount();
		}
		if (($select & $this->B_m_pk_x_free_time) != 0) {
			$return_array["m_pk_free_time"] = $this->get_m_pk_free_time();
			$return_array["m_pk_disabled_free_time"] = $this->get_m_pk_disabled_free_time();
			$return_array["m_pk_severe_disabled_free_time"] = $this->get_m_pk_severe_disabled_free_time();
		}
		if (($select & $this->B_m_pk_edit_datetime) != 0) {
			$return_array["m_pk_create_datetime"] = $this->get_m_pk_create_datetime();
			$return_array["m_pk_last_edit_datetime"] = $this->get_m_pk_last_edit_datetime();
		}
		if (($select & $this->B_m_pk_file_logs) != 0) {
			//parking space本來沒有file logs所以這邊不加
			$return_array["file_logs_array"] = $this->get_file_logs_array();
		}
		return $return_array;
	}

	public function generate_sql_string($select = "")
	{
		if ($select == "") {
			return "*";
		}
		$return_text = "";
		if (($select & $this->B_m_pk_sn) != 0) {
			$return_text .= $this->S_m_pk_sn . ", ";
		}
		if (($select & $this->B_m_pk_id) != 0) {
			$return_text .= $this->S_m_pk_id . ", ";
		}
		if (($select & $this->B_m_plots_id) != 0) {
			$return_text .= $this->S_m_plots_id . ", ";
		}
		if (($select & $this->B_m_id) != 0) {
			$return_text .= $this->S_m_id . ", ";
		}
		if (($select & $this->B_pkt_id) != 0) {
			$return_text .= $this->S_pkt_id . ", ";
		}
		if (($select & $this->B_m_pk_place_type) != 0) {
			$return_text .= $this->S_m_pk_place_type . ", ";
		}
		if (($select & $this->B_m_pk_delete) != 0) {
			$return_text .= $this->S_m_pk_delete . ", ";
		}

		if (($select & $this->B_m_pk_address) != 0) {
			$return_text .= $this->S_m_pk_address_country . ", ";
			$return_text .= $this->S_m_pk_address_post_code . ", ";
			$return_text .= $this->S_m_pk_address_state . ", ";
			$return_text .= $this->S_m_pk_address_city . ", ";
			$return_text .= $this->S_m_pk_address_line1 . ", ";
			$return_text .= $this->S_m_pk_address_line2 . ", ";
			$return_text .= $this->S_m_pk_address_line3 . ", ";
		}
		if (($select & $this->B_m_pk_floor) != 0) {
			$return_text .= $this->S_m_pk_floor . ", ";
		}
		if (($select & $this->B_m_pk_number) != 0) {
			$return_text .= $this->S_m_pk_number . ", ";
		}
		if (($select & $this->B_m_pk_community_name) != 0) {
			$return_text .= $this->S_m_pk_community_name . ", ";
		}
		if (($select & $this->B_m_pk_only_same_community) != 0) {
			$return_text .= $this->S_m_pk_only_same_community . ", ";
		}
		if (($select & $this->B_m_pk_lng) != 0) {
			$return_text .= $this->S_m_pk_lng . ", ";
		}
		if (($select & $this->B_m_pk_lat) != 0) {
			$return_text .= $this->S_m_pk_lat . ", ";
		}
		if (($select & $this->B_m_pk_alt) != 0) {
			$return_text .= $this->S_m_pk_alt . ", ";
		}
		if (($select & $this->B_m_pk_right_enable) != 0) {
			$return_text .= $this->S_m_pk_right_enable . ", ";
		}
		if (($select & $this->B_m_pk_inout_way) != 0) {
			$return_text .= $this->S_m_pk_inout_way . ", ";
		}
		if (($select & $this->B_m_pk_owner) != 0) {
			$return_text .= $this->S_m_pk_owner . ", ";
		}
		if (($select & $this->B_m_pk_inout_side) != 0) {
			$return_text .= $this->S_m_pk_inout_side . ", ";
		}
		if (($select & $this->B_m_pk_size) != 0) {
			$return_text .= $this->S_m_pk_length . ", ";
			$return_text .= $this->S_m_pk_width . ", ";
			$return_text .= $this->S_m_pk_height . ", ";
			$return_text .= $this->S_m_pk_weight . ", ";
		}
		if (($select & $this->B_m_pk_agent) != 0) {
			$return_text .= $this->S_m_pk_agent_m_id . ", ";
			$return_text .= $this->S_m_pk_agent_number . ", ";
		}
		if (($select & $this->B_m_pk_owner) != 0) {
			$return_text .= $this->S_m_pk_owner_m_id . ", ";
			$return_text .= $this->S_m_pk_owner_number . ", ";
		}
		if (($select & $this->B_m_pk_developer_m_id) != 0) {
			$return_text .= $this->S_m_pk_developer_m_id . ", ";
		}
		if (($select & $this->B_m_is_close) != 0) {
			$return_text .= $this->S_m_is_close . ", ";
		}
		if (($select & $this->B_m_notification_enable) != 0) {
			$return_text .= $this->S_m_notification_enable . ", ";
		}
		if (($select & $this->B_m_pk_max_discount) != 0) {
			$return_text .= $this->S_m_pk_max_discount . ", ";
		}
		if (($select & $this->B_m_pk_max_free_discount) != 0) {
			$return_text .= $this->S_m_pk_max_free_discount . ", ";
		}
		if (($select & $this->B_m_pk_x_free_time) != 0) {
			$return_text .= $this->S_m_pk_free_time . ", ";
			$return_text .= $this->S_m_pk_disabled_free_time . ", ";
			$return_text .= $this->S_m_pk_severe_disabled_free_time . ", ";
		}
		if (($select & $this->B_m_pk_edit_datetime) != 0) {
			$return_text .= $this->S_m_pk_create_datetime . ", ";
			$return_text .= $this->S_m_pk_last_edit_datetime . ", ";
		}
		if (($select & $this->B_m_pk_file_logs) != 0) {
			$return_text .= $this->S_m_pk_file_logs . ", ";
		}
		$return_text = substr($return_text, 0, -2);
		return $return_text;
	}

	private function put_data_to_class($ans = array(), $select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		if (($select & $this->B_m_pk_sn) != 0) {
			$this->m_pk_sn = $ans["m_pk_sn"];
		}
		if (($select & $this->B_m_pk_id) != 0) {
			$this->m_pk_id = $ans["m_pk_id"];
		}
		if (($select & $this->B_m_plots_id) != 0) {
			$this->m_plots_id = $ans["m_plots_id"];
		}
		if (($select & $this->B_m_id) != 0) {
			$this->m_id = $ans["m_id"];
		}
		if (($select & $this->B_pkt_id) != 0) {
			$this->pkt_id = $ans["pkt_id"];
		}
		if (($select & $this->B_m_pk_place_type) != 0) {
			$this->m_pk_place_type = $ans["m_pk_place_type"];
		}
		if (($select & $this->B_m_pk_delete) != 0) {
			$this->m_pk_delete = $ans["m_pk_delete"];
		}
		if (($select & $this->B_m_pk_address) != 0) {
			$this->m_pk_address_country = $ans["m_pk_address_country"];
			$this->m_pk_address_post_code = $ans["m_pk_address_post_code"];
			$this->m_pk_address_state = $ans["m_pk_address_state"];
			$this->m_pk_address_city = $ans["m_pk_address_city"];
			$this->m_pk_address_line1 = $ans["m_pk_address_line1"];
			$this->m_pk_address_line2 = $ans["m_pk_address_line2"];
			$this->m_pk_address_line3 = $ans["m_pk_address_line3"];
		}
		if (($select & $this->B_m_pk_floor) != 0) {
			$this->m_pk_floor = $ans["m_pk_floor"];
		}
		if (($select & $this->B_m_pk_number) != 0) {
			$this->m_pk_number = $ans["m_pk_number"];
		}
		if (($select & $this->B_m_pk_community_name) != 0) {
			$this->m_pk_community_name = $ans["m_pk_community_name"];
		}
		if (($select & $this->B_m_pk_only_same_community) != 0) {
			$this->m_pk_only_same_community = $ans["m_pk_only_same_community"];
		}
		if (($select & $this->B_m_pk_lng) != 0) {
			$this->m_pk_lng = $ans["m_pk_lng"];
		}
		if (($select & $this->B_m_pk_lat) != 0) {
			$this->m_pk_lat = $ans["m_pk_lat"];
		}
		if (($select & $this->B_m_pk_alt) != 0) {
			$this->m_pk_alt = $ans["m_pk_alt"];
		}
		if (($select & $this->B_m_pk_right_enable) != 0) {
			$this->m_pk_right_enable = $ans["m_pk_right_enable"];
		}
		if (($select & $this->B_m_pk_inout_way) != 0) {
			$this->m_pk_inout_way = $ans["m_pk_inout_way"];
		}
		if (($select & $this->B_m_pk_owner) != 0) {
			$this->m_pk_owner = $ans["m_pk_owner"];
		}
		if (($select & $this->B_m_pk_inout_side) != 0) {
			$this->m_pk_inout_side = $ans["m_pk_inout_side"];
		}
		if (($select & $this->B_m_pk_size) != 0) {
			$this->m_pk_length = $ans["m_pk_length"];
			$this->m_pk_width = $ans["m_pk_width"];
			$this->m_pk_height = $ans["m_pk_height"];
			$this->m_pk_weight = $ans["m_pk_weight"];
		}
		if (($select & $this->B_m_pk_agent) != 0) {
			$this->m_pk_agent_m_id = $ans["m_pk_agent_m_id"];
			$this->m_pk_agent_number = $ans["m_pk_agent_number"];
		}
		if (($select & $this->B_m_pk_owner) != 0) {
			$this->m_pk_owner_m_id = $ans["m_pk_owner_m_id"];
			$this->m_pk_owner_number = $ans["m_pk_owner_number"];
		}
		if (($select & $this->B_m_pk_developer_m_id) != 0) {
			$this->m_pk_developer_m_id = $ans["m_pk_developer_m_id"];
		}
		if (($select & $this->B_m_is_close) != 0) {
			$this->m_is_close = $ans["m_is_close"];
		}
		if (($select & $this->B_m_notification_enable) != 0) {
			$this->m_notification_enable = $ans["m_notification_enable"];
		}
		if (($select & $this->B_m_pk_rating) != 0) {
			//TODO : 技術債, 要用另外一組方式計算評分
			$this->m_pk_rating = 8;
			global $conn, $dbName;
			check_conn($conn, $dbName);
			$tmp_score = json_decode(ParkingSpaceRating($conn, $this->m_pk_id), true);
			if ($tmp_score["result"] == 1) {
				$this->m_pk_rating = $tmp_score["score"];
			}
			//$this->GetBookingLog(" AND m_bkl_cancel='0'",$this->B_m_pk_rating);
		}
		if (($select & $this->B_m_pk_max_discount) != 0) {
			$this->m_pk_max_discount = $ans["m_pk_max_discount"];
		}
		if (($select & $this->B_m_pk_max_free_discount) != 0) {
			$this->m_pk_max_free_discount = $ans["m_pk_max_free_discount"];
		}
		if (($select & $this->B_m_pk_x_free_time) != 0) {
			$this->m_pk_free_time = $ans["m_pk_free_time"];
			$this->m_pk_disabled_free_time = $ans["m_pk_disabled_free_time"];
			$this->m_pk_severe_disabled_free_time = $ans["m_pk_severe_disabled_free_time"];
		}
		if (($select & $this->B_m_pk_edit_datetime) != 0) {
			$this->m_pk_create_datetime = $ans["m_pk_create_datetime"];
			$this->m_pk_last_edit_datetime = $ans["m_pk_last_edit_datetime"];
		}
		if (($select & $this->B_m_pk_file_logs) != 0) {
			$this->file_logs = json_decode($ans["m_pk_file_logs"]);
		}
	}

	public function GetGoogleMap()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		try {
			$key = "AIzaSyCExYYdo6yEeyYf4_Y_IGNS-pds0iQlYV4";
			$google_map_src = "http://maps.googleapis.com/maps/api/staticmap?center=" . $this->m_pk_lat . "," . $this->m_pk_lng;
			$google_map_src .= "&markers=color:red|label:|" . $this->m_pk_lat . "," . $this->m_pk_lng;
			$google_map_src .= "&zoom=19&size=640x370&maptype=roadmap&sensor=false";
			$google_map_src .= "&key=" . $key;
			$files ["name"] = "google_map.jpg";
			$files ["tmp_name"] = $google_map_src;
			$files ["size"] = 640 * 370;

			$activity = "UPLOAD PICTURE";
			$this_member = new UPK_Member($this->m_pk_owner_m_id);
			$token = $this_member->get_m_token();
			$fl_type = 1000;//1000以上不用給type_id
			$fl_type_id = $this->m_pk_id;
			$fl_description = "";
			$arg["is_admin"] = 0;
			include_once("/../main_api/UploadFileFunc.php");
			$ans = UploadFileFunc($activity, $token, $fl_type, $fl_type_id, $fl_description, $files, $arg);

			$ttans = json_decode($ans, true);
			if ($ttans["result"] == 0) {
				return $ans;
			}
			include_once("/../select_api/GetFileLogFunc.php");
			$ans = GetFileLogFunc("GET FILE LOG", $token, "1000", $this->m_pk_id);
			$ttans = json_decode($ans, true);
			if ($ttans["result"] == 0) {
				return $ans;
			}
			$this->file_logs = $ttans;
			$sql = "UPDATE tb_Member_ParkingSpace SET  m_pk_file_logs='" . $ans . "' WHERE m_pk_id='" . $this->m_pk_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "更新車位照片失敗", "description" => ""));
			}
			MemcacheSetParkingSpace('_UPK_ParkingSpace:', $this->m_pk_id);
		}
		catch (Exception $exception) {
			rg_activity_log($conn,"","更新車位照片失敗","GetGoogleMap錯誤",$this->m_pk_id ,print_r($exception,true));
		}
		return json_encode(array("result"=>1));
	}
}

?>
