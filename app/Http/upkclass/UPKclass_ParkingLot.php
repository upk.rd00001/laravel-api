<?php

class UPK_ParkingLot
{
	//extends UPK_ParkingSpace
	private $m_plots_sn;
	private $m_plots_id;
	private $m_id;
	private $m_plots_delete;
	private $m_plots_address_country;
	private $m_plots_address_post_code;
	private $m_plots_address_state;
	private $m_plots_address_city;
	private $m_plots_address_line1;
	private $m_plots_address_line2;
	private $m_plots_address_line3;
	private $m_plots_address_description;
	private $m_plots_lng;
	private $m_plots_lat;
	private $m_plots_alt;
	private $m_plots_name;
	private $m_plots_phone_number;
	private $m_plots_opening_time;
	private $m_plots_description;
	private $m_plots_community_name;
	private $m_plots_right_enable;
	private $m_plots_manager;
	private $m_is_close;
	private $m_close_temporary;
	private $m_plots_height;
	private $m_plots_can_ipass;
	private $m_plots_can_easycard;
	private $m_plots_can_icash20;
	private $m_plots_type;
	private $m_plots_auto_approach;
	private $m_plots_polygon;
	private $m_plots_only_same_community;
	private $m_plots_space_count;
	private $m_plots_free_pk_amount;
	private $m_plots_member_ref_profit_earn;
	private $m_plots_invoice_type;
	private $m_plots_invoice_pos;
	private $m_plots_invoice_last_num;
	private $m_plots_invoice_last_word;
	private $m_plots_invoice_month;
	private $m_plots_invoice_word;
	private $m_plots_invoice_start_no;
	private $m_plots_invoice_end_no;
	private $m_plots_is_lpr_tower;

	//自訂義參數
	private $m_plots_rating;
	private $m_plots_remain_count;
	private $m_plots_total_count;
	private $is_valid;

	public $B_m_plots_sn = 1;
	public $B_m_plots_id = 2;
	public $B_m_id = 4;
	public $B_m_plots_delete = 8;
	public $B_m_plots_address = 16;
	/*public $B_m_plots_address_country		=16;
	public $B_m_plots_address_post_code		=32;
	public $B_m_plots_address_state		=64;
	public $B_m_plots_address_city		=128;
	public $B_m_plots_address_line1		=256;
	public $B_m_plots_address_line2		=512;
	public $B_m_plots_address_line3		=1024;*/
	public $B_m_plots_address_description = 2048;
	public $B_m_plots_lng = 4096;
	public $B_m_plots_lat = 8192;
	public $B_m_plots_alt = 16384;
	public $B_m_plots_name = 32768;
	public $B_m_plots_phone_number = 65536;
	public $B_m_plots_opening_time = 131072;
	public $B_m_plots_description = 262144;
	public $B_m_plots_community_name = 524288;
	public $B_m_plots_right_enable = 1048576;
	public $B_m_plots_manager = 2097152;
	public $B_m_is_close = 4194304;
	public $B_m_plots_rating = 8388608;
	public $B_m_plots_height = 16777216;
	public $B_m_plots_paid_support = 33554432;
	public $B_m_plots_type = 67108864;
	public $B_m_plots_auto_approach = 134217728;
	public $B_m_plots_polygon = 268435456;
	public $B_m_plots_only_same_community = 536870912;
	public $B_m_close_temporary = 1073741824;
	public $B_m_plots_space_count = 32;
	public $B_m_plots_free_pk_amount = 128;
	public $B_m_plots_file_logs = 64;
	public $B_m_plots_member_ref_profit_earn = 256;
	public $B_m_plots_invoice_data = 512;
	public $B_m_plots_is_lpr_tower = 1024;
	public $B_ALL;

	private $S_m_plots_sn = "m_plots_sn";
	private $S_m_plots_id = "m_plots_id";
	private $S_m_id = "m_id";
	private $S_m_plots_delete = "m_plots_delete";
	private $S_m_plots_address_country = "m_plots_address_country";
	private $S_m_plots_address_post_code = "m_plots_address_post_code";
	private $S_m_plots_address_state = "m_plots_address_state";
	private $S_m_plots_address_city = "m_plots_address_city";
	private $S_m_plots_address_line1 = "m_plots_address_line1";
	private $S_m_plots_address_line2 = "m_plots_address_line2";
	private $S_m_plots_address_line3 = "m_plots_address_line3";
	private $S_m_plots_address_description = "m_plots_address_description";
	private $S_m_plots_lng = "m_plots_lng";
	private $S_m_plots_lat = "m_plots_lat";
	private $S_m_plots_alt = "m_plots_alt";
	private $S_m_plots_name = "m_plots_name";
	private $S_m_plots_phone_number = "m_plots_phone_number";
	private $S_m_plots_opening_time = "m_plots_opening_time";
	private $S_m_plots_description = "m_plots_description";
	private $S_m_plots_community_name = "m_plots_community_name";
	private $S_m_plots_right_enable = "m_plots_right_enable";
	private $S_m_plots_manager = "m_plots_manager";
	private $S_m_is_close = "m_is_close";
	private $S_m_close_temporary = "m_close_temporary";
	private $S_m_plots_height = "m_plots_height";
	private $S_m_plots_can_ipass = "m_plots_can_ipass";
	private $S_m_plots_can_easycard = "m_plots_can_easycard";
	private $S_m_plots_can_icash20 = "m_plots_can_icash20";
	private $S_m_plots_type = "m_plots_type";
	private $S_m_plots_auto_approach = "m_plots_auto_approach";
	private $S_m_plots_polygon = "m_plots_polygon";
	private $S_m_plots_file_logs = "m_plots_file_logs";
	private $S_m_plots_only_same_community = "m_plots_only_same_community";
	private $S_m_plots_space_count = "m_plots_space_count";
	private $S_m_plots_free_pk_amount = "m_plots_free_pk_amount";
	private $S_m_plots_member_ref_profit_earn = "m_plots_member_ref_profit_earn";
	private $S_m_plots_invoice_type = "m_plots_invoice_type";
	private $S_m_plots_invoice_pos = "m_plots_invoice_pos";
	private $S_m_plots_invoice_last_num = "m_plots_invoice_last_num";
	private $S_m_plots_invoice_month = "m_plots_invoice_month";
	private $S_m_plots_invoice_word = "m_plots_invoice_word";
	private $S_m_plots_invoice_start_no = "m_plots_invoice_start_no";
	private $S_m_plots_invoice_end_no = "m_plots_invoice_end_no";
	private $S_m_plots_is_lpr_tower = "m_plots_is_lpr_tower";

	public $parking_spaces;// = array(new UPK_ParkingSpace);
	public $discount_plan;// = array(new UPK_DiscountPlan);
	public $file_logs;
	public $gov_parking_space_area;

	public function __construct($m_plots_id, $sql_logic = "", $m_plots_delete = 0, $select = "")
	{
		$this->discount_plan=null;
		$this->is_valid=false;
		$this->B_ALL = $this->B_m_plots_sn | $this->B_m_plots_id | $this->B_m_id | $this->B_m_plots_delete | $this->B_m_plots_address | $this->B_m_plots_address_description | $this->B_m_plots_lng | $this->B_m_plots_lat | $this->B_m_plots_alt | $this->B_m_plots_name | $this->B_m_plots_phone_number | $this->B_m_plots_opening_time | $this->B_m_plots_description | $this->B_m_plots_community_name | $this->B_m_plots_right_enable | $this->B_m_plots_manager | $this->B_m_is_close | $this->B_m_plots_rating | $this->B_m_plots_height | $this->B_m_plots_paid_support | $this->B_m_plots_type | $this->B_m_plots_auto_approach | $this->B_m_plots_polygon | $this->B_m_plots_only_same_community | $this->B_m_close_temporary | $this->B_m_plots_space_count | $this->B_m_plots_free_pk_amount | $this->B_m_plots_file_logs | $this->B_m_plots_member_ref_profit_earn | $this->B_m_plots_invoice_data | $this->B_m_plots_is_lpr_tower;
		return $this->init_plot_id($m_plots_id, $sql_logic = "" , $m_plots_delete, $select);
	}

	public function init_plot_id($m_plots_id, $sql_logic = "", $m_plots_delete = 0, $select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$select = $select | $this->B_m_plots_type;//一定要有plots_type後面要判斷是否路邊要自己去撈路段資料
		$select = $select | $this->B_m_plots_id;//一定要有plots_id後面要判斷是否路邊要自己去撈路段資料
		$sql_select=$this->generate_sql_string($select);
		if ($m_plots_delete === 0)
			$sql = "select ".$sql_select." from tb_Member_ParkingLots where m_plots_id = '" . $m_plots_id . "' and m_plots_delete=b'0' ";
		elseif ($m_plots_delete === 1)
			$sql = "select ".$sql_select." from tb_Member_ParkingLots where m_plots_id = '" . $m_plots_id . "' and m_plots_delete=b'1' ";
		elseif ($m_plots_delete === 2)
			$sql = "select ".$sql_select." from tb_Member_ParkingLots where m_plots_id = '" . $m_plots_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("失敗" => mysql_error($conn) . $sql));
		} else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->put_data_to_class($ans, $select);
			$tmp_rating = false;

			//@memcache_close($memcache);
			$this->gov_parking_space_area = null;
			if ($this->m_plots_type == "100") {
				$sql = "select * from tb_Gov_ParkingSpace_Area where m_plots_id = '" . $m_plots_id . "' ";
				$result = mysql_query($sql, $conn);
				if (!$result) {
					return json_encode(array("失敗" => mysql_error($conn) . $sql));
				} else if (mysql_num_rows($result) == 1) {
					$ans = mysql_fetch_assoc($result);
					$this->gov_parking_space_area = $ans;
				}
			}
			$this->is_valid=true;
			return 1;

		} else if (mysql_num_rows($result) == 0) {
			return json_encode(array("result" => 0, "title" => "無此車場ID", "description" => mysql_error()));
		} else {
			//多個相同ID
			return json_encode(array("result" => 0, "title" => "有多個相同停車場ID", "description" => mysql_error()));
		}
	}

	public function GetFileLog($token)
	{
		#不用GetFileLogFunc
		/*include_once("/../select_api/GetFileLogFunc.php");
		$ttans=json_decode(GetFileLogFunc("GET FILE LOG",$token,"9",$this->m_plots_id),true);
		//$tmp["file_logs"]=$ttans;
		if($ttans["result"]==0)
			return 0;
		$this->file_logs = $ttans["data"];*/
		return 1;
	}
	public function GetParkingSpaces_v2($sql_logic = "", $select = "", $is_delete = 0)
	{
		//v2可以針對欄位篩選需要的值
		if ($select == "")
			$select = $this->B_ALL;

		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql = "select m_pk_id from tb_Member_ParkingSpace where m_plots_id = '" . $this->m_plots_id . "' ";
		if ($is_delete != 2)
			$sql .= " AND m_pk_delete='" . $is_delete . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "搜尋失敗", "description" => mysql_error()));
		} else if (mysql_num_rows($result) == 0) {
			$this->parking_spaces = null;
			return;
		}
		$total_score = 0;
		$score_cnt = 0;
		$temp_parking_spaces_array = array();
		$memcache=get_memcache();
		while ($ans = mysql_fetch_assoc($result)) {
			$tmp_parking_space=false;
			if ($memcache) {
				$tmp_parking_space=$memcache->get($dbName.'_UPK_ParkingSpace:'.$ans["m_pk_id"]);
			}
			else{
				#記憶體快取不可用 先不做事反正都要重撈
			}
			if(!$tmp_parking_space){
				$tmp_parking_space=MemcacheSetParkingSpace('_UPK_ParkingSpace:',$ans["m_pk_id"],$memcache);
			}
			array_push($temp_parking_spaces_array, $tmp_parking_space);
			$tmp_score = $tmp_parking_space->get_m_pk_rating();
			if ($tmp_score != null) {
				$total_score += $tmp_score;
				$score_cnt++;
			}
		}
		$this->parking_spaces = $temp_parking_spaces_array;
	}
	public function GetParkingSpaces($sql_logic = "", $value = 0, $is_delete = 0, $memcache = false)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql = "select * from tb_Member_ParkingSpace where m_plots_id = '" . $this->m_plots_id . "' ";
		if ($is_delete != 2)
			$sql .= " AND m_pk_delete='" . $is_delete . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);

		if (!$result) {
			return json_encode(array("result" => 0, "title" => "搜尋失敗", "description" => mysql_error()));
		} else if (mysql_num_rows($result) == 0) {
			$this->parking_spaces = null;
			return;
		}
		$total_score = 0;
		$score_cnt = 0;
		$temp_parking_spaces_array = array();
		$memcache=get_memcache();
		while ($ans = mysql_fetch_assoc($result)) {
			$tmp_parking_space = null;
			if ($memcache) {
				$tmp_parking_space = $memcache->get($dbName . '_UPK_ParkingSpace:' . $ans["m_pk_id"]);
			} else {
				#記憶體快取不可用 先不做事反正都要重撈
			}
			if (!$tmp_parking_space) {
				$tmp_parking_space = MemcacheSetParkingSpace('_UPK_ParkingSpace:', $ans["m_pk_id"], $memcache);
			}
			array_push($temp_parking_spaces_array, $tmp_parking_space);
			$tmp_score = $tmp_parking_space->get_array($tmp_parking_space->B_m_pk_rating)["m_pk_rating"];
			if ($tmp_score != null) {
				$total_score += $tmp_score;
				$score_cnt++;
			}
		}
		$this->parking_spaces = $temp_parking_spaces_array;
		if ($score_cnt != 0) {
			$this->m_plots_rating = round($total_score / $score_cnt,1);
		}
	}

	public function GetDiscountPlan($sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql = "SELECT dcp_id FROM tb_DiscountPlan where dcp_type_id = '" . $this->m_plots_id . "' AND dcp_type='9' AND dcp_delete='0' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);

		if (!$result) {
			return json_encode(array("result" => 0, "title" => "搜尋失敗", "description" => mysql_error()));
		}
		else if (mysql_num_rows($result) == 0) {
			$this->discount_plan=array();
			return 1;
		}
		$this->discount_plan=array();
		while ($ans = mysql_fetch_assoc($result)) {
			$this_discount_plan = new UPK_DiscountPlan($ans["dcp_id"]);
			array_push($this->discount_plan,$this_discount_plan);
		}
	}
	public function GetTotalCount()
	{
		if ($this->get_m_plots_type() == "302" || $this->get_m_plots_type() == "301") {
			$this->m_plots_total_count = $this->m_plots_space_count;
		} else {
			if ($this->parking_spaces == null) {
				$this->GetParkingSpaces();
			}
			$this->m_plots_total_count = count($this->parking_spaces);
		}
		return (int)$this->m_plots_total_count;
	}

	public function GetRemainCount($start_date, $end_date, $start_time, $end_time)
	{

		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		if ($this->m_plots_remain_count !== null) { //怕是等於零近來所以需要型別判斷
			return (int)$this->m_plots_remain_count;
		}
		if ($this->get_m_plots_free_pk_amount() != -1) {
			//如果不是-1則看該值，是-1的話用總車位數去扣除未出場的預約單
			$this->m_plots_remain_count = $this->get_m_plots_free_pk_amount();
			return (int) $this->m_plots_remain_count;
		}
		if ($this->parking_spaces == null) {
			$this->GetParkingSpaces();
		}
		$remain_count = 0;
		if ($this->get_m_plots_type() == "301") {
			$sql = "SELECT * FROM tb_Member_ParkingSpace_Booking_Log as tb_bkl 
				LEFT JOIN tb_Member_Parking_Log as tb_pl ON tb_bkl.m_bkl_id=tb_pl.m_bkl_id 
				WHERE tb_pl.m_pl_end_time IS NULL AND m_bkl_cancel='0' 
				AND tb_bkl.m_plots_id='".$this->get_m_plots_id()."' ";
			if ($this->isWithGate()) {
				$sql .= "";
			}
			else {
				$sql .= " GROUP BY tb_pl.m_pk_id ";
			}
			$result = mysql_query($sql, $conn);
			if (!$result) {
				$count_booking = 0;
			}
			else {
				$count_booking = mysql_num_rows($result);
			}
			$remain_count = $this->GetTotalCount() - $count_booking;

			//避免如果是新建的車位沒有填值的時候給個0不要顯示負數
			if ($remain_count < 0) {
				$remain_count = 0;
			}
		}
		elseif ($this->get_m_plots_type() == "302") {
			//如果是302車格則用parking_lot資料欄位的m_plots_space_count去計算
			$sql="SELECT * FROM tb_Member_ParkingSpace_Booking_Log as tb_bkl 
				LEFT JOIN tb_Member_Parking_Log as tb_pl ON tb_bkl.m_bkl_id=tb_pl.m_bkl_id 
				WHERE tb_pl.m_pl_end_time IS NULL AND m_bkl_cancel='0' 
				AND tb_bkl.m_plots_id='".$this->get_m_plots_id()."' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				$count_booking = 0;
			}
			else $count_booking = mysql_num_rows($result);
			$remain_count = $this->GetTotalCount() - $count_booking;

			//避免如果是新建的車位沒有填值的時候給個0不要顯示負數
			if ($remain_count < 0)
				$remain_count = 0;
		}
		elseif ($this->get_m_plots_type() == "100") {
			//如果是用費率的停車格則 沒有schedule代表目前空位，有schedule則被占住
			foreach ((array)$this->parking_spaces as $this_parking_space) {
				$this_parking_space->GetPricingLog_v2($start_date, $end_date, $start_time, $end_time);
				if (count($this_parking_space->pricing_logs) == 0) {
					$remain_count++;
				} else {
					//		echo $this_parking_space->pricing_logs[0]->get_m_ppl_id()."\n";
				}
			}
		}
		elseif ($this->get_m_plots_type() == "300") {
			global $conn, $dbName;
			check_conn($conn, $dbName);
			$sql_deive = "SELECT * FROM tb_Devices WHERE dv_type_id IN (SELECT m_pk_id FROM tb_Member_ParkingSpace WHERE m_plots_id='" . $this->get_m_plots_id() . "') AND dv_type='5' AND dv_status='0' AND is_delete='0' ORDER BY dv_sn DESC ";
			$result_deive = mysql_query($sql_deive, $conn);
			if (!$result_deive) {
				$remain_count = 0;
			} else $remain_count = mysql_num_rows($result_deive);
		}
		else {
			//如果是一班的停車格則 有schedule代表目前有空位，沒schedlue則..有車位但是不可以停
			//TODO: 這邊算有點久，可以優化
			foreach ($this->parking_spaces as $this_parking_space) {
				$this_parking_space->GetPricingLog_v2($start_date, $end_date, $start_time, $end_time);
				if (count($this_parking_space) != 0) {
					$remain_count++;
				}
			}
		}
		$this->m_plots_remain_count = $remain_count;
		return $remain_count;
	}
	public function getSameCommunityDisplay($conn, $m_id)
	{
		$plots_same_display = 0;
		if ($this->get_m_plots_only_same_community() == 1) {
			$sql = "SELECT * FROM tb_Member_Only_Same_Community WHERE m_osc_is_delete=0 AND m_osc_type=9 AND m_osc_type_id='" . $this->get_m_plots_id() . "'	AND m_osc_m_id='" . $m_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				#先顯示
				$plots_same_display = 1;
			} else if (mysql_num_rows($result) == 0) {
				#不再訪客名單內不顯示
				$plots_same_display = 0;
			} else {
				#否則就顯示
				$plots_same_display = 1;
			}
		} else #不限鄰居訪客
			$plots_same_display = 1;
		return $plots_same_display;
	}

	public function text()//__toString()
	{
		return array(
			"m_plots_sn" => $this->m_plots_sn,
			"m_plots_id" => $this->m_plots_id,
			"m_id" => $this->m_id,
			"m_plots_lng" => $this->m_plots_lng,
			"m_plots_lat" => $this->m_plots_lat,
			"m_plots_alt" => $this->m_plots_alt,
			"m_plots_name" => $this->m_plots_name,
			"m_plots_address_description" => $this->m_plots_address_description,
			"m_plots_opening_time" => $this->m_plots_opening_time,
			"m_plots_description" => $this->m_plots_description,
			"m_is_close" => $this->m_is_close
		);
	}

	public function get_discount_plan_desc()
	{
		if($this->discount_plan==null)
			$this->GetDiscountPlan();
		$temp_text="";
		$i=1;
		foreach($this->discount_plan as $each_discount_plan) {
			$temp_text .=  $each_discount_plan->get_dcp_onsale_type_description() . ", ";
			$i++;
		}
		if($temp_text!="")
			$temp_text = substr($temp_text,0,-2);
		return $temp_text;
	}
	public function isWithGate() {
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$is_have_device = false;
		$sql = "SELECT 1 FROM tb_Devices WHERE dv_type='".CONST_DV_TYPE::GATE."' AND is_delete='0' 
			AND dv_type_id='".$this->get_m_plots_id()."' ";
		$result = mysql_query($sql, $conn);
		if (! $result) {
			//失敗就算了
			$is_have_device = false;
		}
		elseif (mysql_num_rows($result) == 0) {
			//不用做事
			$is_have_device = false;
		}
		else {
			//有設備才要判斷剩餘車位數決定是否進場、怕有人忘記繳費導致進步了場
			$is_have_device=true;
		}
		return $is_have_device;
	}
	public function get_m_plots_sn()
	{
		return $this->m_plots_sn;
	}

	public function get_m_plots_id()
	{
		return $this->m_plots_id;
	}

	public function get_m_id()
	{
		return $this->m_id;
	}

	public function get_m_plots_delete()
	{
		return $this->m_plots_delete;
	}

	public function get_m_plots_address()
	{//或許要能過濾要什麼地址欄位
		return $this->get_m_plots_address_post_code()
			. $this->get_m_plots_address_country()
			. $this->get_m_plots_address_city()
			. $this->get_m_plots_address_line1()
			. $this->get_m_plots_address_line2()
			. $this->get_m_plots_address_line3();
	}

	public function get_m_plots_address_country()
	{
		return $this->m_plots_address_country;
	}

	public function get_m_plots_address_post_code()
	{
		return $this->m_plots_address_post_code;
	}

	public function get_m_plots_address_state()
	{
		return $this->m_plots_address_state;
	}

	public function get_m_plots_address_city()
	{
		return $this->m_plots_address_city;
	}

	public function get_m_plots_address_line1()
	{
		return $this->m_plots_address_line1;
	}

	public function get_m_plots_address_line2()
	{
		return $this->m_plots_address_line2;
	}

	public function get_m_plots_address_line3()
	{
		return $this->m_plots_address_line3;
	}

	public function get_m_plots_address_description()
	{
		return $this->m_plots_address_description;
	}

	public function get_m_plots_lng()
	{
		return $this->m_plots_lng;
	}

	public function get_m_plots_lat()
	{
		return $this->m_plots_lat;
	}

	public function get_m_plots_alt()
	{
		return $this->m_plots_alt;
	}

	public function get_m_plots_name()
	{
		return $this->m_plots_name;
	}

	public function get_m_plots_phone_number()
	{
		return $this->m_plots_phone_number;
	}

	public function get_m_plots_opening_time()
	{
		return $this->m_plots_opening_time;
	}

	public function get_m_plots_description()
	{
		return $this->m_plots_description;
	}

	public function get_m_plots_community_name()
	{
		return $this->m_plots_community_name;
	}

	public function get_m_plots_right_enable()
	{
		return $this->m_plots_right_enable;
	}

	public function get_m_plots_manager()
	{
		return $this->m_plots_manager;
	}

	public function get_m_is_close()
	{
		return $this->m_is_close;
	}

	public function get_m_close_temporary()
	{
		return $this->m_close_temporary;
	}

	public function get_m_plots_height()
	{
		return $this->m_plots_height;
	}

	public function get_m_plots_can_ipass()
	{
		return $this->m_plots_can_ipass;
	}

	public function get_m_plots_can_easycard()
	{
		return $this->m_plots_can_easycard;
	}

	public function get_m_plots_can_icash20()
	{
		return $this->m_plots_can_icash20;
	}

	public function get_m_plots_type()
	{
		return $this->m_plots_type;
	}

	public function get_gov_parking_space_area()
	{
		return $this->gov_parking_space_area;
	}

	public function get_m_plots_auto_approach()
	{
		return $this->m_plots_auto_approach;
	}

	public function get_m_plots_polygon()
	{
		return $this->m_plots_polygon;
	}

	public function get_m_plots_rating()
	{
		return $this->m_plots_rating;
	}

	public function get_m_plots_only_same_community()
	{
		return $this->m_plots_only_same_community;
	}

	public function get_m_plots_space_count()
	{
		return $this->m_plots_space_count;
	}
	public function get_m_plots_free_pk_amount()
	{
		return $this->m_plots_free_pk_amount;
	}
	public function get_m_plots_member_ref_profit_earn()
	{
		return $this->m_plots_member_ref_profit_earn;
	}
	public function get_m_plots_invoice_type()
	{
		return $this->m_plots_invoice_type;
	}
	public function get_m_plots_invoice_pos()
	{
		return $this->m_plots_invoice_pos;
	}
	public function get_m_plots_invoice_last_num()
	{
		return $this->m_plots_invoice_last_num;
	}
	public function get_m_plots_invoice_last_word()
	{
		return $this->m_plots_invoice_last_word;
	}
	public function get_m_plots_invoice_month()
	{
		return $this->m_plots_invoice_month;
	}
	public function get_m_plots_invoice_word()
	{
		return $this->m_plots_invoice_word;
	}
	public function get_m_plots_invoice_start_no()
	{
		return $this->m_plots_invoice_start_no;
	}
	public function get_m_plots_invoice_end_no()
	{
		return $this->m_plots_invoice_end_no;
	}
	public function get_m_plots_is_lpr_tower()
	{
		return $this->m_plots_is_lpr_tower;
	}
	public function get_file_logs_array($m_id="")
	{
		if(UPK_File_Logs::isDisplayDefaultId($m_id)) {
			$this_file_log = new UPK_File_Logs(CONST_FL_TYPE::DISPLAY_DEFAULT,"",$m_id);
			return $this_file_log->get_array();
		}
		else {
			if (isset($this->file_logs->data)) {
				return $this->file_logs->data;
			}
			else {
				return [];
			}
		}
	}

	public function get_array($select = "")
	{
		if ($select == "")
			$select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_m_plots_sn) != 0) {
			$return_array["m_plots_sn"] = $this->get_m_plots_sn();
		}
		if (($select & $this->B_m_plots_id) != 0) {
			$return_array["m_plots_id"] = $this->get_m_plots_id();
		}
		if (($select & $this->B_m_id) != 0) {
			$return_array["m_id"] = $this->get_m_id();
		}
		if (($select & $this->B_m_plots_delete) != 0) {
			$return_array["m_plots_delete"] = $this->get_m_plots_delete();
		}
		if (($select & $this->B_m_plots_address) != 0) {
			$return_array["m_plots_address_country"] = $this->get_m_plots_address_country();
			$return_array["m_plots_address_post_code"] = $this->get_m_plots_address_post_code();
			$return_array["m_plots_address_state"] = $this->get_m_plots_address_state();
			$return_array["m_plots_address_city"] = $this->get_m_plots_address_city();
			$return_array["m_plots_address_line1"] = $this->get_m_plots_address_line1();
			$return_array["m_plots_address_line2"] = $this->get_m_plots_address_line2();
			$return_array["m_plots_address_line3"] = $this->get_m_plots_address_line3();
		}
		if (($select & $this->B_m_plots_address_description) != 0) {
			$return_array["m_plots_address_description"] = $this->get_m_plots_address_description();
		}
		if (($select & $this->B_m_plots_lng) != 0) {
			$return_array["m_plots_lng"] = $this->get_m_plots_lng();
		}
		if (($select & $this->B_m_plots_lat) != 0) {
			$return_array["m_plots_lat"] = $this->get_m_plots_lat();
		}
		if (($select & $this->B_m_plots_alt) != 0) {
			$return_array["m_plots_alt"] = $this->get_m_plots_alt();
		}
		if (($select & $this->B_m_plots_name) != 0) {
			$return_array["m_plots_name"] = $this->get_m_plots_name();
		}
		if (($select & $this->B_m_plots_phone_number) != 0) {
			$return_array["m_plots_phone_number"] = $this->get_m_plots_phone_number();
		}
		if (($select & $this->B_m_plots_opening_time) != 0) {
			$return_array["m_plots_opening_time"] = $this->get_m_plots_opening_time();
		}
		if (($select & $this->B_m_plots_description) != 0) {
			$return_array["m_plots_description"] = $this->get_m_plots_description();
		}
		if (($select & $this->B_m_plots_community_name) != 0) {
			$return_array["m_plots_community_name"] = $this->get_m_plots_community_name();
		}
		if (($select & $this->B_m_plots_right_enable) != 0) {
			$return_array["m_plots_right_enable"] = $this->get_m_plots_right_enable();
		}
		if (($select & $this->B_m_plots_manager) != 0) {
			$return_array["m_plots_manager"] = $this->get_m_plots_manager();
		}
		if (($select & $this->B_m_is_close) != 0) {
			$return_array["m_is_close"] = $this->get_m_is_close();
		}
		if (($select & $this->B_m_close_temporary) != 0) {
			$return_array["m_close_temporary"] = $this->get_m_close_temporary();
		}
		if (($select & $this->B_m_plots_height) != 0) {
			$return_array["m_plots_height"] = $this->get_m_plots_height();
		}
		if (($select & $this->B_m_plots_paid_support) != 0) {
			$return_array["m_plots_can_ipass"] = $this->get_m_plots_can_ipass();
			$return_array["m_plots_can_easycard"] = $this->get_m_plots_can_easycard();
			$return_array["m_plots_can_icash20"] = $this->get_m_plots_can_icash20();
		}
		if (($select & $this->B_m_plots_height) != 0) {
			$return_array["m_plots_height"] = $this->get_m_plots_height();
		}
		if (($select & $this->B_m_plots_type) != 0) {
			$return_array["m_plots_type"] = $this->get_m_plots_type();
			if ($this->get_m_plots_type() == "100") {
				$return_array["gov_parkingspace_area"] = $this->get_gov_parking_space_area();
			}
		}
		if (($select & $this->B_m_plots_auto_approach) != 0) {
			$return_array["m_plots_auto_approach"] = $this->get_m_plots_auto_approach();
		}
		if (($select & $this->B_m_plots_polygon) != 0) {
			$return_array["m_plots_polygon"] = $this->get_m_plots_polygon();
		}
		if (($select & $this->B_m_plots_only_same_community) != 0) {
			$return_array["m_plots_only_same_community"] = $this->get_m_plots_only_same_community();
		}
		if (($select & $this->B_m_plots_space_count) != 0) {
			$return_array["m_plots_space_count"] = $this->get_m_plots_space_count();
		}
		if (($select & $this->B_m_plots_free_pk_amount) != 0) {
			$return_array["m_plots_free_pk_amount"] = $this->get_m_plots_free_pk_amount();
		}
		if (($select & $this->B_m_plots_member_ref_profit_earn) != 0) {
			$return_array["m_plots_member_ref_profit_earn"] = $this->get_m_plots_member_ref_profit_earn();
		}
		if (($select & $this->B_m_plots_invoice_data) != 0) {
			$return_array["m_plots_invoice_type"] = $this->get_m_plots_invoice_type();
			$return_array["m_plots_invoice_pos"] = $this->get_m_plots_invoice_pos();
			$return_array["m_plots_invoice_last_num"] = $this->get_m_plots_invoice_last_num();
			$return_array["m_plots_invoice_last_word"] = $this->get_m_plots_invoice_last_word();
			$return_array["m_plots_invoice_month"] = $this->get_m_plots_invoice_month();
			$return_array["m_plots_invoice_word"] = $this->get_m_plots_invoice_word();
			$return_array["m_plots_invoice_start_no"] = $this->get_m_plots_invoice_start_no();
			$return_array["m_plots_invoice_end_no"] = $this->get_m_plots_invoice_end_no();
		}
		if (($select & $this->B_m_plots_rating) != 0) {
			$return_array["m_plots_rating"] = $this->get_m_plots_rating();
		}
		if (($select & $this->B_m_plots_is_lpr_tower) != 0) {
			$return_array["m_plots_is_lpr_tower"] = $this->get_m_plots_is_lpr_tower();
		}
		return $return_array;
	}

	public function generate_sql_string($select = "")
	{
		if ($select == "") {
			$select .= $this->B_ALL;
			return "*";
		}
		$return_text="";
		if (($select & $this->B_m_plots_sn) != 0) {
			$return_text .= $this->S_m_plots_sn.", ";
		}
		if (($select & $this->B_m_plots_id) != 0) {
			$return_text .= $this->S_m_plots_id.", ";
		}
		if (($select & $this->B_m_id) != 0) {
			$return_text .= $this->S_m_id.", ";
		}
		if (($select & $this->B_m_plots_delete) != 0) {
			$return_text .= $this->S_m_plots_delete.", ";
		}
		if (($select & $this->B_m_plots_address) != 0) {
			$return_text .= $this->S_m_plots_address_country.", ";
			$return_text .= $this->S_m_plots_address_post_code.", ";
			$return_text .= $this->S_m_plots_address_state.", ";
			$return_text .= $this->S_m_plots_address_city.", ";
			$return_text .= $this->S_m_plots_address_line1.", ";
			$return_text .= $this->S_m_plots_address_line2.", ";
			$return_text .= $this->S_m_plots_address_line3.", ";
		}
		if (($select & $this->B_m_plots_address_description) != 0) {
			$return_text .= $this->S_m_plots_address_description.", ";
		}
		if (($select & $this->B_m_plots_lng) != 0) {
			$return_text .= $this->S_m_plots_lng.", ";
		}
		if (($select & $this->B_m_plots_lat) != 0) {
			$return_text .= $this->S_m_plots_lat.", ";
		}
		if (($select & $this->B_m_plots_alt) != 0) {
			$return_text .= $this->S_m_plots_alt.", ";
		}
		if (($select & $this->B_m_plots_name) != 0) {
			$return_text .= $this->S_m_plots_name.", ";
		}
		if (($select & $this->B_m_plots_phone_number) != 0) {
			$return_text .= $this->S_m_plots_phone_number.", ";
		}
		if (($select & $this->B_m_plots_opening_time) != 0) {
			$return_text .= $this->S_m_plots_opening_time.", ";
		}
		if (($select & $this->B_m_plots_description) != 0) {
			$return_text .= $this->S_m_plots_description.", ";
		}
		if (($select & $this->B_m_plots_community_name) != 0) {
			$return_text .= $this->S_m_plots_community_name.", ";
		}
		if (($select & $this->B_m_plots_right_enable) != 0) {
			$return_text .= $this->S_m_plots_right_enable.", ";
		}
		if (($select & $this->B_m_plots_manager) != 0) {
			$return_text .= $this->S_m_plots_manager.", ";
		}
		if (($select & $this->B_m_is_close) != 0) {
			$return_text .= $this->S_m_is_close.", ";
		}
		if (($select & $this->B_m_close_temporary) != 0) {
			$return_text .= $this->S_m_close_temporary.", ";
		}
		if (($select & $this->B_m_plots_height) != 0) {
			$return_text .= $this->S_m_plots_height.", ";
		}
		if (($select & $this->B_m_plots_paid_support) != 0) {
			$return_text .= $this->S_m_plots_can_ipass.", ";
			$return_text .= $this->S_m_plots_can_easycard.", ";
			$return_text .= $this->S_m_plots_can_icash20.", ";
		}
		if (($select & $this->B_m_plots_height) != 0) {
			$return_text .= $this->S_m_plots_height.", ";
		}
		if (($select & $this->B_m_plots_type) != 0) {
			$return_text .= $this->S_m_plots_type.", ";
		}
		if (($select & $this->B_m_plots_auto_approach) != 0) {
			$return_text .= $this->S_m_plots_auto_approach.", ";
		}
		if (($select & $this->B_m_plots_polygon) != 0) {
			$return_text .= $this->S_m_plots_polygon.", ";
		}
		if (($select & $this->B_m_plots_file_logs) != 0) {
			$return_text .= $this->S_m_plots_file_logs.", ";
		}
		if (($select & $this->B_m_plots_only_same_community) != 0) {
			$return_text .= $this->S_m_plots_only_same_community.", ";
		}
		if (($select & $this->B_m_plots_space_count) != 0) {
			$return_text .= $this->S_m_plots_space_count.", ";
		}
		if (($select & $this->B_m_plots_free_pk_amount) != 0) {
			$return_text .= $this->S_m_plots_free_pk_amount.", ";
		}
		if (($select & $this->B_m_plots_member_ref_profit_earn) != 0) {
			$return_text .= $this->S_m_plots_member_ref_profit_earn.", ";
		}
		if (($select & $this->B_m_plots_is_lpr_tower) != 0) {
			$return_text .= $this->S_m_plots_is_lpr_tower.", ";
		}
		if (($select & $this->B_m_plots_invoice_data) != 0) {
			$return_text .= $this->S_m_plots_invoice_type.", ";
			$return_text .= $this->S_m_plots_invoice_pos.", ";
			$return_text .= $this->S_m_plots_invoice_last_num.", ";
			$return_text .= $this->S_m_plots_invoice_month.", ";
			$return_text .= $this->S_m_plots_invoice_word.", ";
			$return_text .= $this->S_m_plots_invoice_start_no.", ";
			$return_text .= $this->S_m_plots_invoice_end_no.", ";
		}
		$return_text = substr($return_text,0,-2);
		return $return_text;
	}
	private function put_data_to_class($ans=array(), $select = "")
	{
		if ($select == "") {
			$select .= $this->B_ALL;
		}
		if (($select & $this->B_m_plots_sn) != 0) {
			$this->m_plots_sn = $ans["m_plots_sn"];
		}
		if (($select & $this->B_m_plots_id) != 0) {
			$this->m_plots_id = $ans["m_plots_id"];
		}
		if (($select & $this->B_m_id) != 0) {
			$this->m_id = $ans["m_id"];
		}
		if (($select & $this->B_m_plots_delete) != 0) {
			$this->m_plots_delete = $ans["m_plots_delete"];
		}
		if (($select & $this->B_m_plots_address) != 0) {
			$this->m_plots_address_country = $ans["m_plots_address_country"];
			$this->m_plots_address_post_code = $ans["m_plots_address_post_code"];
			$this->m_plots_address_state = $ans["m_plots_address_state"];
			$this->m_plots_address_city = $ans["m_plots_address_city"];
			$this->m_plots_address_line1 = $ans["m_plots_address_line1"];
			$this->m_plots_address_line2 = $ans["m_plots_address_line2"];
			$this->m_plots_address_line3 = $ans["m_plots_address_line3"];
		}
		if (($select & $this->B_m_plots_address_description) != 0) {
			$this->m_plots_address_description = $ans["m_plots_address_description"];
		}
		if (($select & $this->B_m_plots_lng) != 0) {
			$this->m_plots_lng = $ans["m_plots_lng"];
		}
		if (($select & $this->B_m_plots_lat) != 0) {
			$this->m_plots_lat = $ans["m_plots_lat"];
		}
		if (($select & $this->B_m_plots_alt) != 0) {
			$this->m_plots_alt = $ans["m_plots_alt"];
		}
		if (($select & $this->B_m_plots_name) != 0) {
			$this->m_plots_name = $ans["m_plots_name"];
		}
		if (($select & $this->B_m_plots_phone_number) != 0) {
			$this->m_plots_phone_number = $ans["m_plots_phone_number"];
		}
		if (($select & $this->B_m_plots_opening_time) != 0) {
			$this->m_plots_opening_time = $ans["m_plots_opening_time"];
		}
		if (($select & $this->B_m_plots_description) != 0) {
			$this->m_plots_description = $ans["m_plots_description"];
		}
		if (($select & $this->B_m_plots_community_name) != 0) {
			$this->m_plots_community_name = $ans["m_plots_community_name"];
		}
		if (($select & $this->B_m_plots_right_enable) != 0) {
			$this->m_plots_right_enable = $ans["m_plots_right_enable"];
		}
		if (($select & $this->B_m_plots_manager) != 0) {
			$this->m_plots_manager = $ans["m_plots_manager"];
		}
		if (($select & $this->B_m_is_close) != 0) {
			$this->m_is_close = $ans["m_is_close"];
		}
		if (($select & $this->B_m_close_temporary) != 0) {
			$this->m_close_temporary = $ans["m_close_temporary"];
		}
		if (($select & $this->B_m_plots_height) != 0) {
			$this->m_plots_height = $ans["m_plots_height"];
		}
		if (($select & $this->B_m_plots_paid_support) != 0) {
			$this->m_plots_can_ipass = $ans["m_plots_can_ipass"];
			$this->m_plots_can_easycard = $ans["m_plots_can_easycard"];
			$this->m_plots_can_icash20 = $ans["m_plots_can_icash20"];
		}
		if (($select & $this->B_m_plots_type) != 0) {
			$this->m_plots_type = $ans["m_plots_type"];
		}
		if (($select & $this->B_m_plots_auto_approach) != 0) {
			$this->m_plots_auto_approach = $ans["m_plots_auto_approach"];
		}
		if (($select & $this->B_m_plots_polygon) != 0) {
			$this->m_plots_polygon = $ans["m_plots_polygon"];
		}
		if (($select & $this->B_m_plots_file_logs) != 0) {
			$this->m_plots_file_logs = json_decode($ans["m_plots_file_logs"]);
		}
		if (($select & $this->B_m_plots_only_same_community) != 0) {
			$this->m_plots_only_same_community = $ans["m_plots_only_same_community"];
		}
		if (($select & $this->B_m_plots_space_count) != 0) {
			$this->m_plots_space_count = $ans["m_plots_space_count"];
		}
		if (($select & $this->B_m_plots_free_pk_amount) != 0) {
			$this->m_plots_free_pk_amount = $ans["m_plots_free_pk_amount"];
		}
		if (($select & $this->B_m_plots_invoice_data) != 0) {
			$this->m_plots_invoice_type = $ans["m_plots_invoice_type"];
			$this->m_plots_invoice_pos = $ans["m_plots_invoice_pos"];
			$this->m_plots_invoice_last_num = substr($ans["m_plots_invoice_last_num"],2);
			$this->m_plots_invoice_last_word = substr($ans["m_plots_invoice_last_num"],0,2);
			$this->m_plots_invoice_month = $ans["m_plots_invoice_month"];
			$this->m_plots_invoice_word = $ans["m_plots_invoice_word"];
			$this->m_plots_invoice_start_no = $ans["m_plots_invoice_start_no"];
			$this->m_plots_invoice_end_no = $ans["m_plots_invoice_end_no"];
		}
		if (($select & $this->B_m_plots_rating) != 0) {
			global $conn, $dbName;
			check_conn($conn,$dbName);
			$memcache = get_memcache();
			if ($memcache) {
				$tmp_rating = $memcache->get($dbName . '_UPK_ParkingLot.m_plots_rating:' . $ans["m_plots_id"]);
			}
			if (!$tmp_rating) {
				$this->m_plots_rating = 10;//$tmp_rating;
				$this->GetParkingSpaces("", $this->B_m_plots_rating);//算一次rating
				if ($memcache)
					$memcache->set($dbName . '_UPK_ParkingLot.m_plots_rating:' . $ans["m_plots_id"], $this->m_plots_rating, MEMCACHE_COMPRESSED, 86400);
			} else {
				$this->m_plots_rating = 10;//$tmp_rating;
			}
		}
		if (($select & $this->B_m_plots_file_logs) != 0) {
			$this->file_logs = json_decode($ans["m_plots_file_logs"]);
		}
		if (($select & $this->B_m_plots_is_lpr_tower) != 0) {
			$this->m_plots_is_lpr_tower = $ans["m_plots_is_lpr_tower"];
		}
		if (($select & $this->B_m_plots_member_ref_profit_earn) != 0) {
			$this->m_plots_member_ref_profit_earn = $ans["m_plots_member_ref_profit_earn"];
		}
	}
	public function is_valid()
	{
		return $this->is_valid;
	}
}
?>