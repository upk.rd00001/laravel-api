<?php
include_once("UPKclass_ParkingSpace.php");
include_once("UPKclass_TimeComponet.php");
include_once("UPKclass_PricingLog.php");

include_once("UPKclass_ParkingLog.php");
include_once("UPKclass_EvaluationLog.php");
include_once("UPKclass_PayLog.php");
include_once("UPKclass_MemberVehicle.php");
include_once("/../fn_package.php");

class UPK_BookingLog extends TimeComponent
{
	private $m_bkl_sn;
	private $m_bkl_id;
	private $m_bkl_create_datetime;
	private $m_pk_id;
	private $m_ppl_id;
	private $m_ve_id;
	private $m_bkl_creator_m_id;
	private $m_ve_m_id;
	private $m_plots_id;

	private $m_bkl_book_type;
	private $m_bkl_start_date;
	private $m_bkl_end_date;
	private $m_bkl_start_time;
	private $m_bkl_end_time;
	private $m_bkl_estimate_point;
	private $m_bkl_estimate_free_point;
	private $m_bkl_estimate_point_int;
	private $m_bkl_estimate_free_point_int;
	private $m_bkl_estimate_free_parking_point;
	private $m_bkl_estimate_paid_parking_point;
	private $m_bkl_estimate_total_point;
	private $m_bkl_estimate_total_point_int;
	private $m_bkl_earlybird_offer;
	private $m_ppl_promotion_code;
	private $m_bkl_visit_description;
	private $m_bkl_cancel;
	private $m_id_cancel;
	private $m_bkl_group_id;
	private $m_bkl_sendpush;
	private $m_bkl_gov_id;
	private $m_bkl_is_paid;
	private $m_bkl_is_pd;
	private $m_bkl_transfer_m_id;
	private $m_bkl_transfer_cellphone;
	private $m_bkl_is_over_night;
	private $m_bkl_plots_type;
	private $m_bkl_is_end;
	private $m_bkl_due_point;
	private $m_bkl_whitelist_point;
	private $dcp_id;
	private $m_bkl_discount_point;
	//view * 4
	private $m_bkl_gov_total_price_without_pay;
	private $m_bkl_disp_unpaid_page;
	private $m_bkl_is_customize;
	private $m_bkl_customize_price;
	private $m_bkl_is_fleeing_fee;
	private $m_bkl_fleeing_fee;
	private $m_bkl_handling_fee;
	private $m_bkl_market_handling_fee;
	private $is_paid_valid_datetime;

	//自訂義參數
	private $m_bkl_total_time;
	private $m_pk_owner_m_id;
	private $m_pk_agent_m_id;
	private $unq_m_bkl_group_id;
	private $m_plots_address_city;
	private $m_bkl_scan_qrcode_paytime;
	private $m_bkl_scan_qrcode_payfee;
	private $m_bkl_in_or_out;
	private $m_bkl_paid_desc;

	private $m_bkl_view_point;
	private $m_bkl_view_parking_point;
	private $m_bkl_view_whitelist_point;
	private $m_bkl_view_discount_point;

	//擴充參數

	public $B_m_bkl_sn = 1;
	public $B_m_bkl_id = 2;
	public $B_m_bkl_create_datetime = 4;
	public $B_m_pk_id = 8;
	public $B_m_ppl_id = 16;
	public $B_m_ve_id = 32;
	public $B_m_bkl_creator_m_id = 131072;
	public $B_m_plots_data = 4096;
	public $B_m_bkl_book_type = 64;
	public $B_m_bkl_datetime = 128;
	/*public $B_m_bkl_start_date		=128;
	public $B_m_bkl_end_date		=256;
	public $B_m_bkl_start_time		=512;
	public $B_m_bkl_end_time		=1024;*/
	public $B_m_bkl_points = 2048;    //各種點數 points有s 欄位沒s
	/*public $B_m_bkl_estimate_point		=2048;
	public $B_m_bkl_estimate_free_point		=4096;*/
	public $B_m_bkl_earlybird_offer = 8192;
	public $B_m_ppl_promotion_code = 16384;
	public $B_m_bkl_cancel = 32768;
	public $B_m_ve_data = 65536;
	/*	public $B_m_ve_share_code		=65536;
		public $B_m_ve_length		=131072;
		public $B_m_ve_width		=262144;
		public $B_m_ve_height		=524288;
		public $B_m_ve_weight		=1048576;*/
	public $B_m_bkl_visit_description = 2097152;
	public $B_m_bkl_total_time = 4194304;
	public $B_m_bkl_group_id = 8388608;
	public $B_m_bkl_sendpush = 16777216;
	public $B_m_bkl_gov_id = 33554432;
	public $B_m_bkl_is_paid = 67108864;
	public $B_m_bkl_is_pd = 134217728;
	public $B_m_bkl_transfer_m_id = 268435456;
	public $B_m_bkl_is_over_night = 536870912;
	public $B_m_id_cancel = 1073741824;
	//public $B_m_plots_address_city = 131072;
	public $B_m_bkl_scan_qrcode_paytime = 262144;
	public $B_m_bkl_scan_qrcode_payfee = 524288;
	public $B_m_bkl_in_or_out = 1048576;
	//public $B_m_bkl_plots_type = 4096;
	public $B_m_bkl_is_end = 256;
	public $B_dcp_id = 512;
	public $B_m_bkl_disp_unpaid_page = 1024;
	//public $B_m_bkl_due_point = 512;
	//public $B_m_bkl_whitelist_point = 1024;
	/*
	public $B_m_ve_m_id			=268435456;
	public $B_m_pk_owner_m_id		=536870912;
	public $B_m_pk_agent_m_id			=1073741824;
	*/
	public $B_ALL;

	public $pay_log;
	public $parking_log;
	public $evaluation_log;
	public $vehicle_data;
	public $pricing_logs;
	public $qrcode_data;
	public $remark_data;
	public $discount_plan;
	public $device_data;

	public $deposit_array;//未來要做成CLASS
	public $last_deposit_data;//CLASS
	public $member_deposits;//2019-01-15 做成CLASS 有多筆deposit
	public $member_unpaid_deposits;//做成CLASS 有多筆unpaid deposit

	private $tiger_check;

	public function __construct($m_bkl_id, $sql_logic = "")
	{
		$this->B_ALL = $this->B_m_bkl_sn | $this->B_m_bkl_id | $this->B_m_bkl_create_datetime | $this->B_m_pk_id | $this->B_m_ppl_id | $this->B_m_ve_id | $this->B_m_bkl_creator_m_id | $this->B_m_bkl_book_type | $this->B_m_bkl_datetime | $this->B_m_bkl_points | $this->B_m_bkl_earlybird_offer | $this->B_m_ppl_promotion_code | $this->B_m_bkl_cancel | $this->B_m_ve_data | $this->B_m_bkl_visit_description | $this->B_m_bkl_total_time | $this->B_m_bkl_group_id | $this->B_m_bkl_sendpush | $this->B_m_bkl_gov_id | $this->B_m_bkl_is_paid | $this->B_m_bkl_is_pd | $this->B_m_bkl_transfer_m_id | $this->B_m_bkl_is_over_night | $this->B_m_id_cancel | $this->B_m_bkl_scan_qrcode_paytime | $this->B_m_bkl_scan_qrcode_payfee | $this->B_m_bkl_in_or_out | $this->B_m_bkl_is_end | $this->B_dcp_id | $this->B_m_bkl_disp_unpaid_page;
		$this->init_bkl_id($m_bkl_id, $sql_logic = "");
		$this->member_unpaid_deposits = array();
		$this->tiger_check = array();
	}

	public function init_bkl_id($m_bkl_id, $sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		$sql = "select * from tb_Member_ParkingSpace_Booking_Log where m_bkl_id = '" . $m_bkl_id . "' ";
		$sql .= $sql_logic;
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return (array("失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$this->m_bkl_id = $ans["m_bkl_id"];
			$this->m_bkl_group_id = $ans["m_bkl_group_id"];
			$this->m_bkl_sn = $ans["m_bkl_sn"];
			$this->m_bkl_create_datetime = $ans["m_bkl_create_datetime"];
			$this->m_pk_id = $ans["m_pk_id"];
			$this->m_ppl_id = $ans["m_ppl_id"];
			$this->m_ve_id = $ans["m_ve_id"];
			$this->m_bkl_creator_m_id = $ans["m_bkl_creator_m_id"];
			$this->m_ve_m_id = $ans["m_ve_m_id"];
			$this->m_plots_id = $ans["m_plots_id"];
			$this->m_bkl_book_type = $ans["m_bkl_book_type"];
			$this->m_bkl_start_date = $ans["m_bkl_start_date"];
			$this->m_bkl_end_date = $ans["m_bkl_end_date"];
			$this->m_bkl_start_time = $ans["m_bkl_start_time"];
			$this->m_bkl_end_time = $ans["m_bkl_end_time"];
			$this->m_bkl_estimate_point = $ans["m_bkl_estimate_point"];
			$this->m_bkl_estimate_free_point = $ans["m_bkl_estimate_free_point"];
			$this->m_bkl_estimate_free_parking_point = $ans["m_bkl_estimate_free_parking_point"];
			$this->m_bkl_estimate_paid_parking_point = $ans["m_bkl_estimate_paid_parking_point"];
			$this->m_bkl_estimate_total_point = $this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point;
			$this->m_bkl_earlybird_offer = $ans["m_bkl_earlybird_offer"];
			$this->m_ppl_promotion_code = $ans["m_ppl_promotion_code"];
			$this->m_bkl_visit_description = $ans["m_bkl_visit_description"];
			$this->m_bkl_cancel = $ans["m_bkl_cancel"];
			$this->m_id_cancel = $ans["m_id_cancel"];
			$this->m_bkl_sendpush = $ans["m_bkl_sendpush"];
			$this->m_bkl_gov_id = $ans["m_bkl_gov_id"];
			$this->m_bkl_is_paid = $ans["m_bkl_is_paid"];
			$this->m_bkl_is_pd = $ans["m_bkl_is_pd"];
			$this->m_bkl_is_pd = "1";//不再使用免費點數
			$this->m_bkl_transfer_m_id = $ans["m_bkl_transfer_m_id"];
			$this->m_bkl_is_over_night = $ans["m_bkl_is_over_night"];
			$this->m_plots_address_city = $ans["m_plots_address_city"];
			$this->m_bkl_scan_qrcode_paytime = $ans["m_bkl_scan_qrcode_paytime"];
			$this->m_bkl_scan_qrcode_payfee = $ans["m_bkl_scan_qrcode_payfee"];
			$this->m_bkl_in_or_out = $ans["m_bkl_in_or_out"];
			$this->m_bkl_plots_type = $ans["m_bkl_plots_type"];
			$this->m_bkl_is_end = $ans["m_bkl_is_end"];
			$this->m_bkl_due_point = $ans["m_bkl_due_point"];
			$this->m_bkl_discount_point = $ans["m_bkl_discount_point"];
			$this->dcp_id = $ans["dcp_id"];
			$this->m_bkl_whitelist_point = $ans["m_bkl_whitelist_point"];
			$this->m_bkl_gov_total_price_without_pay = $ans["m_bkl_gov_total_price_without_pay"];
			$this->m_bkl_disp_unpaid_page = $ans["m_bkl_disp_unpaid_page"];
			$this->m_bkl_is_customize = $ans["m_bkl_is_customize"];
			$this->m_bkl_customize_price = $ans["m_bkl_customize_price"];
			$this->m_bkl_is_fleeing_fee = $ans["m_bkl_is_fleeing_fee"];
			$this->m_bkl_fleeing_fee = $ans["m_bkl_fleeing_fee"];
			$this->m_bkl_handling_fee = $ans["m_bkl_handling_fee"];
			$this->m_bkl_market_handling_fee = $ans["m_bkl_market_handling_fee"];
			$this->is_paid_valid_datetime = $ans["is_paid_valid_datetime"];
			if ($this->dcp_id != "") {
				$this->discount_plan = new UPK_DiscountPlan($this->dcp_id);
			}
			if($this->m_ve_m_id == "") {
				$sql = "SELECT m_id FROM tb_Member_Vehicle WHERE m_ve_id='" . $this->m_ve_id . "' ";
				$result = mysql_query($sql, $conn);
				if (!$result) {
					return (array("失敗" => mysql_error($conn) . $sql));
				}
				else if (mysql_num_rows($result) >= 1) {
					$ans = mysql_fetch_assoc($result);
					#might be one result
					$this->m_ve_m_id = $ans["m_id"];
				}
				$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_ve_m_id='".$this->m_ve_m_id."'  WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
				$result = mysql_query($sql, $conn);
				if (!$result) {
					return (array("失敗" => mysql_error($conn) . $sql));
				}
			}
			if($this->m_plots_id == "") {
				$sql = "SELECT m_plots_id FROM tb_Member_ParkingSpace WHERE m_pk_id='" . $this->m_pk_id . "' ";
				$result = mysql_query($sql, $conn);
				if (!$result) {
					return (array("失敗" => mysql_error($conn) . $sql));
				}
				else if (mysql_num_rows($result) >= 1) {
					$ans = mysql_fetch_assoc($result);
					#might be one result
					$this->m_plots_id = $ans["m_plots_id"];
				}
				$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_plots_id='".$this->m_plots_id."'  WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
				$result = mysql_query($sql, $conn);
				if (!$result) {
					return (array("失敗" => mysql_error($conn) . $sql));
				}
			}

			$sql = "SELECT m_cellphone FROM tb_Member WHERE m_id='" . $this->m_bkl_transfer_m_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return (array("失敗" => mysql_error($conn) . $sql));
			}
			else if (mysql_num_rows($result) >= 1) {
				$ans = mysql_fetch_assoc($result);
				#might be one result
				$this->m_bkl_transfer_cellphone = $ans["m_cellphone"];
			}
			$sql = "SELECT m_pk_owner_m_id,m_pk_agent_m_id FROM tb_Member_ParkingSpace WHERE m_pk_id='" . $this->m_pk_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return (array("失敗" => mysql_error($conn) . $sql));
			}
			else if (mysql_num_rows($result) >= 1) {
				$ans = mysql_fetch_assoc($result);
				#might be one result
				$this->m_pk_owner_m_id = $ans["m_pk_owner_m_id"];
				$this->m_pk_agent_m_id = $ans["m_pk_agent_m_id"];
			}
			if ($this->m_bkl_is_paid == "1") {
				//友付款才合併
				if ($this->m_bkl_group_id == null) {
					$this->unq_m_bkl_group_id = $m_bkl_id;
					#第一筆，試著找最後一筆
					$sql = "SELECT m_bkl_end_date,m_bkl_end_time FROM tb_Member_ParkingSpace_Booking_Log WHERE (m_bkl_group_id = '" . $m_bkl_id . "' OR m_bkl_id = '" . $m_bkl_id . "') AND m_bkl_is_paid='1'  ORDER BY m_bkl_id DESC";
					$sql .= $sql_logic;
					$result = mysql_query($sql, $conn);
					if (!$result) {
						return (array("失敗" => mysql_error($conn) . $sql));
					}
					else if (mysql_num_rows($result) >= 1) {
						$ans = mysql_fetch_assoc($result);
						#撈最後一筆
						$this->m_bkl_end_date = $ans["m_bkl_end_date"];
						$this->m_bkl_end_time = $ans["m_bkl_end_time"];

					}
					else {
						#只有一筆
					}
					$sql = "SELECT sum(m_bkl_estimate_free_point) as total_free_points,sum(m_bkl_estimate_point) as total_points FROM tb_Member_ParkingSpace_Booking_Log WHERE (m_bkl_id = '" . $m_bkl_id . "' OR m_bkl_group_id = '" . $m_bkl_id . "') AND m_bkl_is_paid='1'";
					$sql .= $sql_logic;
					$result = mysql_query($sql, $conn);
					if (!$result) {
						return (array("失敗" => mysql_error($conn) . $sql));
					}
					else if (mysql_num_rows($result) >= 1) {
						$ans = mysql_fetch_assoc($result);
						$this->m_bkl_estimate_point = $ans["total_points"];
						$this->m_bkl_estimate_free_point = $ans["total_free_points"];
					}
				}
				else {
					$this->unq_m_bkl_group_id = $this->m_bkl_group_id;
					#中間或最後
					//找第一筆
					$sql = "SELECT * from tb_Member_ParkingSpace_Booking_Log where m_bkl_id = '" . $this->m_bkl_group_id . "' AND m_bkl_is_paid='1' ";
					$sql .= $sql_logic;
					$result = mysql_query($sql, $conn);
					if (!$result) {
						return (array("失敗" => mysql_error($conn) . $sql));
					}
					else if (mysql_num_rows($result) == 1) {
						$ans = mysql_fetch_assoc($result);
						#撈第一筆
						$this->m_bkl_start_date = $ans["m_bkl_start_date"];
						$this->m_bkl_start_time = $ans["m_bkl_start_time"];
					}
					else {
						#不可能!!
						return (array("失敗" => mysql_error($conn) . $sql));
					}
					#算總金額
					$sql = "SELECT m_bkl_end_date,m_bkl_end_time from tb_Member_ParkingSpace_Booking_Log where (m_bkl_id = '" . $this->m_bkl_group_id . "' OR m_bkl_group_id = '" . $this->m_bkl_group_id . "') AND m_bkl_is_paid='1' ORDER BY m_bkl_id DESC";
					$sql .= $sql_logic;
					$result = mysql_query($sql, $conn);
					if (!$result) {
						return (array("失敗" => mysql_error($conn) . $sql));
					}
					else if (mysql_num_rows($result) >= 1) {
						$ans = mysql_fetch_assoc($result);
						#撈最後一筆
						$this->m_bkl_end_date = $ans["m_bkl_end_date"];
						$this->m_bkl_end_time = $ans["m_bkl_end_time"];

					}
					else {
						#不可能!!
						return (array("失敗" => mysql_error($conn) . $sql));
					}
					$sql = "SELECT sum(m_bkl_estimate_free_point) as total_free_points,sum(m_bkl_estimate_point) as total_points from tb_Member_ParkingSpace_Booking_Log where (m_bkl_id = '" . $this->m_bkl_group_id . "' OR m_bkl_group_id = '" . $this->m_bkl_group_id . "') AND m_bkl_is_paid='1'";
					$sql .= $sql_logic;
					$result = mysql_query($sql, $conn);
					if (!$result) {
						return (array("失敗" => mysql_error($conn) . $sql));
					}
					else if (mysql_num_rows($result) >= 1) {
						$ans = mysql_fetch_assoc($result);
						$this->m_bkl_estimate_point = $ans["total_points"];
						$this->m_bkl_estimate_free_point = $ans["total_free_points"];

					}
				}
			}
			else {//沒付款就用原本的m_bkl_id
				$this->unq_m_bkl_group_id = $m_bkl_id;
			}

			$this->m_bkl_id = $this->unq_m_bkl_group_id;
			$this->m_bkl_total_time = DisplayBookingLogTime($this->m_bkl_book_type, $this->m_bkl_start_date, $this->m_bkl_start_time, $this->m_bkl_end_date, $this->m_bkl_end_time);
			parent::__construct($this->m_bkl_start_date, $this->m_bkl_end_date, $this->m_bkl_start_time, $this->m_bkl_end_time);
		}
		else {
			//多個相同ID
			return (array("有多個相同預約ID" => mysql_error($conn) . $sql));
		}
	}

	public function AutoInsertParkingLogFunc()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		include_once("/../pay_api/AutoInsertParkingLogFunc.php");
		AutoInsertParkingLogFunc($conn, $this->m_bkl_id);
	}

	public function GetPricingLog($sql_logic = "", $memcache = false)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$tmp_pricing_log = null;
		$memcache = get_memcache();
		if ($memcache) {
			$tmp_pricing_log = $memcache->get($dbName . '_UPK_PricingLog:' . $this->m_ppl_id);
		}
		else {
			#記憶體快取不可用 先不做事反正都要重撈
		}
		//檢查pricing型態是否正常，若前次發生  Fatal error: Allowed memory size of 52428800 bytes exhausted (tried to allocate 77 bytes)
		//則有機率在下次get時發生錯誤
		//則撈一次pricing_log
		if ($tmp_pricing_log instanceof UPK_PricingLog) {
		}
		else {
			$tmp_pricing_log = MemcacheSetPricingLog('_UPK_PricingLog:', $this->m_ppl_id, $memcache);
		}
		$this->pricing_logs = $tmp_pricing_log;
		return (array("result" => 1));
	}

	public function GetEvaluationLog($sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);

		$sql = "select m_pel_id from tb_Member_Parking_Evaluation_Log where m_bkl_id = '" . $this->m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return (array("失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) >= 1)//記得改回==
		{
			$ans = mysql_fetch_assoc($result);
			$this->evaluation_log = new UPK_EvaluationLog($ans["m_pel_id"], $sql_logic);
		}
		else if (mysql_num_rows($result) != 0) {
			//多個相同ID
			return (array("有多個回報紀錄" => mysql_error($conn) . $sql));
		}
		return (array("result" => 1));
	}

//先撈有m_id的如果撈不到再撈沒有m_id的
	public function GetBookingLogView($m_id,$args=array())
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql_where = "";
		if(isset($args["point"])) {
			//超商條碼專用，付款成功帶入這個值
			 $sql_where .= " AND m_blv_due_point='".$args["point"]."' ";
		}
		//不要判斷是否刪除，直接拿該會員的最後一筆
		$sql = "SELECT * from tb_Member_ParkingSpace_Booking_Log_View WHERE m_id='" . $m_id . "' AND m_bkl_id='" . $this->m_bkl_id . "' ".$sql_where." ORDER BY m_blv_sn DESC LIMIT 1";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return false;
		}
		elseif (mysql_num_rows($result) == 0) {
			$sql = "SELECT * from tb_Member_ParkingSpace_Booking_Log_View WHERE m_bkl_id='" . $this->m_bkl_id . "' ".$sql_where." ORDER BY m_blv_sn DESC LIMIT 1";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return false;
			}
			elseif (mysql_num_rows($result) == 0) {

				$sql = "select * from tb_Member_ParkingSpace_Booking_Log where m_bkl_id='" . $this->m_bkl_id . "' and m_bkl_cancel='0' ";
				$result_booking = mysql_query($sql, $conn);
				if (!$result_booking) {
					return false;
				}
				else if (mysql_num_rows($result_booking) == 0) {
					return false;
				}
				else if (mysql_num_rows($result_booking) > 1) {
					return false;
				}
				$ans = mysql_fetch_array($result_booking);
				$this->m_bkl_view_point = $ans["m_bkl_view_point"];
				$this->m_bkl_view_parking_point = $ans["m_bkl_view_parking_point"];
				$this->m_bkl_view_whitelist_point = $ans["m_bkl_view_whitelist_point"];
				$this->m_bkl_view_discount_point = $ans["m_bkl_view_discount_point"];
				$this->m_bkl_due_point = $ans["m_bkl_due_point"];
				$this->dcp_id = $ans["dcp_id"];
				return true;
			}
			else {
				$ans = mysql_fetch_assoc($result);
				$this->m_bkl_view_point = $ans["m_blv_point"];
				$this->m_bkl_view_parking_point = $ans["m_blv_parking_point"];
				$this->m_bkl_view_whitelist_point = $ans["m_blv_whitelist_point"];
				$this->m_bkl_view_discount_point = $ans["m_blv_discount_point"];
				$this->m_bkl_due_point = $ans["m_blv_due_point"];
				$this->dcp_id = $ans["m_blv_dcp_id"];
				return true;
			}
		}
		else {
			$ans = mysql_fetch_assoc($result);
			$this->m_bkl_view_point = $ans["m_blv_point"];
			$this->m_bkl_view_parking_point = $ans["m_blv_parking_point"];
			$this->m_bkl_view_whitelist_point = $ans["m_blv_whitelist_point"];
			$this->m_bkl_view_discount_point = $ans["m_blv_discount_point"];
			$this->m_bkl_due_point = $ans["m_blv_due_point"];
			$this->dcp_id = $ans["m_blv_dcp_id"];
			return true;
		}
	}

	public function UpdateView2Data()
	{
		$this->m_bkl_estimate_point = $this->m_bkl_view_point;//改放實付金額
		$this->m_bkl_estimate_free_point = $this->m_bkl_view_parking_point;//free_point //mobile用這個欄位顯示付款金額的
		$this->m_bkl_estimate_point_int = (int)$this->m_bkl_view_point;//改放實付金額
		$this->m_bkl_estimate_free_point_int = (int)$this->m_bkl_view_parking_point;//free_point //mobile用這個欄位顯示付款金額的
		$total_points = $this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point;
		$this->m_bkl_estimate_total_point = (int)$total_points;
		$this->m_bkl_whitelist_point = $this->m_bkl_view_whitelist_point;
		$this->m_bkl_discount_point = $this->m_bkl_due_point - $total_points;
		$this->m_bkl_due_point = (int)$this->m_bkl_due_point;
	}

	public function UpdateBookingLogView($m_id, $point, $parking_point, $whitelist_point, $discount_point, $due_point, $dcp_id)
	{

		$this->tiger_check = array();
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$pure_data["m_id"] = $m_id;
		$pure_data["point"] = $point;
		$pure_data["parking_point"] = $parking_point;
		$pure_data["whitelist_point"] = $whitelist_point;
		$pure_data["discount_point"] = $discount_point;
		$pure_data["due_point"] = $due_point;
		$pure_data["dcp_id"] = $dcp_id;
		$language = "zh-tw";
		//Temp id before we give it proper one
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		$tmp_m_blv_id = "tmp" . GenerateRandomString(11, "0123456789");
		$sql = "INSERT INTO tb_Member_ParkingSpace_Booking_Log_View (`m_blv_id`, `m_id`, `m_bkl_id`,
 			`m_blv_point`, `m_blv_parking_point`, `m_blv_whitelist_point`, `m_blv_discount_point`, `m_blv_due_point`, `m_blv_dcp_id`) 
 			VALUES ('" . $tmp_m_blv_id . "','" . $m_id . "','" . $this->m_bkl_id . "',
 			'" . $point . "','" . $parking_point . "','" . $whitelist_point . "','" . $discount_point . "','" . $due_point . "','" . $dcp_id . "')";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約失敗", "description" => mysql_error()));
		}
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));

		//Time to give proper id. Few steps involved.
		$sql = "SELECT m_blv_sn from tb_Member_ParkingSpace_Booking_Log_View WHERE m_blv_id='" . $tmp_m_blv_id . "' ";

		$result = mysql_query($sql, $conn);
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約失敗", "description" => mysql_error()));
		}

		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		$ans = mysql_fetch_assoc($result);
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		$new_m_blv_id = $ans["m_blv_sn"];
		Sn2Id("BLVD", $new_m_blv_id);

		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log_View SET m_blv_id='" . $new_m_blv_id . "' WHERE m_blv_sn='" . $ans["m_blv_sn"] . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約失敗", "description" => mysql_error()));
		}

		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		//刪除舊的
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log_View SET m_blv_delete=1 WHERE m_id='" . $m_id . "' AND m_bkl_id='" . $this->m_bkl_id . "' AND m_blv_sn<" . $ans["m_blv_sn"];
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約失敗", "description" => mysql_error()));
		}

		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
	}

	public function GetRemark($sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);

		$sql = "select rmk_id from tb_Remark where rmk_type_id = '" . $this->m_bkl_id . "' AND rmk_type='4' AND rmk_delete='0' order by rmk_sn desc ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return (array("失敗" => mysql_error($conn) . $sql));
		}
		$this->remark_data = array();
		while ($ans = mysql_fetch_assoc($result)) {
			$this_remark_data = new UPK_Remark($ans["rmk_id"], $sql_logic);
			array_push($this->remark_data, $this_remark_data);
		}
		return (array("result" => 1));
	}

	public function GetMemberVehicle($sql_logic = "")
	{
		$this->vehicle_data = new UPK_MemberVehicle($this->m_ve_id, $sql_logic);
		return (array("result" => 1));
	}

	public function GetDiscountPlan($sql_logic = "")
	{
		if ($this->dcp_id != "") {
			$this->discount_plan = new UPK_DiscountPlan($this->dcp_id, $sql_logic);
		}
		return (array("result" => 1));
	}

	public function GetParkingLog($sql_logic = "")
	{
		$this->parking_log = new UPK_ParkingLog("", $sql_logic);//先給預設值
		global $conn, $dbName;
		check_conn($conn, $dbName);
		if ($this->m_bkl_group_id != "")
			$sql = "select m_pl_id from tb_Member_Parking_Log where m_bkl_id = '" . $this->m_bkl_group_id . "' ";
		else
			$sql = "select m_pl_id from tb_Member_Parking_Log where m_bkl_id = '" . $this->m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return (array("失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) >= 1) {
			//記得改回==
			$ans = mysql_fetch_assoc($result);
			$this->parking_log = new UPK_ParkingLog($ans["m_pl_id"], $sql_logic);
		}
		else if (mysql_num_rows($result) != 0) {
			//多個相同ID
			return (array("有多個停車紀錄" => mysql_error($conn) . $sql));
		}
		return (array("result" => 1));
	}

	public function GetPayLog($sql_logic = "")
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "select m_pyl_id from tb_Member_Pay_Log where m_bkl_id = '" . $this->m_bkl_id . "' ";

		$result = mysql_query($sql, $conn);
		if (!$result) {
			return (array("result" => 0, "失敗" => mysql_error($conn) . $sql));
		}
		//301 302 有可能繳費之後沒有出場，錢又繼續算所以有可能有多筆繳費紀錄
		else if (mysql_num_rows($result) >= 1) {
			$ans = mysql_fetch_assoc($result);
			$this->pay_log = new UPK_PayLog($ans["m_pyl_id"], $sql_logic);
		}
		else if (mysql_num_rows($result) != 0) {
			//多個相同ID
			return (array("result" => 0, "有多個付費紀錄" => mysql_error($conn) . $sql));
		}
		return (array("result" => 1));
	}

	public function GetQrcode($conn, $sql_logic = "")
	{
		$this->qrcode_data = null;
		$sql_qrcode = "SELECT * FROM tb_ParkingMeter_Qrcode WHERE m_bkl_id = '" . $this->m_bkl_group_id . "' OR m_bkl_id = '" . $this->m_bkl_id . "' ";
		$result_qrcode = mysql_query($sql_qrcode, $conn);
		if (!$result_qrcode) {

		}
		else if (mysql_num_rows($result_qrcode) == 1) {
			$ans_qrcode = mysql_fetch_assoc($result_qrcode);
			$ans_qrcdoe2["ung_bkl_group_id"] = $this->unq_m_bkl_group_id;
			$ans_qrcdoe2["paytype"] = $ans_qrcode["pmq_paytype"];
			$ans_qrcdoe2["id"] = $ans_qrcode["pmq_serial"];
			$ans_qrcdoe2["bayid"] = $ans_qrcode["pmq_bayid"];
			$ans_qrcdoe2["deposit"] = $ans_qrcode["pmq_deposit"];
			$ans_qrcdoe2["time"] = $ans_qrcode["pmq_time"];
			$ans_qrcdoe2["cost"] = $ans_qrcode["pmq_cost"];
			$this->qrcode_data = $ans_qrcdoe2;
		}
		return (array("result" => 1));
	}

	//更新舊資料用的
	public function updateIsPaidValidDatetime()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		//只有301有可能付款後沒出場需補付款
		//302是付款視為出場，所以不可能付款無出場
		//建立繳費單會有已出場但未付款
		//白名單會未付款可直接出場
		$is_paid_valid_datetime = "";
		$is_paid = "1";
		if ($this->parking_log == null) {
			$this->GetParkingLog();
		}
		if ($this->m_bkl_plots_type == "0") {
			//300跟預約 is_paid永遠正確
			//不過還是判斷一下deposit cnt
			if ($this->getDepositCnt() >= 1) {
				$is_paid_valid_datetime = "2199-12-31 23:59:59";
				$is_paid = "1";
			}
			else {
				$is_paid_valid_datetime = "2199-12-31 23:59:59";
				$is_paid = "0";
			}
		}
		elseif ($this->m_bkl_plots_type == "300") {
			//300跟預約 is_paid永遠正確
			//不過還是判斷一下deposit cnt
			if ($this->getDepositCnt() >= 1) {
				$is_paid_valid_datetime = "2199-12-31 23:59:59";
				$is_paid = "1";
			}
			else {
				$is_paid_valid_datetime = "2199-12-31 23:59:59";
				$is_paid = "0";
			}
		}
		elseif ($this->m_bkl_plots_type == "100") {
			//路邊要point+free_point不等於零代表有付款過 因為會先出場，所以不能用出場時間判斷
			if (($this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point) != 0) {
				$is_paid_valid_datetime = "2199-12-31 23:59:59";
				$is_paid = "1";
			}
			else {
				$is_paid_valid_datetime = "2199-12-31 23:59:59";
				$is_paid = "0";
			}
		}
		elseif ($this->m_bkl_plots_type == "301") {
			//掃馬進場繳費出場，需要出場才能用2199-12-31否則判斷繳費時間+15分
			//建立繳費單會有已出場但未付款
			//白名單會未付款可直接出場
			//退款全退要算未付款，半退算已付款 有停車點數折抵全退算已付款
			$this->GetParkingLog(); //強制重撈不然會會使用到舊資料
			if($this->getDepositCnt()>1) {
				//有付款未出場要有15分緩衝
				if ($this->parking_log->get_m_pl_end_time() != "") {
					$is_paid_valid_datetime = "2199-12-31 23:59:59";
					$is_paid = "1";
				}
				else {
					$this->GetDeposit();
					$DT_last_deposit_pay_datetime_p_15m = new DateTime($this->last_deposit_data->get_m_dpt_pay_datetime());
					$DT_last_deposit_pay_datetime_p_15m->modify("+15 minute");
					$is_paid_valid_datetime = $DT_last_deposit_pay_datetime_p_15m->format("Y-m-d H:i:s");
					$is_paid = "1";
				}
			}
			else {
				if($this->reCalculatePrice($this->get_m_ve_m_token())==0) {
					//如果沒有付過款，且他應付金額為0，則代表是白名單或不須付款
					if ($this->parking_log->get_m_pl_end_time() != "") {
						$is_paid_valid_datetime = "2199-12-31 23:59:59";
						$is_paid = "1";
					}
					else {
						$is_paid_valid_datetime = "2199-12-31 23:59:59";
						$is_paid = "0";
					}
				}
				else {
					if ($this->parking_log->get_m_pl_end_time() != "") {
						$is_paid_valid_datetime = "2199-12-31 23:59:59";
						$is_paid = "0";
					}
					else {
						$is_paid_valid_datetime = "2199-12-31 23:59:59";
						$is_paid = "0";
					}
				}
			}
		}
		elseif ($this->m_bkl_plots_type == "302") {
			//車牌辨識 繳費視同出場
			//建立繳費單會有已出場但未付款
			//白名單會未付款可直接出場
			//退款全退要算未付款，半退算已付款 有停車點數折抵全退算已付款
			$this->GetParkingLog(); //強制重撈不然會會使用到舊資料
			if($this->getDepositCnt()>1) {
				if ($this->parking_log->get_m_pl_end_time() != "") {
					$is_paid_valid_datetime = "2199-12-31 23:59:59";
					$is_paid = "1";
				}
				else {
					$this->GetDeposit();
					$DT_last_deposit_pay_datetime_p_15m = new DateTime($this->last_deposit_data->get_m_dpt_pay_datetime());
					$DT_last_deposit_pay_datetime_p_15m->modify("+15 minute");
					$is_paid_valid_datetime = $DT_last_deposit_pay_datetime_p_15m->format("Y-m-d H:i:s");
					$is_paid = "1";
				}
			}
			else {
				if($this->reCalculatePrice($this->get_m_ve_m_token())==0) {
					//如果沒有付過款，且他應付金額為0，則代表是白名單或不須付款
					if ($this->parking_log->get_m_pl_end_time() != "") {
						$is_paid_valid_datetime = "2199-12-31 23:59:59";
						$is_paid = "1";
					}
					else {
						$is_paid_valid_datetime = "2199-12-31 23:59:59";
						$is_paid = "0";
					}
				}
				else {
					if ($this->parking_log->get_m_pl_end_time() != "") {
						$is_paid_valid_datetime = "2199-12-31 23:59:59";
						$is_paid = "0";
					}
					else {
						$is_paid_valid_datetime = "2199-12-31 23:59:59";
						$is_paid = "0";
					}
				}
			}
		}
		else {
			//不知道是哪一種停車模式 當作未付款去計算應該不會錯
			$is_paid_valid_datetime = "2199-12-31 23:59:59";
			$is_paid = "0";
		}
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET is_paid_valid_datetime='" . $is_paid_valid_datetime . "',
			m_bkl_is_paid='" . $is_paid . "' 
			WHERE m_bkl_id='" . $this->m_bkl_id . "' OR m_bkl_group_id='" . $this->m_bkl_id . "' ";
		$result = mysql_query($sql,$conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新失敗", "description" => mysql_error()));
		}
		else
			return json_encode(array("result" => 1));
	}

	//退款後還原成要顯示在繳費紀錄頁面
	public function updateUnpaidPage() {
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$is_unpaid_page_unpaid = 0; //補件繳費的單是否位付款

		if ($this->m_bkl_plots_type == "0") {
			if($this->getDepositCnt()==0)
				$is_unpaid_page_unpaid = 1;
		}
		elseif ($this->m_bkl_plots_type == "100") {
			if($this->getDepositCnt()<=1)
				$is_unpaid_page_unpaid = 1;
		}
		elseif ($this->m_bkl_plots_type == "300") {
			if($this->getDepositCnt()==0)
				$is_unpaid_page_unpaid = 1;
		}
		elseif ($this->m_bkl_plots_type == "301") {
			if($this->getDepositCnt()<=1)
				$is_unpaid_page_unpaid = 1;
		}
		elseif ($this->m_bkl_plots_type == "302") {
			if($this->getDepositCnt()<=1)
				$is_unpaid_page_unpaid = 1;
		}
		if($is_unpaid_page_unpaid==1 && $this->get_m_bkl_disp_unpaid_page()=="2") {
			$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_disp_unpaid_page='1' WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
			$result = mysql_query($sql,$conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "更新失敗", "description" => mysql_error()));
			}
			else
				return json_encode(array("result" => 1));
		}
		return json_encode(array("result" => 1));
	}
	public function checkIsPaidAvailable() {
		$DT_now = new DateTime();
		$DT_is_paid_valid_datetime = new DateTime($this->is_paid_valid_datetime);
		if($this->m_bkl_is_paid=="1" && $DT_is_paid_valid_datetime>$DT_now) {
			return true;
		}
		else return false;
	}
	/** 重算實付金額
	 *
	 * @param string $token 代表要用哪個角色來算這筆訂單(帶入Calculate)
	 * @param string $sql_logic 無用
	 * @param array $args 整個包給GovCalculate用來做為要不要算到一半的依據
	 * @return int 代表應付總金額，0代表不需付款
	 */
	public function reCalculatePrice($token, $sql_logic = "",$args = array())
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		//重算實付金額
		$this->tiger_check = array();
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		if ($this->deposit_array == null) {
			$this->GetDeposit();
		}
		if ($this->parking_log == null) {
			$this->GetParkingLog();
		}
		if ($this->vehicle_data == null) {
			$this->GetMemberVehicle();
		}
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		//如果是超商條碼繳費的話，就算已經出場也要重算(不進入IF內)
		//如果不是超商條碼繳費但以出廠的話，就直接以資料庫的值作顯示(已完結單)
		$tmp_barcode_array = $this->GetUnpaidBarcodeArray();
		if(count($tmp_barcode_array)!=0) {
			$total_point = 0;
			foreach($tmp_barcode_array as $each_barcode_data) {
				$total_point += $each_barcode_data["m_dpt_amount"];
			}
			$this->CalcMemberParkingPoints($total_point);
			return $total_point;
		}
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		if ($this->parking_log->get_m_pl_end_time() != "" && $this->m_bkl_plots_type != "100" && $this->m_bkl_disp_unpaid_page != 1) {
			//301與302沒出場在繳費頁面的時候要重算，300在後台已出場的就直接回傳已付的值
			//如果有出場 而且有繳款 回傳0
			//路邊停車是出場後才結算付款，所以重算不能用出場來判斷
			if ($this->getDepositCnt() > 1) {
				return 0;
			}
			//return $this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point;
		}

		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		if ($this->parking_log->get_m_pl_end_time() != "" && $this->m_bkl_plots_type == "100" && ($this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point) > 0) {
			//路邊理論上只能付成功一次，有付成功就不用再付款，因為也不會有補付款
			if ($this->getDepositCnt() > 1) {
				return 0;
			}
			else {
				return $this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point;
			}
		}
		//如果路邊停車出場後但是沒有扣款成功，就要用當時出場的總金額來推算現金與點數讓民眾付款
		else if ($this->parking_log->get_m_pl_end_time() != "" && $this->m_bkl_plots_type == "100" && ($this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point) == 0 && $this->m_bkl_gov_total_price_without_pay > 0) {
			$total_points = $this->m_bkl_gov_total_price_without_pay;
			$member_paid_class = new UPK_MemberPaid($this->m_ve_m_id);
			$total_pd_points = $member_paid_class->get_total_pd_points();
			if ($total_points > $total_pd_points) {
				#如果停車點數不足
				if($total_pd_points <= 0) {
					$tmp_free_point = 0;
					$tmp_point = $total_points;
				}
				else {
					$tmp_free_point = $total_pd_points;
					$tmp_point = $total_points - $total_pd_points;
				}
				$discountt = $tmp_point / $total_points;
			}
			else {
				#停車點數足夠
				$tmp_free_point = $total_points;
				$tmp_point = 0;
				$discountt = 0;
			}
			$not_count_price_package = array();
			array_push($not_count_price_package, array(
				"id" => $this->m_pk_id,
				"start_date" => $this->m_bkl_start_date,
				"end_date" => $this->m_bkl_end_date,
				"start_time" => $this->m_bkl_start_time,
				"end_time" => $this->m_bkl_end_time));
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			include_once("/../main_api/GovPriceCalculateFunc_v2.php");
			$tmp = json_decode(GovPriceCalculateFunc2("GOV PRICE CALCULATE", $token, $not_count_price_package), true);//先算一下原價
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			if ($tmp["result"] == 1) {
				$original_total_point = $tmp["data"][0]["total"];
			}
			else $original_total_point = $total_points;
			$this->m_bkl_estimate_point = $tmp_point;//改放實付金額
			$this->m_bkl_estimate_free_point = $tmp_free_point;//free_point //mobile用這個欄位顯示付款金額的
			$this->m_bkl_estimate_point_int = (int)$tmp_point;//改放實付金額
			$this->m_bkl_estimate_free_point_int = (int)$tmp_free_point;//free_point //mobile用這個欄位顯示付款金額的
			$this->m_bkl_estimate_total_point = (int)$total_points;
			$this->m_bkl_whitelist_point = 0;
			$this->m_bkl_discount_point = $original_total_point - $total_points;
			$this->m_bkl_due_point = (int)$original_total_point;
			//$this->dcp_id = $ttans["data"][0]["dcp_id"];
			$this->m_bkl_is_pd = 1;
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			return $this->m_bkl_estimate_total_point;
		}
		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
		if ($this->m_bkl_plots_type == "0") {
			//預約
			if ($this->getDepositCnt() == 0) {
				return $this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point;
			}
			else return 0;
		}
		else if ($this->m_bkl_plots_type == "100") {
			$not_count_price_package = array();
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			include_once("/../main_api/GovPriceCalculateFunc_v2.php");
			if (strpos($this->m_plots_address_city, "新北市") !== false) {
				//新北市的邏輯
				$DT_pl_end_time = new DateTime($this->parking_log->get_m_pl_end_time());//如果是空的則是現在時間
				array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
				array_push($not_count_price_package, array(
					"id" => $this->m_pk_id,
					"start_date" => $this->m_bkl_start_date,
					"end_date" => $DT_pl_end_time->format("Y-m-d"),
					"start_time" => $this->m_bkl_start_time,
					"end_time" => $DT_pl_end_time->format("H:i:s")));
				$m_ve_special_level = 0;//先以一班車輛計算
				//因為路邊付款後會更新estimate_point跟point 重算金額可以知道要要補多少
				array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
				//後面的arg是用來判斷是否要算到有付款就不要算的參數
				$ttans = json_decode(GovPriceCalculateFunc("GOV PRICE CALCULATE", $token, $not_count_price_package, $this->m_bkl_id, "0", true, $this->m_ve_id,false,0,$args), true);

				$tmp_calc_data = array("activity"=>"GOV PRICE CALCULATE","token"=>$token,"data"=>$not_count_price_package,"m_bkl_id"=>$this->unq_m_bkl_group_id,"m_ve_id"=>$this->m_ve_id,"args"=>$args);
				array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0),"CalculateData"=>$tmp_calc_data));
				if (isset($ttans["result"]) && $ttans["result"] == 1) {
					#我付款頁面就濾掉以付款的
					//重新計算金額後要記得改總金額
					$this->m_bkl_end_date = $DT_pl_end_time->format("Y-m-d");
					$this->m_bkl_end_time = $DT_pl_end_time->format("H:i:s");
					//$booking_ans_array["deposit_data"]["m_dpt_point"]=$ttans["data"][0]["point"];
					//$booking_ans_array["deposit_data"]["m_dpt_amount"]=$ttans["data"][0]["point"];

					$this->m_bkl_estimate_point = $ttans["data"][0]["point"];//改放實付金額
					$this->m_bkl_estimate_free_point = $ttans["data"][0]["free_point"];//free_point //mobile用這個欄位顯示付款金額的
					$this->m_bkl_estimate_point_int = (int)$ttans["data"][0]["point"];//改放實付金額
					$this->m_bkl_estimate_free_point_int = (int)$ttans["data"][0]["free_point"];//free_point //mobile用這個欄位顯示付款金額的
					$this->m_bkl_estimate_total_point = (int)($ttans["data"][0]["point"] + $ttans["data"][0]["free_point"]);
					$this->m_bkl_whitelist_point = (int)$ttans["data"][0]["whitelist_point"];
					$this->m_bkl_discount_point = (int)$ttans["data"][0]["discount_point"];
					$this->m_bkl_due_point = (int)$ttans["data"][0]["due_point"];
					$this->dcp_id = $ttans["data"][0]["dcp_id"];
					$this->m_bkl_total_time = DisplayBookingLogTime($this->m_bkl_book_type, $this->m_bkl_start_date, $this->m_bkl_start_time, $DT_pl_end_time->format("Y-m-d"), $DT_pl_end_time->format("H:i:s"));
					$this->m_bkl_is_pd = $ttans["m_bkl_is_pd"];
					array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
					return $this->m_bkl_estimate_total_point;
				}
				else {
					//continue;//金額計算錯誤 有個車位不知道為啥踩掉禁停時段
					$this->m_bkl_estimate_total_point = 0;
					//$this->SetIsEnd();
					return 0;
				}
			}
			else {
				//其他路邊不計算
				$this->m_bkl_estimate_total_point = 0;
				//$this->SetIsEnd();
				return 0;
			}
		}
		else if ($this->m_bkl_plots_type == "300") {
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			if ($this->getDepositCnt() == 0) {
				$total_point = $this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point;
				$this->CalcMemberParkingPoints($total_point);
				array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
				return $total_point;
			}
			else return 0;
		}
		else if ($this->m_bkl_plots_type == "301" || $this->m_bkl_plots_type == "302") {
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			if ($this->vehicle_data == null) {
				$this->GetMemberVehicle();
			}
			$sql = "SELECT * FROM tb_Member_Deposit WHERE m_bkl_id='" . $this->m_bkl_id . "' AND m_dpt_pay_datetime IS NOT NULL";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "搜尋付款失敗", "description" => mysql_error($conn)));
			}
			$dpt_cnt = mysql_num_rows($result);
			$DT_this_m_dpt_pay_datetime = new DateTime($this->deposit_array["m_dpt_pay_datetime"]);
			$DT_now_m_15min = new DateTime();
			$DT_now_m_15min->modify("-15 min");
			//$booking_ans_array["deposit_data"]["m_dpt_pay_datetime"]=null;//塞回去... 301要能在這邊付款
			//首次付款不用判斷這個時間(dpt_cnt=1)的時候不用判斷，因為是假的零元付款
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			if ($DT_now_m_15min < $DT_this_m_dpt_pay_datetime && ($this->m_bkl_estimate_point + $this->m_bkl_estimate_free_point != "0" && $dpt_cnt > 1)) {
				//付款後15分鐘內不顯示
				return 0;
				//continue;
			}
			if($this->get_m_bkl_book_type() == "1002") {
				$DT_pl_end_time = new DateTime($this->m_bkl_end_date." ".$this->m_bkl_end_time);//月租採用預約結束時間
			}
			else {
				$DT_pl_end_time = new DateTime($this->parking_log->get_m_pl_end_time());//如果是空的則是現在時間
			}
			include_once("/../main_api/GovPriceCalculateFunc_v2.php");
			$not_count_price_package = array();
			$now = new DateTime();
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			array_push($not_count_price_package, array(
				"id" => $this->m_pk_id,
				"start_date" => $this->m_bkl_start_date,
				"end_date" => $DT_pl_end_time->format("Y-m-d"),
				"start_time" => $this->m_bkl_start_time,
				"end_time" => $DT_pl_end_time->format("H:i:s")));
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
			//後面的arg是用來判斷是否要算到有付款就不要算的參數
			$ttans = json_decode(GovPriceCalculateFunc("GOV PRICE CALCULATE", $token, $not_count_price_package, $this->unq_m_bkl_group_id, "0", true, $this->m_ve_id,false,0,$args), true);
			$tmp_calc_data = array("activity"=>"GOV PRICE CALCULATE","token"=>$token,"data"=>$not_count_price_package,"m_bkl_id"=>$this->unq_m_bkl_group_id,"m_ve_id"=>$this->m_ve_id,"args"=>$args);
			array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0),"CalculateData"=>$tmp_calc_data));
			if (isset($ttans["result"]) && $ttans["result"] == 1) {
				//重新計算金額後要記得改總金額
				$this->m_bkl_estimate_point = $ttans["data"][0]["point"];
				$this->m_bkl_estimate_free_point = $ttans["data"][0]["free_point"];
				$this->m_bkl_estimate_point_int = (int)$ttans["data"][0]["point"];
				$this->m_bkl_estimate_free_point_int = (int)$ttans["data"][0]["free_point"];
				$this->m_bkl_estimate_total_point = (int)$ttans["data"][0]["total"];
				$this->m_bkl_whitelist_point = (int)$ttans["data"][0]["whitelist_point"];
				$this->m_bkl_discount_point = (int)$ttans["data"][0]["discount_point"];
				$this->m_bkl_due_point = (int)$ttans["data"][0]["due_point"];
				$this->dcp_id = $ttans["data"][0]["dcp_id"];
				$this->m_bkl_end_date = $DT_pl_end_time->format("Y-m-d");
				$this->m_bkl_end_time = $DT_pl_end_time->format("H:i:s");
				//	$booking_ans_array["deposit_data"]["m_dpt_point"]=$ttans["data"][0]["point"];
				//	$booking_ans_array["deposit_data"]["m_dpt_amount"]=$ttans["data"][0]["point"];//free_point
				$this->m_bkl_total_time = DisplayBookingLogTime($this->m_bkl_book_type, $this->m_bkl_start_date, $this->m_bkl_start_time, $DT_pl_end_time->format("Y-m-d"), $DT_pl_end_time->format("H:i:s"));
				$this->m_bkl_is_pd = $ttans["m_bkl_is_pd"];
				if ($ttans["data"][0]["point"] == 0 && $ttans["data"][0]["free_point"] == 0) {
					return 0;
					//continue;
				}
				return $this->m_bkl_estimate_total_point;
			}
			else {
				$pure_data = file_get_contents('php://input');
				array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
				include_once("/../template/email_template.php");
				global $conn, $dbName;
				check_conn($conn, $dbName);
				EmailTemplate($conn, "Yang Tiger", "upk.rd00001@gmail.com", "易停網 reCalc計算301/302金額錯誤", json_encode($ttans) . "<br>" . $pure_data. "<br>" . json_encode($not_count_price_package));
				EmailTemplate($conn, "Luke Lu", "luu0930@gmail.com", "易停網 reCalc計算301/302金額錯誤", json_encode($ttans) . "<br>" . $pure_data. "<br>" . json_encode($not_count_price_package));
			}
		}
		else if ($this->m_bkl_plots_type == "303") {

		}
		else {
			//未知的plots type
		}

		array_push($this->tiger_check,array("line"=>__LINE__,"time"=>getMicroTime(0)));
	}

	//用總金額計算該會員的勝於停車點數寫入應付現金及應付點數
	//這個function不能知道他白名單折多少或優惠多少所以不要覆寫該兩個值
	public function CalcMemberParkingPoints($total_points) {
		$member_paid_class = new UPK_MemberPaid($this->m_ve_m_id);
		$total_pd_points = $member_paid_class->get_total_pd_points();
		$tmp_point = $total_points;//直接當作沒有折抵  //路邊停車都沒有免費點數抵用
		$tmp_free_point = 0;
		if ($tmp_point > ($total_points - $total_pd_points)) {
			if ($total_points > $total_pd_points) {
				#如果停車點數不足
				if($total_pd_points <= 0) {
					$tmp_free_point = 0;
					$tmp_point = $total_points;
				}
				else {
					$tmp_free_point = $total_pd_points;
					$tmp_point = $total_points - $total_pd_points;
				}
				$discountt = $tmp_point / $total_points;
			}
			else {
				#停車點數足夠
				$tmp_free_point = $total_points;
				$tmp_point = 0;
				$discountt = 0;
			}
			$this->m_bkl_estimate_point = $tmp_point;
			$this->m_bkl_estimate_free_point = $tmp_free_point;
			$this->m_bkl_estimate_point_int = (int)$tmp_point;
			$this->m_bkl_estimate_free_point_int = (int)$tmp_free_point;
			$this->m_bkl_estimate_total_point = (int)$total_points;
			$this->m_bkl_due_point = (int)$total_points;
		}
		$this_parking_space = new UPK_ParkingSpace($this->m_pk_id);
		if ((1 - $this_parking_space->get_m_pk_max_discount()) * $total_points > $tmp_point) { //算出來的付款金額小於最小應付金額
			//$tmp_point = floor((1-$this_parking_space->get_m_pk_max_discount())*$total_points);
			//$tmp_free_point = ceil($this_parking_space->get_m_pk_max_discount()*$total_points);
			//2018/08/31 現金無條件進位
			$tmp_point = ceil((1 - $this_parking_space->get_m_pk_max_discount()) * $total_points);
			$tmp_free_point = floor($this_parking_space->get_m_pk_max_discount() * $total_points);
			$discountt = $this_parking_space->get_m_pk_max_discount();
		}
		$this->m_bkl_estimate_point = $tmp_point;
		$this->m_bkl_estimate_free_point = $tmp_free_point;
		$this->m_bkl_estimate_point_int = (int)$tmp_point;
		$this->m_bkl_estimate_free_point_int = (int)$tmp_free_point;
		$this->m_bkl_estimate_total_point = (int)$total_points;
		$this->m_bkl_due_point = (int)$total_points;
		return $total_points;
	}
	public function GetDevice($conn)
	{
		$this->device_data = array();
		$now_date_time_str = strtotime(date_create('now')->format("Y-m-d H:i:s"));
		$bkl_start_date_time = strtotime(date_create($this->m_bkl_start_date . " " . $this->m_bkl_start_time)->format("Y-m-d H:i:s"));
		$bkl_end_date_time = strtotime(date_create($this->m_bkl_end_date . " " . $this->m_bkl_end_time)->format("Y-m-d H:i:s"));
		if ($bkl_start_date_time < $now_date_time_str && $now_date_time_str < $bkl_end_date_time + 900) {//還沒超過出場時間 // tiger 900 = 15分鐘
			$sql_deive = "select * from tb_Devices where dv_type_id = '" . $this->m_pk_id . "' AND is_delete='0' AND dv_type!='1' ";
			$result_deive = mysql_query($sql_deive, $conn);
			if (!$result_deive) {

			}
			else if (mysql_num_rows($result_deive) >= 1) {
				while ($ans_deive = mysql_fetch_assoc($result_deive)) {
					if ($ans_deive["dv_parameter"] == "" || $ans_deive["dv_type"] == "1")
						continue;
					$ans_deive["dv_parameter"] = json_decode($ans_deive["dv_parameter"], true);
					array_push($this->device_data, $ans_deive);
				}
			}
		}
	}

	public function GetPaidPoints($conn = "")
	{
		//取得已付款總金額
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT sum(m_bkl_estimate_point) as sum_point FROM tb_Member_ParkingSpace_Booking_Log WHERE m_bkl_is_paid='1' AND (m_bkl_id='" . $this->m_bkl_id . "' OR m_bkl_group_id='" . $this->m_bkl_id . "') AND m_bkl_cancel='0' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return 0;
		}
		elseif (mysql_num_rows($result) == 0) {
			return 0;
		}
		$ans = mysql_fetch_assoc($result);
		$point_sum = $ans["sum_point"];
		return $point_sum;
	}

	public function GetPaidParkingPoints($conn = "")
	{
		//取得已使用的停車點數
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT sum(m_bkl_estimate_free_point) as sum_point FROM tb_Member_ParkingSpace_Booking_Log WHERE m_bkl_is_paid='1' AND (m_bkl_id='" . $this->m_bkl_id . "' OR m_bkl_group_id='" . $this->m_bkl_id . "') AND m_bkl_cancel='0' AND m_bkl_is_pd='1' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return 0;
		}
		elseif (mysql_num_rows($result) == 0) {
			return 0;
		}
		$ans = mysql_fetch_assoc($result);
		$parking_point_sum = $ans["sum_point"];
		return $parking_point_sum;
	}

	public function GetPaidParkingPointsType($type = 0)
	{
		//取得購買或贈送的停車點數 (要撈m_pd_method判別)
		//type : 0 購買的停車點數 default 為了開發票
		//type : 1 贈送的停車點數
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT * FROM tb_Member_Pay_Log as tb_pyl 
			LEFT JOIN tb_Member_Paid_Log as tb_pdl ON tb_pdl.m_pyl_id=tb_pyl.m_pyl_id 
			LEFT JOIN tb_Member_Paid as tb_pd ON tb_pd.m_pd_id=tb_pdl.m_pd_id 
			WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
		//查tb_Member_Paid是要知道他這個市贈送的停車點數還是購買的停車點數
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return 0;
		}
		elseif (mysql_num_rows($result) == 0) {
			return 0;
		}
		$total_free_parking_point = 0;
		$total_paid_parking_point = 0;
		while ($ans = mysql_fetch_assoc($result)) {
			if (is_m_pd_method_paid($ans["m_pd_method"])) {
				//1=信用卡 2=第三方
				//如果是用付費的停車點數就
				$total_paid_parking_point += $ans["m_pyl_point_free"];  //可用tb_Member_Pay_Log.m_pyl_point_free 或 tb_Member_Paid.m_point
			}
			else {
				//3=後台贈送,4=每日送60,5=特約店贈送停車點數 8=會員註冊贈送
				$total_free_parking_point += $ans["m_pyl_point_free"];  //可用tb_Member_Pay_Log.m_pyl_point_free 或 tb_Member_Paid.m_point
			}
		}
		if ($type == 0)
			return $total_paid_parking_point;
		else
			return $total_free_parking_point;
	}

	public function GetPaidFreePoints($conn = "")
	{
		//取得已使用的免費點數
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT sum(m_bkl_estimate_free_point) as sum_point FROM tb_Member_ParkingSpace_Booking_Log WHERE m_bkl_is_paid='1' AND (m_bkl_id='" . $this->m_bkl_id . "' OR m_bkl_group_id='" . $this->m_bkl_id . "') AND m_bkl_cancel='0' AND m_bkl_is_pd='0' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return 0;
		}
		elseif (mysql_num_rows($result) == 0) {
			return 0;
		}
		$ans = mysql_fetch_assoc($result);
		$free_point_sum = $ans["sum_point"];
		return $free_point_sum;
	}

	public function GetUnpaidDeposit()
	{
		$this->member_unpaid_deposits = array();
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT  m_dpt_id FROM tb_Member_Deposit where m_bkl_id = '" . $this->m_bkl_id . "' AND m_dpt_pay_datetime IS NULL ORDER BY m_dpt_id DESC";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return (array("result" => 0, "失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) >= 1) {//如果多筆則取最新的一筆
			while ($ans = mysql_fetch_assoc($result)) {
				$this_deposit_data = new UPK_MemberDeposit($ans["m_dpt_id"]);//覺得這兩個用不同的記憶體比較好
				array_push($this->member_unpaid_deposits, $this_deposit_data);
			}
		}
		return (array("result" => 1));
	}

	public function GetDeposit($sql_logic = "")
	{
		$this->deposit_array = null;
		$this->member_deposits = array();
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "select  tb_Member_Deposit.m_dpt_sn,
				   tb_Member_Deposit.m_dpt_id,
				   tb_Member_Deposit.m_dpt_create_datetime,
				   tb_Member_Deposit.m_dpt_pay_datetime,
				   tb_Member_Deposit.m_dpt_amount,
				   tb_Member_Deposit.m_dpt_point,
				   tb_Member_Deposit.m_dpt_method,
				   tb_Member_Deposit.m_dpt_handling_charge,
				   tb_Member_Deposit.m_dpt_error_reason,
				   tb_Member_Deposit.m_transaction_id,
				   tb_Member_Deposit.m_dpt_trade_platform,
				   tb_Member_Deposit.m_dpt_origin_point,
				   tb_Member_Deposit.m_dpt_update_point from tb_Member_Deposit where m_bkl_id = '" . $this->m_bkl_id . "' AND m_dpt_pay_datetime IS NOT NULL ORDER BY m_dpt_id DESC";

		$result = mysql_query($sql, $conn);
		if (!$result) {
			return (array("result" => 0, "失敗" => mysql_error($conn) . $sql));
		}
		else if (mysql_num_rows($result) >= 1) {//如果多筆則取最新的一筆
			while ($ans = mysql_fetch_assoc($result)) {
				if ($this->deposit_array == null) {
					$this->deposit_array = $ans;//以後要做成CLASS
					$this_deposit_data = new UPK_MemberDeposit($ans["m_dpt_id"]);//覺得這兩個用不同的記憶體比較好
					$this->last_deposit_data = $this_deposit_data;
				}
				$this_deposit_data = new UPK_MemberDeposit($ans["m_dpt_id"]);//覺得這兩個用不同的記憶體比較好
				array_push($this->member_deposits, $this_deposit_data);
			}
		}
		else if (mysql_num_rows($result) == 0) {
			$this_deposit_data = new UPK_MemberDeposit("");//給個讓last_deposit_data有資料可以去給V5判斷
			$this->last_deposit_data = $this_deposit_data;
			$this->deposit_array = null;
			$this->deposit_array["m_dpt_sn"] = null;
			$this->deposit_array["m_dpt_id"] = null;
			$this->deposit_array["m_dpt_create_datetime"] = null;
			$this->deposit_array["m_dpt_pay_datetime"] = null;
			//$this->deposit_array["m_dpt_amount"]=$this->m_bkl_estimate_point;
			//$this->deposit_array["m_dpt_point"]=$this->m_bkl_estimate_point;
			$this->deposit_array["m_dpt_amount"] = 0;
			$this->deposit_array["m_dpt_point"] = 0;
			$this->deposit_array["m_dpt_method"] = 0;
			$this->deposit_array["m_dpt_handling_charge"] = 0;
			$this->deposit_array["m_dpt_error_reason"] = "尚未產生訂單";
			$this->deposit_array["m_transaction_id"] = "";
			$this->deposit_array["m_dpt_trade_platform"] = 0;
			$this->deposit_array["m_dpt_origin_point"] = 0;
			$this->deposit_array["m_dpt_update_point"] = 0;

			return (array("result" => 0, "無付費紀錄" => mysql_error($conn) . $sql));
		}
		return (array("result" => 1));
	}

	public function GetUnpaidBarcodeArray()
	{
		//如果這筆訂單已付款則不要取得這一筆的超商條碼
		//覺得要再多變數來接了，用return array的方式
		$barcode_deposit_array = array();
		global $conn, $dbName;
		check_conn($conn, $dbName);
		//先取得最後一筆付款的時間，如果時間比超商繳費還晚代表付款成功 比他之前的超商繳費要都當作不存在不然Calculate會先拉有沒有超商繳費，如果有會被誤判已付款
		$last_pay_datetime_sql=" ";
		$sql = "SELECT  m_dpt_pay_datetime FROM tb_Member_Deposit where m_bkl_id = '" . $this->m_bkl_id . "' AND m_dpt_pay_datetime IS NOT NULL ORDER BY m_dpt_pay_datetime DESC LIMIT 1";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return array();
		}
		else if (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$last_pay_datetime = $ans["m_dpt_pay_datetime"];
			$last_pay_datetime_sql = " AND '".$last_pay_datetime."' < m_dpt_create_datetime ";
		}
		$sql = "SELECT  m_dpt_id FROM tb_Member_Deposit where m_bkl_id = '" . $this->m_bkl_id . "' AND m_dpt_pay_datetime IS NULL AND m_dpt_trade_platform=15 ".$last_pay_datetime_sql." ORDER BY m_dpt_id DESC";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return array();
		}
		else if (mysql_num_rows($result) >= 1) {
			//如果多筆則取最新的一筆
			while ($ans = mysql_fetch_assoc($result)) {
				$this_deposit_data = new UPK_MemberDeposit($ans["m_dpt_id"]);//覺得這兩個用不同的記憶體比較好
				array_push($barcode_deposit_array, $this_deposit_data->get_array());
			}
		}
		return $barcode_deposit_array;
	}

	public function isSignUp()
	{
		$waiting_status = 1;
		$m_pl_end_time = "";
		if ($waiting_status == 1)
			return 1;//可加簽
		else return 0;//結束
		if ($m_pl_end_time != "") {
			return 0;//結束
		}
	}

	public function insertParkingLog($m_pl_end_time = "",$m_id_appear = "")
	{
		$DT_start_datetime = new Datetime($this->m_bkl_start_date . " " . $this->m_bkl_start_time);
		$DT_end_datetime = new Datetime($m_pl_end_time);

		$c_booking_manager = new UPK_BookingLogManager();
		$c_booking_manager->set_m_bkl_id($this->m_bkl_id);
		$c_booking_manager->set_vehicle($this->m_ve_id);
		$c_booking_manager->set_parking_space($this->m_pk_id);
		$c_booking_manager->set_datetime($DT_start_datetime, $DT_end_datetime);
		$c_booking_manager->set_m_id_appear($m_id_appear);
		return $c_booking_manager->insertParkingLog();
	}
	public function updateOutTime($DT_end_datetime) {
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$sql = "SELECT * FROM tb_Member_Parking_Log WHERE m_bkl_id='" . $this->m_bkl_id . "' AND m_pl_end_time IS NOT NULL ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "修改訂單出場時間失敗", "description" => mysql_error()));
		}
		elseif(mysql_num_rows($result)==0) {
			include_once("/../main_api/AppearReturnFunc.php");
			$score = "";
			$evalution = "";
			$suggestion = "";
			$is_appeal = "";
			$is_contactus = "";
			$appeal_content = "";
			$type = "";
			$is_pk_favorite = "0";
			$is_plot_favorite = "0";
			$pure_data = file_get_contents('php://input');
			$ttans=AppearReturnFunc("APPEAR RETURN",$this->get_m_ve_m_token(),$this->get_m_bkl_id(),$score,$evalution,$suggestion,$is_appeal,$is_contactus,$appeal_content,$type,$is_pk_favorite,$is_plot_favorite,0,$pure_data);
			$tans=json_decode($ttans,true);
			if($tans["result"]==1) {
				//確認出場後就直接走下去改寫他的結束時間以及出場時間
			}
			else {
				return $ttans;
			}
		}

		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_end_date='" . $DT_end_datetime->format("Y-m-d") . "', m_bkl_end_time='" . $DT_end_datetime->format("H:i:s") . "' WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "修改訂單出場時間失敗", "description" => mysql_error()));
		}
		$sql = "UPDATE tb_Member_Parking_Log SET m_pl_end_time='" . $DT_end_datetime->format("Y-m-d H:i:s") . "' WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "修改訂單出場時間失敗", "description" => mysql_error()));
		}
		return json_encode(array("result" => 1));
	}
	public function canAppear($is_admin = 0, $id = "")
	{

		if ($this->parking_log == null) {
			$this->GetParkingLog();
		}
		$now = new DateTime();
		$DT_bkl_end_datetime = new DateTime($this->m_bkl_end_date . " " . $this->m_bkl_end_time);
		if ($this->parking_log->get_m_pl_end_time() == null
			&& $this->parking_log->get_m_pl_start_time() != null
			&& ($id == $this->vehicle_data->get_m_id() || $id == $this->m_bkl_transfer_m_id || $now > $DT_bkl_end_datetime || $is_admin == 1)
			&& $this->m_bkl_cancel == "0") {
			#判斷是否可以出場 而且如果非預約者查看的話必須預約結束後才能按
			#(邏輯寫成 如果是停車的人查看或是預約過期 就可以點出場)
			#可出場
			if ($this->m_bkl_book_type == "1002" && $now < $DT_bkl_end_datetime) {
				#如果月租超過現在時間則顯示退租
				return 2;
			}
			else {
				return 1;
			}
		}
		else {
			return 0;
		}
	}

	public function canApproach()
	{
		if ($this->pricing_logs == null) {
			$this->GetPricingLog();
		}
		if ($this->m_bkl_book_type == "1") {
			$DT_m_ppl_start_datetime = new DateTime($this->pricing_logs->get_m_ppl_start_date() . " " . $this->pricing_logs->get_m_ppl_start_time());
			$DT_m_bkl_start_datetime = new DateTime($this->m_bkl_start_date . " " . $this->m_bkl_start_time);
			$now = new DateTime();
			//一般預約判斷能不能提早進場
			if ($now > $DT_m_ppl_start_datetime && $DT_m_bkl_start_datetime > $now) {//如果空位時段在現在之前則可以續費延時
				return 1;
				//$booking_ans_array["can_appear"]=0;//強制設0不顯示按鈕
			}
		}
		return 0;
	}

	public function canDelete($is_admin = 0)
	{
		if ($this->pricing_logs == null) {
			$this->GetPricingLog();
		}
		if ($this->parking_log == null) {
			$this->GetParkingLog();
		}
		$DT_m_bkl_start_datetime = new DateTime($this->m_bkl_start_date . " " . $this->m_bkl_start_time);
		$now = new DateTime();
		if ($this->m_bkl_plots_type == "100" || $this->m_bkl_plots_type == "300" || $this->m_bkl_plots_type == "301" || $this->m_bkl_plots_type == "302" || $this->m_bkl_plots_type == "303") {
			#3xx都不能刪除
			return 0;
		}
		else if ($this->m_bkl_cancel == "1") {
			#都刪除了還刪屁喔
			return 0;
		}
		else if ($this->m_bkl_is_paid == 0) {
			#若未付款，可直接刪除
			#若未付款且為接收轉移之會員重新計算金額
			return 1;
		}
		else if ($this->parking_log->get_m_pl_start_time() != "" || $this->parking_log->get_m_pl_start_time() != null) {
			return 0;
		}
		else if ($DT_m_bkl_start_datetime >= $now) {
			#需補上時間判斷  //Tiger: 付款後30分內可退款 //已改30分退
			//2017/08/04 Tiger: 改進場前可退款但須補手續費
			return 1;
		}
		else if ($DT_m_bkl_start_datetime < $now) {
			//進場30分內不可退
			//2017/08/04 Tiger: 改進場前可退款但須補手續費
			return 0;
		}
	}

	/**
	 * @return int|string
	 */
	public function getDepositCnt()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT * FROM tb_Member_Deposit WHERE m_bkl_id='" . $this->m_bkl_id . "' 
			AND m_dpt_pay_datetime IS NOT NULL
			AND (m_dpt_amount != 0 OR (m_dpt_amount = 0 AND m_dpt_refund_point=0))";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "搜尋付款失敗", "description" => mysql_error($conn)));
		}
		$dpt_cnt = mysql_num_rows($result);

		//因為預約跟300的estimate_free_point 是建立訂單時就決定好 301 302則是付款後才有值
		if($this->m_bkl_plots_type=="0" || $this->m_bkl_plots_type=="300") {
			//弱曾經有付過款 就判斷停車點數是否為0，因為停車點數不會退款
			$sql_paid_already = "SELECT * FROM tb_Member_Deposit WHERE m_bkl_id='".$this->m_bkl_id."' 
			AND m_dpt_pay_datetime IS NOT NULL";
			$result_paid_already = mysql_query($sql_paid_already, $conn);
			if (! $result_paid_already) {
				return json_encode(["result" => 0, "title" => "搜尋付款失敗", "description" => mysql_error($conn)]);
			}
			$dpt_cnt_paid_already = mysql_num_rows($result_paid_already);
			if ($dpt_cnt_paid_already >= 1) {
				if ($this->m_bkl_estimate_free_point != "0") {
					$dpt_cnt++;
				}
			}
		}
		elseif($this->m_bkl_plots_type=="100" || $this->m_bkl_plots_type=="301" || $this->m_bkl_plots_type=="302") {
			//100 301 302有付款退款後不會退回停車點數
			if($this->m_bkl_estimate_free_point != "0")
				$dpt_cnt++;
		}
		rg_activity_log($conn, "", "LOG_BookingLog->getDepositCnt", $this->m_bkl_id, "dpt_cnt=".$dpt_cnt.",freepoint=".$this->m_bkl_estimate_free_point, "");
		return $dpt_cnt;
	}

	public function text()//__toString()
	{
		return array(
			"m_bkl_id" => $this->m_bkl_id,
			"m_bkl_start_date" => $this->m_bkl_start_date,
			"m_bkl_end_date" => $this->m_bkl_end_date,
			"m_bkl_start_time" => substr($this->m_bkl_start_time, 0, -3),
			"m_bkl_end_time" => substr($this->m_bkl_end_time, 0, -3));
	}

	public function __toString()
	{
		return json_encode(array(
			"m_bkl_id" => $this->m_bkl_id,
			"m_ve_id" => $this->m_ve_id,
			"m_bkl_book_type" => $this->m_bkl_book_type,
			"m_bkl_start_date" => $this->m_bkl_start_date,
			"m_bkl_end_date" => $this->m_bkl_end_date,
			"m_bkl_start_time" => substr($this->m_bkl_start_time, 0, -3),
			"m_bkl_end_time" => substr($this->m_bkl_end_time, 0, -3),
			//"pay_log"=>$this->pay_log->get_array(),
			//"parking_log"=>$this->parking_log->get_array(),
			//"evaluation_log"=>$this->evaluation_log->get_array(),
			"vehicle_data" => $this->vehicle_data->get_array(),
			"pricing_logs" => $this->pricing_logs
		));
	}

	public function DisplayPricingLog()
	{
		if (isset($this->pricing_logs))
			return $this->pricing_logs->get_array($this->pricing_logs->B_ALL);
		return null;
	}

	public function DisplayDiscountPlan()
	{
		if (isset($this->discount_plan))
			return $this->discount_plan->get_array($this->discount_plan->B_ALL);
		return null;
	}

	public function DisplayPayLog()
	{
		if (isset($this->pay_log))
			return $this->pay_log->get_array($this->pay_log->B_ALL);
		return null;
	}

	public function DisplayParkingLog()
	{
		if (isset($this->parking_log))
			return $this->parking_log->get_array($this->parking_log->B_ALL);
		return null;
	}

	public function DisplayEvaluationLog()
	{
		if (isset($this->evaluation_log) && $this->evaluation_log != null)
			return $this->evaluation_log->get_array($this->evaluation_log->B_ALL);
		return null;
	}

	public function DisplayVehicleData()
	{
		if (isset($this->vehicle_data) && $this->vehicle_data != null)
			return $this->vehicle_data->get_array($this->vehicle_data->B_ALL);
		return null;
	}

	public function DisplayDeposit()
	{
		if (isset($this->deposit_array) && $this->deposit_array != null)
			return $this->deposit_array;//未來要改CLASS
		return null;
	}

	public function isPaid()
	{

	}

	public function get_m_bkl_sn()
	{
		return $this->m_bkl_sn;
	}

	public function get_m_bkl_id()
	{
		return $this->m_bkl_id;
	}

	public function get_m_bkl_group_id()
	{
		return $this->m_bkl_group_id;
	}

	public function get_m_bkl_create_datetime($sec = false)
	{
		if ($sec)
			return $this->m_bkl_create_datetime;
		else
			return substr($this->m_bkl_create_datetime, 0, -3);
	}

	public function get_m_pk_id()
	{
		return $this->m_pk_id;
	}

	public function get_m_ppl_id()
	{
		return $this->m_ppl_id;
	}

	public function get_m_ve_id()
	{
		return $this->m_ve_id;
	}
	public function get_m_ve_m_id()
	{
		return $this->m_ve_m_id;
	}
	public function get_m_bkl_creator_m_id()
	{
		return $this->m_bkl_creator_m_id;
	}
	
	public function get_m_plots_id()
	{
		return $this->m_plots_id;
	}

	public function get_m_bkl_book_type()
	{
		return $this->m_bkl_book_type;
	}

	public function get_m_bkl_start_date()
	{
		return $this->m_bkl_start_date;
	}

	public function get_m_bkl_end_date()
	{
		return $this->m_bkl_end_date;
	}

	// 預設不顯示秒數
	public function get_m_bkl_start_time($sec = false)
	{
		if ($sec)
			return $this->m_bkl_start_time;
		else
			return substr($this->m_bkl_start_time, 0, -3);
	}

	// 預設不顯示秒數
	public function get_m_bkl_end_time($sec = false)
	{
		if ($sec)
			return $this->m_bkl_end_time;
		else
			return substr($this->m_bkl_end_time, 0, -3);
	}

	public function get_DT_m_bkl_start_datetime()
	{
		$DT_tmp = new DateTime($this->m_bkl_start_date . " " . $this->m_bkl_start_time);
		return $DT_tmp;
	}

	public function get_DT_m_bkl_end_datetime()
	{
		$DT_tmp = new DateTime($this->m_bkl_end_date . " " . $this->m_bkl_end_time);
		return $DT_tmp;
	}

	public function undoOutReport($args=array())
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";
		if (isset($args["m_id"]))
			$id = $args["m_id"];
		else $id = "";
		if (isset($args["pure_data"]))
			$pure_data = $args["pure_data"];
		else $pure_data = "";
		//改成未出場 需要按造m_plots_type去判斷
		if ($this->get_m_bkl_plots_type() == "1") {
			//只改m_pl_end_time
			$sql = "UPDATE tb_Member_Parking_Log SET m_pl_end_time = NULL WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "後台修改訂單失敗", "description" => mysql_error()));
			}
		}
		elseif ($this->get_m_bkl_plots_type() == "100") {
			//不能改這個
			rg_activity_log($conn, $id, "後台修改訂單失敗", "路邊開單不能修改出場狀態", $pure_data, "");
			$ans = GetSystemCode("3050008", $language, $conn);
			return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
		}
		elseif ($this->get_m_bkl_plots_type() == "300") {
			//進場時間是由設備判斷，若未出場則應是刪除預約單
			rg_activity_log($conn, $id, "後台修改訂單失敗", "路邊開單不能修改出場狀態", $pure_data, "");
			$ans = GetSystemCode("3050008", $language, $conn);
			return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
		}
		elseif ($this->get_m_bkl_plots_type() == "301") {
			//要改booking log and parking log

			$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_end_date='2199-12-31' , m_bkl_end_time='23:59:59' WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "後台修改訂單失敗", "description" => mysql_error()));
			}
			$sql = "UPDATE tb_Member_Parking_Log SET m_pl_end_time = NULL WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "後台修改訂單失敗", "description" => mysql_error()));
			}
		}
		elseif ($this->get_m_bkl_plots_type() == "302") {
			//需要多判斷dv_type
			$sql = "SELECT dv_id, dv_status, dv_type FROM tb_Devices WHERE dv_type_id='" . $this->m_bkl_id . "' ORDER BY dv_sn DESC";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "後台修改訂單失敗", "description" => mysql_error()));
			}
			elseif(mysql_num_rows($result)==0){
				//車牌辨識會沒有設備紀錄媽?
			}
			$ans = mysql_fetch_assoc($result);
			$dv_id = $ans["dv_id"];
			$dv_type = $ans["dv_type"];
			$dv_status = $ans["dv_status"];
			if ($dv_type == CONST_DV_TYPE::LPR_IN_OR_OUT) {
				//要付款才能出場，若已經出場則設備會給出場，
				//還原未出場要令 dv_status = 1
			}
			elseif ($dv_type == CONST_DV_TYPE::LPR_TOWER) {
				//先出場再付款
				//已付款者不用調整，
			}
			$sql = "UPDATE tb_Devices SET dv_status='1' WHERE dv_id='" . $dv_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "後台修改訂單失敗", "description" => mysql_error()));
			}
			$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_end_date='2199-12-31' , m_bkl_end_time='23:59:59' WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "後台修改訂單失敗", "description" => mysql_error()));
			}
			$sql = "UPDATE tb_Member_Parking_Log SET m_pl_end_time = NULL WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "後台修改訂單失敗", "description" => mysql_error()));
			}
		}
		MemcacheSetBookingLog('_UPK_BookingLog:', $this->m_bkl_id);
		return json_encode(array("result" => 1));
	}
	public function getPastPrice()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT sum(m_pyl_point) as sum FROM tb_Member_Pay_Log WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		//基本不會失敗但打保險
		if (!$result) {
			return $this->m_bkl_estimate_point;
		}
		elseif (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$past_point = $ans["sum"];
			return $past_point;
		}
		//打保險
		else {
			return $this->m_bkl_estimate_point;
		}
	}

	public function getPastParkingPoint()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT sum(m_pyl_point_free) as sum FROM tb_Member_Pay_Log WHERE m_bkl_id='" . $this->m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		//基本不會失敗但打保險
		if (!$result) {
			return $this->m_bkl_estimate_free_point;
		}
		elseif (mysql_num_rows($result) == 1) {
			$ans = mysql_fetch_assoc($result);
			$past_free_point = $ans["sum"];
			return $past_free_point;
		}
		//打保險
		else {
			return $this->m_bkl_estimate_free_point;
		}
	}
	function getPaidOrFreeParkingPoint()	{

		//如果發現不一致，就更新之後再傳送出去
		if(($this->m_bkl_estimate_paid_parking_point + $this->m_bkl_estimate_free_parking_point) == $this->m_bkl_estimate_free_point) {
			return json_encode(array("result" => 1, "paid_parking_point" => $this->m_bkl_estimate_paid_parking_point, "free_parking_point" => $this->m_bkl_estimate_free_parking_point));
		}
		$m_bkl_id = $this->get_m_bkl_id();
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT m_pyl_id  FROM tb_Member_Pay_Log WHERE m_bkl_id='" . $m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "取得停車付費紀錄錯誤", "description" => mysql_error()));
		}
		elseif (mysql_num_rows($result) == 0) {

		}
		$paid_parking_point = 0;
		$free_parking_point = 0;
		while ($ans = mysql_fetch_assoc($result)) {
			$sql_pdl = "SELECT m_pd_id,m_point FROM tb_Member_Paid_Log WHERE m_pyl_id = '" . $ans["m_pyl_id"] . "' ";
			$result_pdl = mysql_query($sql_pdl, $conn);
			if (!$result_pdl) {
				return json_encode(array("result" => 0, "title" => "取得停車付費紀錄錯誤", "description" => mysql_error()));
			}
			elseif (mysql_num_rows($result_pdl) == 0) {
				//會來找基本上就會有紀錄，不過如果該預約沒有使用停車點數付款，進function前沒判斷的話還是會進來
			}
			while ($ans_pdl = mysql_fetch_assoc($result_pdl)) {
				$m_pd_id = $ans_pdl["m_pd_id"];
				$m_point = $ans_pdl["m_point"];
				$sql_pd = "SELECT m_pd_method FROM tb_Member_Paid WHERE m_pd_id='" . $m_pd_id . "' ";
				if (!$result_pdl) {
					return json_encode(array("result" => 0, "title" => "取得停車付費紀錄錯誤", "description" => mysql_error()));
				}
				$result_pd = mysql_query($sql_pd, $conn);
				if (!$result_pd) {
					return json_encode(array("result" => 0, "title" => "取得停車付費紀錄錯誤", "description" => mysql_error()));
				}
				elseif (mysql_num_rows($result_pd) != 1) {
					//不可能!!
				}
				else {
					$ans = mysql_fetch_assoc($result_pd);
					$m_pd_method = $ans["m_pd_method"];
					if (isParkingPointPaid($m_pd_method)) {
						$paid_parking_point += $m_point;
					}
					else {
						$free_parking_point += $m_point;
					}
				}
			}
		}

		//如果發現該兩個籃為沒有更新的話就呼叫此function更新m_bkl_estimate_free_parking_point跟m_bkl_estimate_paid_parking_point
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_estimate_free_parking_point='" . $free_parking_point . "', m_bkl_estimate_paid_parking_point='" . $paid_parking_point . "' WHERE m_bkl_id='" . $this->m_bkl_id . "'";
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "取得停車付費紀錄錯誤", "description" => mysql_error()));
		}
		$this->m_bkl_estimate_free_parking_point = $free_parking_point;
		$this->m_bkl_estimate_paid_parking_point = $paid_parking_point;
		MemcacheSetBookingLog('_UPK_BookingLog:',$this->m_bkl_id);
		return json_encode(array("result" => 1, "paid_parking_point" => $paid_parking_point, "free_parking_point" => $free_parking_point));
	}
	public function get_m_bkl_estimate_point()
	{
		return $this->m_bkl_estimate_point;
	}

	public function get_m_bkl_estimate_free_point()
	{
		return $this->m_bkl_estimate_free_point;
	}

	public function get_m_bkl_estimate_point_int()
	{
		return (int)$this->m_bkl_estimate_point;
	}

	public function get_m_bkl_estimate_free_point_int()
	{
		return (int)$this->m_bkl_estimate_free_point;
	}
	public function get_m_bkl_estimate_free_parking_point()
	{
		return $this->m_bkl_estimate_free_parking_point;
	}

	public function get_m_bkl_estimate_free_parking_point_int()
	{
		return (int)$this->m_bkl_estimate_free_parking_point;
	}
	public function get_m_bkl_estimate_paid_parking_point()
	{
		return $this->m_bkl_estimate_paid_parking_point;
	}

	public function get_m_bkl_estimate_paid_parking_point_int()
	{
		return (int)$this->m_bkl_estimate_paid_parking_point;
	}

	public function get_m_bkl_estimate_total_point()
	{
		return (string)((int)$this->m_bkl_estimate_point + (int)$this->m_bkl_estimate_free_point);
	}

	public function get_m_bkl_estimate_total_point_int()
	{
		return (int)$this->m_bkl_estimate_point + (int)$this->m_bkl_estimate_free_point;
	}

	public function get_m_bkl_earlybird_offer()
	{
		return $this->m_bkl_earlybird_offer;
	}

	public function get_m_ppl_promotion_code()
	{
		return $this->m_ppl_promotion_code;
	}

	public function get_m_bkl_visit_description()
	{
		return $this->m_bkl_visit_description;
	}

	public function get_m_bkl_total_time()
	{
		return $this->m_bkl_total_time;
	}

	public function get_m_bkl_cancel()
	{
		return $this->m_bkl_cancel;
	}

	public function get_m_id_cancel()
	{
		return $this->m_id_cancel;
	}

	public function get_m_bkl_sendpush()
	{
		return $this->m_bkl_sendpush;
	}

	public function get_m_bkl_gov_id()
	{
		return $this->m_bkl_gov_id;
	}

	public function get_m_bkl_is_paid()
	{
		return $this->m_bkl_is_paid;
	}

	public function get_m_bkl_is_pd()
	{
		return $this->m_bkl_is_pd;
	}

	public function get_m_pk_owner_m_id()
	{
		return $this->m_pk_owner_m_id;
	}

	public function get_m_pk_agent_m_id()
	{
		return $this->m_pk_agent_m_id;
	}

	public function get_m_bkl_transfer_m_id()
	{
		return $this->m_bkl_transfer_m_id;
	}

	public function get_m_bkl_transfer_cellphone()
	{
		return $this->m_bkl_transfer_cellphone;
	}

	public function get_m_bkl_is_over_night()
	{
		return $this->m_bkl_is_over_night;
	}

	public function get_unq_m_bkl_group_id()
	{
		return $this->unq_m_bkl_group_id;
	}

	public function get_m_plots_address_city()
	{
		return $this->m_plots_address_city;
	}

	public function get_m_bkl_scan_qrcode_paytime()
	{
		return $this->m_bkl_scan_qrcode_paytime;
	}

	public function get_m_bkl_scan_qrcode_payfee()
	{
		return $this->m_bkl_scan_qrcode_payfee;
	}

	public function get_m_bkl_in_or_out()
	{
		return $this->m_bkl_in_or_out;
	}

	public function get_m_bkl_plots_type()
	{
		return $this->m_bkl_plots_type;
	}

	public function get_m_bkl_is_end()
	{
		return $this->m_bkl_is_end;
	}

	public function get_m_bkl_due_point()
	{
		return $this->m_bkl_due_point;
	}

	public function get_dcp_id()
	{
		return $this->dcp_id;
	}

	public function get_m_bkl_whitelist_point()
	{
		return $this->m_bkl_whitelist_point;
	}

	public function get_m_bkl_discount_point()
	{
		if ($this->m_bkl_discount_point == null)
			return 0;
		return $this->m_bkl_discount_point;
	}

	public function get_m_bkl_gov_total_price_without_pay()
	{
		return $this->m_bkl_gov_total_price_without_pay;
	}

	public function get_m_bkl_is_customize()
	{
		return $this->m_bkl_is_customize;
	}

	public function get_m_bkl_customize_price()
	{
		return $this->m_bkl_customize_price;
	}

	public function get_m_bkl_is_fleeing_fee()
	{
		return $this->m_bkl_is_fleeing_fee;
	}

	public function get_m_bkl_fleeing_fee()
	{
		return $this->m_bkl_fleeing_fee;
	}

	public function get_m_bkl_handling_fee()
	{
		return $this->m_bkl_handling_fee;
	}

	public function get_m_bkl_market_handling_fee()
	{
		return $this->m_bkl_market_handling_fee;
	}

	public function get_m_bkl_view_point()
	{
		return $this->m_bkl_view_point;
	}

	public function get_m_bkl_view_parking_point()
	{
		return $this->m_bkl_view_parking_point;
	}

	public function get_m_bkl_view_whitelist_point()
	{
		return $this->m_bkl_view_whitelist_point;
	}

	public function get_m_bkl_disp_unpaid_page()
	{
		return $this->m_bkl_disp_unpaid_page;
	}

	public function get_m_bkl_view_discount_point()
	{
		return $this->m_bkl_view_discount_point;
	}

	public function get_is_paid_valid_datetime()
	{
		return $this->is_paid_valid_datetime;
	}
	public function get_tiger_check()
	{
		return $this->tiger_check;
	}

	public function get_PaidDesc($token = "")
	{
		$this->set_PaidDesc($token);
		return $this->m_bkl_paid_desc;
	}

	public function get_m_ve_m_token()
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT m_token FROM tb_Member WHERE m_id='" . $this->m_ve_m_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return "";
		}
		else {
			$ans = mysql_fetch_assoc($result);
			$token = $ans["m_token"];
		}
		return $token;
	}

	public function set_PaidDesc($token = "")
	{
		if ($token == "") {
			global $conn, $dbName;
			check_conn($conn, $dbName);
			$sql = "SELECT m_token FROM tb_Member WHERE m_id='" . $this->m_ve_m_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
			}
			else {
				$ans = mysql_fetch_assoc($result);
				$token = $ans["m_token"];
			}
		}
		$hasnt_paid = 0;
		/*
		 is_paid可用與不可用

		可用
		預約 - 停車時間固定，無付款不會進入Group
		路邊 - 出場後才付款
		300 - 出場才付款，沒付款會砍單
		302車辨 - 付款代表出場

		不可用
		301 - 付款後可繼續計算金額補付款
		302車柱 - 付款後可繼續計算金額補付款

		其他
		301 - 就算未付款 但 0 元不顯示
		302 - 就算未付款 但 0 元不顯示
		*/
		$DT_now = new DateTime();
		$DT_is_paid_valid_datetime = new DateTime($this->is_paid_valid_datetime);

		if($this->m_bkl_is_paid==1 && $DT_now<$DT_is_paid_valid_datetime) {
			$this->m_bkl_paid_desc = "已付款";
			return $this->m_bkl_paid_desc;
		}
		if ($token != "") {
			$hasnt_paid = $this->reCalculatePrice($token);
		}
		$has_paid = $this->GetPaidPoints() + $this->GetPaidParkingPoints();
		if ($hasnt_paid == 0 && $has_paid == 0)//免付費
			$this->m_bkl_paid_desc = "已付款";
		else if ($hasnt_paid != 0 && $has_paid == 0)//未付款
			$this->m_bkl_paid_desc = "未付款 " . $hasnt_paid . " 元";
		else if ($hasnt_paid == 0 && $has_paid != 0)//已付款
			$this->m_bkl_paid_desc = "已付款";
		//($hasnt_paid != 0 && $has_paid != 0)
		else
			$this->m_bkl_paid_desc = "已付款(尚欠 " . $hasnt_paid . " 元)";
		return $this->m_bkl_paid_desc;
	}

	public function get_array($select = "", $sel_class = "",$sel_paid_desc=0,$args=[])
	{
		$display_sec = false;
		if (isset($args["display_sec"])) {
			$display_sec = $args["display_sec"];
		}
		if ($select == "")
			$select = $this->B_ALL;
		$return_array = array();
		if (($select & $this->B_m_bkl_sn) != 0) {
			$return_array["m_bkl_sn"] = $this->get_m_bkl_sn();
		}
		if (($select & $this->B_m_bkl_id) != 0) {
			$return_array["m_bkl_id"] = $this->get_m_bkl_id();
		}
		if (($select & $this->B_m_bkl_group_id) != 0) {
			$return_array["m_bkl_group_id"] = $this->get_m_bkl_group_id();
		}
		if (($select & $this->B_m_bkl_create_datetime) != 0) {
			$return_array["m_bkl_create_datetime"] = $this->get_m_bkl_create_datetime();
		}
		if (($select & $this->B_m_pk_id) != 0) {
			$return_array["m_pk_id"] = $this->get_m_pk_id();
		}
		if (($select & $this->B_m_ppl_id) != 0) {
			$return_array["m_ppl_id"] = $this->get_m_ppl_id();
		}
		if (($select & $this->B_m_ve_id) != 0) {
			$return_array["m_ve_id"] = $this->get_m_ve_id();
			$return_array["m_ve_m_id"] = $this->get_m_ve_m_id();
		}
		if (($select & $this->B_m_bkl_creator_m_id) != 0) {
			$return_array["m_bkl_creator_m_id"] = $this->get_m_bkl_creator_m_id();
		}
		if (($select & $this->B_m_bkl_book_type) != 0) {
			$return_array["m_bkl_book_type"] = $this->get_m_bkl_book_type();
			if($this->get_m_bkl_book_type()=="1002")
				$return_array["is_monthly_rental"] = 1;
			else
				$return_array["is_monthly_rental"] = 0;
		}
		if (($select & $this->B_m_bkl_datetime) != 0) {
			$return_array["m_bkl_start_date"] = $this->get_m_bkl_start_date();
			$return_array["m_bkl_end_date"] = $this->get_m_bkl_end_date();
			$return_array["m_bkl_start_time"] = $this->get_m_bkl_start_time($display_sec);
			$return_array["m_bkl_end_time"] = $this->get_m_bkl_end_time($display_sec);
		}
		if (($select & $this->B_m_bkl_points) != 0) {
			$return_array["m_bkl_estimate_point"] = $this->get_m_bkl_estimate_point();
			$return_array["m_bkl_estimate_free_point"] = $this->get_m_bkl_estimate_free_point();
			$return_array["m_bkl_estimate_point_int"] = $this->get_m_bkl_estimate_point_int();
			$return_array["m_bkl_estimate_free_point_int"] = $this->get_m_bkl_estimate_free_point_int();
			$return_array["m_bkl_estimate_free_parking_point"] = $this->get_m_bkl_estimate_free_parking_point();
			$return_array["m_bkl_estimate_free_parking_point_int"] = $this->get_m_bkl_estimate_free_parking_point_int();
			$return_array["m_bkl_estimate_paid_parking_point"] = $this->get_m_bkl_estimate_paid_parking_point();
			$return_array["m_bkl_estimate_paid_parking_point_int"] = $this->get_m_bkl_estimate_paid_parking_point_int();
			$return_array["m_bkl_estimate_total_point"] = $this->get_m_bkl_estimate_point_int() + $this->get_m_bkl_estimate_free_point_int();
			$return_array["m_bkl_due_point"] = $this->get_m_bkl_due_point();
			$return_array["m_bkl_whitelist_point"] = $this->get_m_bkl_whitelist_point();
			$return_array["m_bkl_discount_point"] = (int)$this->get_m_bkl_discount_point();
			$return_array["m_bkl_is_customize"] = $this->get_m_bkl_is_customize();
			$return_array["m_bkl_customize_price"] = $this->get_m_bkl_customize_price();
			$return_array["m_bkl_is_fleeing_fee"] = $this->get_m_bkl_is_fleeing_fee();
			$return_array["m_bkl_fleeing_fee"] = $this->get_m_bkl_fleeing_fee();
			$return_array["m_bkl_handling_fee"] = $this->get_m_bkl_handling_fee();
			$return_array["m_bkl_market_handling_fee"] = $this->get_m_bkl_market_handling_fee();
		}
		/*if(($select & $this->B_m_bkl_estimate_free_point) != 0 ){
			$return_array["m_bkl_estimate_free_point"]=$this->get_m_bkl_estimate_free_point();
		}
		if(($select & $this->B_m_bkl_estimate_point) != 0 ){
			$return_array["m_bkl_estimate_point_int"]=$this->get_m_bkl_estimate_point_int();
		}
		if(($select & $this->B_m_bkl_estimate_free_point) != 0 ){
			$return_array["m_bkl_estimate_free_point_int"]=$this->get_m_bkl_estimate_free_point_int();
		}
		if(($select & $this->B_m_bkl_estimate_point) != 0 &&
			($select & $this->B_m_bkl_estimate_free_point) != 0){//覺得用&&才正確
			$return_array["m_bkl_estimate_total_point"]=$this->get_m_bkl_estimate_point_int()+$this->get_m_bkl_estimate_free_point_int();
		}*/
		if (($select & $this->B_m_bkl_earlybird_offer) != 0) {
			$return_array["m_bkl_earlybird_offer"] = $this->get_m_bkl_earlybird_offer();
		}
		if (($select & $this->B_m_ppl_promotion_code) != 0) {
			$return_array["m_ppl_promotion_code"] = $this->get_m_ppl_promotion_code();
		}
		if (($select & $this->B_m_bkl_visit_description) != 0) {
			$return_array["m_bkl_visit_description"] = $this->get_m_bkl_visit_description();
		}
		if (($select & $this->B_m_bkl_total_time) != 0) {
			$return_array["m_bkl_total_time"] = $this->get_m_bkl_total_time();
		}
		if (($select & $this->B_m_bkl_cancel) != 0) {
			$return_array["m_bkl_cancel"] = $this->get_m_bkl_cancel();
		}
		if (($select & $this->B_m_id_cancel) != 0) {
			$return_array["m_id_cancel"] = $this->get_m_id_cancel();
		}
		if (($select & $this->B_m_bkl_sendpush) != 0) {
			$return_array["m_bkl_sendpush"] = $this->get_m_bkl_sendpush();
		}
		if (($select & $this->B_m_bkl_gov_id) != 0) {
			$return_array["m_bkl_gov_id"] = $this->get_m_bkl_gov_id();
		}
		if (($select & $this->B_m_bkl_is_paid) != 0) {
			$return_array["m_bkl_is_paid"] = $this->get_m_bkl_is_paid();
			$return_array["is_paid_valid_datetime"] = $this->get_is_paid_valid_datetime();
		}
		if (($select & $this->B_m_bkl_is_pd) != 0) {
			$return_array["m_bkl_is_pd"] = $this->get_m_bkl_is_pd();
		}
		if (($select & $this->B_m_bkl_transfer_m_id) != 0) {
			$return_array["m_bkl_transfer_m_id"] = $this->get_m_bkl_transfer_m_id();
			$return_array["m_bkl_transfer_cellphone"] = $this->get_m_bkl_transfer_cellphone();
		}
		if (($select & $this->B_m_bkl_is_over_night) != 0) {
			$return_array["m_bkl_is_over_night"] = $this->get_m_bkl_is_over_night();
		}
		if (($select & $this->B_m_bkl_scan_qrcode_paytime) != 0) {
			$return_array["m_bkl_scan_qrcode_paytime"] = $this->get_m_bkl_scan_qrcode_paytime();
		}
		if (($select & $this->B_m_bkl_scan_qrcode_payfee) != 0) {
			$return_array["m_bkl_scan_qrcode_payfee"] = $this->get_m_bkl_scan_qrcode_payfee();
		}
		if (($select & $this->B_m_bkl_in_or_out) != 0) {
			$return_array["m_bkl_in_or_out"] = $this->get_m_bkl_in_or_out();
		}
		if (($select & $this->B_m_plots_data) != 0) {
			$return_array["m_plots_id"] = $this->get_m_plots_id();
			$return_array["m_bkl_plots_type"] = $this->get_m_bkl_plots_type();
			$return_array["m_plots_address_city"] = $this->get_m_plots_address_city();
		}
		if (($select & $this->B_m_bkl_is_end) != 0) {
			$return_array["m_bkl_is_end"] = $this->get_m_bkl_is_end();
		}
		if (($select & $this->B_dcp_id) != 0) {
			$return_array["dcp_id"] = $this->get_dcp_id();
		}
		if (($select & $this->B_m_bkl_disp_unpaid_page) != 0) {
			$return_array["m_bkl_disp_unpaid_page"] = $this->get_m_bkl_disp_unpaid_page();
		}

		if ($sel_class === "" || $sel_class == 1) {
			$return_array["pay_log"] = $this->DisplayPayLog();
			$return_array["parking_log"] = $this->DisplayParkingLog();
			$return_array["evaluation_log"] = $this->DisplayEvaluationLog();
			$return_array["vehicle_data"] = $this->DisplayVehicleData();
			$return_array["pricing_logs"] = $this->DisplayPricingLog();
			$return_array["discount_plan"] = $this->DisplayDiscountPlan();
			//$return_array["deposit"]=$this->DisplayDeposit();
		}
		//給前端(mobile)的時候不要顯示 後端要顯示欠費描述(白名單也要)
		if ($sel_paid_desc == 1)
			$return_array["PaidDesc"] = $this->get_PaidDesc();
		else
			$return_array["PaidDesc"] = "";
		return $return_array;
	}
}

?>