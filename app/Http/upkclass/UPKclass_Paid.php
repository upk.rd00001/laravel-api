<?php

use App\Http\Enum\CONST_M_PD_METHOD;

class UPK_Paid{
	private $m_pd_sn;
	private $m_pd_id;
	private $m_id;
	private $m_pd_monthlyfee;
	private $m_pd_create_datetime;
	private $m_pd_pay_datetime;
	private $m_pd_method;
	private $m_pd_error_reason;
	private $m_pd_request_log;
	private $m_pd_response_log;
	private $m_pd_transaction_id;
	private $m_pd_limit_datetime;
	private $m_dpf_id;
	private $m_pd_point;
	private $m_pd_origin_point;
	private $m_pd_title;
	
	
	public $B_m_pd_sn		=1;
	public $B_m_pd_id		=2;
	public $B_m_id		=4;
	public $B_m_pd_monthlyfee		=8;
	public $B_m_pd_create_datetime		=16;
	public $B_m_pd_pay_datetime		=32;
	public $B_m_pd_method		=64;
	public $B_m_pd_error_reason		=128;
	public $B_m_pd_request_log		=256;
	public $B_m_pd_response_log		=512;
	public $B_m_pd_transaction_id		=1024;
	public $B_m_pd_limit_datetime		=2048;
	public $B_m_dpf_id		=4096;
	public $B_m_pd_point		=8192;
	public $B_m_pd_origin_point		=16384;
	public $B_m_pd_title		=32768;
	public $B_ALL;
	
	
	public function __construct($m_pd_id,$sql_logic=""){
		$this->B_ALL =$this->B_m_pd_sn | $this->B_m_pd_id | $this->B_m_id | $this->B_m_pd_monthlyfee | $this->B_m_pd_create_datetime | $this->B_m_pd_pay_datetime | $this->B_m_pd_method | $this->B_m_pd_error_reason | $this->B_m_pd_request_log | $this->B_m_pd_response_log | $this->B_m_pd_transaction_id | $this->B_m_pd_limit_datetime | $this->B_m_dpf_id | $this->B_m_pd_point | $this->B_m_pd_origin_point | $this->B_m_pd_title; 
		$this->init_m_pd_id($m_pd_id,$sql_logic="");
    }
	public function init_m_pd_id($m_pd_id,$sql_logic=""){
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$language = "zh-tw";
		/*********************************** Put your table name here ***********************************/
		$sql="select * from tb_Member_Paid where m_pd_id = '".$m_pd_id."' ";
		
		$sql.=$sql_logic;
		$result = mysql_query($sql, $conn);
		if(! $result){
			echo json_encode(array("失敗" => mysql_error($conn).$sql));
			mysql_close($conn);
			return;
		}else if(mysql_num_rows($result) == 1)
		{
			$ans=mysql_fetch_assoc($result);
			$this->m_pd_sn=(int)$ans["m_pd_sn"];
			$this->m_pd_id=$ans["m_pd_id"];
			$this->m_id=$ans["m_id"];
			$this->m_pd_monthlyfee=(int)$ans["m_pd_monthlyfee"];
			$this->m_pd_create_datetime=$ans["m_pd_create_datetime"];
			$this->m_pd_pay_datetime=$ans["m_pd_pay_datetime"];
			$this->m_pd_method=(int)$ans["m_pd_method"];
			$this->m_pd_error_reason=$ans["m_pd_error_reason"];
			$this->m_pd_request_log=$ans["m_pd_request_log"];
			$this->m_pd_response_log=$ans["m_pd_response_log"];
			$this->m_pd_transaction_id=$ans["m_pd_transaction_id"];
			$this->m_pd_limit_datetime=$ans["m_pd_limit_datetime"];
			$this->m_pd_origin_point=$ans["m_pd_origin_point"];
			$this->m_dpf_id=$ans["m_dpf_id"];
			$this->m_pd_point=(int)$ans["m_pd_point"];
			
			
			$sql="select * from tb_Member_Paid_Method where m_pd_method = '".$this->m_pd_method."' ";
			$result = mysql_query($sql, $conn);
			if(! $result){
				echo json_encode(array("失敗" => mysql_error($conn).$sql));
				mysql_close($conn);
				return;
			}else if(mysql_num_rows($result) == 1){
				$ans=mysql_fetch_array($result);
				$m_pd_title=$ans["m_pd_description"];
				
				if($this->m_pd_monthlyfee!="0" && ($this->m_pd_method==1 || $this->m_pd_method==2)){
					//如果是綠界信用卡付款或者是其他第三方支付
					$m_pd_title=$this->m_pd_monthlyfee.$m_pd_title;
				}
				elseif($this->m_pd_method==CONST_M_PD_METHOD::AGENT_POINT){
					//如果是會員推薦贈送的點數則顯示會員ID
					$this_member = new UPK_Member($this->m_pd_error_reason);
					if($this_member->get_m_sn()!="") {
						$m_pd_title = $m_pd_title . "(推薦會員ID: " .$this_member->get_m_sn(). ")";
					}
				}
				elseif($this->m_pd_method==CONST_M_PD_METHOD::USE_APP_PAY_FEEDBACK){
					//如果是會員於易停網銷費則用error reason當作title用來顯示幾%
					$m_pd_title = $this->m_pd_error_reason;
				}
				elseif($this->m_pd_method==CONST_M_PD_METHOD::STORE_TRANSFER){
					//店家轉給會員有%s要用printf取代掉
					// 取得店家%s%s轉移點數
					// 會員獲得點數的訊息
					$request_log_array = json_decode($this->m_pd_request_log,true);
					$str_m_as_name="";
					$str_m_as_m_id="";
					if(isset($request_log_array["m_as_id"])) {
						$sql_as = "SELECT * FROM tb_Member_Authorized_Store WHERE m_as_id = '" . $request_log_array["m_as_id"] . "' ";
						$result_as = mysql_query($sql_as, $conn);
						$ans_as = mysql_fetch_array($result_as);
						$str_m_as_name = $ans_as["m_as_name"];
						$this_member = new UPK_Member($ans_as["m_id"]);
						$str_m_as_m_id = "(店家會員ID: " . $this_member->get_m_sn() . ")";
					}
					$m_pd_title = sprintf($ans["m_pd_description"],$str_m_as_name,$str_m_as_m_id);
				}
				elseif($this->m_pd_method==CONST_M_PD_METHOD::MEMBER_TRANSFER){
					//會員贈送給店家
					// 店家%s取得會員%s停車點數
					// 店家獲得點數的訊息
					$request_log_array = json_decode($this->m_pd_request_log,true);
					$str_m_as_name="";
					$str_m_id="";
					if(isset($request_log_array["m_as_id"])) {
						$sql_as = "SELECT * FROM tb_Member_Authorized_Store WHERE m_as_id = '" . $request_log_array["m_as_id"] . "' ";
						$result_as = mysql_query($sql_as, $conn);
						$ans_as=mysql_fetch_array($result_as);
						$str_m_as_name=$ans_as["m_as_name"];
					}
					if(isset($request_log_array["m_id"])) {
						$this_member = new UPK_Member($request_log_array["m_id"]);
						$str_m_id = "(ID: ".$this_member->get_m_sn().")";
					}
					$m_pd_title = sprintf($ans["m_pd_description"],$str_m_as_name,$str_m_id);
				}
				elseif ($this->m_pd_method == CONST_M_PD_METHOD::TRASNFER_PAID_FREE) {
					$send_m_id = substr($this->m_pd_error_reason, 0, 14);
					$this_send_member = new UPK_Member($send_m_id);
					$this_member = new UPK_Member($this->m_id);
					$str_m_as_m_id = "會員ID: " . $this_member->get_m_sn() . "";
					$str_send_m_id = "會員ID: " . $this_send_member->get_m_sn() . "";
					$m_pd_title = sprintf($ans["m_pd_description"], $str_send_m_id, $str_m_as_m_id);
				}
				elseif ($this->m_pd_method  == CONST_M_PD_METHOD::TRASNFER_PAID) {
					$send_m_id = substr($this->m_pd_error_reason, 0, 14);
					$this_send_member = new UPK_Member($send_m_id);
					$this_member = new UPK_Member($this->m_id);
					$str_m_as_m_id = "會員ID: " . $this_member->get_m_sn() . "";
					$str_send_m_id = "會員ID: " . $this_send_member->get_m_sn() . "";
					$m_pd_title = sprintf($ans["m_pd_description"], $str_send_m_id, $str_m_as_m_id);
				}
				elseif ($this->m_pd_method  == CONST_M_PD_METHOD::AS_TO_CUSTOMER_REFUND) {
					$sql_aspd = "SELECT * FROM tb_Member_As_Paid WHERE m_aspd_id='".$this->m_pd_error_reason."' ";
					$result_aspd = mysql_query($sql_aspd, $conn);
					$m_as_id = "";
					if(!$result_aspd) {
					}
					elseif(mysql_num_rows($result_aspd) > 0) {
						$ans_aspd = mysql_fetch_assoc($result_aspd);
						$m_as_id = $ans_aspd["m_aspd_id"];
					}
					$this_authorized_store = new UPK_AuthorizedStore($m_as_id);
					$m_pd_title = $this_authorized_store->get_m_as_name().$ans["m_pd_description"];
				}
				elseif ($this->m_pd_method  == CONST_M_PD_METHOD::AS_TO_CUSTOMER_PAID_BACK_AS_REFUND) {
					$this_authorized_store = new UPK_AuthorizedStore($this->m_pd_error_reason);
					$m_pd_title = $this_authorized_store->get_m_as_name().$ans["m_pd_description"];
				}
				elseif ($this->m_pd_method  == CONST_M_PD_METHOD::AS_TO_CUSTOMER_PAID_BACK) {
					$tmp_split = explode(":", $this->m_pd_error_reason);
					// $this->m_pd_error_reason = 媽媽2 ASID0000001960: 特約店回饋停車點數
					$tmp_split_space = explode(" ", $tmp_split[0]);
					$m_as_name = $tmp_split_space[0];
					$m_pd_title = $m_as_name.$ans["m_pd_description"];
				}
				elseif ($this->m_pd_method  == CONST_M_PD_METHOD::UNDEFINED) {
				}
			}
			else{
				$sql="select * from tb_Member_Paid_Method where m_pd_method = '0' ";
				$result = mysql_query($sql, $conn);
				$ans=mysql_fetch_array($result);
				$m_pd_title=$ans["m_pd_description"];
				//$m_pd_title="停車點數贈送";
			}
			$this->m_pd_title=$m_pd_title;
		}
	}
public function get_m_pd_sn(){
	return $this->m_pd_sn;
}
public function get_m_pd_id(){
	return $this->m_pd_id;
}
public function get_m_id(){
	return $this->m_id;
}
public function get_m_pd_monthlyfee(){
	return $this->m_pd_monthlyfee;
}
public function get_m_pd_create_datetime(){
	return $this->m_pd_create_datetime;
}
public function get_m_pd_pay_datetime(){
	return $this->m_pd_pay_datetime;
}
public function get_m_pd_method(){
	return $this->m_pd_method;
}
public function get_m_pd_error_reason(){
	return $this->m_pd_error_reason;
}
public function get_m_pd_request_log(){
	return $this->m_pd_request_log;
}
public function get_m_pd_response_log(){
	return $this->m_pd_response_log;
}
public function get_m_pd_transaction_id(){
	return $this->m_pd_transaction_id;
}
public function get_m_pd_limit_datetime(){
	return $this->m_pd_limit_datetime;
}
public function get_m_dpf_id(){
	return $this->m_dpf_id;
}
public function get_m_pd_point(){
	return $this->m_pd_point;
}
public function get_m_pd_origin_point(){
	return $this->m_pd_origin_point;
}
public function get_m_pd_title(){
	return $this->m_pd_title;
}
public function get_array($select=""){
	if($select=="")
		$select=$this->B_ALL;
	$return_array=array();
	if(($select & $this->B_m_pd_sn) != 0 ){
		$return_array["m_pd_sn"]=$this->get_m_pd_sn();
	}
	if(($select & $this->B_m_pd_id) != 0 ){
		$return_array["m_pd_id"]=$this->get_m_pd_id();
	}
	if(($select & $this->B_m_id) != 0 ){
		$return_array["m_id"]=$this->get_m_id();
	}
	if(($select & $this->B_m_pd_monthlyfee) != 0 ){
		$return_array["m_pd_monthlyfee"]=$this->get_m_pd_monthlyfee();
	}
	if(($select & $this->B_m_pd_create_datetime) != 0 ){
		$return_array["m_pd_create_datetime"]=$this->get_m_pd_create_datetime();
	}
	if(($select & $this->B_m_pd_pay_datetime) != 0 ){
		$return_array["m_pd_pay_datetime"]=$this->get_m_pd_pay_datetime();
	}
	if(($select & $this->B_m_pd_method) != 0 ){
		$return_array["m_pd_method"]=$this->get_m_pd_method();
	}
	if(($select & $this->B_m_pd_error_reason) != 0 ){
		$return_array["m_pd_error_reason"]=$this->get_m_pd_error_reason();
	}
	if(($select & $this->B_m_pd_request_log) != 0 ){
		$return_array["m_pd_request_log"]=$this->get_m_pd_request_log();
	}
	if(($select & $this->B_m_pd_response_log) != 0 ){
		$return_array["m_pd_response_log"]=$this->get_m_pd_response_log();
	}
	if(($select & $this->B_m_pd_transaction_id) != 0 ){
		$return_array["m_pd_transaction_id"]=$this->get_m_pd_transaction_id();
	}
	if(($select & $this->B_m_pd_limit_datetime) != 0 ){
		$return_array["m_pd_limit_datetime"]=$this->get_m_pd_limit_datetime();
	}
	if(($select & $this->B_m_dpf_id) != 0 ){
		$return_array["m_dpf_id"]=$this->get_m_dpf_id();
	}
	if(($select & $this->B_m_pd_point) != 0 ){
		$return_array["m_pd_point"]=$this->get_m_pd_point();
	}
	if(($select & $this->B_m_pd_origin_point) != 0 ){
		$return_array["m_pd_origin_point"]=$this->get_m_pd_origin_point();
	}
	if(($select & $this->B_m_pd_title) != 0 ){
		$return_array["m_pd_title"]=$this->get_m_pd_title();
	}
	return $return_array;
}
}
?>