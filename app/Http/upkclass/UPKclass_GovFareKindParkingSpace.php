<?php
class UPK_GovFareKindParkingSpace{
	private $sn;
	private $fare_id;
	private $m_pk_id;
	private $fare_parameters;
	private $fare_parameters_class;
	private $fare_parameters_array;
	private $create_datetime;
	private $update_datetime;
	private $fare_desc;
	private $is_disable;
	public $B_sn		=1;
	public $B_fare_id		=2;
	public $B_m_pk_id		=4;
	public $B_fare_parameters		=8;
	public $B_create_datetime		=16;
	public $B_update_datetime		=32;
	public $B_fare_parameters_class		=64;
	public $B_fare_desc		=128;
	public $B_fare_parameters_array		=256;
	public $B_is_disable		=512;
	public $B_ALL;
	public function __construct($sn,$sql_logic=""){
		$this->B_ALL =$this->B_sn | $this->B_fare_id | $this->B_m_pk_id | $this->B_fare_parameters | $this->B_create_datetime | $this->B_update_datetime | $this->B_fare_parameters_class | $this->B_fare_desc | $this->B_is_disable | $this->B_fare_parameters_array; 
		$this->init_sn($sn,$sql_logic="");
    }
	public function init_sn($sn,$sql_logic=""){
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$language = "zh-tw";
		/*********************************** Put your table name here ***********************************/
		$sql="select * from tb_Gov_FareKind_ParkingSpace where sn = '".$sn."' ";
		
		$sql.=$sql_logic;
		$result = mysql_query($sql, $conn);
		if(! $result){
			echo json_encode(array("失敗" => mysql_error($conn).$sql));
			mysql_close($conn);
			return;
		}else if(mysql_num_rows($result) == 1)
		{
			$ans=mysql_fetch_assoc($result);
			$this->sn=$ans["sn"];
			$this->fare_id=$ans["fare_id"];
			$this->m_pk_id=$ans["m_pk_id"];
			$this->fare_parameters=$ans["fare_parameters"];
			$this->fare_parameters_array=json_decode($ans["fare_parameters"],true);
			if(!isset($this->fare_parameters_array["daily_max_price"]))
				$this->fare_parameters_array["daily_max_price"]=0;
			if(!isset($this->fare_parameters_array["daily_max_price_min_unit"]))
				$this->fare_parameters_array["daily_max_price_min_unit"]=1440;
			if(!isset($this->fare_parameters_array["min_unit"]))
				$this->fare_parameters_array["min_unit"]=0;
			if(!isset($this->fare_parameters_array["min_time"]))
				$this->fare_parameters_array["min_time"]=0;
			if(!isset($this->fare_parameters_array["weekly"]))
				$this->fare_parameters_array["weekly"]=127;
			if(!isset($this->fare_parameters_array["use_holiday"]))
				$this->fare_parameters_array["use_holiday"]=0;
			if(!isset($this->fare_parameters_array["holiday_fare"]))
				$this->fare_parameters_array["holiday_fare"]=0;
			if(!isset($this->fare_parameters_array["is_cross_day"]))
				$this->fare_parameters_array["is_cross_day"]=1;//是要跨日重計
			$ans["fare_parameters"]=json_encode($this->fare_parameters_array);
			$this->fare_parameters_class=json_decode($ans["fare_parameters"]);
			$this->create_datetime=$ans["create_datetime"];
			$this->update_datetime=$ans["update_datetime"];
			$this->is_disable=$ans["is_disable"];
			#國定假日補上班
			$DT_date=new Datetime();
			if($DT_date->format("Y-m-d")=="2019-02-23" ||
				$DT_date->format("Y-m-d") == "2019-10-05" ||
				$DT_date->format("Y-m-d") == "2020-02-15" ||
				$DT_date->format("Y-m-d") == "2020-06-20" ||
				$DT_date->format("Y-m-d") == "2020-09-26" ||
				$DT_date->format("Y-m-d") == "2021-02-20") {
				$this_parking_space = new UPK_ParkingSpace($this->m_pk_id);
				$this_parking_space->GetParkingLot();
				$this_parking_lot = $this_parking_space->parking_lot;
				if ($this_parking_lot->get_m_plots_type() == "100") {
					//應急當天路邊強制收費weekly複寫城1~6
					if ($this->fare_parameters_array["weekly"] == "62") {
						$this->fare_parameters_array["weekly"] = "126";
						$this->fare_parameters = json_encode($this->fare_parameters_array);
						$this->fare_parameters_class = json_decode($ans["fare_parameters"]);
					}
					elseif ($this->fare_parameters_array["weekly"] == "65") {
						$this->fare_parameters_array["weekly"] = "1";
						$this->fare_parameters = json_encode($this->fare_parameters_array);
						$this->fare_parameters_class = json_decode($ans["fare_parameters"]);
					}
				}
			}
			$this->fare_desc="";
			$this->fare_desc=$this->get_custom_fare_desc();
			
		}
	}
	function FareKindToDateTime($DT_date){
		if($this->is_charge($DT_date)){
			return $this->fare_parameters_array["part"];
		}
		else return array();
	}
	function is_charge($DT_date)
	{
		include_once("/../main_api/GovPriceCalculateFunc_v2.php");
		$this_parking_space = new UPK_ParkingSpace($this->m_pk_id);
		$this_parking_space->GetParkingLot();
		$this_parking_lot = $this_parking_space->parking_lot;
		$m_plots_type = $this_parking_lot->get_m_plots_type();
		return isDateCharge($DT_date,$this->fare_parameters_array,$m_plots_type);
	}
	function is_time_charge($DT_datetime)
	{
		if($this->is_charge($DT_datetime)) {
			foreach($this->fare_parameters_array["part"] as $fare_para) {
				$fare_start_time = new DateTime($fare_para["start_time"]);
				$fare_end_time = new DateTime($fare_para["end_time"]);
				$DT_time = new DateTime($DT_datetime->format("H:i:s"));
				if ($fare_start_time<=$DT_time && $DT_time<=$fare_end_time) {
					return true;
				}
			}
			return false;
		}
		else return false;
	}
	function GovTimeComprasion($start_date,$end_date,$start_time,$end_time,$mode="INSIDE",$waiting_min=0){
		//如果傳進來的waiting_min不等於凌則已經被控衛時段佔住
		$waiting_status=1;
		//插入國定假日與Weekly判斷，從GovPriceCalculateFunc.php複製過來，讓變數名稱一樣
		$DT_start_date=new DateTime($start_date);
		$DT_start_time=new DateTime($start_time);
		$DT_end_date=new DateTime($end_date);
		$DT_end_time=new DateTime($end_time);
		$DT_start_datetime=new DateTime($start_date." ".$start_time);
		$DT_end_datetime=new DateTime($end_date." ".$end_time);
		$gov_current_desc="";
		if(!$this->is_charge($DT_start_date) || !$this->is_charge($DT_end_date)){
			//如果兩個日期其中有一天不收費
			//$waiting_min=-1;
			$waiting_status=0;
			return ["waiting_min"=>$waiting_min,"waiting_status"=>$waiting_status,"gov_current_desc"=>""];
		}
		$matched_flag=0;
		if($this->is_disable=="1"){
			//$waiting_min=-1;
			$waiting_status=0;
			return ["waiting_min"=>$waiting_min,"waiting_status"=>$waiting_status,"gov_current_desc"=>""];
		}
		$farekind_waiting_min=99999;
		foreach($this->fare_parameters_array["part"] as $fare_para){
			$fare_start_time=new DateTime($fare_para["start_time"]);
			$fare_end_time=new DateTime($fare_para["end_time"]);
			$logiced_time=DateTimeBoolean($DT_start_datetime,$DT_end_datetime,$fare_start_time,$fare_end_time,"AND");
			//print_r($logiced_time);
			if($matched_flag==0&&$logiced_time["time"]){//有定義時間
				$matched_flag=1;
				$gov_current_desc=$fare_para["part_desc"];
				if(isset($fare_para["disp_waiting_min"])
					&&((int)$fare_para["disp_waiting_min"]<(int)$waiting_min)){
			//		$waiting_min=$fare_para["disp_waiting_min"];
					//$waiting_min=-$waiting_min;
					$waiting_status=3; //如果已經有空位時段，則可接漏時間
				}else{
					if($DT_start_time>$fare_start_time){
						$farekind_waiting_min=0;
						$waiting_status=2;
					}else{
						$tmp_waiting_min=(int)(DateTimeSub($fare_start_time,$DT_start_time)/60);
						if($farekind_waiting_min>$tmp_waiting_min)
							$farekind_waiting_min=$tmp_waiting_min;
						$waiting_status=0;
					}
				}
			}
			if($waiting_status==2){
				$waiting_min=$farekind_waiting_min;
			}
			if(isset($fare_para["limit_time"]) && $fare_para["limit_time"]==-2 && $logiced_time["time"]){//禁停
				$matched_flag=1;
				//$waiting_min=-2;
				$gov_current_desc=$fare_para["part_desc"];
				$waiting_status=4;
			}
			if(isset($fare_para["progressive"][0]["elapse"]) && $fare_para["progressive"][0]["elapse"]==-2 && $logiced_time["time"]){
				//累進的第一個時段就禁停
				$matched_flag=1;
				//$waiting_min=-2;
				$gov_current_desc=$fare_para["part_desc"];
				$waiting_status=4;
			}

		}
		if($matched_flag==0){//如果沒有定義任何時間則回傳未知狀態
			//$waiting_min=-1;
			$waiting_status=0;
		}
		return ["waiting_min"=>$waiting_min,"waiting_status"=>$waiting_status,"gov_current_desc"=>$gov_current_desc];
	}
	public function get_custom_fare_desc($weekly=1,$time=1,$money=1)
	{
		$tmp_fare_desc = "";
		if ($weekly == 1) {
			if ($this->fare_parameters_array["holiday_fare"] == 1)
				$tmp_fare_desc .= "國定假日";
			elseif ($this->fare_parameters_array["weekly"] == 127)
				$tmp_fare_desc .= "每日";
			elseif ($this->fare_parameters_array["weekly"] == 62)
				$tmp_fare_desc .= "平日";
			elseif ($this->fare_parameters_array["weekly"] == 65)
				$tmp_fare_desc .= "六日";
			else {
				if (($this->fare_parameters_array["weekly"] & 1) != 0)
					$tmp_fare_desc .= "星期日 ";
				if (($this->fare_parameters_array["weekly"] & 2) != 0)
					$tmp_fare_desc .= "星期一 ";
				if (($this->fare_parameters_array["weekly"] & 4) != 0)
					$tmp_fare_desc .= "星期二 ";
				if (($this->fare_parameters_array["weekly"] & 8) != 0)
					$tmp_fare_desc .= "星期三 ";
				if (($this->fare_parameters_array["weekly"] & 16) != 0)
					$tmp_fare_desc .= "星期四 ";
				if (($this->fare_parameters_array["weekly"] & 32) != 0)
					$tmp_fare_desc .= "星期五 ";
				if (($this->fare_parameters_array["weekly"] & 64) != 0)
					$tmp_fare_desc .= "星期六 ";
			}
			$tmp_fare_desc .= "";
		}
		/*if($this->fare_parameters_array["free_time"] != "0"){
			$tmp_fare_desc.="前".$this->fare_parameters_array["free_time"]."分免費";
			$tmp_fare_desc.=", ";
		}
		if($this->fare_parameters_array["min_time"] != "0"){
			$tmp_fare_desc.="停靠時間".$this->fare_parameters_array["min_time"]."分起算";
			$tmp_fare_desc.=", ";
		}
		if($this->fare_parameters_array["min_unit"] != "0"){
			$tmp_fare_desc.="未滿".$this->fare_parameters_array["min_unit"]."分以".$this->fare_parameters_array["min_unit"]."分計";
			$tmp_fare_desc.=",";
		}*/
		$daily_max_desc="";
		if (isset($this->fare_parameters_array["daily_max_price_min_unit"]) && isset($this->fare_parameters_array["daily_max_price"]) && 
			$this->fare_parameters_array["daily_max_price_min_unit"] != "0" && $this->fare_parameters_array["daily_max_price"] != "0") {
			if ($this->fare_parameters_array["daily_max_price_min_unit"] % 60 == 0) {
				//如果小時是整數就顯示小時
				$hour = $this->fare_parameters_array["daily_max_price_min_unit"] / 60;
				$day = $hour/24;
				//如果剛好一整天 (1440分鐘) 就顯示一天
				if ($hour % 24 == 0)
					$daily_max_desc .= "每" . $day . "天上限" . $this->fare_parameters_array["daily_max_price"] . "元";
				else
					$daily_max_desc .= "每" . $hour . "小時上限" . $this->fare_parameters_array["daily_max_price"] . "元";
			}
			else {
				$daily_max_desc .= "每" . $this->fare_parameters_array["daily_max_price_min_unit"] . "分鐘上限" . $this->fare_parameters_array["daily_max_price"] . "元";
			}
		}
		switch ($this->fare_id) {
			case "1":
			case "4"://卸貨計時
			//	$tmp_fare_desc .= ("時段計時: ");
				$i=1;
			//	$tmp_fare_desc.=", ";
				foreach((array)$this->fare_parameters_array["part"] as $key => $each_fare){
					//$tmp_fare_desc.="時段".$i;
					$i++;
				//	$tmp_fare_desc.=": ";
					if($time==1){
						$tmp_fare_desc.= "".$each_fare["start_time"]."~".$each_fare["end_time"];
						$tmp_fare_desc.=" ";
					}
					if($money==1){
						if (isset($this->fare_parameters_array["min_unit"]) &&
							$this->fare_parameters_array["min_unit"] != "0" ) {

							$price=$this->fare_parameters_array["min_unit"]/30*$each_fare["price"];
							if ($this->fare_parameters_array["min_unit"] % 60 == 0) {
								//如果小時是整數就顯示小時
								$hour = $this->fare_parameters_array["min_unit"] / 60;
								$day = $hour/24;
								//如果剛好一整天 (1440分鐘) 就顯示一天
								if ($hour % 24 == 0)
									$tmp_fare_desc .= "每" . $day . "天" . $price . "元";
								else
									$tmp_fare_desc .= "每" . $hour . "小時" . $price . "元";
							}
							else {
								$tmp_fare_desc .= "每" . $this->fare_parameters_array["min_unit"] . "分" . $price . "元";
							}
						}
						else{
							$tmp_fare_desc .= "每30分" . $each_fare["price"] . "元";
						}
						$tmp_fare_desc.="";
					}
					$this->fare_parameters_array["part"][$key]["part_desc"]="".$tmp_fare_desc.$daily_max_desc;
				}
			break;
			case "2":
				$tmp_fare_desc .= ("計時");
			break;
			case "3"://計次
			//	$tmp_fare_desc .= ("計次");//覺得顯示這個怪怪的
				foreach($this->fare_parameters_array["part"] as $key => $each_fare){
					//$tmp_fare_desc.="時段";
				//	$tmp_fare_desc.=": ";
					if($time==1){
						$tmp_fare_desc.= " ".$each_fare["start_time"]."~".$each_fare["end_time"];
						$tmp_fare_desc.=" ";
					}
					if($money==1){
						$tmp_fare_desc.= "每次".$each_fare["price"]."元";
						$tmp_fare_desc.="";
					}
					$this->fare_parameters_array["part"][$key]["part_desc"]="".$each_fare["start_time"]."~".$each_fare["end_time"]." 每次".$each_fare["price"]."元";
				}
			break;
//			case "4":
//				$tmp_fare_desc .= ("卸貨");
//			break;
			case "5": //$tmp_fare_desc .= ("身障累進");
				$tmp_tmp_desc=$tmp_fare_desc;//紀錄一夏
				if($this->fare_id=="5")
					$tmp_fare_desc .= ("累進");
				else
					$tmp_fare_desc .= ("身障累進");
				foreach($this->fare_parameters_array["part"] as $key => $each_fare){
				//希望只有一筆
					//$tmp_fare_desc.="時段".$i;
					$i=0;
				//	$tmp_fare_desc.=": ";
					$tmp_fare_desc.= "".$each_fare["start_time"]."~".$each_fare["end_time"];
					$tmp_fare_desc.=" ";
					$next_hour=0;
					for($i=0; $i<count($each_fare["progressive"]); $i++){
						$each_prog=$each_fare["progressive"][$i];
						if($each_prog["elapse"]==-2){
							$tmp_fare_desc.="禁停；";
							continue;
						}
						$each_elapse=round($each_prog["elapse"]/60,2);
						$this_hour=$next_hour;
						$next_hour=$this_hour+$each_elapse;
						// 第1小時30元；第2~3小時每30分鐘40元；第4~5小時每30分鐘50元；第6小時以上每30分鐘60元。
						$price_text="每30分鐘".$each_prog["price"]."元";
						if($each_prog["price"]==0){
							$price_text="免費";
						}
						if($i==0)//第一筆
							$tmp_fare_desc.= "前".$next_hour."小時".$price_text.",";
						elseif($i==(count($each_fare["progressive"])-1))
							//最後一筆
							$tmp_fare_desc.= "第".$this_hour."小時以上".$price_text."；";
						else
							$tmp_fare_desc.= "第".$this_hour."~".$next_hour."小時".$price_text.",";
					}
					//$tmp_fare_desc.= "每30分鐘".$each_fare["price"]."元";
					$tmp_fare_desc.="";
					$this->fare_parameters_array["part"][$key]["part_desc"]=$tmp_fare_desc;
				}
				if($money!=1||$time!=1)
					$tmp_fare_desc=$tmp_tmp_desc;//之前都不算
					break;
			break;

			case "F"://身障計次
			case "B"://身障累進
			case "A"://身障計時
				foreach($this->fare_parameters_array["part"] as $key => $each_fare) {
					// 2019-07-25 因現在無身障計費，所以一律先採用這個描述，不須顯示費率
					$this->fare_parameters_array["part"][$key]["part_desc"] = "依公告牌面收費";
				}
			break;
			case "6":
			$tmp_fare_desc .= ("限時");
			break;
			case "7":
			$tmp_fare_desc .= ("計時時段限停");
			break;
			case "8":
			$tmp_fare_desc .= ("累進時段限停");
			break;
			case "9":
			$tmp_fare_desc .= ("身障零費率");
			break;
			case "C":
			$tmp_fare_desc .= ("身障計時時段限停");
			break;
			case "D":
			$tmp_fare_desc .= ("身障累進時段限停");
			break;
			case "E":
			$tmp_fare_desc .= ("警車零費率");
			break;
		}
		$cross_day_desc="";
		if(isset($this->fare_parameters_array["is_cross_day"])) {
			if($this->fare_parameters_array["is_cross_day"]==1) {
				$cross_day_desc="跨日另計";
			}
			else {
				$cross_day_desc="";
			}
		}
		else {
			$cross_day_desc="";
		}
		$tmp_fare_desc=$tmp_fare_desc.$cross_day_desc;
		return $tmp_fare_desc;
	}
public function get_sn(){
	return $this->sn;
}
public function get_fare_id(){
	return $this->fare_id;
}
public function get_m_pk_id(){
	return $this->m_pk_id;
}
public function get_fare_parameters(){
	return $this->fare_parameters;
}
public function get_fare_parameters_class(){
	return $this->fare_parameters_class;
}
public function get_fare_parameters_array(){
	return $this->fare_parameters_array;
}
public function get_create_datetime(){
	return $this->create_datetime;
}
public function get_update_datetime(){
	return $this->update_datetime;
}
public function get_fare_desc(){
	return $this->fare_desc;
}
public function get_is_disable(){
	return $this->is_disable;
}
public function get_array($select=""){
	if($select=="")
		$select=$this->B_ALL;
	$return_array=array();
	if(($select & $this->B_sn) != 0 ){
		$return_array["sn"]=$this->get_sn();
	}
	if(($select & $this->B_fare_id) != 0 ){
		$return_array["fare_id"]=$this->get_fare_id();
	}
	if(($select & $this->B_m_pk_id) != 0 ){
		$return_array["m_pk_id"]=$this->get_m_pk_id();
	}
	if(($select & $this->B_fare_parameters) != 0 ){
		$return_array["fare_parameters"]=$this->get_fare_parameters();
	}
	if(($select & $this->B_fare_parameters_class) != 0 ){
		$return_array["fare_parameters_class"]=$this->get_fare_parameters_class();
	}
	if(($select & $this->B_fare_parameters_array) != 0 ){
		$return_array["fare_parameters_array"]=$this->get_fare_parameters_array();
	}
	if(($select & $this->B_create_datetime) != 0 ){
		$return_array["create_datetime"]=$this->get_create_datetime();
	}
	if(($select & $this->B_update_datetime) != 0 ){
		$return_array["update_datetime"]=$this->get_update_datetime();
	}
	if(($select & $this->B_fare_desc) != 0 ){
		$return_array["fare_desc"]=$this->get_fare_desc();
	}
	if(($select & $this->B_is_disable) != 0 ){
		$return_array["is_disable"]=$this->get_is_disable();
	}
	return $return_array;
}
}
?>