<?php
include_once("fn_package.php");
function insertArr($db, $tableName, $insertData)
{
	global $conn, $dbName;
	check_conn($conn, $dbName);
	$columns = implode(", ", array_keys($insertData));
	$values = parseInsertValues($insertData);
	$sql = "INSERT INTO $tableName ($columns) VALUES ($values)";
	if (!mysql_query($sql, $conn)) 
	{
	
	}
}

function parseInsertValues($insertData)
{
  foreach ($insertData as $idx => $data) $insertData[$idx] = "'" . $data . "'";
  $values = implode(", ", $insertData);
  return $values;
}

class UPKclass_Dashboard_Parking
{
	public $dbpg_sn                ;
	public $m_plots_id             ;
	public $m_pk_id                ;
	public $dbpg_type              ;
	public $m_id                   ;
	public $dbpg_create_datetime   ;
	public $dbpg_desc              ;
	public $m_ve_id                ;
	public $m_bk_id                ;
	public $m_dpt_id               ;
	// Methods
	function set_m_plots_id($m_plots_id) {
		$this->m_plots_id = $m_plots_id;
	}
	function set_m_pk_id($m_pk_id) {
		$this->m_pk_id = $m_pk_id;
	} 
	function set_dbpg_type($dbpg_type) {
		$this->dbpg_type = $dbpg_type;
	} 
 
	function set_m_id($m_id) {
		$this->m_id = $m_id;
	}
	function set_dbpg_create_datetime($dbpg_create_datetime) {
		$this->dbpg_create_datetime = $dbpg_create_datetime;
	}
	function set_dbpg_desc($dbpg_desc) {
		$this->dbpg_desc = $dbpg_desc;
	}	
	function set_m_ve_id($m_ve_id) {
		$this->m_ve_id = $m_ve_id;
	}
	function set_m_bk_id($m_bk_id) {
		$this->m_bk_id = $m_bk_id;
	}
	function set_m_dpt_id ($dpt_id ) {
		$this->m_dpt_id  = $dpt_id ;
	}
	
	function __construct()
	{
	
	}
  
	function insert($allData)
	{
		  
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$dashParking = array();
		foreach (DashboardParkingFields::$DashboardParkingFieldsArray as $k) {
		  if (array_key_exists($k, $allData))
			$dashParking[$k] = $allData[$k];
		}
		try {
			insertArr($conn, 'tb_Dashboard_Parking', $dashParking, true);
			return json_encode(['result'=>1, 'desc' => '新增成功'], JSON_UNESCAPED_UNICODE);
		} catch (Exception $e) {
			exceptionHandler($e);
		}		
	}
	
	//找出某種類型,在這時間區間內發生次數
	function getTypeCount($type, $startdate, $enddate)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$sql = "SELECT  count(*) count from tb_Dashboard_Parking where dbpg_type = '$type' 
				and dbpg_create_datetime >= '$startdate' 
				and dbpg_create_datetime <= '$enddate' ";
		$res = mysql_query($sql, $conn);
		if (!$res ) 
		{
		}
		else
		{
			$ans = mysql_fetch_assoc($res);
			return json_encode($ans);//.$sql;
		}
		
	}
	
	//找出某種類型,在這時間區間內發生次數
	function getTypeDetail($type, $startdate, $enddate, $count=10, $page=1)
	{
		global $conn, $dbName;
		$items_per_page = $count;
		check_conn($conn, $dbName);
		$offset = ($page - 1) * $items_per_page;
		$sql = "SELECT  * from tb_Dashboard_Parking where dbpg_type = '$type' 
				and dbpg_create_datetime >= '$startdate' 
				and dbpg_create_datetime <= '$enddate' LIMIT " . $offset . "," . $items_per_page;
		$res = mysql_query($sql, $conn);
		$allAnd = [];
		if (!$res ) 
		{
			return $sql;
		}
		else
		{
			 
			while($ans=mysql_fetch_assoc($res)) {
				 array_push($allAnd, $ans);
			}
			return $allAnd;
		}
		
	}
}

class DashboardParkingFields
{

	public static $fieldsArray = [
		'dbpg_sn' => ['label' => '序號', 'value' => ''],//B
		'm_plots_id' => ['label' => '停車場ID', 'value' => ''],//C,
		'm_pk_id' => ['label' => '停車位ID'],//D
		'dbpg_type' => ['label' => '事件類別', 'value' => ''],//E
		'm_id' => ['label' => '會員ID', 'value' => ''],//F
		'dbpg_create_datetime' => ['label' => '建立時間', 'value' => ''],//G
		'dbpg_desc' => ['label' => '描述', 'value' => ''],//H
		'm_ve_id' => ['label' => '車輛ID', 'value' => ''],//I
		'm_bk_id' => ['label' => '預約ID', 'value' => ''],//J
		'm_dpt_id' => ['label' => '交易ID', 'value' => ''],//K
		'm_as_id' => ['label' => '特約店ID', 'value' => ''],//L
		'para1' => ['label' => '參數1', 'value' => ''],//M
		'para2' => ['label' => '參數2', 'value' => ''],//N
		'para3' => ['label' => '參數3', 'value' => ''],//O
		'para4' => ['label' => '參數4', 'value' => ''],//P
		'para5' => ['label' => '參數5', 'value' => '']
	];
	public static $DashboardParkingFieldsArray = [
		'dbpg_sn',
		'm_plots_id',
		'm_pk_id',
		'dbpg_type',
		'm_id',
		'dbpg_create_datetime',
		'dbpg_desc',
		'm_ve_id',
		'm_bk_id',
		'm_dpt_id',
		'm_as_id',
		'para1',
		'para2',
		'para3',
		'para4',
		'para5'
	];
}
?>
<?php

//test();

function insert_test(){
	$data = [];
	$data['m_plots_id'] = 'PLOT0000001231';
	$data['m_pk_id'] = 'MPID0000001231';
	$data['dbpg_type'] = '0';
	$data['m_id'] = 'UMID0000000012';
	$data['dbpg_create_datetime'] = '2020-10-10 11:11:22';
	$data['dbpg_desc'] = 'dbpg_desc';
	$data['m_ve_id'] = 'm_ve_id';
	$data['m_bk_id'] = 'm_bk_id';
	$data['m_dpt_id'] = 'm_dpt_id';

	$Dashboard_Parking = new UPKclass_Dashboard_Parking();
	$Dashboard_Parking->insert($data);
	return "OK";
}

function test(){
	$Dashboard_Parking = new UPKclass_Dashboard_Parking();
	echo insert_test();
	echo $Dashboard_Parking->getTypeCount(0, '2020-01-01', '2020-12-31');
	echo json_encode($Dashboard_Parking->getTypeDetail(0, '2020-01-01 00:00:00', '2020-12-31 23:59:59',100,1));
}
?>