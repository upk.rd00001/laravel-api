<?php

class UPK_ParkingLog {
	private $m_pl_sn;
	public $m_pl_id;
	private $m_pyl_id;
	private $m_bkl_id;
	private $m_ve_id;
	private $m_pk_id;
	private $m_pl_start_time;
	private $m_pl_end_time;
	private $m_id_appear;

	public $B_m_pl_sn=			1;
	public $B_m_pl_id=			2;
	public $B_m_pyl_id=			4;
	public $B_m_bkl_id=			8;
	public $B_m_ve_id=			16;
	public $B_m_pk_id=			32;
	public $B_m_pl_start_time=	64;
	public $B_m_pl_end_time=	128;
	public $B_m_id_appear	=	256;

	public $B_TIME;
	public $B_ID;
	public $B_ALL;
	public $booking_log;
	public function __construct($m_pl_id,$sql_logic=""){

		$this->B_TIME=$this->B_m_pl_start_time | $this->B_m_pl_end_time;
		$this->B_ID = $this->B_m_pl_id | $this->B_m_pyl_id | $this->B_m_bkl_id | $this->B_m_ve_id | $this->B_m_pk_id;
		$this->B_ALL = $this->B_m_pl_sn | $this->B_TIME | $this->B_ID | $this->B_m_id_appear;
		$this->init_pl_id($m_pl_id,$sql_logic="");
    }

    public function init_pl_id($m_pl_id,$sql_logic=""){
		if($m_pl_id=="") {
			$this->m_pl_sn="";
			$this->m_pl_id="";
			$this->m_pyl_id="";
			$this->m_bkl_id="";
			$this->m_ve_id="";
			$this->m_pk_id="";
			$this->m_pl_start_time=null;
			$this->m_pl_end_time=null;
			$this->m_id_appear="";
			return;
		}
		global $conn, $dbName;
		check_conn($conn,$dbName);
		$language = "zh-tw";
		$sql="select * from tb_Member_Parking_Log where m_pl_id = '".$m_pl_id."' ";
		$sql.=$sql_logic;
		$result = mysql_query($sql, $conn);
		if(! $result){
			return json_encode(array("result"=>0,"失敗" => mysql_error($conn).$sql));
		}
		else if(mysql_num_rows($result) == 1) {
			$ans=mysql_fetch_assoc($result);
			$this->m_pl_sn=$ans["m_pl_sn"];
			$this->m_pl_id=$ans["m_pl_id"];
			$this->m_pyl_id=$ans["m_pyl_id"];
			$this->m_bkl_id=$ans["m_bkl_id"];
			$this->m_ve_id=$ans["m_ve_id"];
			$this->m_pk_id=$ans["m_pk_id"];
			$this->m_pl_start_time=$ans["m_pl_start_time"];
			$this->m_pl_end_time=$ans["m_pl_end_time"];
			$this->m_id_appear=$ans["m_id_appear"];
		}
		else{
			//多個相同ID
			include_once ("/../template/email_template.php");
			EmailTemplate($conn, "易停網", "upk.rd00001@gmail.com", "易停網 多筆相同parking log ID", $m_pl_id);
			return json_encode(array("result"=>0,"有多個相同停車紀錄ID" => mysql_error($conn).$sql));
		}
    }

    public function GetBookingLog(){
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$language = "zh-tw";

		$sql="select m_bkl_id from tb_Member_ParkingSpace_Booking_Log where m_bkl_id = '".$this->m_bkl_id."' ";
		$result = mysql_query($sql, $conn);
		if(! $result){
			return (array("失敗" => mysql_error($conn).$sql));
		}
		else if(mysql_num_rows($result) >= 1){
			//記得改回==
			$ans=mysql_fetch_assoc($result);
				$this->booking_log=new UPK_BookingLog($ans["m_bkl_id"]);
			}
		else if(mysql_num_rows($result) != 0){
			//多個相同ID
			return (array("有多個回報紀錄" => mysql_error($conn).$sql));
		}
		return (array("result"=>1));
    }
    public function get_m_pl_sn(){
		return $this->m_pl_sn;
    }
    public function get_m_pl_id(){
		return $this->m_pl_id;
    }
    public function get_m_pyl_id(){
		return $this->m_pyl_id;
    }
    public function get_m_bkl_id(){
		return $this->m_bkl_id;
    }
    public function get_m_ve_id(){
		return $this->m_ve_id;
    }
    public function get_m_pk_id(){
		return $this->m_pk_id;
    }
    public function get_m_pl_start_time(){
		return $this->m_pl_start_time;
    }
    public function get_m_pl_end_time(){
		return $this->m_pl_end_time;
    }
    public function get_m_id_appear(){
		return $this->m_id_appear;
    }
	public function get_pricing_logs(){
		return $this->pricing_logs;
	}
	public function get_array($select=""){
		if($select=="")
			$select=$this->B_ALL;
    	$return_array=array();
		if(($select&$this->B_m_pl_sn)!=0){
			$return_array["m_pl_sn"]=$this->get_m_pl_sn();
		}
		if(($select&$this->B_m_pl_id)!=0){
			$return_array["m_pl_id"]=$this->get_m_pl_id();
		}
		if(($select&$this->B_m_pyl_id)!=0){
			$return_array["m_pyl_id"]=$this->get_m_pyl_id();
		}
		if(($select&$this->B_m_bkl_id)!=0){
			$return_array["m_bkl_id"]=$this->get_m_bkl_id();
		}
		if(($select&$this->B_m_ve_id)!=0){
			$return_array["m_ve_id"]=$this->get_m_ve_id();
		}
		if(($select&$this->B_m_pk_id)!=0){
			$return_array["m_pk_id"]=$this->get_m_pk_id();
		}
		if(($select&$this->B_m_pl_start_time)!=0){
			$return_array["m_pl_start_time"]=$this->get_m_pl_start_time();
		}
		if(($select&$this->B_m_pl_end_time)!=0){
			$return_array["m_pl_end_time"]=$this->get_m_pl_end_time();
		}
		if(($select&$this->B_m_id_appear)!=0){
			$return_array["m_id_appear"]=$this->get_m_id_appear();
		}
		return $return_array;
	}
}
?>
