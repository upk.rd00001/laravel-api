<?php 

class TimeComponent{
	private $start_date;
	private $end_date;
	private $start_time;
	private $end_time;
	private $DT_start_datetime;
	private $DT_end_datetime;
	private $disp_start_datetime;
	private $disp_end_datetime;
	
	private $start_date_field_name;
	private $end_date_field_name;
	private $start_time_field_name;
	private $end_time_field_name;
	
	public function get_DT_start_datetime(){
		return $this->DT_start_datetime;
	}
	public function get_DT_end_datetime(){
		return $this->DT_end_datetime;
	}
	public function __construct($st_date,$ed_date,$st_time,$ed_time) {
		if($st_date==null||$ed_date==null||$st_time==null||$ed_time==null){
			return;
		}
		$this->DataPrepare($st_date,$ed_date,$st_time,$ed_time);
    }
	public function sql_field_name_init($st_date_field_name,$ed_date_field_name,$st_time_field_name,$ed_time_field_name) {

		$this->start_date_field_name=$st_date_field_name;
		$this->end_date_field_name=$ed_date_field_name;
		$this->start_time_field_name=$st_time_field_name;
		$this->end_time_field_name=$ed_time_field_name;
    }
    public function TimeComparison_datetime($st_datetime,$ed_datetime,$mode="INSIDE"){
		if($st_datetime==null||$ed_datetime==null){
			return;
		}
		$st_datetime_array=explode(" ",$st_datetime);
		$st_date=$st_datetime_array[0];
		$st_time=$st_datetime_array[1];
		$ed_datetime_array=explode(" ",$ed_datetime);
		$ed_date=$ed_datetime_array[0];
		$ed_time=$ed_datetime_array[1];
		return $this->TimeComparison($st_date,$ed_date,$st_time,$ed_time,$mode);
	}
	public function TimeComparison($st_date,$ed_date,$st_time,$ed_time,$mode="INSIDE"){
		
		if($st_date==null||$ed_date==null||$st_time==null||$ed_time==null){
			return;
		}
		//x,y means    input    start,end
		//A,B means databases's start,end
		// x<y and A<B
		$input_time = new TimeComponent($st_date,$ed_date,$st_time,$ed_time);
		switch (strtolower($mode)){
			case "inside"://xy is in AB 					---A------x----y-----------B---->(時間軸)
				//A<=x<=B AND A<=y<=B
				if($this->DT_start_datetime <= $input_time->DT_start_datetime && $input_time->DT_start_datetime <= $this->DT_end_datetime &&
				   $this->DT_start_datetime <= $input_time->DT_end_datetime && $input_time->DT_end_datetime <= $this->DT_end_datetime)
				   return true;
				else return false;
			break;
			case "outside"://x and y is out of AB 			--x--y------A-------B----x---y-->(時間軸)
				//y<=A or B<=x
				if($input_time->DT_end_datetime <= $this->DT_start_datetime || $this->DT_end_datetime <= $input_time->DT_start_datetime)
				   return true;
				else return false;
			break;
			case "cross"://x or y is in AB					------x------A------y----B------>(時間軸)
				//A<=x<=B xor A<=y<=B
				if($this->DT_start_datetime <= $input_time->DT_start_datetime && $input_time->DT_start_datetime <= $this->DT_end_datetime ^
				   $this->DT_start_datetime <= $input_time->DT_end_datetime && $input_time->DT_end_datetime <= $this->DT_end_datetime)
				   return true;
				else return false;
			break;
			case "include"://AB is in xy					---x-------A-------B-----y------>(時間軸)
				//x<=A<=y AND x<=B<=y
				if($input_time->DT_start_datetime <= $this->DT_start_datetime && $this->DT_start_datetime <= $input_time->DT_end_datetime &&
				   $input_time->DT_start_datetime <= $this->DT_end_datetime && $this->DT_end_datetime <= $input_time->DT_end_datetime)
				   return true;
				else return false;
			break;
		}
	}
    public function SqlTimeComparison_datetime($st_datetime,$ed_datetime,$mode="INSIDE"){
		if($st_datetime==null||$ed_datetime==null){
			return;
		}
		$st_datetime_array=explode(" ",$st_datetime);
		$st_date=$st_datetime_array[0];
		$st_time=$st_datetime_array[1];
		$ed_datetime_array=explode(" ",$ed_datetime);
		$ed_date=$ed_datetime_array[0];
		$ed_time=$ed_datetime_array[1];
		return $this->SqlTimeComparison($st_date,$ed_date,$st_time,$ed_time,$mode);
	}
	public function SqlTimeComparison($st_date,$ed_date,$st_time,$ed_time,$mode="INSIDE"){
		
		if($st_date==null||$ed_date==null||$st_time==null||$ed_time==null){
			return;
		}
		//x,y means    input    start,end
		//A,B means databases's start,end
		// x<y and A<B
		$input_time = new TimeComponent($st_date,$ed_date,$st_time,$ed_time);
		$field_start_datetime=" CONCAT(".$this->start_date_field_name.",' ',".$this->start_time_field_name.") ";
		$field_end_datetime=" CONCAT(".$this->end_date_field_name.",' ',".$this->end_time_field_name.") ";
		$input_start_datetime=" CONCAT('".$input_time->start_date."',' ','".$input_time->start_time."') ";
		$input_end_datetime=" CONCAT('".$input_time->end_date."',' ','".$input_time->end_time."') ";
		switch (strtolower($mode)){
			case "inside"://xy is in AB 					---A------x----y-----------B---->(時間軸)
				//A<=x<=B AND A<=y<=B
				$rtn_sql="(".$field_start_datetime." <= ".$input_start_datetime." AND ".$input_start_datetime." <= ".$field_end_datetime;
				$rtn_sql.=" AND ".$field_start_datetime." <= ".$input_end_datetime." AND ".$input_end_datetime." <= ".$field_end_datetime.")";
				return $rtn_sql;
			break;
			case "outside"://x and y is out of AB 			--x--y------A-------B----x---y-->(時間軸)
				//y<=A or B<=x
				$rtn_sql="(".$input_end_datetime." <= ".$field_start_datetime." OR ".$field_end_datetime." <= ".$input_start_datetime.")";
				return $rtn_sql;
			break;
			case "cross"://x or y is in AB					------x------A------y----B------>(時間軸)
				//A<=x<=B xor A<=y<=B
				$rtn_sql="((".$field_start_datetime." <= ".$input_start_datetime." AND ".$input_start_datetime." <= ".$field_end_datetime.")";
				$rtn_sql.=" XOR (".$field_start_datetime." <= ".$input_end_datetime." AND ".$input_end_datetime." <= ".$field_end_datetime."))";
				return $rtn_sql;
			break;
			case "include"://AB is in xy					---x-------A-------B-----y------>(時間軸)
				//x<=A<=y AND x<=B<=y
				$rtn_sql="(".$input_start_datetime." <= ".$field_start_datetime." AND ".$field_start_datetime." <= ".$input_end_datetime;
				$rtn_sql.=" AND ".$input_start_datetime." <= ".$field_end_datetime." AND ".$field_end_datetime." <= ".$input_end_datetime.")";
				return $rtn_sql;
			break;
		}
	}

	public function DataPrepare($st_date,$ed_date,$st_time,$ed_time){
		
		$this->start_date=$st_date;
		$this->end_date=$ed_date;
		$this->start_time=$st_time;
		$this->end_time=$ed_time;
		
		$this->DT_start_datetime = new DateTime($this->start_date." ".$this->start_time);
		$this->DT_end_datetime = new DateTime($this->end_date." ".$this->end_time);

		$this->disp_start_datetime = $this->DT_start_datetime->format('Y-m-d H:i:s');
		$this->disp_end_datetime = $this->DT_end_datetime->format('Y-m-d H:i:s');
		/*
		//判斷用 2017/02/22 Tiger: 時間函數使用DateTime
		$start_time_tmp = date_create($this->start_date." ".$this->start_time);
		$end_time_tmp = date_create($this->end_date." ".$this->end_time);
		$this->DT_start_datetime = strtotime($start_time_tmp->format("Y-m-d H:i:s"));
		$this->DT_end_datetime = strtotime($end_time_tmp->format("Y-m-d H:i:s"));*/
	}
	public function __toString(){
		return $this->start_date.$this->end_date. $this->start_time.$this->end_time;
	}
}

?>