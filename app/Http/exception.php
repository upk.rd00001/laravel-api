<?php
// error handler function
function myErrorHandler($errno, $errstr, $errfile, $errline)
{
	$pure_data = file_get_contents('php://input');
	$errstr=mb_convert_encoding($errstr, "UTF-8", "auto");
	if (!(error_reporting() & $errno)) {
	    // This error code is not included in error_reporting, so let it fall
	    // through to the standard PHP error handler
	    return false;
	}
    $email_body="";
    $echo_text="";
    switch ($errno) {
    case E_USER_ERROR:
        $email_body.= "<b>ERROR</b> [$errno] $errstr<br />\n";
        $email_body.= "  Fatal error on line $errline in file $errfile";
        $email_body.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        $echo_text = json_encode(array("result"=>0,"title"=>"Error","description"=>"[".$errno."]".$errstr." #".$errline));
        break;

    case E_ERROR:
        $email_body.= "<b>ERROR</b> [$errno] $errstr<br />\n";
        $email_body.= "  Fatal error on line $errline in file $errfile";
        $email_body.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        $echo_text = json_encode(array("result"=>0,"title"=>"Error","description"=>"[".$errno."]".$errstr." #".$errline));
        break;

    case E_USER_WARNING:
        $email_body.= "<b>WARNING</b> [$errno] $errstr<br />\n";
        $email_body.= "  Fatal error on line $errline in file $errfile";
        $email_body.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        $echo_text = json_encode(array("result"=>0,"title"=>"Warning","description"=>"[".$errno."]".$errstr." #".$errline));
        break;

    case E_WARNING:
        $email_body.= "<b>WARNING</b> [$errno] $errstr<br />\n";
        $email_body.= "  Fatal error on line $errline in file $errfile";
        $email_body.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        $echo_text = json_encode(array("result"=>0,"title"=>"Warning","description"=>"[".$errno."]".$errstr." #".$errline));
        break;

    case E_USER_NOTICE:
        $email_body.= "<b>USER_NOTICE</b> [$errno] $errstr<br />\n";
        $email_body.= "  Fatal error on line $errline in file $errfile";
        $email_body.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        $echo_text = json_encode(array("result"=>0,"title"=>"Notice","description"=>"[".$errno."]".$errstr." #".$errline));
        break;

    case E_NOTICE:
		$email_body.= "<b>NOTICE</b> [$errno] $errstr<br />\n";
		$email_body.= "  Run-time notices on line $errline in file $errfile";
		$email_body.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
		$email_body.= "Indicate that the script encountered something that could indicate an error, but could also happen in the normal course of running a script.<br />";
		$echo_text = json_encode(array("result"=>0,"title"=>"Notice","description"=>"[".$errno."]".$errstr." #".$errline));
		break;

    default:
        $email_body.= "Unknown error type: [$errno] $errstr<br />\n";
        $email_body.= "  Fatal error on line $errline in file $errfile";
        $email_body.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ") " . PHP_URL_PATH . "<br />\n";
        $echo_text = json_encode(array("result"=>0,"title"=>"Unknown","description"=>"[".$errno."]".$errstr." #".$errline));
        break;
    }

	include_once("/template/email_template.php");

	include_once("/fn_package.php");
	if(IsDebug())
		$email_title="測試板信息: api exception";
	elseif(IsPreview())
		$email_title="預覽板信息: api exception";
	else
		$email_title="api exception";
	if(!IsDebug())
		$tans=EmailTemplate("","易停網營運團隊","luu0930@gmail.com",$email_title,$email_body."<br>".$pure_data);

	$tans=EmailTemplate("","易停網營運團隊","upk.rd00001@gmail.com",$email_title,$email_body."<br>".$pure_data);
	
    /* Don't execute PHP internal error handler */
	if($errno==E_NOTICE){
		//不重要繼續做
		if (isTextContain($errstr, "MemcachePool")) {
			//do nothing
		}
		else{
			echo $echo_text;
			exit(1);
		}
	}
	elseif($errno==E_NOTICE){
		echo $echo_text;
		exit(1);
	}
	elseif($errno==E_WARNING){
		if(isTextContain($errstr,"SoapClient")){
			//do nothing
		}
		else{
			echo $echo_text;
			exit(1);
		}
	}
	else {
		echo $echo_text;
    		exit(1);
	}
    return true;
}
// set to the user defined error handler
$old_error_handler = set_error_handler("myErrorHandler");

