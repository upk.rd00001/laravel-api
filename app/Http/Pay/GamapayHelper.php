<?php
namespace App\Http\Pay;

use DateTime;
use UPK_MemberDeposit;

class GamapayHelper
{
	function GetTransOrderFunc($activity, $token, $m_dpt_id, $pure_data)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";

		if ($activity == null || $token == null) {
			rg_activity_log($conn, "", "橘子支查詢交易訂單失敗", "必填欄位未填", $pure_data, "");
			$ans = GetSystemCode("5030000", $language, $conn);
			return json_encode(["result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]]);
		}
		if ($activity != "GET TRANS ORDER") {
			rg_activity_log($conn, "", "橘子支查詢交易訂單失敗", "activity錯誤", $pure_data, "");
			$ans = GetSystemCode("5030001", $language, $conn);
			return json_encode(["result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]]);
		}
		$sql = "SELECT m_id, m_language FROM tb_Member WHERE m_token='".$token."'";
		$result = mysql_query($sql, $conn);
		if (! $result) {
			return json_encode(["result" => 0, "title" => "取得會員資料失敗", "description" => mysql_error()]);
		}
		if (mysql_num_rows($result) == 0) {    #token失效
			rg_activity_log($conn, "", "取得會員資料失敗", "token失效", $pure_data, "");
			$ans = GetSystemCode("9", $language, $conn);
			return json_encode(["result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]]);
		}
		$ans = mysql_fetch_assoc($result);
		$id = $ans["m_id"];
		$language = $ans["m_language"];

		$this_member_deposit = new UPK_MemberDeposit($m_dpt_id);
		$G_GAMAPAY_TRANS_ORDER_URL = GetSystemParameter($conn, "gamapay_transorder_url");

		$G_GAMAPAY_HASHKEY = GetSystemParameter($conn, "gamapay_hashkey");
		$G_GAMAPAY_HASHIV = GetSystemParameter($conn, "gamapay_hashiv");
		$G_GAMAPAY_MERCHANTID = GetSystemParameter($conn, "gamapay_merchant_id");
		//$G_GAMAPAY_MERCHANT_ACCOUNT = GetSystemParameter($conn, "gamapay_merchant_account");

		//$GamaPayID="space4car";
		//$MerchantID="1906";
		//$MerchantIV="dnl9STaweYT69JfSGRV+Lw==";

		$MerchantID = $G_GAMAPAY_MERCHANTID;
		$MerchantIV = $G_GAMAPAY_HASHIV;
		$MerchantOrderID = $m_dpt_id;
		$TransactionID = $this_member_deposit->get_m_transaction_id();
		//MAC DATA = MerchantID + SubMerchantID + MerchantOrderID + CurrencyCode +
		//TransAmount(14,2) + MerchantIV(商家 IV 金鑰)
		$SubMerchantID = "";
		$CurrencyCode = "TWD";

		$TransAmount = (string) $this_member_deposit->get_m_dpt_amount();
		Sn2Id("00", $TransAmount);        //00000000000006
		$TransAmount = $TransAmount."00";    //0000000000000600
		//交易金額 TransAmount: 12 碼整數 + 2 碼小數，不含小數點(ex:50 AMOUNT= 00000000005000

		//Tiger 這邊的TransAmount沒有小數點的
		$tmpTransAmount = (string) $this_member_deposit->get_m_dpt_amount();
		Sn2Id("0000", $tmpTransAmount);        //00000000000006
		
		$TransType = 10; //SaleOrder 10一般訂單交易(收款交易)
		$MAC = gamapay_mac($MerchantID.$SubMerchantID.$MerchantOrderID.$CurrencyCode.$TransAmount.$MerchantIV, $G_GAMAPAY_HASHKEY, $MerchantIV);
		$array = [];
		$array["MerchantID"] = $MerchantID;//Y 商家帳號代碼
		$array["SubMerchantID"] = $SubMerchantID;//N 商家子帳號代碼
		$array["MerchantOrderID"] = $MerchantOrderID;// Y 商家訂單編號 原始商家訂單編號
		$array["TransactionID"] = $TransactionID;// String(20) Y GAMA PAY 交易單號
		$array["TransType"] = $TransType;//Int Y 交易類別
		$array["TransAmount"] = $tmpTransAmount;//Number(14, 2) Y 交易金額
		$array["CurrencyCode"] = $CurrencyCode;//String(3) Y 幣別代碼
		$array["MAC"] = $MAC;
		$tans = json_decode(PostJsonTo($G_GAMAPAY_TRANS_ORDER_URL, $array), true);

		if ($tans["ResultCode"] == "0000" || $tans["ResultCode"] == "0") {

			//if($tans["MAC"]==gamapay_mac($tans["GamaPayAccount"].$MerchantIV,$G_GAMAPAY_HASHKEY,$MerchantIV)){//驗證MAC
			return json_encode(["result" => 1, "data" => $tans]);
		}
		else {
			rg_activity_log($conn, $id, "橘子支查詢交易訂單失敗", $tans["ResultMessage"], $pure_data, "");
			return json_encode([
				"result" => 0,
				"title" => "橘子支查詢交易訂單失敗",
				"description" => $tans["ResultMessage"],
				"ResultCode" => $tans["ResultCode"],
				"url" => $G_GAMAPAY_TRANS_ORDER_URL,
				"array" => $array,
				"debug" => $tans
			]);
		}
	}

	function OrderCreateFunc(
		$activity,
		$token,
		$pure_data,
		$purchase_list,
		$booking_id,
		$type,
		$mode,
		$isup = "",
		$level = "",
		$searchkey = "",
		$identity = "",
		$is_epm_account = 0
	) {
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$language = "zh-tw";

		if ($activity == null || $token == null || $purchase_list == null) {
			rg_activity_log($conn, "", "橘子支建立交易訂單失敗", "必填欄位未填", $pure_data, "");
			$ans = GetSystemCode("5030000", $language, $conn);
			return json_encode([
				"result" => 0,
				"systemCode" => $ans[0],
				"title" => $ans[1],
				"description" => $ans[2]
			]);
		}
		else {
			if ($activity != "ORDER CREATE") {
				rg_activity_log($conn, "", "橘子支建立交易訂單失敗", "activity錯誤", $pure_data, "");
				$ans = GetSystemCode("5030001", $language, $conn);
				return json_encode([
					"result" => 0,
					"systemCode" => $ans[0],
					"title" => $ans[1],
					"description" => $ans[2]
				]);
			}
		}
		$sql = "SELECT m_id, m_language, m_gamapay_id, m_cellphone FROM tb_Member WHERE m_token='".$token."'";
		$result = mysql_query($sql, $conn);
		if (! $result) {
			return json_encode(["result" => 0, "title" => "取得會員資料失敗", "description" => mysql_error()]);
		}
		else {
			if (mysql_num_rows($result) == 0) {    #token失效
				rg_activity_log($conn, "", "取得會員資料失敗", "token失效", $pure_data, "");
				$ans = GetSystemCode("9", $language, $conn);
				return json_encode([
					"result" => 0,
					"systemCode" => $ans[0],
					"title" => $ans[1],
					"description" => $ans[2]
				]);
			}
		}
		$ans = mysql_fetch_assoc($result);
		$id = $ans["m_id"];
		$language = $ans["m_language"];
		$cellphone = $ans["m_cellphone"];
		$m_gamapay_id = $ans["m_gamapay_id"];
		$TradingContent = "";
		$total_p = 0.0;
		foreach ($purchase_list as $element) {
			$total_p = (double) $total_p + ((double) ($element->{'Quantity'}) * (double) ($element->{'Price'}));
			$TradingContent .= $element->{'Name'}."x".$element->{'Quantity'}." ".$element->{'Price'}.$element->{'Currency'}.";";
		}

		if ($m_gamapay_id == "") {
			rg_activity_log($conn, $id, "橘子支建立交易訂單失敗", "尚未綁定帳號", $pure_data, "");
			$ans = GetSystemCode("5030002", $language, $conn);
			return json_encode([
				"result" => 0,
				"systemCode" => $ans[0],
				"title" => $ans[1],
				"description" => $ans[2]
			]);
		}

		$G_GAMAPAY_ORDER_CREATE_URL = GetSystemParameter($conn, "gamapay_ordercreate_url");

		#2017-12-22 Tiger: 目前看來似乎不需要這些東西驗證只需要URL跟VerifyCode
		if ($is_epm_account == 1) {
			$G_GAMAPAY_HASHKEY = GetSystemParameter($conn, "epm_gamapay_hashkey");
			$G_GAMAPAY_HASHIV = GetSystemParameter($conn, "epm_gamapay_hashiv");
			$G_GAMAPAY_MERCHANTID = GetSystemParameter($conn, "epm_gamapay_merchant_id");
			//$G_GAMAPAY_MERCHANT_ACCOUNT = GetSystemParameter($conn, "gamapay_merchant_account");
		}
		else {
			$G_GAMAPAY_HASHKEY = GetSystemParameter($conn, "gamapay_hashkey");
			$G_GAMAPAY_HASHIV = GetSystemParameter($conn, "gamapay_hashiv");
			$G_GAMAPAY_MERCHANTID = GetSystemParameter($conn, "gamapay_merchant_id");
			//$G_GAMAPAY_MERCHANT_ACCOUNT = GetSystemParameter($conn, "gamapay_merchant_account");
		}
		//$GamaPayID="space4car";
		//$MerchantID="1906";
		//$MerchantIV="dnl9STaweYT69JfSGRV+Lw==";
		$date = new DateTime('now');
		$MerchantID = $G_GAMAPAY_MERCHANTID;
		$MerchantIV = $G_GAMAPAY_HASHIV;
		$MerchantAccount = $id;
		$tmp_id = "DPTD".$date->format('YmdHis');
		$sec = microtime();
		$new_id = $tmp_id.$sec[2].$sec[3];//毫秒 $sec[0]=0 $sec[1]=.  $sec[2..3]為毫秒後兩位數  正常顯示為 0.xx (秒)
		//MAC DATA = MerchantID + SubMerchantID + MerchantOrderID + CurrencyCode + TransAmount(14,2) + MerchantIV(商家 IV 金鑰)
		$SubMerchantID = "";
		$MerchantOrderID = "";
		$CurrencyCode = "TWD";
		$TransAmount = $total_p; //交易金額
		$TransType = "10"; //SaleOrder 10一般訂單交易(收款交易)
		include_once "InsertDepositFunc.php";
		$ttans = json_decode(InsertDepositFunc("INSERT DEPOSIT", $token, $total_p, $booking_id, $type, $mode, $pure_data, $isup, $level, $searchkey, $identity), true);
		if ($ttans["result"] == 1) {
			$MerchantOrderID = $ttans["new_id"];
		}
		else {
			return json_encode($ttans);
		}
		$array = [];
		$array["MerchantID"] = $MerchantID;//Y 商家帳號代碼
		$array["SubMerchantID"] = $SubMerchantID;//N 商家子帳號代碼
		$array["MerchantOrderID"] = $MerchantOrderID;// Y 商家訂單編號 原始商家訂單編號
		$array["TransType"] = (int) $TransType;//Y 交易類別 請參閱「附件1」
		$array["TradingContent"] = "易停網停車媒合服務費";//Y 交易內容 多筆商品請以 ; 分隔
		$array["TradingToken"] = "";//N 會員交易金鑰 被掃/待付款訂單需填寫
		$array["Memo"] = "";//N 備註 交易備註
		$array["TransAmount"] = $TransAmount;//Y 交易金額
		$array["ExcludeAmount"] = "0";//N 不適用促銷活動金額 菸酒類、無法折扣商品金額
		$array["ExcludeItem"] = "";//N 不適用促銷活動商品
		$array["CurrencyCode"] = $CurrencyCode;//Y 幣別
		$array["BusinessDate"] = $date->format('Ymd');//Y 商家營業日 yyyyMMdd
		$array["Remark1"] = "";//N 商家保留欄位 1 提供分類統計或說明之用
		$array["Remark2"] = "";//N 商家保留欄位 2 提供分類統計或說明之用
		$array["InboundAuthToken"] = "";//N 境外支付授權碼 TransType=60 時需填寫
		$array["ExpiredDateTime"] = "";//N 交易有效日期， 格式 : yyyyMMddHHmmss

		Sn2Id("00", $TransAmount);        //00000000000006
		$TransAmount = $TransAmount."00";    //0000000000000600
		$MAC = gamapay_mac($MerchantID.$SubMerchantID.$MerchantOrderID.$CurrencyCode.$TransAmount.$MerchantIV, $G_GAMAPAY_HASHKEY, $MerchantIV);
		$array["MAC"] = $MAC;
		$tans = json_decode(PostJsonTo($G_GAMAPAY_ORDER_CREATE_URL, $array), true);

		if ($tans["ResultCode"] == "0000" || $tans["ResultCode"] == "0") {

			//if($tans["MAC"]==gamapay_mac($tans["GamaPayAccount"].$MerchantIV,$G_GAMAPAY_HASHKEY,$MerchantIV)){//驗證MAC
			return json_encode(["result" => 1, "data" => $tans, "array" => $array]);
		}
		else {
			rg_activity_log($conn, $id, "橘子支建立交易訂單失敗", $tans["ResultMessage"], $pure_data, "");
			return json_encode([
				"result" => 0,
				"title" => "橘子支建立交易訂單失敗",
				"description" => $tans["ResultMessage"],
				"ResultCode" => $tans["ResultCode"],
				"array" => $array
			]);
		}
	}
}

?>