<?php
/**
 * 補強如果應該有停車紀錄的訂單要補強資料，避免資料不完整
 *
 * @param $conn
 * @param $booking_id
 * @param null $this_parking_lot
 * @return false|string
 */
function AutoInsertParkingLogFunc($conn,$booking_id,$this_parking_lot=null)
{
	$pure_data = "";
	$sql = "SELECT * FROM tb_Member_ParkingSpace_Booking_Log WHERE m_bkl_id='" . $booking_id . "' ";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		//echo json_encode(array("result"=>0, "title"=>"搜尋預約失敗", "description"=> mysql_error($conn)));
	}
	else if (mysql_num_rows($result) >= 1) {
		$ans = mysql_fetch_assoc($result);
		$tmp_m_pl_id = 'tmp' . GenerateRandomString(11, '0123456789');
		$m_bkl_book_type = $ans["m_bkl_book_type"];
		$m_bkl_group_id = $ans["m_bkl_group_id"];
		$pk_id = $ans["m_pk_id"];
		$ve_id = $ans["m_ve_id"];
		$m_bkl_start_date = $ans["m_bkl_start_date"];
		$m_bkl_start_time = $ans["m_bkl_start_time"];
		$m_bkl_end_date = $ans["m_bkl_end_date"];
		$m_bkl_end_time = $ans["m_bkl_end_time"];
		$m_plots_type = $ans["m_bkl_plots_type"];

		if ($m_plots_type == "300") {

			$sql = "SELECT * FROM tb_Member_Deposit WHERE m_bkl_id='" . $booking_id . "' AND m_dpt_pay_datetime IS NOT NULL";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result"=>0, "title"=>"搜尋預約失敗", "description"=> ""));
			}
			elseif(mysql_num_rows($result)==0){
				//沒有付款紀錄就不要補自動進出場
				return json_encode(array("result"=>0, "title"=>"無付款不需補進出場", "description"=> ""));
			}
		}
		//自動加簽不再進場
		//除了預約外，沒有進場紀錄的都要給進場紀錄
		if (($m_plots_type == "100" || $m_plots_type == "300" || $m_plots_type == "301" || $m_plots_type == "302" || $m_plots_type == "303") && $m_bkl_group_id == "") {
			$sql = "SELECT m_pl_id,m_pl_end_time FROM tb_Member_Parking_Log WHERE m_bkl_id='" . $booking_id . "' ";
			$result_select = mysql_query($sql, $conn);
			if (!$result_select) {
				if ($m_plots_type == "300") {
					rg_activity_log($conn, $booking_id, "AutoInsertParkingLogFunc", "找停車紀錄錯誤", $pure_data, $sql);
				}
			}
			else if (mysql_num_rows($result_select) == 0) {
				#自動入場
				$sql = "INSERT INTO tb_Member_Parking_Log (m_pl_id, m_bkl_id, m_pk_id, m_ve_id, m_pl_start_time) VALUES ('" . $tmp_m_pl_id . "', '" . $booking_id . "', '" . $pk_id . "', '" . $ve_id . "', '" . $m_bkl_start_date . " " . $m_bkl_start_time . "')";
				if (!mysql_query($sql, $conn)) {
					if ($m_plots_type == "300") {
						rg_activity_log($conn, $booking_id, "AutoInsertParkingLogFunc", "插入停車紀錄錯誤", $pure_data, $sql);
					}
				}
				else {
					$sql = "SELECT m_pl_sn FROM tb_Member_Parking_Log WHERE m_pl_id='" . $tmp_m_pl_id . "' and "
						. "m_bkl_id='" . $booking_id . "' and "
						. "m_pk_id='" . $pk_id . "' ";
					$ans = mysql_fetch_assoc(mysql_query($sql, $conn));

					$new_id = $ans["m_pl_sn"];
					Sn2Id("PLID", $new_id);
					$pl_end_time_sql = "";
					if ($m_plots_type == "300")
						$pl_end_time_sql = ", m_pl_end_time='" . $m_bkl_end_date . " " . $m_bkl_end_time . "' ";
					$sql = "UPDATE tb_Member_Parking_Log SET m_pl_id='" . $new_id . "'" . $pl_end_time_sql . " WHERE m_pl_sn='" . $ans["m_pl_sn"] . "' ";
					if (!mysql_query($sql, $conn)) {
						return json_encode(array("result" => 0, "title" => "update faild"));
					}
					MemcacheSetBookingLog('_UPK_BookingLog:', $booking_id);
					return json_encode(array("result" => 1));
					//	if($m_plots_type=="300")
					//		rg_activity_log($conn, $booking_id, "AutoInsertParkingLogFunc", "成功", $pure_data, $sql);
				}
			}
			else {
				if ($m_plots_type == "300") {
			//		rg_activity_log($conn, $booking_id, "AutoInsertParkingLogFunc", "不可能已入場阿", $pure_data, $sql);
				}
				return json_encode(array("result"=>0, "title"=>"已有進出紀錄", "description"=> ""));
			}
		}
	}
	else{
		return json_encode(array("result"=>0, "title"=>"搜尋預約失敗", "description"=> ""));
	}
}
?>