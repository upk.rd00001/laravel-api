<?php
function InsertDepositFunc($activity,$token,$total_p,$booking_id,$type,$mode,$pure_data,$isup="",$level="",$searchkey="",$identity="",$invoice_arg=array(),$args=array())
{
	$m_as_id="";
	if(isset($args["m_as_id"]))
		$m_as_id=$args["m_as_id"];
	$m_aspd_remark="";
	if(isset($args["m_aspd_remark"]))
		$m_aspd_remark=$args["m_aspd_remark"];
	$customer_invoice_arg="";
	if(isset($args["customer_invoice_arg"]))
		$customer_invoice_arg=$args["customer_invoice_arg"];
	global $conn, $dbName;
	check_conn($conn, $dbName);
	$language = "zh-tw";
	$sql = "SELECT m_id, m_own_point, m_language FROM tb_Member WHERE m_token='" . $token . "'";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return json_encode(array("搜尋token失敗" => mysql_error($conn)));
	}
	else if (mysql_num_rows($result) == 0) {
		#token失效
		rg_activity_log($conn, "", "信用卡新增訂單失敗", "token失效", $pure_data, "");
		$ans = GetSystemCode("9", $language, $conn);
		return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2], "debug" => "86"));
	}
	$ans = mysql_fetch_assoc($result);
	$id = $ans["m_id"];
	$language = $ans["m_language"];
	//2016-12-16 DPTD修改為 DPTD年月日時分秒
	#為了取得 DPT ID先加入資料庫
	//$tmp_id = "temp".GenerateRandomString(10, '1234567890');
	$date = new DateTime();
	$transaction_id = "temp" . GenerateRandomString(26, '1234567890abcdefghijklmnopqrstuvwxyz');
	if ($type == 1)
		$m_dpt_trade_platform = 2;//O'pay
	elseif ($type == 2)
		$m_dpt_trade_platform = 3;//綠界
	elseif ($type == 3)
		$m_dpt_trade_platform = 4;//gamapay
	elseif ($type == 4)
		$m_dpt_trade_platform = 5;//Line Pay
	elseif ($type == 5)
		$m_dpt_trade_platform = 6;//Google Pay
	elseif ($type == 6)
		$m_dpt_trade_platform = 7;//APPLE_PAY
	elseif ($type == 7)
		$m_dpt_trade_platform = 103;//綠界
	elseif ($type == 8)
		$m_dpt_trade_platform = 102;//O'pay
	elseif ($type == 9)
		$m_dpt_trade_platform = 104;//O'pay
	elseif ($type == 10)
		$m_dpt_trade_platform = 10;//directpay
	elseif ($type == 11)
		$m_dpt_trade_platform = 11;//directpaybycard
	elseif ($type == 13)
		$m_dpt_trade_platform = 13;//jkopay
	elseif ($type == 15)
		$m_dpt_trade_platform = 15;//barcode
	elseif ($type == CONST_GENERATE_CREDIT_ORDER_TYPE::EASY_WALLET)
		$m_dpt_trade_platform = CONST_M_DPT_TRADE_PLATFORM::EASY_WALLET;//barcode
	else
		$m_dpt_trade_platform = 0;

	$tb_name = "tb_Member_Deposit";
	if ($type == 7 || $type == 8 || $type == 9) {
		$tb_name = "tb_EPM_Deposit";
	}
	if ($mode == CONST_GENERATE_CREDIT_ORDER_MODE::PARKING_PAY) {
		#付款可能要再算一次該付多少，點數夠不夠，之類的再判斷一次

		$tmp_id = "DPTD" . $date->format('YmdHis');
		$sec = microtime();
		$new_id = $tmp_id . $sec[2] . $sec[3];//毫秒 $sec[0]=0 $sec[1]=.  $sec[2..3]為毫秒後兩位數  正常顯示為 0.xx (秒)

		$sql = "INSERT INTO " . $tb_name . " (m_dpt_id, m_id, m_bkl_id, m_dpt_create_datetime, m_dpt_amount, m_dpt_point, 
				m_dpt_method, m_dpt_handling_charge, m_dpt_error_reason, m_dpt_request_log, m_transaction_id, 
				m_dpt_origin_point, m_dpt_update_point,m_dpt_trade_platform,invoice_arg) "
			. "VALUES ('" . $new_id . "', '" . $id . "','" . $booking_id . "', now(), " . $total_p . ", " . $total_p . ", 1, 0, '尚未繳費', '" . $pure_data . "', '" . $transaction_id . "', " . $ans["m_own_point"] . ", " . $ans["m_own_point"] . ", " . $m_dpt_trade_platform . ", '" . mysql_escape_string(json_encode($invoice_arg)) . "')";
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "更新錯誤", "description" => mysql_error(), "debug" => $sql));
		}
		if ($type == 7 || $type == 8 || $type == 9) {
		}
		else {
			$new_id = update_dpt_id($new_id, $booking_id);
		}
	}
	else if ($mode == CONST_GENERATE_CREDIT_ORDER_MODE::UPGRADE_MEMBER_LEVEL) {
		//會員升等(現在沒有用)
		$tmp_id = "MLVD" . $date->format('YmdHis');
		$sec = microtime();
		$new_id = $tmp_id . $sec[2] . $sec[3];//毫秒 $sec[0]=0 $sec[1]=.  $sec[2..3]為毫秒後兩位數  正常顯示為 0.xx (秒)
		$sql = "INSERT INTO tb_Member_Level_Paid (m_lvl_id, m_id, m_lvl_create_datetime, m_lvl_amount, 
				m_lvl_method, m_lvl_handling_charge, m_lvl_error_reason, m_lvl_request_log, m_lvl_transaction_id, m_level,m_lvl_identity, m_lvl_isup, m_lvl_search_key) "
			. "VALUES ('" . $new_id . "', '" . $id . "', now(), " . $total_p . ", 1, 0, '尚未繳費', '" . $pure_data . "', '" . $transaction_id . "','" . $level . "','" . $identity . "','" . $isup . "','" . $searchkey . "')";
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "更新錯誤", "description" => mysql_error(), "debug" => $sql));
		}
	}
	else if ($mode == CONST_GENERATE_CREDIT_ORDER_MODE::MEMBER_PAID) {
		//購買停車點數
		$tmp_id = "MBPD" . $date->format('YmdHis');
		$sec = microtime();
		$new_id = $tmp_id . $sec[2] . $sec[3];//毫秒 $sec[0]=0 $sec[1]=.  $sec[2..3]為毫秒後兩位數  正常顯示為 0.xx (秒)
		$earn_parking_point = 0;
		$DT_m_pd_limit_datetime = new DateTime('now');
		if ($total_p == 300) {
			$earn_parking_point = 300;
			$DT_m_pd_limit_datetime->modify('+360 day');
		}
		elseif ($total_p == 1000) {
			$earn_parking_point = 1000;
			$DT_m_pd_limit_datetime->modify('+360 day');
		}
		else {
			$earn_parking_point = $total_p;
			$DT_m_pd_limit_datetime->modify('+360 day');
		}

		$DT_m_pd_limit_datetime = new DateTime('2199-12-31 23:59:59');//2019-01-15 改成無限期
		$sql = "INSERT INTO tb_Member_Paid (m_pd_id, m_id, m_pd_create_datetime, m_pd_monthlyfee, 
				m_pd_method, m_pd_error_reason, m_pd_request_log, m_pd_transaction_id, m_pd_limit_datetime, m_pd_origin_point) "
			. "VALUES ('" . $new_id . "', '" . $id . "', now(), " . $total_p . ", 1, '尚未繳費', '" . $pure_data . "', '" . $transaction_id . "', '" . $DT_m_pd_limit_datetime->format("Y-m-d H:i:s") . "','" . $earn_parking_point . "')";
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "更新錯誤", "description" => mysql_error(), "debug" => $sql));
		}
	}
	//mode=4 捐贈
	elseif ($mode == CONST_GENERATE_CREDIT_ORDER_MODE::DONATE) {
		$tmp_id = "DNTD" . $date->format('YmdHis');
		$sec = microtime();
		$new_id = $tmp_id . $sec[2] . $sec[3];//毫秒 $sec[0]=0 $sec[1]=.  $sec[2..3]為毫秒後兩位數  正常顯示為 0.xx (秒)

		$DT_m_dnt_limit_datetime = new DateTime('2199-12-31 23:59:59');//2019-01-15 改成無限期
		$sql = "INSERT INTO tb_Member_Donate (m_dnt_id, m_id, m_bkl_id, m_dnt_create_datetime,  
				m_dnt_method, m_dnt_error_reason, m_dnt_request_log, m_transaction_id, m_dnt_amount, m_dnt_point) "
			. "VALUES ('" . $new_id . "', '" . $id . "', '" . $booking_id . "', now(), 1, '尚未繳費', '" . $pure_data . "', '" . $transaction_id . "','" . $total_p . "','" . $total_p . "')";
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "更新錯誤", "description" => mysql_error(), "debug" => $sql));
		}
	}
	elseif ($mode == CONST_GENERATE_CREDIT_ORDER_MODE::AS_PAID) {

		$tmp_id = "ASPD" . $date->format('YmdHis');
		$sec = microtime();
		$new_id = $tmp_id . $sec[2] . $sec[3];//毫秒 $sec[0]=0 $sec[1]=.  $sec[2..3]為毫秒後兩位數  正常顯示為 0.xx (秒
		$this_authorized_store = new UPK_AuthorizedStore($m_as_id);
		$s4c_handling_fee = $total_p * $this_authorized_store->get_m_as_s4c_handling_fee();
		$m_aspd_fee = $total_p-$s4c_handling_fee;
		$sql = "INSERT INTO tb_Member_AS_Paid (m_aspd_id, m_id, m_as_id, m_id_as_owner, m_aspd_create_datetime,  
				m_aspd_error_reason, m_aspd_request_log, m_aspd_transaction_id, m_aspd_fee, m_aspd_as_fee, m_aspd_space4car_handling_fee,m_aspd_trade_platform, customer_invoice_arg) "
			. "VALUES ('" . $new_id . "', '" . $id . "', '" . $m_as_id . "', '" . $this_authorized_store->get_m_id() . "', now(),  '尚未繳費', '" . $pure_data . "', '" . $transaction_id . "','" . $total_p . "','" . $m_aspd_fee . "','".$s4c_handling_fee."','".$m_dpt_trade_platform."','".mysql_escape_string(json_encode($customer_invoice_arg))."')";
		if (!mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "更新錯誤", "description" => mysql_error(), "debug" => $sql));
		}
		if($m_aspd_remark != "") {
			include_once("/../main_api/AddRemarkFunc.php");
			AddRemarkFunc("ADD REMARK",$token,"至".$this_authorized_store->get_m_as_name()."消費備註",$m_aspd_remark,CONST_REMARK_TYPE::GOV_OPEN_PARKING_LOG,$new_id);
		}
	}
	else {
		$new_id = "";
	}
	return json_encode(array("result" => 1, "new_id" => $new_id));
}
?>