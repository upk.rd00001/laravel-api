<?php

namespace App\Http\Pay;

abstract class TappayType
{
	const LINE_PAY = 0;

	const GOOGLE_PAY = 1;

	const APPLE_PAY = 2;

	const DIRECT_PAY = 3;

	const DIRECT_PAY_BY_CARD = 4;

	const BIND_CARD = 5;

	const REMOVE_CARD = 6;

	const RECORD = 7;

	const EASY_WALLET = 8;
	// etc.
}