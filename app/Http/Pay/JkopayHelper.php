<?php
namespace App\Http\Pay;

use CONST_GENERATE_CREDIT_ORDER_MODE;

class JkopayHelper
{
	/*{"store_id": "d7120db2-8c76-4124-bf08-02e5b775d8fe", "platform_order_id": 87, "currency":
"TWD", "total_price": 1000, "final_price": 1000, "escrow": false, "payment_type": "onetime"}*/
/*
$key="VxfD6oNI0XEvLCSXnFoF903oGzQxz12xqmhk";
$secret_key="rg7oN2RBr67M8ql4MdFcG4R7_dQoZ8pNduptpoNZH5ZnPKrrre1uO6LOOShEcbczvrnRbHWbxkkFYXpjkutIZw";
$arr["store_id"]="d40f0024-c26d-11e9-9ebc-0050568403ed";
$arr["platform_order_id"]=87;
$arr["currency"]="TWD";
$arr["total_price"]=1000;
$arr["final_price"]=1000;
$arr["escrow"]=false;
$arr["payment_type"]="onetime";*/

	private $jkopay_url = ""; //傳送的url//https://uat-onlinepay.jkopay.app/platform/entry
	private $result_url = ""; //receive的url //https://api.space4car.com/api/jko_api/backend_notify_url.php
	private $result_display_url = ""; //傳送的url //https://member.space4car.com/....
	private $api_key = "VxfD6oNI0XEvLCSXnFoF903oGzQxz12xqmhk"; //API Key:
	private $secret_key = "rg7oN2RBr67M8ql4MdFcG4R7_dQoZ8pNduptpoNZH5ZnPKrrre1uO6LOOShEcbczvrnRbHWbxkkFYXpjkutIZw"; //Secret Key
	private $store_id = "d40f0024-c26d-11e9-9ebc-0050568403ed"; //
	private $currency = "TWD"; //
	private $mode = ""; //

	function __construct($mode=1)
	{
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$this->jkopay_url = GetSystemParameter($conn, 'jkopay_url');
		$this->api_key = GetSystemParameter($conn, 'jkopay_api_key');
		$this->secret_key = GetSystemParameter($conn, 'jkopay_secret_key');
		$this->store_id = GetSystemParameter($conn, 'jkopay_store_id');
		$this->mode = $mode;
		if($mode==CONST_GENERATE_CREDIT_ORDER_MODE::PARKING_PAY) {
			if(IsPreview())
				$this->result_url = "https://api.space4car.com/preview_api/jko_api/backend_notify_url.php";
			else
				$this->result_url = GetSystemParameter($conn, 'jkopay_result_url');
		}
		elseif($mode==CONST_GENERATE_CREDIT_ORDER_MODE::MEMBER_PAID)
			$this->result_url = GetSystemParameter($conn, 'jkopay_result_url_for_member_paid');
		elseif($mode==CONST_GENERATE_CREDIT_ORDER_MODE::AS_PAID)
			$this->result_url = GetSystemParameter($conn, 'jkopay_result_url_for_authorized_store_paid');
		$this->result_display_url = GetSystemParameter($conn, 'jkopay_result_display_url');
	}
	function PayRequest($m_dpt_id,$total_p,$details,$redirect_url="") {
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$DT_now = new DateTime();
		$DT_now_p_15 = new DateTime();
		$DT_now_p_15->modify('+ 15 min');

		$request = array(
			"platform_order_id" => $m_dpt_id,
			"store_id" => $this->store_id,
			"currency" => $this->currency,
			"total_price" => $total_p,
			"final_price" => $total_p,

			//"unredeem" => 0,	//不可折抵金額
			"valid_time" => $DT_now_p_15->format("Y-m-d H:i"), //訂單有效期限，依 UTC+8 時區。" YYYY-mm-dd HH:MM "

			//由商家實作此callback URL (https)。買家在街口確認付款頁面輸入密碼後，街口服務器訪問此電商平台服務器網址確認訂單正確性與存貨彈性。
			//"confrim_url" => $this->confirm_url,

			//由電商平台實作此callback URL (https)。消費者付款完成後，街口服務器訪問此電商平台服務器網址，並在參數中提供街口交易序號與訂單交易狀態代碼。
			"result_url" => $this->result_url,

			//web才要用 由電商平台實作此客戶端http/s url。消費者付款完成後點選完成按鈕，將消費者導向此電商平台客戶端付款結果頁網址。
			"result_display_url" => $this->result_display_url,

			//"payment_type" => "onetime", // “onetime”為一次性付款，”regular”為定期定額付款
			//"escrow" => false, //是否支持價金保管，預設為 False 不支持。

			"products" => array(array(
				"name"=>"易停網停車媒合服務費",
				//"img"=>"",
				"unit_count"=>1,
				"unit_price"=>$total_p,
				"unit_final_price"=>$total_p
			))
		);
		$request_json=json_encode($request);
		$digest=hash_hmac ( "sha256", $request_json , $this->secret_key);
		$headers[] = "Content-Type: application/json";
		$headers[] = "API-KEY: ".$this->api_key;
		$headers[] = "DIGEST: ".$digest;
		//rg_activity_log($conn, "", "JkoPay PayRequest", $this->jkopay_url, json_encode($request), "");

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_URL, $this->jkopay_url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);	//
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request_json);
		$result_raw = curl_exec($curl);
		curl_close($curl);
		rg_activity_log($conn, "", "JkoPay PayRequest", $this->jkopay_url, json_encode($request), $result_raw);
		$result_jko=json_decode($result_raw,true);
		if($result_jko["result"]=="000") {
			$result["result"] = 1;
			$result["payment_url"] = $result_jko["result_object"]["payment_url"];
			$result["qr_img"] = $result_jko["result_object"]["qr_img"];
			$result["qr_timeout"] = $result_jko["result_object"]["qr_timeout"];
		}
		else {
			$result["result"] = 0;
			$result["title"] = "街口付款失敗";
			$result["description"] = $result_jko["message"];
		}
		return ($result);//PayRequset不要json_encode
	}
	function RefundRequest($m_id, $m_dpt_id, $amount = 0) {
		$pure_data = file_get_contents('php://input');
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$language = "zh-tw";
		$this->jkopay_url = GetSystemParameter($conn, 'jkopay_refund_url');
		if($this->mode == CONST_GENERATE_CREDIT_ORDER_MODE::PARKING_PAY) {
			$sql = "SELECT m_dpt_amount FROM tb_Member_Deposit WHERE m_dpt_id='".$m_dpt_id."' ";
			$result = mysql_query($sql,$conn);
			if(!$result) {
				rg_activity_log($conn, $m_id, "交易失敗", "付款訂單錯誤", $pure_data, "");
				$ans = GetSystemCode("5020006", $language, $conn);
				return ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
			elseif(mysql_num_rows($result)==0) {
				rg_activity_log($conn, $m_id, "交易失敗", "無此訂單", $pure_data, "");
				$ans = GetSystemCode("5020006", $language, $conn);
				return ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
			$ans = mysql_fetch_assoc($result);
			$total_amount = $ans["m_dpt_amount"];
		}
		elseif($this->mode == CONST_GENERATE_CREDIT_ORDER_MODE::AS_PAID) {

			$sql = "SELECT m_aspd_fee FROM tb_Member_As_Paid WHERE m_aspd_id='".$m_dpt_id."' ";
			$result = mysql_query($sql,$conn);
			if(!$result) {
				rg_activity_log($conn, $m_id, "交易失敗", "付款訂單錯誤", $pure_data, "");
				$ans = GetSystemCode("5020006", $language, $conn);
				return ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
			elseif(mysql_num_rows($result)==0) {
				rg_activity_log($conn, $m_id, "交易失敗", "無此訂單", $pure_data, "");
				$ans = GetSystemCode("5020006", $language, $conn);
				return ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
			$ans = mysql_fetch_assoc($result);
			$total_amount = $ans["m_aspd_fee"];
		}

		$request = array(
			"platform_order_id" => $m_dpt_id,
			"refund_amount" => $total_amount
		);
		$request_json=json_encode($request);
		$digest=hash_hmac ( "sha256", $request_json , $this->secret_key);
		$headers[] = "Content-Type: application/json";
		$headers[] = "API-KEY: ".$this->api_key;
		$headers[] = "DIGEST: ".$digest;
		//rg_activity_log($conn, "", "JkoPay PayRequest", $this->jkopay_url, json_encode($request), "");

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_URL, $this->jkopay_url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);	//
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request_json);
		$result_raw = curl_exec($curl);
		curl_close($curl);
		rg_activity_log($conn, "", "JkoPay PayRequest", $this->jkopay_url, json_encode($request), $result_raw);
		$result_jko=json_decode($result_raw,true);
		$response = array();
		if($result_jko["result"]=="000") {
			$response["result"] = 1;
//			{
//				 "result": "000",
//				 "message": null,
//				 "result_object": {
//						"refund_tradeNo": "K0026910118070413C4001",
//						"debit_amount": 70,
//						" redeem_amount": 30,
//						"refund_time": "2018-07-01- 12:20:20"
//				 }
//			}
		}
		else {
			$response["result"] = 0;
			$response["title"] = "街口付款失敗";
			$response["description"] = $result_jko["message"];
		}
		return ($response);//PayRequset不要json_encode
	}

	function InquiryRequest($platform_order_ids=array()){
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$this->result_url = GetSystemParameter($conn, 'jkopay_inquiry');
		$get_url = $this->result_url."?platform_order_ids=".implode(",",$platform_order_ids);
		$digest=hash_hmac ( "sha256", "platform_order_ids=".implode(",",$platform_order_ids) , $this->secret_key);
		$headers[] = "Content-Type: application/json";
		$headers[] = "API-KEY: ".$this->api_key;
		$headers[] = "DIGEST: ".$digest;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_URL, $get_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$result_raw = curl_exec($curl);
		curl_close($curl);

		rg_activity_log($conn, "", "JkoPay InquiryRequest", $get_url, $platform_order_ids, $result_raw);
		$result_jko=json_decode($result_raw,true);
		if(isset($result_jko["result_object"]["transactions"])
			&& isset($result_jko["result"])
			&& $result_jko["result"] == "000"
		) {
			$result["result"] = 1;
			$result["data"] = $result_jko["result_object"]["transactions"];
			return $result;
		}
		else {
			$result["result"] = 0;
			$result["title"] = "街口付款失敗";
			$result["description"] = $result_jko["message"];
			return $result;
		}
	}
	static function BackendNotifyParse()
	{
		if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0) {
			throw new Exception('Request method must be POST!');
		}

		$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
		if (strpos($contentType, 'application/json') != 0) {
			throw new Exception('Content type must be: application/json, not ' . $contentType);
		}

		$content = trim(file_get_contents("php://input"));

		$decoded = json_decode($content, true);

		if (!is_array($decoded)) {
			throw new Exception('Received content contained invalid JSON!');
		}
		return $decoded;
	}
}