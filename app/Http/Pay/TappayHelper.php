<?php
namespace App\Http\Pay;

use App\Http\CUrlHelper;
use CONST_GENERATE_CREDIT_ORDER_MODE;
use DateTime;
use UPK_BookingLog;

class TappayHelper
{
	private $type = null;
	public $name = '';
	private $partnerKey = 'partner_izNiB31jkOCfqI3YEsQzcQfhToNpphzru9b4JHZJnJeM8muX49ZfsOs8';
	private $merchant_id = 'Space4car_LINEPAY';
	private $frontend_redirect_url = 'https://api.space4car.com/kevin_api/index.php';
	private $backend_notify_url = 'https://api.space4car.com/kevin_api/line_pay/backend_notify_url.php';
	private $TapPayApiUrl = "https://sandbox.tappaysdk.com/tpc/payment/pay-by-prime";//sandbox
	private $LinePayTradeHistoryUrl = "https://sandbox.tappaysdk.com/tpc/transaction/trade-history";//sandbox
	//mode: 1 = 車位媒合  2 = 會員升等(特約店/經銷商/區代理)  3=會員升等(100%抵用點數) 4 = 捐贈
	function __construct($conn, $type, $frontend_redirect_url = "",$mode=1)
	{
		$this->type = $type;
		$this->TapPayApiUrl = GetSystemParameter($conn, 'tappay_linepay_api_url');
		$this->LinePayTradeHistoryUrl = GetSystemParameter($conn, 'tappay_trade_history_api_url');
		$this->partnerKey = GetSystemParameter($conn, 'tappay_partner_key');
		switch ($type) {
			case TappayType::LINE_PAY:
			default:
				$this->name = 'Line Pay';
				$this->merchant_id = GetSystemParameter($conn, 'tappay_linepay_merchant_id');
				$this->backend_notify_url = GetSystemParameter($conn, 'tappay_linepay_backend_notify_url');
				$this->frontend_redirect_url = $frontend_redirect_url;
				break;
			case TappayType::GOOGLE_PAY:
				$this->name = 'Google Pay';
				$this->merchant_id = GetSystemParameter($conn, 'tappay_googlepay_merchant_id');
				break;
			case TappayType::APPLE_PAY:
				$this->name = 'Apple Pay';
				$this->merchant_id = GetSystemParameter($conn, 'tappay_applepay_merchant_id');
				break;
			case TappayType::DIRECT_PAY:
				$this->name = 'Direct Pay';
				$this->merchant_id = GetSystemParameter($conn, 'tappay_directpay_merchant_id');
				break;
			case TappayType::DIRECT_PAY_BY_CARD:
				$this->name = 'Direct Pay By Card';
				$this->TapPayApiUrl = GetSystemParameter($conn, 'tappay_directpaybycard_api_url');
				$this->merchant_id = GetSystemParameter($conn, 'tappay_directpay_merchant_id');
				break;
			case TappayType::BIND_CARD:
				$this->name = 'Bind Card';
				$this->TapPayApiUrl = GetSystemParameter($conn, 'tappay_bindcard_api_url');
				$this->merchant_id = GetSystemParameter($conn, 'tappay_directpay_merchant_id');
				break;
			case TappayType::REMOVE_CARD:
				$this->name = 'Remove Card';
				$this->TapPayApiUrl = GetSystemParameter($conn, 'tappay_removecard_api_url');
				$this->merchant_id = GetSystemParameter($conn, 'tappay_directpay_merchant_id');
				break;

			case TappayType::RECORD:
				$this->name = 'Record Card';
				$this->TapPayApiUrl = GetSystemParameter($conn, 'tappay_record_url');
				$this->merchant_id = GetSystemParameter($conn, 'tappay_directpay_merchant_id');
				break;

			case TappayType::EASY_WALLET:
				$this->name = 'Easy Wallet';
				$this->TapPayApiUrl = GetSystemParameter($conn, 'tappay_easywallet_api_url');
				$this->merchant_id = GetSystemParameter($conn, 'tappay_easywallet_merchant_id');
				$this->backend_notify_url = GetSystemParameter($conn, 'tappay_easywallet_backend_notify_url');
				if($mode==CONST_GENERATE_CREDIT_ORDER_MODE::MEMBER_PAID){
					$this->backend_notify_url = GetSystemParameter($conn,'tappay_easywallet_receive_for_member_paid');
				}
				elseif($mode==CONST_GENERATE_CREDIT_ORDER_MODE::AS_PAID){
					$this->backend_notify_url = GetSystemParameter($conn,'tappay_easywallet_receive_for_authorized_store_paid');
				}
				if($frontend_redirect_url!="")
					$this->frontend_redirect_url = $frontend_redirect_url;//前端要用
				else
					$this->frontend_redirect_url = 'https://member.space4car.com/';
				break;

		}
	}

	function PayRequest(
		$prime,
		$order_number,
		$amount,
		$phone_number,
		$name,
		$email,
		$address,
		$details,
		$remember = false,
		$card_token = "",
		$card_key = ""
	)
	{
		$phone_number=$phone_number+0;
		$phone_number="0".$phone_number;
		$headers = array(
			'x-api-key: ' . $this->partnerKey,
			'Content-Type: application/json'
		);
		$request = array(
			"prime" => $prime,
			"order_number" => $order_number,
			"partner_key" => $this->partnerKey,
			"merchant_id" => $this->merchant_id,
			"details" => $details,
			"amount" => $amount,
			"cardholder" => array(
				"phone_number" => $phone_number,
				"name" => $name,
				"email" => $email,
//                "zip_code" => $zip_code,
				"address" => $address,
//                "national_id" => $national_id
			)
		);
		switch ($this->type) {
			case TappayType::LINE_PAY:
				$request['result_url'] = array(
					"frontend_redirect_url" => $this->frontend_redirect_url,
					"backend_notify_url" => $this->backend_notify_url
				);
				break;
			case TappayType::DIRECT_PAY:
				$request["remember"] = $remember;
				break;
			case TappayType::DIRECT_PAY_BY_CARD:
				$request["card_key"] = $card_key;
				$request["card_token"] = $card_token;
				$request["currency"] = "TWD";
				unset($request["prime"]);
				break;
			case TappayType::EASY_WALLET:
				$request['result_url'] = [
					"frontend_redirect_url" => $this->frontend_redirect_url,
					"backend_notify_url" => $this->backend_notify_url
				];
				break;
			case TappayType::GOOGLE_PAY:
			default:
				break;

		}
		global $conn,$dbName;
		check_conn($conn,$dbName);
		$result = CUrlHelper::jsonPost($this->TapPayApiUrl,
			$request,
			$headers);
		rg_activity_log($conn, "", "PayRequest", $this->TapPayApiUrl, json_encode($request), $result);
		$result_array = json_decode($result, true);
		if($result_array["status"] != 0) {
			if(isset($result_array["msg"]) && $result_array["msg"]=="Gateway Timeout.") {
				for ($i = 0; $i < 3; $i++) {
					$tappay = new TappayHelper($conn, TappayType::RECORD);
					$result_record = $tappay->RecordRequest($order_number);
					if (isset($result_record["result"]) && $result_record["result"] == 1) {
						if ($result_record["count"] == 1) {
							$trade_record = $result_record["data"][0];
							// record_status 交易紀錄的狀態
							//-1	ERROR	交易錯誤
							//0	AUTH	銀行已授權交易，但尚未請款
							//1	OK	交易完成
							//2	PARTIALREFUNDED	部分退款
							//3	REFUNDED	完全退款
							//4	PENDING	待付款
							//5	CANCEL	取消交易
							if($trade_record["record_status"]==0 ||$trade_record["record_status"]==1) {
								$result_record_data = $trade_record;
								$result_record_data["status"] = 0;//製造出原本成功的result
								$result_record_data["rec_trade_id"] = $trade_record["rec_trade_id"];//製造出原本成功的result
								$result_record_data["transaction_time_millis"] = $trade_record["bank_transaction_start_millis"];//製造出原本成功的result
								$result_record_data["amount"] = $trade_record["amount"];//製造出原本成功的result
								$result_record_data["msg"] = "Success";//製造出原本成功的result
								return $result_record_data;
							}
							elseif($i==2) {
								//失敗就拿API撈出來的 bank error msg
								if(isset($result_array["bank_result_msg"]))
									$result_array["msg"]=$trade_record["bank_result_msg"];
								return $result_array;
							}
						}
						else {
							//失敗的畫繼續嘗試，嘗試3次
						}
					}
				}
			}
			else { //BANK ERROR 去找詳細的原因
				if(isset($result_array["bank_result_msg"]))
					$result_array["msg"]=$result_array["bank_result_msg"];
			}
			return $result_array;//3次嘗試都失敗的話回傳gateway timeout
		}
		return $result_array;
	}

	function RecordRequest($order_number="") {
		global $conn, $dbName;
		check_conn($conn, $dbName);
		$DT_now = new DateTime();
		$DT_now_p_15 = new DateTime();
		$DT_now_p_15->modify('+ 15 min');
		$filter=array();
		if($order_number!="")
			$filter["order_number"] = $order_number;
		//$filter["currency"] = "";
		//$filter["record_status"] = 0; //0 means successful //失敗的也要撈出來吐msg
		$tmp_filter = json_encode($filter);
		$filter_obj = json_decode($tmp_filter);
		$request = array(
			"partner_key" => $this->partnerKey,
			"records_per_page" => 50,
			"page" => 0,
			"filters" => $filter_obj,
			"order_by"=>array("attribute"=>"time","is_descending"=>true)
		);
		$request_json=json_encode($request);
		$headers[] = "Content-Type: application/json";
		$headers[] = "x-api-key: ".$this->partnerKey;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_URL, $this->TapPayApiUrl);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);	//
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request_json);
		$result_raw = curl_exec($curl);
		curl_close($curl);
		rg_activity_log($conn, "", "TapPay RecordRequest", $this->TapPayApiUrl, json_encode($request), $result_raw);
		$result_tappay=json_decode($result_raw,true);
		$email_body="";
		$email_body .= "url: ".$this->TapPayApiUrl."<br><br>";
		$email_body .= "request: ".json_encode($request)."<br><br>";//PayRequset不要json_encode
		$email_body .= "response: ".$result_raw."<br><br>";//PayRequset不要json_encode

		if(isset($result_tappay["status"]) && $result_tappay["status"]==2) { //2 的話代表在當前過濾條件內，已無更多紀錄
			if(count($result_tappay["trade_records"])==0){
				return array("result"=>1,"count"=>count($result_tappay["trade_records"]),"data"=>$result_tappay["trade_records"]);
			}
			elseif(count($result_tappay["trade_records"])==1){
				return array("result"=>1,"count"=>count($result_tappay["trade_records"]),"data"=>$result_tappay["trade_records"]);
			}
			else {
				//怎麼可能會有多筆，發信檢查一下
				EmailTemplate($conn,"易停網營運團隊","upk.rd00001@gmail.com","Tappay檢查到多筆單號",$email_body);
				return array("result"=>1,"count"=>count($result_tappay["trade_records"]),"data"=>$result_tappay["trade_records"]);
			}
		}
		//失敗....先發信以後可以關掉
		EmailTemplate($conn,"易停網營運團隊","upk.rd00001@gmail.com","Tappay檢查單號失敗",$email_body);
		return array("result"=>0,"title"=>"Tappay檢查單號失敗","description"=>"");
		//return ($result_tappay);//PayRequset不要json_encode

	}
	function BindCardRequest(
		$prime,
		$phone_number,
		$name,
		$email,
		$address,
		$upk_mid
	)
	{
		$headers = array(
			'x-api-key: ' . $this->partnerKey,
			'Content-Type: application/json'
		);
		$request = array(
			"prime" => $prime,
			"partner_key" => $this->partnerKey,
			"merchant_id" => $this->merchant_id,
			"currency" => "TWD",
			"cardholder" => array(
				"phone_number" => $phone_number,
				"name" => $name,
				"email" => $email,
//                "zip_code" => $zip_code,
				"address" => $address,
//                "national_id" => $national_id
			)
		);
		global $conn,$dbName;
		check_conn($conn,$dbName);
		rg_activity_log($conn, $upk_mid, "Bind Card Request", $this->TapPayApiUrl, json_encode($request), "");
		$receiveData = CUrlHelper::jsonPost($this->TapPayApiUrl,
			$request,
			$headers);
		$result = json_decode($receiveData, true);
		rg_activity_log($conn, $upk_mid, "Bind Card Request", $this->TapPayApiUrl, json_encode($request), $result);
		if (isset($result["status"]) && $result["status"] == 0 && isset($result["card_secret"]) && isset($result["card_info"])) {
			$card_secret = $result["card_secret"];
			$card_info = $result["card_info"];
			$sql = "UPDATE tb_Member_CreditCard SET m_cdc_is_delete=1 WHERE m_id='" . $upk_mid . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return ReturnJsonModule(array("result" => 0, "title" => "更新信用卡失敗", "description" => mysql_error($conn)));
			}
			//如果要綁定信用卡則要存信用卡資料
			$tmp_m_cdc_id = 'tmp' . GenerateRandomString(11, '0123456789');
			$sql = "INSERT INTO tb_Member_CreditCard (`m_cdc_id`, `m_id`, `m_cdc_bin_code`, `m_cdc_last_four`, `m_cdc_issuer`, `m_cdc_funding`, `m_cdc_type`, `m_cdc_level`, `m_cdc_country`, `m_cdc_country_code`, `m_cdc_expiry_date`) VALUES ('" . $tmp_m_cdc_id . "','" . $upk_mid . "','" . $card_info["bin_code"] . "','" . $card_info["last_four"] . "','" . $card_info["issuer"] . "','" . $card_info["funding"] . "','" . $card_info["type"] . "','" . $card_info["level"] . "','" . $card_info["country"] . "','" . $card_info["country_code"] . "','" . $card_info["expiry_date"] . "') ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return ReturnJsonModule(array("result" => 0, "title" => "更新信用卡失敗", "description" => mysql_error($conn)));
			}
			$sql = "SELECT m_cdc_sn FROM tb_Member_CreditCard WHERE m_id='" . $upk_mid . "' and m_cdc_id = '" . $tmp_m_cdc_id . "' and m_cdc_is_delete = b'0' and "
				. "m_cdc_bin_code='" . $card_info["bin_code"] . "' and "
				. "m_cdc_last_four='" . $card_info["last_four"] . "' and "
				. "m_cdc_issuer='" . $card_info["issuer"] . "' and "
				. "m_cdc_funding='" . $card_info["funding"] . "' and "
				. "m_cdc_type='" . $card_info["type"] . "' and "
				. "m_cdc_level='" . $card_info["level"] . "' and "
				. "m_cdc_country='" . $card_info["country"] . "' and "
				. "m_cdc_country_code='" . $card_info["country_code"] . "' and "
				. "m_cdc_expiry_date='" . $card_info["expiry_date"] . "' ";
			$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
			if (!$ans) {
				rg_activity_log($conn, $upk_mid, "Bind Card Request Failed", "查詢失敗", $sql, "");
				return json_encode(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error()));
			}
			$new_id = $ans["m_cdc_sn"];
			Sn2Id("CDCD", $new_id);
			$sql = "UPDATE tb_Member_CreditCard SET m_cdc_id='" . $new_id . "' WHERE m_cdc_sn='" . $ans["m_cdc_sn"] . "' and m_cdc_is_delete=b'0'";
			if (!mysql_query($sql, $conn)) {
				rg_activity_log($conn, $upk_mid, "Bind Card Request Failed", "更新失敗", $sql, "");
				return json_encode(array("result" => 0, "title" => "更新失敗", "description" => mysql_error()));
			}
			$m_cdc_id = $new_id;
			$sql = "UPDATE tb_Member SET m_tappay_card_token='" . $card_secret["card_token"] . "', m_tappay_card_key='" . $card_secret["card_key"] . "'  WHERE m_id='" . $upk_mid . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return ReturnJsonModule(array("result" => 0, "title" => "更新信用卡失敗", "description" => mysql_error($conn)));
			}
			return json_encode(array("result" => 1));
		}
		else {
			return ReturnJsonModule(array("result" => 0, "title" => "信用卡綁定失敗", "description" => "錯誤碼: " . $result["msg"]));
		}
	}

	function RemoveCardRequest(
		$card_token,
		$card_key,
		$upk_mid
	)
	{
		$headers = array(
			'x-api-key: ' . $this->partnerKey,
			'Content-Type: application/json'
		);
		$request = array(
			"partner_key" => $this->partnerKey,
			"card_key" => $card_key,
			"card_token" => $card_token
		);
		global $conn,$dbName;
		check_conn($conn,$dbName);
		rg_activity_log($conn, "", "Remove Card Request", $this->TapPayApiUrl, json_encode($request), "");
		$receiveData = CUrlHelper::jsonPost($this->TapPayApiUrl,
			$request,
			$headers);
		$result = json_decode($receiveData, true);
		if ($result["status"] == 0) {
			$sql = "UPDATE tb_Member_CreditCard SET m_cdc_is_delete=1 WHERE m_id='" . $upk_mid . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return ReturnJsonModule(array("result" => 0, "title" => "更新信用卡失敗", "description" => mysql_error($conn)));
			}
			$sql = "UPDATE tb_Member SET m_tappay_card_token='', m_tappay_card_key=''  WHERE m_id='" . $upk_mid . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return ReturnJsonModule(array("result" => 0, "title" => "更新信用卡失敗", "description" => mysql_error($conn)));
			}
			return json_encode(array("result" => 1));
		}
		else {
			return ReturnJsonModule(array("result" => 0, "title" => "信用卡解除綁定失敗", "description" => $result["msg"]));
		}
	}

	static function CheckPaymentState($conn, $rec_trade_id)
	{
		//tappay 成功代表 status=0
		//查為何自動扣後又會出現在繳費頁面然後使用者再付一次 https://trello.com/c/JboV7RJe
		return 0;
		$headers = array(
			'x-api-key: ' . GetSystemParameter($conn, 'tappay_partner_key'),
			'Content-Type: application/json'
		);
		$request = array(
			"rec_trade_id" => $rec_trade_id,
			"partner_key" => GetSystemParameter($conn, 'tappay_partner_key'),
		);
		$result = CUrlHelper::jsonPost(
			GetSystemParameter($conn, 'tappay_trade_history_api_url'),
			$request,
			$headers);

		rg_activity_log($conn, "", "二次確認繳款LOG", "", $request, $result);
		return json_decode($result, true)["status"];
	}

	static function BackendNotifyParse()
	{
		if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0) {
			throw new Exception('Request method must be POST!');
		}

		$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
		if (strpos($contentType, 'application/json') != 0) {
			throw new Exception('Content type must be: application/json, not ' . $contentType);
		}

		$content = trim(file_get_contents("php://input"));

		$decoded = json_decode($content, true);

		if (!is_array($decoded)) {
			throw new Exception('Received content contained invalid JSON!');
		}
		return $decoded;
	}

	static function postTreatmentForTappay(
		$m_dpt_id,
		$m_transaction_id,
		$paymentDate,
		$tradeAmount,
		$status,
		$isSuccess,
		$receiveData,
		$conn,
		$language,
		$remember = false,
		$invoice_arg = array()
	)
	{
		$G_SYSTEM_ERROR_RECEIVE_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
		#取得必要資訊
		$sql = "SELECT m_id, m_dpt_origin_point,m_bkl_id,m_dpt_pay_datetime FROM tb_Member_Deposit WHERE m_dpt_id='" . $m_dpt_id . "'";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("查詢錯誤" => mysql_error()));
		}

		$ans = mysql_fetch_assoc($result);
		$upk_mid = $ans["m_id"];
		$m_bkl_id = $ans["m_bkl_id"];
		$this_booking_log = new UPK_BookingLog($m_bkl_id);
		$this_booking_log->GetPricingLog();
		$this_booking_log->GetParkingLog();
		$this_booking_log->GetMemberVehicle();
		$this_booking_log->pricing_logs->GetParkingSpace();
		$this_booking_log->pricing_logs->parking_space->GetParkingLot();
		$this_parking_lot = $this_booking_log->pricing_logs->parking_space->parking_lot;
		$m_dpt_pay_datetime = $ans["m_dpt_pay_datetime"];
		if ($m_dpt_pay_datetime != "") {
			if ($this_parking_lot->get_m_plots_type() == "301") {
				//北大後付款的情況畫有二次付款，所以給我繼續付
				#&& $this_booking_log->parking_log->get_m_pl_end_time()==""
			}
			else {
				rg_activity_log($conn, "", "交易失敗", "已付款", "已付款", "");
				$ans = GetSystemCode("5020006", $language, $conn);
				return json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
			}
		}
		$update_point = (double)$ans["m_dpt_origin_point"];
		$update_point += (double)$tradeAmount;
		$sql = "SELECT m_language,m_token,m_email,m_first_name,m_last_name FROM tb_Member WHERE m_id='" . $upk_mid . "'";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("查詢錯誤" => mysql_error()));
		}
		$ans = mysql_fetch_assoc($result);
		$language = $ans["m_language"];
		$email = $ans["m_email"];
		$first_name = $ans["m_first_name"];
		$last_name = $ans["m_last_name"];
		$token = $ans["m_token"];
		#成功繳費or失敗繳費
		if ($isSuccess) {
			#成功
			//double check
			if (self::CheckPaymentState($conn, $m_transaction_id) != 0) {
				$tans = EmailTemplate($conn, $last_name . " " . $first_name, $G_SYSTEM_ERROR_RECEIVE_EMAIL, "易停網 預約付款錯誤 請盡速處理", $receiveData . "pay_api/GenerateCreditOrdersReceive.php #268");
				rg_activity_log($conn, $upk_mid, "信用卡繳費失敗", "二次確認繳款失敗", $receiveData, "");
				return json_encode(array("result" => 0, "title" => "二次確認繳款失敗", "description" => "查詢tap pay相關資料，並未付款成功"));
			}
			//file_put_contents('log_' . date("j.n.Y") . '.txt', "tap pay 繳款確認無誤\r\n", FILE_APPEND);

			//tap pay沒有szPaymentTypeChargeFee這個欄位可以用
			$sql = "UPDATE tb_Member_Deposit SET m_transaction_id='" . $m_transaction_id . "',"
				. " m_dpt_pay_datetime='" . $paymentDate->format('Y-m-d H:i:s') . "', m_dpt_handling_charge= 0 , m_dpt_error_reason='', m_dpt_response_log='" . $receiveData . "', m_dpt_update_point=" . $update_point
				. " WHERE m_dpt_id='" . $m_dpt_id . "'";
			if (!mysql_query($sql, $conn)) {
				rg_activity_log($conn, $upk_mid, "信用卡繳費失敗", "更新付款資訊失敗", $receiveData, "");
				return json_encode(array("result" => 0, "更新錯誤" => mysql_error()));
			}
			rg_activity_log($conn, $upk_mid, "信用卡新增訂單回傳成功", "", $receiveData, "");

			$m_dpt_pay_datetime = $paymentDate->format('Y-m-d H:i:s');//付款時間
			$this_booking_log->updateIsPaidValidDatetime();
			#把點數加到tb_member去
			$sql = "UPDATE tb_Member SET m_own_point=" . $update_point . " WHERE m_id='" . $upk_mid . "'";
			if (!mysql_query($sql, $conn)) {
				rg_activity_log($conn, $upk_mid, "信用卡繳費失敗", "嚴重錯誤，請確認該會員點數", $receiveData, "");
				return json_encode(array("result" => 0, "更新錯誤" => mysql_error()));
			}
			rg_activity_log($conn, $upk_mid, "信用卡繳費成功", "", $receiveData, "");
			$ttans = json_decode(ReceiveSendMessageFunc($conn, $token, $upk_mid, $m_bkl_id, $m_dpt_id, $receiveData, false, $invoice_arg), true);
			if ($ttans["result"] == 0) {
				return json_encode($ttans);
			}
			AutoInsertParkingLogFunc($conn, $m_bkl_id, $this_parking_lot);
			$result = json_decode($receiveData, true);
			if ($remember && isset($result["card_secret"]) && isset($result["card_info"])) {
				$card_secret = $result["card_secret"];
				$card_info = $result["card_info"];
				$sql = "UPDATE tb_Member_CreditCard SET m_cdc_is_delete=1 WHERE m_id='" . $upk_mid . "' ";

				$result = mysql_query($sql, $conn);
				if (!$result) {
					return ReturnJsonModule(array("result" => 0, "title" => "更新信用卡失敗", "description" => mysql_error($conn)));
				}
				//如果要綁定信用卡則要存信用卡資料
				$tmp_m_cdc_id = 'tmp' . GenerateRandomString(11, '0123456789');
				$sql = "INSERT INTO tb_Member_CreditCard (`m_cdc_id`, `m_id`, `m_cdc_bin_code`, `m_cdc_last_four`, `m_cdc_issuer`, `m_cdc_funding`, `m_cdc_type`, `m_cdc_level`, `m_cdc_country`, `m_cdc_country_code`, `m_cdc_expiry_date`) VALUES ('" . $tmp_m_cdc_id . "','" . $upk_mid . "','" . $card_info["bin_code"] . "','" . $card_info["last_four"] . "','" . $card_info["issuer"] . "','" . $card_info["funding"] . "','" . $card_info["type"] . "','" . $card_info["level"] . "','" . $card_info["country"] . "','" . $card_info["country_code"] . "','" . $card_info["expiry_date"] . "') ";
				$result = mysql_query($sql, $conn);
				if (!$result) {
					return ReturnJsonModule(array("result" => 0, "title" => "更新信用卡失敗", "description" => mysql_error($conn)));
				}
				$sql = "SELECT m_cdc_sn FROM tb_Member_CreditCard WHERE m_id='" . $upk_mid . "' and m_cdc_id = '" . $tmp_m_cdc_id . "' and m_cdc_is_delete = b'0' and "
					. "m_cdc_bin_code='" . $card_info["bin_code"] . "' and "
					. "m_cdc_last_four='" . $card_info["last_four"] . "' and "
					. "m_cdc_issuer='" . $card_info["issuer"] . "' and "
					. "m_cdc_funding='" . $card_info["funding"] . "' and "
					. "m_cdc_type='" . $card_info["type"] . "' and "
					. "m_cdc_level='" . $card_info["level"] . "' and "
					. "m_cdc_country='" . $card_info["country"] . "' and "
					. "m_cdc_country_code='" . $card_info["country_code"] . "' and "
					. "m_cdc_expiry_date='" . $card_info["expiry_date"] . "' ";
				$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
				if (!$ans) {
					return json_encode(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error()));
				}
				$new_id = $ans["m_cdc_sn"];
				Sn2Id("CDCD", $new_id);
				$sql = "UPDATE tb_Member_CreditCard SET m_cdc_id='" . $new_id . "' WHERE m_cdc_sn='" . $ans["m_cdc_sn"] . "' and m_cdc_is_delete=b'0'";
				if (!mysql_query($sql, $conn)) {
					return json_encode(array("result" => 0, "title" => "更新失敗", "description" => mysql_error()));
				}
				$m_cdc_id = $new_id;
				$sql = "UPDATE tb_Member SET m_tappay_card_token='" . $card_secret["card_token"] . "', m_tappay_card_key='" . $card_secret["card_key"] . "'  WHERE m_id='" . $upk_mid . "' ";
				$result = mysql_query($sql, $conn);
				if (!$result) {
					return ReturnJsonModule(array("result" => 0, "title" => "更新信用卡失敗", "description" => mysql_error($conn)));
				}
			}
			else {
				//沒有記憶一的時候不取消，由另一個地方發動
				/*
				$sql="UPDATE tb_Member_CreditCard SET m_cdc_is_delete=1 WHERE m_id='".$upk_mid."' ";
				$result = mysql_query($sql, $conn);
				if(! $result)
				{
					return ReturnJsonModule(array("result"=>0, "title"=>"更新信用卡失敗", "description"=> mysql_error($conn)));
				}
				$sql="UPDATE tb_Member SET m_tappay_card_token='', m_tappay_card_key='',  WHERE m_id='".$upk_mid."' ";
				$result = mysql_query($sql, $conn);
				if(! $result)
				{
					return ReturnJsonModule(array("result"=>0, "title"=>"更新信用卡失敗", "description"=> mysql_error($conn)));
				}*/
			}
		}
		else {
			#失敗
			$sql = "UPDATE tb_Member_Deposit SET m_dpt_error_reason='" . $status . "', m_dpt_response_log='" . $receiveData . "' WHERE m_dpt_id='" . $m_dpt_id . "'";
			if (!mysql_query($sql, $conn)) {
				return json_encode(array("result" => 0, "更新錯誤" => mysql_error()));
			}
			rg_activity_log($conn, $upk_mid, "信用卡新增訂單回傳失敗", "TappayHelper", $receiveData, "");
			$tans = EmailTemplate($conn, "易停網", $G_SYSTEM_ERROR_RECEIVE_EMAIL, "易停網 預約付款錯誤 請盡速處理", $receiveData . "pay_api/GenerateCreditOrdersReceive.php #" . __LINE__);
			$description = "";
			if (isset($receiveData["msg"])) {
				$description = $receiveData["msg"];
			}
			return ReturnJsonModule(array("result" => 0, "title" => "信用卡新增訂單回傳失敗", "description" => $description));
		}
	}
}

