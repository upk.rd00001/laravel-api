<?php
function Send4000ToGreenIdea($data)
{

	$arr["data"] = json_encode($data);
//form-data
//[{"key":"data","value":"{\"paytype\":1,\"id\":\"00000014\",\"bayid\":2,\"deposit\":1520850780,\"time\":30,\"cost\":20}","description":"","type":"text","enabled":true}] 
	//$testurl="http://fep.mygesoft.com:4000/syncEPay.action";
	//$testurl="http://52.199.197.77/PM/syncEPay.action"; 
	$testurl = "http://54.95.177.240/PM/syncEPay.action"; //2018-05-28更換IP
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $testurl);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);    //
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_TIMEOUT, 1);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $arr);
	$result = curl_exec($curl);
	curl_close($curl);
	return $result;
	return json_encode(array("result" => 1));
}
function GovOverSignUp($conn,$booking_id)
{
	include_once("/../template/email_template.php");
	$this_booking_log = new UPK_BookingLog($booking_id);
	$this_parking_space = new UPK_ParkingSpace($this_booking_log->get_m_pk_id());
	$this_parking_lot = new UPK_ParkingLot($this_parking_space->get_m_plots_id());
	$m_bkl_gov_id = $this_booking_log->get_m_bkl_gov_id();
	$this_booking_log->GetMemberVehicle();
	$m_ve_plate_no = $this_booking_log->vehicle_data->get_m_ve_plate_no();
	$billing_time = $this_booking_log->get_m_bkl_start_date() . " " . $this_booking_log->get_m_bkl_start_time(); //開單時間
	$DT_appear_datetime = new DateTime($this_booking_log->get_m_bkl_end_date() . " " . $this_booking_log->get_m_bkl_end_time()); //開單時間
	$this_parking_space->GetFareKindParkingSpace();
	$email_title_text = "自助計費結算通知 單號: " . $m_bkl_gov_id;
	$email_addr_text = "";
	if (isset($this_parking_lot->gov_parking_space_area["start_address"]))
		$gov_start_address = $this_parking_lot->gov_parking_space_area["start_address"];
	else $gov_start_address = "起點未知";

	if (isset($this_parking_lot->gov_parking_space_area["end_address"]))
		$gov_end_address = $this_parking_lot->gov_parking_space_area["end_address"];
	else $gov_end_address = "迄點未知";
	if ($this_parking_lot->gov_parking_space_area != "") {
		$email_addr_text = "路段起訖點: " . $gov_start_address . " ~ " . $gov_end_address . "<br>";
	}
	else {
		$email_addr_text = "　　　地址: " . $this_parking_lot->get_m_plots_address_post_code()
			. $this_parking_lot->get_m_plots_address_country()
			. $this_parking_lot->get_m_plots_address_city()
			. $this_parking_lot->get_m_plots_address_line1()
			. $this_parking_lot->get_m_plots_address_line2()
			. $this_parking_lot->get_m_plots_address_line3() . "<br>";
		//	."　地址說明: ".$this_parking_lot->get_m_plots_address_description()."<br>"
		//	."　連絡電話: ".$this_parking_lot->get_m_plots_phone_number()."<br>"
		//	."　　　說明: ".$this_parking_lot->get_m_plots_description()."<br>"
		//	."　社區名稱: ".$this_parking_lot->get_m_plots_community_name()."<br><br>"
	}
	$email_body_user = "感謝您使用易停網，以下是您自助計費的詳細資料，請妥善保存。";
//	print_r($this_parking_space->gov_farekind_parkingspaces[0]);
	$email_body2 = "<br><br>"
		. "　結算時間: " . $DT_appear_datetime->format("Y-m-d H:i") . "<br>"
		//."　付款連結: "."<a href='".$G_GOV_PAY_URL."'>點我付款</a>"."<br>"
		//."　付款網址: ".$G_GOV_PAY_URL."<br><br>"
		. "　開單時間: " . $billing_time . "<br>"
		. "　車牌號碼: " . $m_ve_plate_no . "<br>"
		. "　路段名稱: " . $this_parking_lot->get_m_plots_name() . "<br>"
		. $email_addr_text
		. "　車位編號: " . $this_parking_space->get_m_pk_number() . "號 車位<br>"
		//.$datetime_text		//加簽時才有
		. "　時段費率: " . $this_parking_space->gov_farekind_parkingspaces[0]->get_custom_fare_desc(0, 1, 1) . "<br>"
		. "　　總金額: " . $this_booking_log->get_m_bkl_estimate_point() . "元<br>";
	//."　預約時間: ".$m_dpt_pay_datetime."<br>";
	//			."停車場編號: ".$m_plots_id."<br>"
	//			."　車位編號: ".$m_pk_id."<br>"
	//			."空位時段號: ".$m_ppl_id."<br>"
	//			."　預約編號: ".$m_bkl_id."<br>";
	$email_body2 .= "　開單單號: " . $m_bkl_gov_id . "<br>";
//	$email_body2.="<br>自助計費於隔日起使用橘子支繳費，可享5次半價優惠,每次最高回饋50元, 優惠活動至2018/6/18，需於繳費期限內完成繳費。<br>"; //2018/09/20 過期了就拿掉吧..
	$email_body = $email_body_user . $email_body2;
	EmailTemplate_m_id($conn, $this_booking_log->vehicle_data->get_m_id(), "易停網 " . $email_title_text, $email_body);
	//除了預約外 其他都不要發信
	if ($this_parking_lot->get_m_plots_type() == '300') {

	}
	elseif ($this_parking_lot->get_m_plots_type() == '301') {

	}
	elseif ($this_parking_lot->get_m_plots_type() == '302') {

	}
	//路邊自助計費不需要推給車位主
	elseif ($this_parking_lot->get_m_plots_type() != '100') {
		EmailTemplate_m_id($conn, $this_parking_space->get_m_pk_owner_m_id(), "車位主 " . $email_title_text, $email_body);
	}
	$m_pk_number = $this_parking_space->get_m_pk_number();
	if(count($this_parking_space->get_file_logs_array())==0) {
		$this_parking_space->GetGoogleMap();
	}
	$this_parking_space_file_log = $this_parking_space->file_logs;

	PushTo($conn, $this_booking_log->vehicle_data->get_m_id(), "完成路邊開單結算", "車號: " . $m_ve_plate_no . " " . $gov_start_address . " 至 " . $gov_end_address . " 車位編號: " . $m_pk_number . " 。", "", array(), true, $booking_id,$this_parking_space_file_log);
	PushTo($conn, $this_parking_space->get_m_pk_owner_m_id(), "有會員完成路邊開單結算", "車號: " . $m_ve_plate_no . " " . $gov_start_address . " 至 " . $gov_end_address . " 車位編號: " . $m_pk_number . " 。", "", array(), true, $booking_id,$this_parking_space_file_log);

	if ($this_parking_space->get_m_pk_agent_m_id() != "") {
		if ($this_parking_lot->get_m_plots_type() != '100') {
			$tans = EmailTemplate_m_id($conn, $this_parking_space->get_m_pk_agent_m_id(), "代管人 " . $email_title_text, $email_body);
			PushTo($conn, $this_parking_space->get_m_pk_owner_m_id(), "有會員完成路邊開單結算", "車號: " . $m_ve_plate_no . " " . $gov_start_address . " 至 " . $gov_end_address . " 車位編號: " . $m_pk_number . " 。", "", array(), true, $booking_id,$this_parking_space_file_log);

		}
	}
}
function ReceiveSendMessageFunc($conn,$token,$upk_mid,$m_bkl_id,$szMerchantTradeNo,$receive_data,$fake_pay=false,$invoice_arg=array())
{
	$pure_data = file_get_contents('php://input');
	if (count($invoice_arg) != 0) {
		$CustomerEmail = $invoice_arg["CustomerEmail"];
		$CustomerPhone = $invoice_arg["CustomerPhone"];
		$CustomerAddr = $invoice_arg["CustomerAddr"];
		$CustomerID = $invoice_arg["CustomerID"];
		$CustomerIdentifier = $invoice_arg["CustomerIdentifier"];
		$CustomerName = $invoice_arg["CustomerName"];
		$Print = $invoice_arg["Print"];
		$Donation = $invoice_arg["Donation"];
		$LoveCode = $invoice_arg["LoveCode"];
		$CarruerType = $invoice_arg["CarruerType"];
		$CarruerNum = $invoice_arg["CarruerNum"];
		$TaxType = $invoice_arg["TaxType"];
		/*//這邊是信用卡付款才會用到的分期，發票參數不該有
		$allpay_credit_mode = $invoice_arg["allpay_credit_mode"];
		$CreditInstallment = $invoice_arg["CreditInstallment"];
		$InstallmentAmount = $invoice_arg["InstallmentAmount"];
		$Redeem = $invoice_arg["Redeem"];
		$UnionPay = $invoice_arg["UnionPay"];
		$PeriodAmount = $invoice_arg["PeriodAmount"];
		$PeriodType = $invoice_arg["PeriodType"];
		$Frequency = $invoice_arg["Frequency"];
		$ExecTimes = $invoice_arg["ExecTimes"];*/
	}
	include_once("/../template/email_template.php");
	include_once("fn_package.php");
	include_once("/../upkclass/UPKclass.php");
	$language = "zh-tw";
	//file_put_contents('log_'.date("j.n.Y").'.txt', "In pay_api\ReceiveSendMessageFunc.php\r\n", FILE_APPEND);
	rg_activity_log($conn, $upk_mid, "ReceiveSendMessageFunc", $m_bkl_id . "開始", $receive_data, json_encode($invoice_arg));
	$G_SYSTEM_ERROR_RECEIVE_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
	#取得必要資訊
	$sql = "SELECT m_id, m_dpt_origin_point,m_bkl_id,m_dpt_pay_datetime,m_dpt_trade_platform,m_dpt_amount FROM tb_Member_Deposit WHERE m_dpt_id='" . $szMerchantTradeNo . "'";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return json_encode(array("result" => 0, "title" => "查詢錯誤:15", "description" => mysql_error()));
	}
	$ans = mysql_fetch_assoc($result);
	$m_dpt_pay_datetime = $ans["m_dpt_pay_datetime"];
	$m_dpt_trade_platform = $ans["m_dpt_trade_platform"];
	$m_dpt_amount = $ans["m_dpt_amount"];
	$sql = "SELECT m_sn,m_first_name, m_last_name,m_email,m_reference_member_cellphone FROM tb_Member WHERE m_id='" . $upk_mid . "' AND m_token='" . $token . "' ";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return json_encode(array("result" => 0, "title" => "查詢錯誤:24", "description" => mysql_error()));
	}
	$ans = mysql_fetch_assoc($result);
	$m_sn = $ans["m_sn"];
	$first_name = $ans["m_first_name"];
	$last_name = $ans["m_last_name"];
	$email = $ans["m_email"];
	$m_reference_member_cellphone = $ans["m_reference_member_cellphone"];
	$this_booking_log = new UPK_BookingLog($m_bkl_id);
	$is_monthly_rental = 0;
	if($m_bkl_id!="") {
		$sql = "SELECT m_bkl_id FROM tb_Member_White_Black_List WHERE m_bkl_id='" . $m_bkl_id ."' ";
		$result = mysql_query($sql, $conn);
		if(!$result){

		}
		elseif(mysql_num_rows($result)!=0) {
			$is_monthly_rental = 1;
		}
		$sql = "UPDATE tb_Member_White_Black_List SET m_wbl_delete=0 WHERE m_bkl_id='" . $m_bkl_id . "' ";
		mysql_query($sql, $conn);// 失敗要處理?
	}

	$args=array();
	if($m_dpt_trade_platform==CONST_M_DPT_TRADE_PLATFORM::EC_PAY_BARCODE) {
	//因為查看與建立時間剛好跨時段
	//而且因為使用超商繳費時會非當下繳費，而有機會去查看繳費頁面更新view
	//然後使用者查看紀錄時會算成建立訂單時間(出場時間)的金額
	//所以超商繳費成功時要用超商繳費的金額
		$args["point"] = $m_dpt_amount;
	}
	$this_booking_log->GetBookingLogView($upk_mid,$args);

	$m_pk_id = $this_booking_log->get_m_pk_id();
	$m_ppl_id = $this_booking_log->get_m_ppl_id();
	$m_ppl_promotion_code = $this_booking_log->get_m_ppl_promotion_code();
	$m_ve_id = $this_booking_log->get_m_ve_id();

	$m_pyl_point = $this_booking_log->get_m_bkl_view_point();
	$m_pyl_point_free = $this_booking_log->get_m_bkl_view_parking_point();
	$m_bkl_view_whitelist_point = $this_booking_log->get_m_bkl_view_whitelist_point();
	$m_bkl_view_discount_point = $this_booking_log->get_m_bkl_view_discount_point();
	$m_bkl_disp_unpaid_page = $this_booking_log->get_m_bkl_disp_unpaid_page();
	$m_bkl_is_customize = $this_booking_log->get_m_bkl_is_customize();
	$m_bkl_due_point = $m_pyl_point + $m_pyl_point_free + $m_bkl_view_whitelist_point + $m_bkl_view_discount_point;
	$m_bkl_is_pd = 1;
	$book_type = $this_booking_log->get_m_bkl_book_type();

	$m_bkl_market_handling_fee = 0;
	if($m_dpt_trade_platform==CONST_M_DPT_TRADE_PLATFORM::EC_PAY_BARCODE) {
		$m_pyl_point = $m_pyl_point + $m_pyl_point_free;
		$m_pyl_point_free = 0;
		$m_bkl_market_handling_fee = 15;
	}
	$not_count_price_package = array();
	//$booking_ans_array = array_merge($booking_ans_array, array("can_delete"=>1));
	#若未付款且為接收轉移之會員重新計算金額
	$this_booking_log->GetPricingLog();

	$this_booking_log->GetParkingLog();
	if ($this_booking_log->get_m_bkl_book_type() == "1" && $this_booking_log->parking_log->get_m_pl_start_time() != null) {
		//是預約才有分潤且要判斷是已經進場者才開始分潤
		include_once("/../main_api/ProfitEarnCalculateFunc.php");
		ProfitEarnCalculateFunc("PROFIT EARN CALCULATE", $token, CONST_M_PFE_TYPE::SHARING_PARKING_SPACE, $this_booking_log->get_m_bkl_id());
	}
	$this_parking_space = new UPK_ParkingSpace($m_pk_id);
	$this_parking_lot = new UPK_ParkingLot($this_parking_space->get_m_plots_id());
	$merged_file_logs = GetPushToFileLog(CONST_GET_PUSH_TO_FILELOG::PARKING_SPACE,$this_parking_space->get_m_pk_id());
	$sql = "SELECT * FROM tb_Member_Vehicle WHERE m_ve_id='" . $m_ve_id . "' ";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return json_encode(array("result" => 0, "title" => "搜尋付款失敗", "description" => mysql_error($conn)));
	}
	$ans = mysql_fetch_assoc($result);
	$m_ve_plate_no = $ans["m_ve_plate_no"];
	$m_plots_type = $this_parking_lot->get_m_plots_type();
	$now_date_time = new DateTime();
	$DT_bkl_datetime = new DateTime($this_booking_log->get_m_bkl_create_datetime(true));
	$DT_bkl_datetime->modify("+15 sec"); //為了判斷進場時的零元付款與繳費時的零元付款
	//判斷如果是第一次假付款還是第二次真的使用者付款，如果付款數量>1則當作使用者付款
	$sql = "SELECT * FROM tb_Member_Deposit WHERE m_bkl_id='" . $m_bkl_id . "' AND m_dpt_pay_datetime IS NOT NULL";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return json_encode(array("result" => 0, "title" => "搜尋付款失敗", "description" => mysql_error($conn)));
	}
	$dpt_cnt = mysql_num_rows($result);

	//如果這次的零元付款只是為了寫入一筆付款資料避免被autorun刪除而非真正的付款的時候
	if ($m_plots_type == "300" || $m_plots_type == "301" || $m_plots_type == "302") {
		$sql = "SELECT tb_pl.m_pl_sn FROM tb_Member_Parking_Log as tb_pl LEFT JOIN tb_Member_Vehicle as tb_ve ON tb_pl.m_ve_id=tb_ve.m_ve_id WHERE tb_ve.m_id='" . $upk_mid . "' LIMIT 2";//只是要看有沒有紀錄所有限制1比
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "查詢進出場紀錄失敗", "description" => mysql_error($conn)));
		}
		elseif (mysql_num_rows($result) <= 1 && $m_reference_member_cellphone == "") {
			//如果之前從來沒有再進出場紀錄存在過就送該停車場的擁有人停車點數，因為剛進場所以至少會有一筆
			include_once("/../main_api/ProfitEarnCalculateFunc.php");
			$tans = json_decode(ProfitEarnCalculateFunc("PROFIT EARN CALCULATE", $token, CONST_M_PFE_TYPE::AGENT_POINT, $this_parking_lot->get_m_plots_id()), true);
			if (isset($tans["result"]) && $tans["result"] == 1) {
				//發送推播
				PushTo($conn, $this_parking_lot->get_m_id(), "您已推薦一名會員, 會員ID: " . $m_sn, "請登入查看獲得之停車點數", "");
			}
		}
		//(2) 300後單出場要作廢前單 [2]
		if ($m_plots_type == "300") {
			$sql = "SELECT tb_bkl.m_bkl_id FROM `tb_Member_ParkingSpace_Booking_Log` as tb_bkl LEFT JOIN tb_Member_Parking_Log as tb_pl ON tb_bkl.m_bkl_id=tb_pl.m_bkl_id WHERE tb_pl.m_pl_end_time IS NULL AND tb_bkl.m_pk_id='" . $this_booking_log->get_m_pk_id() . "' AND m_bkl_cancel='0' AND tb_bkl.m_bkl_id!='" . $m_bkl_id . "' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
			}
			while ($ans = mysql_fetch_array($result)) {
				$sql_cancel_bkl = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_cancel='1' WHERE m_bkl_id='" . $ans["m_bkl_id"] . "' ";
				$result_cancel_bkl = mysql_query($sql_cancel_bkl, $conn);
				if (!$result_cancel_bkl) {
				}
				MemcacheSetBookingLog('_UPK_BookingLog:',$ans["m_bkl_id"]);
			}
		}
	}
	if ($fake_pay) {
		MemcacheSetBookingLog('_UPK_BookingLog:', $m_bkl_id);

		if ($m_plots_type == "100") { //0=一般民眾刊登    20=民營    100=路邊
			$this_booking_log->GetParkingLog();
			$todays_gov_farekind_parkinspace = null;
			$this_parking_space->GetFareKindParkingSpace();
			if ($this_parking_space->gov_farekind_parkingspaces != null) {
				foreach ((array)$this_parking_space->gov_farekind_parkingspaces as $this_gov_farekind_parkinspace) {    #只會有一個
					if ($this_gov_farekind_parkinspace->is_charge(new DateTime()))
						$todays_gov_farekind_parkinspace = $this_gov_farekind_parkinspace;
					//$fare_id=$this_gov_farekind_parkinspace->get_fare_id();
				}
			}
			if (isset($this_parking_lot->gov_parking_space_area["start_address"]))
				$gov_start_address = $this_parking_lot->gov_parking_space_area["start_address"];
			else $gov_start_address = "起點未知";

			if (isset($this_parking_lot->gov_parking_space_area["end_address"]))
				$gov_end_address = $this_parking_lot->gov_parking_space_area["end_address"];
			else $gov_end_address = "迄點未知";
			$m_pk_number=$this_parking_space->get_m_pk_number();
			$m_bkl_group_id=$this_booking_log->get_m_bkl_group_id();
			$start_date=$this_booking_log->get_m_bkl_start_date();
			$start_time=$this_booking_log->get_m_bkl_start_time();
			if ($m_bkl_group_id == "" && $this_booking_log->parking_log->get_m_pl_end_time() == "") {
				if(count($this_parking_space->get_file_logs_array())==0) {
					$this_parking_space->GetGoogleMap();
				}
				$this_parking_space_file_log = $this_parking_space->file_logs;
				PushTo($conn, $upk_mid, "開始自助計費", "您的車號: ".$m_ve_plate_no." 於".$start_date . " " . $start_time."在" . $this_parking_lot->get_m_plots_name() . $m_pk_number . "號格開始自助計費(單號: ".$this_booking_log->get_m_bkl_gov_id().")，提醒您出場時請至進出控制頁面按下出場結算按鈕。", "", array(), true, $m_bkl_id,$this_parking_space_file_log);
				if(isTextContain($this_parking_lot->get_m_plots_address_line1(), "板橋區"))
					pushStore($upk_mid, "ASID0000000518"); //天邑汽車美容
				if(isTextContain($this_parking_lot->get_m_plots_address_line1(), "淡水區"))
					pushStore($upk_mid, "ASID0000000936"); //同感咖啡
				if(isTextContain($this_parking_lot->get_m_plots_address_line1(), "樹林區")) {
					if(IsDebug())
						pushStore($upk_mid, "ASID0000001913");//I have an apple.
					else
						pushStore($upk_mid, "ASID0000000514");//汽車醫美-車輛翻新專門店
				}
			}
		}
		elseif ($m_plots_type == "301") {

			# 301's fake pay
			if($this_parking_lot->isWithGate()) {
				PushTo($conn, $upk_mid, "掃碼進場成功",
					"您的車號: ".$m_ve_plate_no." 在".$this_parking_lot->get_m_plots_name().
					"掃碼進場成功，如車位已滿請於5分鐘內至出口直接掃碼離場，請勿占用私人車位，謝謝。",
					"", [], true, $m_bkl_id, $merged_file_logs);
			}
			else {
				PushTo($conn, $upk_mid, "掃碼進場成功", "車號: ".$m_ve_plate_no." 在".$this_parking_lot->get_m_plots_name()."掃碼進場成功，離場時請記得至易停網APP繳費頁面進行繳費再離場，謝謝。", "", [], true, $m_bkl_id, $merged_file_logs);
			}
		}
		return json_encode(array("result" => 1));
	}
	//後台增加的強制顯示在未付款頁面在付款結束之後強制變回已付款，不顯示在未付款頁面
	if ($m_bkl_disp_unpaid_page == '1') {
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_disp_unpaid_page='2' WHERE m_bkl_id='" . $m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約價格失敗", "description" => mysql_error($conn)));
		}
	}
	if ($m_bkl_is_customize == '1') {
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_is_customize='2' WHERE m_bkl_id='" . $m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約價格失敗", "description" => mysql_error($conn)));
		}
	}
	if ($m_plots_type == "302") {
		//如果該單是建立繳費單所建立的，因一開始就有指定會員，
		//所以之後(給車輛信用查)繳費時就不要再將車輛給繳費會員，直接沿用車輛ID當作不取代
		if($this_booking_log->get_m_bkl_disp_unpaid_page()!='0') {
			$m_ve_id = $this_booking_log->get_m_ve_id();
		}
		else {
			$ans_ve = GetMemberVehicle($conn, $token, $m_ve_plate_no);
			$tans_ve = json_decode($ans_ve, true);
			if (isset($tans_ve["result"]) && $tans_ve["result"] == 1) {
				$m_ve_id = $tans_ve["m_ve_id"];
			}
			else {
				//想不到如何能進來這邊.....
			}
		}
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_estimate_point=m_bkl_estimate_point+" . $m_pyl_point . ", m_bkl_estimate_free_point=m_bkl_estimate_free_point+'" . $m_pyl_point_free . "', m_bkl_due_point='" . $m_bkl_due_point . "', m_ve_id='" . $m_ve_id . "', m_bkl_is_pd='" . $m_bkl_is_pd . "', m_bkl_whitelist_point=m_bkl_whitelist_point+'" . $m_bkl_view_whitelist_point . "', m_bkl_discount_point=m_bkl_discount_point+'" . $m_bkl_view_discount_point . "', m_bkl_market_handling_fee='".$m_bkl_market_handling_fee."' WHERE m_bkl_id='" . $m_bkl_id . "' ";
		$wd_data = json_encode($not_count_price_package) . $sql;
		rg_activity_log($conn, $upk_mid, "ReceiveSendMessageFunc", "計算金額" . $m_ve_id.",".$m_bkl_id ."," . $m_pk_id.",pyl_point=" . $m_pyl_point.",pyl_point_free=" . $m_pyl_point_free.",due_point=" . $m_bkl_due_point.",white_list_point"  . $m_bkl_view_whitelist_point."," . $m_bkl_view_discount_point, $receive_data, $wd_data);
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約價格失敗", "description" => mysql_error($conn)));
		}
		$sql = "UPDATE tb_Member_Parking_Log SET m_ve_id='" . $m_ve_id . "' WHERE m_bkl_id='" . $m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新停車紀錄失敗", "description" => mysql_error($conn)));
		}
		$this_booking_log = MemcacheSetBookingLog('_UPK_BookingLog:', $m_bkl_id);

		$DT_now = new Datetime();
		$this_dashboard_parking = new UPKclass_Dashboard_Parking();
		$allData["dbpg_type"] = CONST_DBPG_TYPE::PARKING_302_PAY;
		$allData["m_plots_id"] = $this_parking_lot->get_m_plots_id();
		$allData["m_pk_id"] = $this_parking_space->get_m_pk_id();
		$allData["m_bk_id"] = $m_bkl_id;
		$allData["m_dpt_id"] = $szMerchantTradeNo;
		$allData["m_id"] = $upk_mid;
		$allData["dbpg_create_datetime"] = $DT_now->format("Y-m-d H:i:s");
		$allData["m_ve_id"] = $this_booking_log->get_m_ve_id();
		$allData["dbpg_desc"] = "車辨停車付款";
		$this_dashboard_parking->insert($allData);
		rg_activity_log($conn, $upk_mid, "車辨停車資訊成功", "", $pure_data, $ans);
	}
	elseif (($m_plots_type == "301" && ($DT_bkl_datetime < $now_date_time || $dpt_cnt > 1))) {

		//未付款時不需要直接把金額加進去(基本0元)
		//新北市付款的時候也要加進去..||($m_plots_type=='100' && strpos($this_parking_lot->get_m_plots_address_city(),"新北市")!==false && $DT_bkl_datetime<$now_date_time)
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_estimate_point=m_bkl_estimate_point+" . $m_pyl_point . ", m_bkl_estimate_free_point=m_bkl_estimate_free_point+'" . $m_pyl_point_free . "', m_bkl_due_point='" . $m_bkl_due_point . "', m_bkl_is_pd='" . $m_bkl_is_pd . "', m_bkl_whitelist_point=m_bkl_whitelist_point+'" . $m_bkl_view_whitelist_point . "', m_bkl_discount_point=m_bkl_discount_point+'" . $m_bkl_view_discount_point . "', m_bkl_market_handling_fee='".$m_bkl_market_handling_fee."'  WHERE m_bkl_id='" . $m_bkl_id . "' ";
		$wd_data = json_encode($not_count_price_package) . $sql;
		rg_activity_log($conn, $upk_mid, "ReceiveSendMessageFunc", "計算金額" . $m_ve_id.",".$m_bkl_id ."," . $m_pk_id.",pyl_point=" . $m_pyl_point.",pyl_point_free=" . $m_pyl_point_free.",due_point=" . $m_bkl_due_point.",white_list_point"  . $m_bkl_view_whitelist_point."," . $m_bkl_view_discount_point, $receive_data, $wd_data);
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約價格失敗", "description" => mysql_error($conn)));
		}
		$this_booking_log = MemcacheSetBookingLog('_UPK_BookingLog:', $m_bkl_id);
	}
	elseif ($m_plots_type == "301") {
		# unpaid means first pay, not send email
		//之前mobile自己加的零元付款fake pay目前不會由mobile call
		if($this_parking_lot->isWithGate()) {
			PushTo($conn, $upk_mid, "掃碼進場成功", "車號: ".$m_ve_plate_no." 在".$this_parking_lot->get_m_plots_name()."掃碼進場成功，如車位已滿請於5分鐘內至出口直接掃碼離場，請勿占用私人車位，謝謝。", "", [], true, $m_bkl_id, $merged_file_logs);
		}
		else {
			PushTo($conn, $upk_mid, "掃碼進場成功", "車號: ".$m_ve_plate_no." 在".$this_parking_lot->get_m_plots_name()."掃碼進場成功，離場時請記得至易停網APP繳費頁面進行繳費再離場，謝謝。", "", [], true, $m_bkl_id, $merged_file_logs);
		}
		$this_booking_log = MemcacheSetBookingLog('_UPK_BookingLog:', $m_bkl_id);
		return json_encode(array("result" => 1));
	}
	elseif ($m_plots_type == "100" && ($DT_bkl_datetime < $now_date_time || $dpt_cnt > 1)) {
		//&& (strpos($this_parking_lot->get_m_plots_address_city(),"新北市")!==false)
		//新北市是先出場再付款
		//其他縣市之前是要加簽的時候就付款
		$this_booking_log->GetParkingLog();
		#結算後才付款 
		//因為是結算後才付款，所以需要這邊用calculater重算
		//if($this_booking_log->parking_log->get_m_pl_end_time()!=null) {
		$DT_m_bkl_start_datetime = new DateTime($this_booking_log->get_m_bkl_start_date() . " " . $this_booking_log->get_m_bkl_start_time(true));
		$DT_m_bkl_end_datetime = new DateTime($this_booking_log->get_m_bkl_end_date() . " " . $this_booking_log->get_m_bkl_end_time(true));
		$start_date = $DT_m_bkl_start_datetime->format("Y-m-d");
		$start_time = $DT_m_bkl_start_datetime->format("H:i:s");
		$end_date = $DT_m_bkl_end_datetime->format("Y-m-d");
		$end_time = $DT_m_bkl_end_datetime->format("H:i:s");
		array_push($not_count_price_package, array(
			"id" => $m_pk_id,
			"start_date" => $start_date,
			"end_date" => $end_date,
			"start_time" => $start_time,
			"end_time" => $end_time));
		include_once("/../main_api/GovPriceCalculateFunc_v2.php");
		$m_ve_special_level = 0;
		$ttans = json_decode(GovPriceCalculateFunc("GOV PRICE CALCULATE", $token, $not_count_price_package, $this_booking_log->get_m_bkl_id(), "0", true, $this_booking_log->get_m_ve_id()), true);
		if (isset($ttans["result"]) && $ttans["result"] == 1) {
			$m_pyl_point = $ttans["data"][0]["point"];
			$m_pyl_point_free = $ttans["data"][0]["free_point"];
			$m_bkl_is_pd = $ttans["m_bkl_is_pd"];
			$m_bkl_due_point = $ttans["data"][0]["due_point"];
			$whitelist_point = $ttans["data"][0]["whitelist_point"];
			if($m_dpt_trade_platform==CONST_M_DPT_TRADE_PLATFORM::EC_PAY_BARCODE) {
				$m_pyl_point=$m_pyl_point+$m_pyl_point_free;
				$m_pyl_point_free=0;
			}
		}
		else {
			$wd_data = json_encode($not_count_price_package) . $sql;
			rg_activity_log($conn, $upk_mid, "ReceiveSendMessageFunc計算金額失敗", "計算金額" . $m_ve_id.",".$m_bkl_id ."," . $m_pk_id.",pyl_point=" . $m_pyl_point.",pyl_point_free=" . $m_pyl_point_free.",due_point=" . $m_bkl_due_point.",white_list_point"  . $m_bkl_view_whitelist_point."," . $m_bkl_view_discount_point, json_encode($ttans), $wd_data);
			return json_encode($ttans);
		}
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_estimate_point=m_bkl_estimate_point+'" . $m_pyl_point . "', m_bkl_estimate_free_point=m_bkl_estimate_free_point+'" . $m_pyl_point_free . "', m_bkl_due_point='" . $m_bkl_due_point . "', m_bkl_is_pd='" . $m_bkl_is_pd . "', m_bkl_whitelist_point=m_bkl_whitelist_point+'" . $whitelist_point . "' , m_bkl_discount_point=m_bkl_discount_point+'" . $m_bkl_view_discount_point . "', m_bkl_gov_total_price_without_pay='0', m_bkl_market_handling_fee='".$m_bkl_market_handling_fee."'  WHERE m_bkl_id='" . $m_bkl_id . "' ";
		$wd_data = json_encode($not_count_price_package) . $sql;
		rg_activity_log($conn, $upk_mid, "ReceiveSendMessageFunc", "計算金額" . $m_ve_id.",".$m_bkl_id ."," . $m_pk_id.",pyl_point=" . $m_pyl_point.",pyl_point_free=" . $m_pyl_point_free.",due_point=" . $m_bkl_due_point.",white_list_point"  . $m_bkl_view_whitelist_point."," . $m_bkl_view_discount_point, json_encode($ttans), $wd_data);
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約價格失敗", "description" => mysql_error($conn)));
		}
		$this_booking_log = MemcacheSetBookingLog('_UPK_BookingLog:', $m_bkl_id);
//		}
	}
	elseif ($m_plots_type == "300") {
		//300付款後塞已付多少的值進BookingLog
		$sql = "UPDATE tb_Member_ParkingSpace_Booking_Log SET m_bkl_estimate_point='" . $m_pyl_point . "', m_bkl_estimate_free_point='" . $m_pyl_point_free . "', m_bkl_due_point='" . $m_bkl_due_point . "', m_bkl_is_pd='" . $m_bkl_is_pd . "', m_bkl_whitelist_point='" . $m_bkl_view_whitelist_point . "', m_bkl_discount_point='" . $m_bkl_view_discount_point . "', m_bkl_market_handling_fee='".$m_bkl_market_handling_fee."'  WHERE m_bkl_id='" . $m_bkl_id . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新預約價格失敗", "description" => mysql_error($conn)));
		}
		//避免後台建立繳費單時清除人工進場時間
		if($m_bkl_disp_unpaid_page==0) {
			#如果有人工改變地磁的情況要清掉
			$sql = "UPDATE tb_Devices SET is_delete=b'1' WHERE dv_type='6' AND dv_type_id='" . $this_booking_log->get_m_pk_id() . "' AND is_delete='0' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				//echo json_encode(array("result"=>0, "title"=>"改變人工地磁狀態失敗", "description"=> mysql_error($conn)));
			}
		}
		$this_booking_log = MemcacheSetBookingLog('_UPK_BookingLog:', $m_bkl_id);
	}

	$this_booking_log->updateIsPaidValidDatetime();//有is_paid會判斷estimate_point所以要寫在後面
	include_once("InsertPayLogFunc.php");
	if ($m_bkl_is_pd == 1) {
		$resulte_IPLF = json_decode(InsertPayLogFunc("INSERT PAY LOG", $token, $m_pyl_point, $m_pyl_point_free, "2", $m_bkl_id, $m_pk_id, $m_ppl_id, $m_ppl_promotion_code, $m_ve_id, 12, "", $m_bkl_is_pd), true);//參數2代表預約扣款之消費種類
		//12代表是帶有優惠方案的扣點(tb_Member_Paid)
		//0=未定義,9=去特約店消費點數,10=預約停車消費點數 11=特約店推廣申請 12=100%停車點數折抵
	}
	else {
		$resulte_IPLF = json_decode(InsertPayLogFunc("INSERT PAY LOG", $token, $m_pyl_point, $m_pyl_point_free, "2", $m_bkl_id, $m_pk_id, $m_ppl_id, $m_ppl_promotion_code, $m_ve_id), true);//參數2代表預約扣款之消費種類
		//預設是10，代表一般預約的免費點數扣點
	}
	if ($resulte_IPLF["result"] == 0) {
		return json_encode($resulte_IPLF);
	}

	$paid_parking_point = $resulte_IPLF["paid_parking_point"];
	$free_parking_point = $resulte_IPLF["free_parking_point"];
	//開發票
	if (count($invoice_arg) != 0) {
		//有電子發票的輸入參數才要開
		//0元就不開
		//路邊只要開超商手續費
		if($this_booking_log->get_m_bkl_plots_type() == "100") {
			$invoice_amount = $m_bkl_market_handling_fee; 
		}
		else {
			$invoice_amount = $m_pyl_point + $paid_parking_point + $m_bkl_market_handling_fee;
		}
		if($invoice_amount > 0) {
			if($this_booking_log->get_m_bkl_plots_type() == "100") {
				$purchase_list_json = '{"Quantity": 1, "URL": "", "Name": "超商手續費", "Price": ' . ($invoice_amount) . ', "Currency": "元"}';
			}
			//原本邏輯
			else {
				$purchase_list_json = '{"Quantity": 1, "URL": "", "Name": "停車媒合服務費", "Price": ' . ($invoice_amount) . ', "Currency": "元"}';
			}
			$purchase_list = array();
			array_push($purchase_list, json_decode($purchase_list_json));
			$is_trigger = 0;//零元付款就不用在觸發了拉，直接開下去就可以了，沒有所謂的receive
			include_once("GenerateElectronicInvoiceFunc.php");
			$activity = "GENERATE ELECTRONIC INVOICE";//activity其實可以不用給
			$ttans = json_decode(GenerateElectronicInvoiceFunc($activity, $token, $szMerchantTradeNo, $purchase_list, $CustomerEmail, $CustomerPhone, $CustomerAddr, $CustomerID, $CustomerIdentifier, $CustomerName, $Print, $Donation, $LoveCode, $CarruerType, $CarruerNum, $TaxType, $is_trigger), true);
			if ($ttans["result"] != 1) {
				//$G_SYSTEM_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
				$email_title = "開立電子發票失敗";
				$G_SYSTEM_EMAIL = "upk.rd00001@gmail.com";
				if(!IsDebug())
					EmailTemplate($conn, $email_title, $G_SYSTEM_EMAIL, "建立電子發票失敗通知", $pure_data . print_r($ttans, true));
				rg_activity_log($conn, $upk_mid, "開立電子發票失敗", $m_bkl_id, $pure_data, "");
			}
			else {
				rg_activity_log($conn, $upk_mid, "開立電子發票成功", "有寄出電子發票", $pure_data, "");
			}
		}
	}

	//如果該車位是充電樁，則繳費後則視同出場
	$sql = "SELECT * FROM tb_Devices WHERE dv_type='".CONST_DV_TYPE::WIN_CHARGE."' AND is_delete='0' AND dv_type_id='".$this_parking_space->get_m_pk_id()."' ";
	$result = mysql_query($sql,$conn);
	if(!$result) {
		//失敗就算了?
	}
	elseif(mysql_num_rows($result)==0){
		//不用做事
	}
	else {
		//出場
		include_once("/../main_api/AppearReturnFunc.php");
		AppearReturn301Func($conn, $upk_mid, $this_booking_log->get_m_bkl_id());
	}

	//事後送點數的優惠寫在金流之後 避免贈點之後有誤判說他有停車點數可以扣款，雖然以後改成如果先將停車點數存入DB是不會有這種邏輯問題
	$dcp_onsale_type=CONST_DCP_ONSALE_TYPE::FIRST_PAY_GIVE_POINT_PERCENT;
	$dcp_type=CONST_DCP_TYPE::PARKING_LOT;
	$c_dcp_manager = new UPK_DiscountPlanManager();
	$c_dcp_manager->select_discount_plan($upk_mid,$dcp_onsale_type,$dcp_type,$this_parking_lot->get_m_plots_id());
	$c_dcp_manager->execute_discount_plan_array($upk_mid, ($m_pyl_point + $paid_parking_point));

	$dcp_onsale_type=CONST_DCP_ONSALE_TYPE::APP_PAY_FEEDBACK;
	$dcp_type=CONST_DCP_TYPE::PARKING_LOT;
	$c_dcp_manager = new UPK_DiscountPlanManager();
	$c_dcp_manager->select_discount_plan($upk_mid,$dcp_onsale_type,$dcp_type,$this_parking_lot->get_m_plots_id());
	$c_dcp_manager->execute_discount_plan_array($upk_mid,($m_pyl_point + $paid_parking_point));

	//發送推播給車位主人與代理人
	//利用m_dpt_id='".$szMerchantTradeNo."'" 找出對應的BookingID再找出對應的pks id,發給車位主人與代理人
	$sql = "SELECT m_pk_owner_m_id, m_pk_agent_m_id FROM `tb_Member_Deposit` 
			JOIN tb_Member_ParkingSpace_Booking_Log on tb_Member_ParkingSpace_Booking_Log.m_bkl_id = tb_Member_Deposit.m_bkl_id 
			JOIN tb_Member_ParkingSpace on tb_Member_ParkingSpace_Booking_Log.m_pk_id = tb_Member_ParkingSpace.m_pk_id 
			WHERE tb_Member_Deposit.m_dpt_id='" . $szMerchantTradeNo . "'";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return json_encode(array("result" => 0, "title" => "查詢錯誤:108", "description" => mysql_error()));
	}
	$ans = mysql_fetch_assoc($result);
	$m_pk_owner_m_id = $ans["m_pk_owner_m_id"];
	$m_pk_agent_m_id = $ans["m_pk_agent_m_id"];
	if ($m_plots_type == "300" || $m_plots_type == "301" || $m_plots_type == "302") {
		//金額 時間 格號
		//車位主根代管人一樣的訊息，不考慮建立人
		//停車場:XXXXX,格號:YYYY,車牌號碼: AAA-1111,
		//於2020-05-04 17:37 完成繳交停車費用69元,
		//停車時段: 2020-05-04 14:37~2020-05-04 17:37
		//301 付款後不代表出場，所以出廠時間會是2199-12-31，不能要該時間做為推波會很怪
		$tmp_end_datetime=$this_booking_log->get_m_bkl_end_date()." ".$this_booking_log->get_m_bkl_end_time();
		if($tmp_end_datetime=="2199-12-31 23:59")
			$tmp_end_datetime=$now_date_time->format("Y-m-d H:i");
		$tmp_pushto_message=
			"會員ID: " . $m_sn .", ".
			"停車場: " . $this_parking_lot->get_m_plots_name() .", ".
			"格號:".$this_parking_space->get_m_pk_number().", ".
			"車牌號碼: " . $m_ve_plate_no .", ".
			"於" . $m_dpt_pay_datetime ."完成繳交停車費用". ($m_pyl_point+$m_pyl_point_free) . "元, ".
			"進場時間: ".$this_booking_log->get_m_bkl_start_date()." ".$this_booking_log->get_m_bkl_start_time().
			"";
		if ($m_pk_owner_m_id != null) {
			PushTo($conn, $m_pk_owner_m_id, "被停車付費成功", $tmp_pushto_message, "", array(), true, $m_bkl_id, $merged_file_logs);
		}
		if ($m_pk_agent_m_id != null) {
			PushTo($conn, $m_pk_agent_m_id, "被停車付費成功", $tmp_pushto_message, "", array(), true, $m_bkl_id, $merged_file_logs);
		}
	}
	elseif ($m_plots_type != "100") {
		if ($m_pk_owner_m_id != null) {
			PushTo($conn, $m_pk_owner_m_id, "被預約付費成功", "會員ID:" . $m_sn . " 車號: " . $m_ve_plate_no . " 在" . $this_parking_lot->get_m_plots_name() . "完成車位預約", "", array(), true, $m_bkl_id, $merged_file_logs);
		}
		if ($m_pk_agent_m_id != null) {
			PushTo($conn, $m_pk_agent_m_id, "被預約付費成功", "會員ID:" . $m_sn . " 車號: " . $m_ve_plate_no . " 在" . $this_parking_lot->get_m_plots_name() . "完成車位預約", "", array(), true, $m_bkl_id, $merged_file_logs);
		}
	}
	$G_BOOKING_REQUSET_FREE_POINT = GetSystemParameter($conn, "booking_request_free_point");
	$G_BOOKING_REQUSET_FREE_POINT_DEAD = GetSystemParameter($conn, "booking_request_free_point_dead");
	include_once("/../main_api/DepositFreePoint_func.php");
	$sql = "SELECT m_br_id,m_ppl_id FROM tb_Booking_Request WHERE m_bkl_id='" . $m_bkl_id . "' ";
	$result_booking_request = mysql_query($sql, $conn);
	if (!$result_booking_request) {
		rg_activity_log($conn, $upk_mid, "搜尋預約失敗", "贈送車位請求免費點數錯誤", "", "");
		return json_encode(array("result" => 0, "贈送車位請求免費點數錯誤" => mysql_error()));
	}
	else if (mysql_num_rows($result_booking_request) == 1) {
		$row = mysql_fetch_array($result_booking_request);
		$sql = "SELECT m_id_create FROM tb_Member_ParkingSpace_Pricing_Log WHERE m_ppl_id='" . $row["m_ppl_id"] . "' ";
		$result_ppl_creator = mysql_query($sql, $conn);
		if (!$result_ppl_creator) {
			rg_activity_log($conn, $upk_mid, "搜尋空位時段失敗", "贈送車位請求免費點數錯誤", "", "");
			return json_encode(array("result" => 0, "贈送車位請求免費點數錯誤" => mysql_error()));
		}
		else if (mysql_num_rows($result_ppl_creator) == 1) {
			$row2 = mysql_fetch_array($result_ppl_creator);
			$ppl_creator = $row2["m_id_create"];
			$ttans = DepositFreePointfunc("DEPOSIT FREE POINT", $ppl_creator, $G_BOOKING_REQUSET_FREE_POINT, "8", "", $G_BOOKING_REQUSET_FREE_POINT_DEAD);
		}
		else {
			#不應該有多筆
			$tans = EmailTemplate($conn, "易停網", $G_SYSTEM_ERROR_RECEIVE_EMAIL, "易停網 預約付款錯誤 請盡速處理", "#147<br>" . $receive_data);
			rg_activity_log($conn, $upk_mid, "搜尋空位時段失敗", "贈送車位請求免費點數錯誤", "", "");
			return json_encode(array("result" => 0, "贈送車位請求免費點數錯誤" => mysql_error()));
		}
		DepositFreePointfunc("DEPOSIT FREE POINT", $upk_mid, $G_BOOKING_REQUSET_FREE_POINT, "8", "", $G_BOOKING_REQUSET_FREE_POINT_DEAD);
	}
	else {
		#不是車位請求不贈送
	}
	$sql = "SELECT * FROM tb_Member_ParkingSpace_Booking_Log WHERE m_bkl_id='" . $m_bkl_id . "' ";
	$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
	if (!$ans) {
		return json_encode(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error()));
	}
	$m_bkl_create_datetime = $ans["m_bkl_create_datetime"];
	$m_bkl_group_id = $ans["m_bkl_group_id"];
	$m_bkl_gov_id = $ans["m_bkl_gov_id"];
	$start_date = $ans["m_bkl_start_date"];
	$start_time = $ans["m_bkl_start_time"];
	$end_date = $ans["m_bkl_end_date"];
	$end_time = $ans["m_bkl_end_time"];
	$m_bkl_sendpush = $ans["m_bkl_sendpush"];
	if ($m_plots_type == "100") {
		if ($book_type == "4000")
			$push_offset = "-27 min";
		else
			$push_offset = "-10 min";
	}
	else
		$push_offset = "-15 min";

	$is_pmq = 0;
	if ($book_type == "4000" || $book_type == "2000") {
		$sql = "SELECT * FROM tb_ParkingMeter_Qrcode WHERE m_bkl_id='" . $this_booking_log->get_unq_m_bkl_group_id() . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "搜尋咪表失敗", "description" => mysql_error($conn)));
		}
		elseif (mysql_num_rows($result) == 0) {
			if ($book_type == "4000")
				EmailTemplate($conn, "易停網", "upk.rd00001@gmail.com", "無此咪表 ", "sql: " . $sql . "<br>" . $m_bkl_id);
		}
		else {
			$is_pmq = 1;
			$ans = mysql_fetch_assoc($result);
			$pmq_id = $ans["pmq_id"];
			$pmq_paytype = $ans["pmq_paytype"];
			$pmq_serial = $ans["pmq_serial"];
			$pmq_bayid = $ans["pmq_bayid"];
			//$pmq_deposit = $ans["pmq_deposit"];
			$end_date = $this_booking_log->get_m_bkl_end_date();
			$end_time = $this_booking_log->get_m_bkl_end_time(true);
			date_default_timezone_set('UTC');// 把我們的時區當作UTC (就是數值的時間就是送出去的時間)
			$pmq_deposit = strtotime($end_date . " " . $end_time);
			date_default_timezone_set('Asia/Taipei');
			$pmq_time = $ans["pmq_time"];
			//$pmq_cost = $ans["pmq_cost"];
			$pmq_cost = $this_booking_log->get_m_bkl_estimate_point();
			$data["paytype"] = $pmq_paytype;
			$data["id"] = $pmq_serial;
			$data["bayid"] = $pmq_bayid;
			$data["deposit"] = $pmq_deposit;
			$data["time"] = $pmq_time;
			$data["cost"] = $pmq_cost;

			if ($m_bkl_group_id == "" && $is_pmq == 1 && isset($pmq_paytype) && $pmq_paytype == 2) {
				#如果是第一次開單且paytype=2的時候就不傳
			}
			else {
				#paytype不是2 或者 易停網開單的時候都要傳
				$result = Send4000ToGreenIdea($data);
				EmailTemplate($conn, "易停網", "upk.rd00001@gmail.com", "咪表送出紀錄", "request: " . json_encode($data) . "<br><br><br>result: " . $result . "<br>" . $m_bkl_id . "<br>" . $upk_mid . "<br>" . $pmq_id);
			}
		}
	}
	$sql = "SELECT m_pk_floor,m_pk_number,m_plots_id FROM tb_Member_ParkingSpace WHERE m_pk_id='" . $m_pk_id . "' ";
	$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
	if (!$ans) {
		return json_encode(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error()));
	}
	$m_pk_floor = $ans["m_pk_floor"];
	$m_pk_number = $ans["m_pk_number"];
	$m_plots_id = $ans["m_plots_id"];
	$date = new DateTime($start_date . " " . $start_time);
	$date->modify($push_offset);
	$push_approach_datetime = $date->format('Y-m-d H:i:s');
	$date = new DateTime($end_date . " " . $end_time);
	$date->modify($push_offset);
	$push_appear_datetime = $date->format('Y-m-d H:i:s');
	$now_date_time = new DateTime();

	//$push_approach_datetime = date("Y-m-d H:i:s",strtotime($start_date." ".$start_time.$push_offset));
	//$push_appear_datetime = date("Y-m-d H:i:s",strtotime($end_date." ".$end_time.$push_offset));
	$G_APPEAR_APPROACH_PUSH_NOTIFY = GetSystemParameter($conn, "appear_approach_push_notify");
	$push_pkspace_text = "";
	if ($m_pk_floor != "" && $m_pk_number != "") {
		if (substr($m_pk_floor, 0, 1) == "B")
			$push_pkspace_text = "" . $m_pk_floor . " " . $m_pk_number . "號 車位";
		else
			$push_pkspace_text = "" . $m_pk_floor . "F " . $m_pk_number . "號 車位";
	}
	$floor_number_text = "";
	if ($m_plots_type == "301")//掃描進場的停車編號為隨機分配所以不顯示停車編號
		$floor_number_text = "";
	else {
		$floor_number_text = "　車位編號: " . $push_pkspace_text . "<br>";
	}
	//您已完成自助計費，請記得出場時至側邊欄進出場回報頁面並按下"出場"。
	//300
	//付款成功", "請於5分鐘離場。
	//301
	//付款成功", "請於15分鐘內離場，若有柵欄請至出口掃描出場QR-CODE。
	//302
	//付款成功", "請於15分鐘內離場。
	//100
	//付款成功", "易停網祝您行車平安。"
	//0
	//付款成功", "你已完成付款，請依照預約時間前往停車地點，記得抵達後按下入場與離場時回報按鈕。"
	//
	//300/301/302月租單 交易成功後顯示
	//付款成功", "你已完成付款，月租時段內皆可進出該停車場，若要續約請洽停車場管理員或加入Line易停網客服 @space4car為您建立繳費單"

	rg_activity_log($conn, $upk_mid, "ReceiveSendMessageFunc", $m_bkl_id . " ".$is_monthly_rental." ".$m_plots_type, $receive_data, json_encode($invoice_arg));
	//超商繳費後付款應該是為2，事後付款的就不用推波幾分鐘內該出場了，直接說付款成功
	$send_device = -1;
	if($m_bkl_disp_unpaid_page!=0) {
		if($m_dpt_trade_platform != 15)
			PushTo($conn, $upk_mid, "付費成功", "你已完成付款！", "", array(), true, $m_bkl_id,$merged_file_logs,null,$send_device);
	}
	else if($is_monthly_rental==1) {
		PushTo($conn, $upk_mid, "付費成功", "你已完成付款，月租時段內皆可進出該停車場，若要續約請洽停車場管理員或加入Line易停網客服 @space4car為您建立繳費單", "", array(), true, $m_bkl_id,$merged_file_logs,null,$send_device);
	}
	else if ($m_plots_type == "100") { //0=一般民眾刊登    20=民營    100=路邊
		$this_booking_log->GetParkingLog();
		$todays_gov_farekind_parkinspace = null;
		$this_parking_space->GetFareKindParkingSpace();
		if ($this_parking_space->gov_farekind_parkingspaces != null) {
			foreach ((array)$this_parking_space->gov_farekind_parkingspaces as $this_gov_farekind_parkinspace) {    #只會有一個
				if ($this_gov_farekind_parkinspace->is_charge(new DateTime()))
					$todays_gov_farekind_parkinspace = $this_gov_farekind_parkinspace;
				//$fare_id=$this_gov_farekind_parkinspace->get_fare_id();
			}
		}
		if (isset($this_parking_lot->gov_parking_space_area["start_address"]))
			$gov_start_address = $this_parking_lot->gov_parking_space_area["start_address"];
		else $gov_start_address = "起點未知";

		if (isset($this_parking_lot->gov_parking_space_area["end_address"]))
			$gov_end_address = $this_parking_lot->gov_parking_space_area["end_address"];
		else $gov_end_address = "迄點未知";
		if ($m_bkl_group_id == "" && $this_booking_log->parking_log->get_m_pl_end_time() == "") {
			$this_parking_space_file_log = $this_parking_space->file_logs;
			PushTo($conn, $upk_mid, "開始自助計費", "您的車號: ".$m_ve_plate_no." 於".$start_date . " " . $start_time."在" . $this_parking_lot->get_m_plots_name() . $m_pk_number . "號格開始自助計費(單號: ".$this_booking_log->get_m_bkl_gov_id().")，提醒您出場時請至進出控制頁面按下出場結算按鈕。", "", array(), true, $m_bkl_id,$this_parking_space_file_log);
		}
		else {
			//自助計費出場結算後彈了兩個畫面 第一個付費成功要關掉
			if($m_dpt_trade_platform != 15)
				PushTo($conn, $upk_mid, "付費成功", "易停網祝您行車平安。", "", array(), true, $m_bkl_id,$merged_file_logs,null,$send_device);
		}
	}
	else if ($m_plots_type == "300") {
		if($m_dpt_trade_platform != 15)
			PushTo($conn, $upk_mid, "付費成功", "請於5分鐘離場。", "", array(), true, $m_bkl_id,$merged_file_logs,null,$send_device);
	}
	else if ($m_plots_type == "301") {
		if($m_dpt_trade_platform != 15) {
			$ans = PushTo($conn, $upk_mid, "付費成功", "請於15分鐘內離場，若有柵欄請至出口掃描出場QR-CODE。", "", array(), true, $m_bkl_id, $merged_file_logs, null, $send_device);
			rg_activity_log($conn, $upk_mid, "ReceiveSendMessageFunc", $m_bkl_id . " " . $is_monthly_rental . " " . $m_plots_type . " 301", $ans, json_encode($invoice_arg));
		}
	}
	else if ($m_plots_type == "302") {
		if($m_dpt_trade_platform != 15)
			PushTo($conn, $upk_mid, "付費成功", "請於15分鐘內離場。", "", array(), true, $m_bkl_id,$merged_file_logs,null,$send_device);
	}
	else if ($m_plots_type == "303") {
		if($m_dpt_trade_platform != 15)
			rg_activity_log($conn, $upk_mid, "ReceiveSendMessageFunc", $m_bkl_id . " ".$is_monthly_rental." ".$m_plots_type." 303", $receive_data, json_encode($invoice_arg));
	}
	//預約 沒意外都是m_plots_type==0
	else {
		if($m_dpt_trade_platform != 15) {
			PushTo($conn, $upk_mid, "付費成功", "你已完成付款，請依照預約時間前往停車地點，記得抵達後按下入場與離場時回報按鈕。", "", array(), true, $m_bkl_id, $merged_file_logs, null, $send_device);
			rg_activity_log($conn, $upk_mid, "ReceiveSendMessageFunc", $m_bkl_id . " " . $is_monthly_rental . " " . $m_plots_type . " other", $receive_data, json_encode($invoice_arg));
		}
	}
	//要判斷完前後四小時後再推
	if ($m_bkl_group_id == "") {
		$datetime_text = "";
		$email_title_text = "自助計費通知 單號: " . $m_bkl_gov_id;
		$billing_time = $start_date . " " . $start_time; //開單時間
		$billing_time = substr($billing_time, 0, -3);
	}
	else {
		$datetime_text = "　加簽時間" . ": " . $start_date . " " . $start_time . "<br>";
		$email_title_text = "自動加簽通知 單號: " . $m_bkl_gov_id;

		$sql = "SELECT * FROM tb_Member_ParkingSpace_Booking_Log WHERE m_bkl_id='" . $m_bkl_group_id . "' ";
		$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
		if (!$ans) {
			return json_encode(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error()));
		}
		$billing_time = $ans["m_bkl_start_date"] . " " . $ans["m_bkl_start_time"]; //開單時間
		$billing_time = substr($billing_time, 0, -3);
	}
	$plots_address = $this_parking_lot->get_m_plots_address_post_code()
		. $this_parking_lot->get_m_plots_address_country()
		. $this_parking_lot->get_m_plots_address_city()
		. $this_parking_lot->get_m_plots_address_line1()
		. $this_parking_lot->get_m_plots_address_line2()
		. $this_parking_lot->get_m_plots_address_line3();

	$DT_start_datetime = new DateTime($start_date . " " . $start_time);
	$DT_end_datetime = new DateTime($end_date . " " . $end_time);

	if ($m_plots_type == "100") {
		//Luke: 2018-12-12 路邊停車都不要發信
		//路邊停車自動加簽不要寄信
		$email_addr_text = "";

		if (isset($this_parking_lot->gov_parking_space_area["start_address"]))
			$gov_start_address = $this_parking_lot->gov_parking_space_area["start_address"];
		else $gov_start_address = "起點未知";

		if (isset($this_parking_lot->gov_parking_space_area["end_address"]))
			$gov_end_address = $this_parking_lot->gov_parking_space_area["end_address"];
		else $gov_end_address = "迄點未知";
		if ($this_parking_lot->gov_parking_space_area != "") {
			$email_addr_text = "路段起訖點: " . $gov_start_address . " ~ " . $gov_end_address . "<br>";
		}
		else {
			$email_addr_text = "　　　地址: " . $plots_address . "<br>";
			//	."　地址說明: ".$this_parking_lot->get_m_plots_address_description()."<br>"
			//	."　連絡電話: ".$this_parking_lot->get_m_plots_phone_number()."<br>"
			//	."　　　說明: ".$this_parking_lot->get_m_plots_description()."<br>"
			//	."　社區名稱: ".$this_parking_lot->get_m_plots_community_name()."<br><br>"
		}
		$email_body_user = "感謝您使用易停網，以下是您自助計費的詳細資料，請妥善保存。";
		$this_booking_log->reCalculatePrice($token);
		$email_body2 = "<br><br>"
			. "　開單時間: " . $billing_time . "<br>"
			. "　車牌號碼: " . $m_ve_plate_no . "<br>"
			. "　路段名稱: " . $this_parking_lot->get_m_plots_name() . "<br>"
			. $email_addr_text
			. "　車位編號: " . $m_pk_number . "號 車位<br>"
			. $datetime_text        //加簽時才有
			. "　時段費率: " . $this_parking_space->gov_farekind_parkingspaces[0]->get_custom_fare_desc(0, 1, 1) . "<br>"
			//."停車總費用: ".$this_booking_log->get_m_bkl_estimate_total_point()."元<br>"
			//."　實付金額: ".$this_booking_log->get_m_bkl_estimate_point()."元<br>"
			//."　停車點數: ".$this_booking_log->get_m_bkl_estimate_free_point()."元<br>"
			//."　預約時間: ".$m_dpt_pay_datetime."<br>"
			//			."停車場編號: ".$m_plots_id."<br>"
			//			."　車位編號: ".$m_pk_id."<br>"
			//			."空位時段號: ".$m_ppl_id."<br>"
			//			."　預約編號: ".$m_bkl_id."<br>"
		;//字串最後一定要有分號
		if ($m_bkl_group_id == "")
			$email_body2 .= "　開單單號: " . $m_bkl_gov_id . "<br>";
		else
			$email_body2 .= "　開單單號: " . $m_bkl_gov_id . "<br>";
		//$email_body2.="<br>自助計費請於隔日起使用橘子支繳費，可享5次半價優惠,每次最高回饋50元, 優惠活動至2018/6/18，需於繳費期限內完成繳費。<br>";
		$email_body = $email_body_user . $email_body2;


		$ics_file = "";
		$ics_id = $m_bkl_gov_id;
		$ics_title = "自助計費";
		$ics_description = $email_body2;
		$DT_start_datetime = new DateTime($start_date . " " . $start_time);
		$DT_end_datetime = new DateTime($end_date . " " . $end_time);
		$ics_start_datetime = $DT_start_datetime;
		$ics_end_datetime = $DT_end_datetime;
		$path_parts = pathinfo(__FILE__);
		$tans = json_decode(PhpToIcs($path_parts['dirname'] . "\\", $ics_id, $ics_title, $ics_description, $ics_start_datetime, $ics_end_datetime, $plots_address), true);
		$ics_file = "";
		if ($tans["result"] == 1) {
			$ics_file = $tans["file_name"];
		}

		include_once("/../main_api/Insert_PushNotificationLogFunc.php");
		/*if ($m_bkl_sendpush == "1" && $todays_gov_farekind_parkinspace->get_fare_id() != "3") {//計次不用推播提醒
			$pnl_title = "路邊停車自動加簽提醒";
			$pnl_msg1 = "自動加簽";
			if ($is_pmq == 1) {
				$pmq_push_datetime = new DateTime();
				$pmq_push_datetime->modify("+3 min");
				$push_appear_datetime = $pmq_push_datetime->format('Y-m-d H:i:s');
				$pnl_title = "路邊停車續費延時提醒";
				$pnl_msg1 = "到時";

			}
			$DT_next_signup_start_datetime = new DateTime($end_date . " " . $end_time);
			$DT_next_signup_end_datetime = new DateTime($end_date . " " . $end_time);
			$DT_next_signup_end_datetime->modify("+30 min");
			$next_end_date = $DT_next_signup_end_datetime->format("Y-m-d");
			$next_end_time = $DT_next_signup_end_datetime->format("H:i:s");
			$push_appear_datetime = $date->format('Y-m-d H:i:s');
			$tmp = $todays_gov_farekind_parkinspace->GovTimeComprasion($end_date, $next_end_date, $end_time, $next_end_time);
			if ($tmp["waiting_status"] == 2 && $tmp["waiting_min"] == 0) {
				//如果接下來的車位可停才可發送加簽推播
				//路邊不用加簽推播了
				//$ttans=json_decode(Insert_PushNotificationLogFunc("INSERT PUSH NOTIFICATION LOG",$token,$pnl_title,"車號: ".$m_ve_plate_no." ".$gov_start_address." 至 ".$gov_end_address." 車位編號: ".$m_pk_number."號 將在10分鐘後".$pnl_msg1."，若您已出場請按下出場按鈕，謝謝。",$push_appear_datetime,$m_bkl_id,json_encode(array("AutoSignUp"=>$m_bkl_id,"BookType"=>$book_type))),true);
			}
		}*/
		@unlink($ics_file);
	}
	//300 301 302都不要發信
	elseif ($this_parking_lot->get_m_plots_type() == "0") {
		#推波+寄信給錢後四小時的會員
		$push_to_self_text = "";
		#前4小時有預約
		$sql = "SELECT timestampdiff(minute,CONCAT(m_bkl_end_date,' ',m_bkl_end_time),'" . $start_date . " " . $start_time . "') as tmp,m_bkl_id  FROM tb_Member_ParkingSpace_Booking_Log WHERE CONCAT(m_bkl_end_date,' ',m_bkl_end_time) < '" . $start_date . " " . $start_time . "' AND timestampdiff(minute,CONCAT(m_bkl_end_date,' ',m_bkl_end_time),'" . $start_date . " " . $start_time . "')<(4*60) AND m_bkl_cancel='0' AND m_pk_id='" . $m_pk_id . "' ORDER BY tmp LIMIT 0,1";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			$tans = EmailTemplate($conn, "易停網", $G_SYSTEM_ERROR_RECEIVE_EMAIL, "易停網 付款錯誤 請盡速處理", "#403<br>" . $receive_data);
			return json_encode(array("result" => 0, "title" => "查詢錯誤:423", "description" => mysql_error()));
		}
		elseif (mysql_num_rows($result) == 0) {

		}
		else {
			$ans = mysql_fetch_assoc($result);
			$after_booking_log = new UPK_BookingLog($ans["m_bkl_id"]);
			$after_booking_log->GetParkingLog();
			if ((!isset($after_booking_log->parking_log) || $after_booking_log->parking_log->get_m_pl_end_time() == null || $after_booking_log->parking_log->get_m_pl_end_time() == "") && $after_booking_log->get_m_ve_m_id() != $upk_mid) {
				$after_push_text = "有其他會員將於 " . $start_date . " " . substr($start_time, 0, -3) . " 進場，請準時出場以免影響他人權益！\n位於您 " . $this_parking_lot->get_m_plots_name() . " " . $after_booking_log->get_m_bkl_start_date() . " " . $after_booking_log->get_m_bkl_start_time() . " ~ " . $after_booking_log->get_m_bkl_end_date() . " " . $after_booking_log->get_m_bkl_end_time() . " 的預約";
				EmailTemplate_m_id($conn, $after_booking_log->get_m_ve_m_id(), "易停網 接近預約提醒信", nl2br($after_push_text));
				if ($push_to_self_text == "") {
					$push_to_self_text .= "該車位於";
				}
				else $push_to_self_text .= "且";
				$after_booking_datetime = new DateTime($after_booking_log->get_m_bkl_end_date() . " " . $after_booking_log->get_m_bkl_end_time());
				$after_booking_datetime_pluse15 = new DateTime($after_booking_log->get_m_bkl_end_date() . " " . $after_booking_log->get_m_bkl_end_time());
				$after_booking_datetime_pluse15->modify("+15 min");
				$push_to_self_text .= " " . $after_booking_datetime->format("Y-m-d H:i") . " 有車輛出場。若您有要提早入場，請於 " . $after_booking_datetime_pluse15->format("Y-m-d H:i") . "後。操作方式：由「進出控制」中「提早進場」";
				//若您有要提早入場，請於2018-03-09 08:45後。操作方式：由「進出控制」中「提早進場」
				PushTo($conn, $after_booking_log->get_m_ve_m_id(), "易停網 接近預約通知", ($after_push_text), "", array(), true, $m_bkl_id,$merged_file_logs);
			}
		}
		#後4小時有預約
		$sql = "SELECT timestampdiff(minute,'" . $end_date . " " . $end_time . "',CONCAT(m_bkl_end_date,' ',m_bkl_end_time)) as tmp,m_bkl_id FROM tb_Member_ParkingSpace_Booking_Log WHERE CONCAT(m_bkl_start_date,' ',m_bkl_start_time) > '" . $end_date . " " . $end_time . "' AND timestampdiff(minute,'" . $end_date . " " . $end_time . "',CONCAT(m_bkl_start_date,' ',m_bkl_start_time))<(4*60) AND m_bkl_cancel='0' AND m_pk_id='" . $m_pk_id . "' ORDER BY tmp LIMIT 0,1";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "查詢錯誤:406", "description" => mysql_error()));
		}
		elseif (mysql_num_rows($result) == 0) {

		}
		else {
			$ans = mysql_fetch_assoc($result);
			$before_booking_log = new UPK_BookingLog($ans["m_bkl_id"]);
			$before_booking_log->GetParkingLog();
			if ((!isset($before_booking_log->parking_log) || $before_booking_log->parking_log->get_m_pl_end_time() == null || $before_booking_log->parking_log->get_m_pl_end_time() == "") && $before_booking_log->get_m_ve_m_id() != $upk_mid) {

				$before_booking_datetime = new DateTime($before_booking_log->get_m_bkl_end_date() . " " . $before_booking_log->get_m_bkl_end_time());
				$before_booking_datetime_pluse15 = new DateTime($end_date . " " . $end_time);
				$before_booking_datetime_pluse15->modify("+15 min");
				$before_push_text = "有其他會員將於 " . $end_date . " " . substr($end_time, 0, -3) . " 出場。\n位於您 " . $this_parking_lot->get_m_plots_name() . " " . $before_booking_log->get_m_bkl_start_date() . " " . $before_booking_log->get_m_bkl_start_time() . " ~ " . $before_booking_log->get_m_bkl_end_date() . " " . $before_booking_log->get_m_bkl_end_time() . " 的預約。\n若您有要提早入場，請於 " . $before_booking_datetime_pluse15->format("Y-m-d H:i") . "後。操作方式：由「進出控制」中「提早進場」";
				EmailTemplate_m_id($conn, $before_booking_log->get_m_ve_m_id(), "易停網 接近預約提醒信", nl2br($before_push_text));
				if ($push_to_self_text == "") {
					$push_to_self_text .= "該車位於";
				}
				else $push_to_self_text .= "且";
				$push_to_self_text .= " " . $before_booking_log->get_m_bkl_start_date() . " " . $before_booking_log->get_m_bkl_start_time() . " 有車輛進場，請準時出場以免影響他人權益！";
				PushTo($conn, $before_booking_log->get_m_ve_m_id(), "易停網 接近預約通知", ($before_push_text), "", array(), true, $m_bkl_id,$merged_file_logs);
			}
		}
		include_once("/../main_api/Insert_PushNotificationLogFunc.php");
		if ($push_approach_datetime < $now_date_time)
			$ttans = json_decode(Insert_PushNotificationLogFunc("INSERT PUSH NOTIFICATION LOG", $token, "進場提醒", "提醒您，" . $push_pkspace_text . "請於預約開始時間".$push_approach_datetime."後進場", $push_approach_datetime, $m_bkl_id,"",$merged_file_logs), true);
		else //現在在15分內直接推播
			PushTo($conn, $upk_mid, "進場提醒", "提醒您: " . $push_pkspace_text . "進場提醒", "", array(), true, $m_bkl_id,$merged_file_logs);
		$ttans = json_decode(Insert_PushNotificationLogFunc("INSERT PUSH NOTIFICATION LOG", $token, "出場提醒", "提醒您，" . $push_pkspace_text . "請於預約結束時間前出場", $push_appear_datetime, $m_bkl_id,"",$merged_file_logs), true);

		if ($push_to_self_text != "")
			PushTo($conn, $upk_mid, "接近預約提醒", $push_to_self_text . "位於您 " . $start_date . " " . substr($start_time, 0, -3) . " ~ " . $end_date . " " . substr($end_time, 0, -3) . " 的預約", "", array(), true, $m_bkl_id,$merged_file_logs);
		$str_booking = "預約";
		$str_booking_space = "預約";
		$str_shared = "共享";
		if ($this_parking_lot->get_m_plots_type() == "300" || $this_parking_lot->get_m_plots_type() == "301") {
			$str_booking = "";
			$str_booking_space = "　　";
			$str_shared = "";
		}
		$email_body_user = "感謝您使用易停網，以下是您" . $str_booking . "停車的詳細資料，請妥善保存。";
		$email_body_owner = "感謝您使用易停網，以下是您的" . $str_shared . "車位被" . $str_booking . "停車的詳細資料，請妥善保存。";
		$email_body2 = "<br><br>"
			. "　車牌號碼: " . $m_ve_plate_no . "<br>"
			. "停車場名稱: " . $this_parking_lot->get_m_plots_name() . "<br>"
			. "　　　地址: " . $plots_address . "<br>"
			//	."　地址說明: ".$this_parking_lot->get_m_plots_address_description()."<br>"
			//	."　連絡電話: ".$this_parking_lot->get_m_plots_phone_number()."<br>"
			//	."　　　說明: ".$this_parking_lot->get_m_plots_description()."<br>"
			//	."　社區名稱: ".$this_parking_lot->get_m_plots_community_name()."<br><br>"
			. $floor_number_text
			. "　" . $str_booking_space . "時段: " . $start_date . " " . substr($start_time, 0, -3) . " ~ " . $end_date . " " . substr($end_time, 0, -3) . "<br>"
			. "　抵用點數: " . $m_pyl_point_free . "(付費點數: " . $paid_parking_point . ")<br>"
			. "　付費金額: " . $m_pyl_point . "元<br>"
			//."　預約時間: ".substr($m_bkl_create_datetime,0,-3)."<br>"//$m_bkl_create_datetime
			//."　交易時間: ".$m_dpt_pay_datetime."<br>"
			//			."停車場編號: ".$m_plots_id."<br>"
			//			."　車位編號: ".$m_pk_id."<br>"
			//			."空位時段號: ".$m_ppl_id."<br>"
			//			."　預約編號: ".$m_bkl_id."<br>"
			. "　" . $str_booking_space . "編號: " . $szMerchantTradeNo . "<br>"
			. $push_to_self_text
			. "<br>";
		$email_body = $email_body_user . $email_body2;

		$ics_file = "";
		$ics_id = $m_bkl_id;
		$ics_title = "" . $str_booking . "停車";
		$ics_description = $email_body2;
		$DT_start_datetime = new DateTime($start_date . " " . $start_time);
		$DT_end_datetime = new DateTime($end_date . " " . $end_time);
		$ics_start_datetime = $DT_start_datetime;
		$ics_end_datetime = $DT_end_datetime;
		$path_parts = pathinfo(__FILE__);
		$tans = json_decode(PhpToIcs($path_parts['dirname'] . "\\", $ics_id, $ics_title, $ics_description, $ics_start_datetime, $ics_end_datetime, $plots_address), true);
		$ics_file = "";
		if ($tans["result"] == 1) {
			$ics_file = $tans["file_name"];
		}
		$tans = EmailTemplate($conn, $last_name . " " . $first_name, $email, "易停網 " . $str_booking . "停車通知", $email_body, 0, $ics_file);
		$email_body3 = $email_body_owner . $email_body2;
		if ($m_pk_owner_m_id != null) {
			$sql = "SELECT m_first_name,m_last_name,m_email FROM tb_Member WHERE m_id='" . $m_pk_owner_m_id . "' ";
			$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
			if (!$ans) {
				$tans = EmailTemplate($conn, "易停網", $G_SYSTEM_ERROR_RECEIVE_EMAIL, "易停網 " . $str_booking . "付款錯誤 請盡速處理", "#377<br>" . $receive_data);
				return json_encode(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error()));
			}
			$m_owner_lastname = $ans["m_last_name"];
			$m_owner_firstname = $ans["m_first_name"];
			$m_owner_email = $ans["m_email"];
			$tans = EmailTemplate($conn, $m_owner_lastname . " " . $m_owner_firstname, $m_owner_email, "易停網 車位被" . $str_booking . "停車通知", $email_body3, 0, $ics_file);
		}
		if ($m_pk_agent_m_id != null) {
			$sql = "SELECT m_first_name,m_last_name,m_email FROM tb_Member WHERE m_id='" . $m_pk_agent_m_id . "' ";
			$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
			if (!$ans) {
				return json_encode(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error()));
			}
			$m_agent_lastname = $ans["m_last_name"];
			$m_agent_firstname = $ans["m_first_name"];
			$m_agent_email = $ans["m_email"];
			if($m_agent_email != "")
				$tans = EmailTemplate($conn, $m_agent_lastname . " " . $m_agent_firstname, $m_agent_email, "易停網 車位被" . $str_booking . "停車通知", $email_body3, 0, $ics_file);
		}
		@unlink($ics_file);
	}
	elseif ($this_parking_lot->get_m_plots_type() == "301") {
		//如果沒有沒有設備可以控制的時候，則繳費=出場
		$in_or_out = $this_booking_log->get_m_bkl_in_or_out();
		if ($in_or_out == "1" || $in_or_out == "-1") {
			$sql = "SELECT * FROM tb_Device_Mqtt WHERE dmqt_type_id='" . $this_parking_lot->get_m_plots_id() . "' AND dmqt_delete='0' ";
			$result = mysql_query($sql, $conn);
			if (!$result) {
				return json_encode(array("result" => 0, "title" => "控制設備失敗", "description" => mysql_error()));
			}
			else if (mysql_num_rows($result) == 0) {
				$in_or_out = "2";
			}
		}
		if ($in_or_out == "2") {

			$this_booking_log->GetParkingLog();
			$this_parking_log = $this_booking_log->parking_log;
			//已經有出場時間者不要出場
			if (isset($this_parking_log) && $this_parking_log != null && $this_parking_log->get_m_pl_end_time() != null) {

			}
			else {
				//如果之前是掃出場付款的話付款後要自動出場
				include_once("/../main_api/AppearReturnFunc.php");
				AppearReturn301Func($conn, $upk_mid, $this_booking_log->get_m_bkl_id());
			}
		}
	}
	elseif ($this_parking_lot->get_m_plots_type() == "302") {
		if($this_parking_lot->get_m_plots_is_lpr_tower()=="1") {
			//繳費=出場
			$this_booking_log->GetParkingLog();
			$this_parking_log = $this_booking_log->parking_log;
			//已經有出場時間者不要出場
			if (isset($this_parking_log) && $this_parking_log != null && $this_parking_log->get_m_pl_end_time() != null) {

			}
			else {
				//如果是出場付款的話付款後要自動出場
				include_once("/../main_api/AppearReturnFunc.php");
				AppearReturn302Func($conn, $upk_mid, $this_booking_log->get_m_bkl_id());
			}
		}
	}

	if ($m_plots_type == "100") {
	}
	else if ($m_plots_type == "300") {
	}
	else if ($m_plots_type == "301") {
	}
	else if ($m_plots_type == "302") {
		pushAsByPlot($upk_mid, $this_parking_space->get_m_plots_id());
	}
	else if ($m_plots_type == "303") {
	}//3xx的時候因為都已經進場不推店家,Luke:施總說路邊停車不推店家
	elseif ($m_plots_type == "0") {
		pushAsByPlot($upk_mid, $this_parking_space->get_m_plots_id());
	}
	$sql = "SELECT pmq_sn FROM tb_ParkingMeter_Qrcode WHERE m_bkl_id='" . $this_booking_log->get_m_bkl_id() . "' ";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return json_encode(array("result" => 0, "title" => "搜尋QRCODE錯誤", "description" => mysql_error($conn)));
	}
	if (mysql_num_rows($result) > 0 || $this_parking_lot->get_m_plots_type() == 300) {
		//如果這個預約ID存在於咪表咪表
		$send_to_meter_array = array(
			"DeviceInfo" => array(
				"uniqno" => "52307327",
				"supply" => "52307327",
				"system" => "PM",
				"event" => "102",
				"status" => "001",
				"feedback" => "Y",
				"etime" => $now_date_time->format("YmdHis"),
				"token" => "630cb222-0a42-dee7-0838-b698ce86d1ff",
				"message" => array("result" => "1", "points" => $this_booking_log->get_m_bkl_estimate_point(),
					"parking_space_id" => $this_booking_log->get_m_pk_id(),
					"booking_id" => $this_booking_log->get_m_bkl_id(),
					"bay" => "0",
					"start_time" => $DT_start_datetime->format("YmdHi"),
					"end_time" => $DT_end_datetime->format("YmdHi")
				)
			)
		);
		$send_to_meter_json = json_encode($send_to_meter_array);
		$result = PostJsonTo("http://54.95.177.240/FEP/fep/dataReceive.action", $send_to_meter_array);
		$tans = json_decode($result, true);
		if (isset($tans["result"]) && $tans["result"] == 1) {
			rg_activity_log($conn, $upk_mid, "傳送至咪表成功", "預約ID: " . $this_booking_log->get_m_bkl_id(), $send_to_meter_json, $result);
		}
		else {
			rg_activity_log($conn, $upk_mid, "傳送至咪表失敗", "預約ID: " . $this_booking_log->get_m_bkl_id(), $send_to_meter_json, $result);
		}
	}
	rg_activity_log($conn, $upk_mid, "ReceiveSendMessage", "結束".__LINE__.$m_bkl_id, "", "");
	MemcacheSetBookingLog('_UPK_BookingLog:', $m_bkl_id);
	return json_encode(array("result" => 1));
}
?>