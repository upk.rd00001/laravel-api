<?php

use App\Http\Enum\CONST_DBPG_TYPE;
use App\Http\Enum\CONST_M_PD_METHOD;

include_once("exception.php");
include_once("fn_package2.php");
include_once("enum.php");
include_once("upkclass/UPKclass.php");
include_once("taoyuanpolygonfunc.php");
include_once("taipeipolygonfunc.php");
include_once("template/email_template.php");
 date_default_timezone_set("Asia/Taipei");
#create random string
#$random_str = GenerateRandomString(10, '0123456789abcdefghijklmnopqrstuvwxyz');
function GenerateRandomString($length, $characters) 
{ 
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; ++$i) 
	{
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>
<?php
#create GUID
#$GUID_str = GenerateGuid();
function GenerateGuid() 
{
	$md5out = md5(uniqid(mt_rand(), true)); 

	$hyphen = chr(45);// "-" 
	$uuid = substr($md5out, 0, 8).$hyphen 
	.substr($md5out, 8, 4).$hyphen 
	.substr($md5out,12, 4).$hyphen 
	.substr($md5out,16, 4).$hyphen 
	.substr($md5out,20,12);
	return $uuid; 
}
?>
<?php
#add string at the beginning of original string
#Sn2Id($begin_str, $origin_str);
function Sn2Id($header, &$sn, $len=10)
{
	$j = strlen($sn);
	for($j;$j < $len;++$j)
	{
		$sn = "0".$sn;
	}
	$sn = $header.$sn;
}
?>
<?php
#get the data from DB tb_SystemCode
#$content_arr = GetSystemCode($sc_sn, $sc_language, $DB_connect);
function GetSystemCode($sn, $lg, $conn)
{
	//$id = "SCID0000000000";
	if ($lg == null)
		$lg = "zh-tw";

	$header = "SCID";
	Sn2Id($header, $sn);
	$sql = "SELECT sc_id, sc_title, sc_description FROM tb_SystemCode WHERE sc_id='" . $sn . "' and sc_language='" . $lg . "'";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		$lg = "zh-tw";
		$sql = "SELECT sc_id, sc_title, sc_description FROM tb_SystemCode WHERE sc_id='SCID0000000001' and sc_language='" . $lg . "'";
		$tans = mysql_fetch_assoc(mysql_query($sql, $conn));
		$ans = array($tans["sc_id"], $tans["sc_title"], $tans["sc_description"] . ",ErrorCode-lng00002");
	} else if (mysql_num_rows($result) == 0) {
		$lg = "zh-tw";
		$sql = "SELECT sc_id, sc_title, sc_description FROM tb_SystemCode WHERE sc_id='" . $sn . "' and sc_language='" . $lg . "'";
		$tans = mysql_fetch_assoc(mysql_query($sql, $conn));
		$ans = array($tans["sc_id"], $tans["sc_title"], $tans["sc_description"] . ",ErrorCode-lng00001");
	} else {
		$tans = mysql_fetch_assoc(mysql_query($sql, $conn));
		$ans = array($tans["sc_id"], $tans["sc_title"], $tans["sc_description"]);
	}
	return $ans;
}
?>
<?php
#write the data to DB tb_Member_Activity_Log
#rg_activity_log($DB_connect, $m_id, $m_act_type, $m_act_fail_reason, $m_act_data_app_to_web, $m_act_data_web_to_db);
//aw_data = app to web data (input)
//wd_data = web to db data (output)
function rg_activity_log($conn, $id, $type, $reason, $aw_data, $wd_data)
{
	global $conn,$dbName;
	check_conn($conn,$dbName);
	if ($id == "") $id = "(NoID)";
	if ($type == "") $type = "(NoType)";
	if ($reason == "") $reason = "(NoReason)";
	if ($aw_data == "") $aw_data = "(NoInput)";
	if ($wd_data == "") $wd_data = "(NoOutput)";
	if (is_array($aw_data)) $aw_data = json_encode($aw_data,JSON_UNESCAPED_UNICODE);
	elseif(gettype($aw_data)=="string") $aw_data = unicodeTool::unicodeDecode($aw_data);
	if (is_array($wd_data)) $wd_data = json_encode($wd_data,JSON_UNESCAPED_UNICODE);
	elseif(gettype($wd_data)=="string") $wd_data = unicodeTool::unicodeDecode($wd_data);
	$trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 5);
	$trace_arr = array();
	foreach ($trace as $each_arr) {
		if(isset($each_arr["args"]) && is_array($each_arr["args"])) {
			foreach ($each_arr["args"] as $i => $v)
				if (is_resource($v))
					$each_arr["args"][$i] = (string)$v . ' (' . get_resource_type($v) . ')';
			array_push($trace_arr, $each_arr);
		}
	}
	$trace_json = json_encode($trace_arr,JSON_UNESCAPED_UNICODE);
	$ip = $_SERVER['REMOTE_ADDR'];
	//$output_json=json_encode(array("result"=>2, "token"=>$token, "member_id"=>$ans["m_id"]));

	$exe_time = time() + microtime() - $_SERVER["REQUEST_TIME_FLOAT"];
	if (IsDebug())
		$sql = "INSERT INTO tb_Member_Activity_Log 
			(m_id, m_act_type, m_act_fail_reason, m_act_data_app_to_web, m_act_data_web_to_db, m_act_user_ip, m_act_record_time,m_act_backtrace,m_act_duration,m_act_cpu_usage) 
			VALUES 
			('" . $id . "', '" . $type . "', '" . $reason . "', '" .
			mysql_real_escape_string(substr(($aw_data), 0, 2000)) . "', '" .
			mysql_real_escape_string(substr(($wd_data), 0, 2000)) . "', '" . $ip . "', now(),'" . $_SERVER["REQUEST_URI"] .
			mysql_real_escape_string(substr(($trace_json), 0, 2000)) . "','" . $exe_time . "','')";
	else
		$sql = "INSERT INTO tb_Member_Activity_Log 
			(m_id, m_act_type, m_act_fail_reason, m_act_data_app_to_web, m_act_data_web_to_db, m_act_user_ip, m_act_record_time,m_act_backtrace) 
			VALUES 
			('" . $id . "', '" . $type . "', '" . $reason . "', '" .
			mysql_real_escape_string(substr(($aw_data), 0, 2000)) . "', '" . 
			mysql_real_escape_string(substr(($wd_data), 0, 1000)) . "', '" . $ip . "', now(),'" . $_SERVER["REQUEST_URI"] .
			mysql_real_escape_string(substr(($trace_json), 0, 2000)) . "')";

	$result = mysql_query($sql, $conn);
	//DB表內設定 varchar 2000 與 varchar 1000 backtrace則是text
	if (!$result) {
		return json_encode(array("result" => 0, "title" => "紀錄activity log失敗", "description" => mysql_error()));
	}
	else {
		return json_encode(array("result" =>1));
	}
}
?>
<?php
#post the data in json
#$return_str = PostJsonTo($url_str, $content_arr);
function PostJsonTo($target, $content_array)
{
	$data = json_encode($content_array);
	$ch = curl_init($target);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json; charset=utf-8',
		'Content-Length: '.strlen($data)));
	return curl_exec($ch);
}
?>
<?php
#get the data from DB tb_Uparking_Settings
#$return_parameter = GetSystemParameter($DB_connect, $ups_name);
function GetSystemParameter($conn, $Parameter)
{
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$sql = "SELECT ups_value, ups_unit FROM tb_Uparking_Settings WHERE ups_name='".$Parameter."' ORDER BY ups_create_time DESC";
	$tans = mysql_fetch_assoc( mysql_query($sql, $conn) );
	if($tans["ups_unit"]==1)
	{
		return (int)$tans["ups_value"];
	}
	if($tans["ups_unit"]==2)
	{
		return (float)$tans["ups_value"];
	}
	if($tans["ups_unit"]==3)
	{
		return (bool)$tans["ups_value"];
	}
	else
	{
		return (string)$tans["ups_value"];
	}
}
?>
<?php 

function SendEmail_Url($conn, $activity, $sndname, $sendto, $title, $content){
$from = "service@spacr4car.com";
$bccEmailAddress = "lukule@spacr4car.com";
$_headers = "";
//$_headers .= "MIME-Version: 1.0\r\n";
//$_headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
$_headers .= "From: $from\r\n";
//$_headers .= "To: ".$sendto."\r\n";
$_headers .= "BCC: ".$bccEmailAddress."\r\n";


$_headers .= "Reply-To: $from\r\n";
$_headers .= "X-Priority: 1\r\n";
$_headers .= "X-MSMail-Priority: High\r\n";
$_headers .= "X-Mailer: Hero Network Email System";

$mailresponse = mail($sendto,
					$title,
					$content,
					 $_headers
					);
}
?><?php
function SendEmail($conn, $activity, $sndname, $sendto, $title, $content, $is_verify=0,$file_name="")
{	
	$id="";
	if(IsDebug()){
		$title="測試版信息: ".$title;
	}
	$content = unicodeTool::unicodeDecode($content);
	$G_GMAIL_SMTPSecure = GetSystemParameter($conn, "gmail_smtp_secure");
	$G_GMAIL_HOST = GetSystemParameter($conn, "gmail_host");
	$G_GMAIL_PORT = GetSystemParameter($conn, "gmail_port");
	$G_GMAIL_ACCOUNT = GetSystemParameter($conn, "sender_email_username");
	$G_GMAIL_PASSWORD = GetSystemParameter($conn, "sender_email_password");
	$G_SYSTEM_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
	#系統信直接發
	//如果成立就要直接發信，不要判斷email是否已驗證
	if($G_SYSTEM_EMAIL==$sendto) {
	}
	elseif($sendto=="upk.rd00001@gmail.com") {
	}
	elseif(!IsDebug() && $sendto=="space4car688@gmail.com") {
	}
	else {
		
		$sql="SELECT m_id,m_email_verify FROM tb_Member WHERE m_email='".$sendto."' AND m_email_verify='1'";
		$result = mysql_query($sql, $conn);
		if(! $result)
		{
			return json_encode(array("result"=>0, "title"=>"搜尋會員ID失敗", "description"=> mysql_error($conn)));
		}
		else if(mysql_num_rows($result) == 0)
		{	
			$id="";
			$m_email_verify=0;
		}
		else if(mysql_num_rows($result) == 1)
		{	
			$ans = mysql_fetch_assoc($result);
			$id=$ans["m_id"];
			$m_email_verify=$ans["m_email_verify"];
		}
		else {
			$id="";
			$m_email_verify=1;
			//信箱有重複但是有驗證 不管一樣發
			/*
			rg_activity_log($conn, $id, "Email發信", "發信失敗(信箱重複)", $title, json_encode(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content)));
			//SendEmail($conn,$activity,"易停網營運團隊",$G_SYSTEM_EMAIL,"Email發信失敗(未認證)",print_r(array("sendto"=>$sendto, "content"=>$content)),true));
			SendEmail($conn,$activity,"易停網營運團隊","upk.rd00001@gmail.com","Email發信失敗(信箱重複)",print_r(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content),true));
			return;*/
		}
		if(($is_verify==0 && $m_email_verify==0)){
			//如果為$is_verify=1 則不判斷該email是不是以驗證 (發送驗證email驗證信)
			//未驗證retrun
			if($sendto!=null){
				rg_activity_log($conn, $id, "Email發信", "發信失敗(未認證)", $title, json_encode(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content)));
				//SendEmail($conn,$activity,"易停網營運團隊",$G_SYSTEM_EMAIL,"Email發信失敗(未認證)",print_r(array("sendto"=>$sendto, "content"=>$content)),true));
				SendEmail($conn,$activity,"易停網營運團隊","upk.rd00001@gmail.com","Email發信失敗(未認證)",print_r(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content),true));
			}else{
				rg_activity_log($conn, $id, "Email發信", "發信失敗(未填寫)", $title, json_encode(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content)));
				//SendEmail($conn,$activity,"易停網營運團隊",$G_SYSTEM_EMAIL,"Email發信失敗(未認證)",print_r(array("sendto"=>$sendto, "content"=>$content)),true));
				SendEmail($conn,$activity,"易停網營運團隊","upk.rd00001@gmail.com","Email發信失敗(未填寫)",print_r(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content),true));
				
			}
			return;
		}
	}
	
	$language = "zh-tw";
	if($sndname==null)$sndname="易停網會員";
	if($activity == null || $sndname==null || $sendto==null || $title==null || $content==null)
	{
		rg_activity_log($conn, $id, "發信失敗", "必填欄位未填", $sendto.$title.$content, '' );
		$ans = GetSystemCode("1", $language, $conn);
		//mysql_close($conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2].$activity.$sndname.$sendto.$title.$content));
	}
	date_default_timezone_set('Etc/UTC');

	require_once 'PHPMailerAutoload.php';

	$mail = new PHPMailer;

	$mail->isSMTP();

	$mail->SMTPDebug = 0; //2;
	$mail->Debugoutput = 'html';

	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 465;

	$mail->SMTPSecure = 'ssl';

	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;

	$mail->SMTPOptions = array(
	     'ssl' => array(
	     'verify_peer' => false,
	     'verify_peer_name' => false,
	     'allow_self_signed' => true
	     )
	    );
	$mail->IsHTML(true);
	$mail->CharSet = "utf-8";
	$mail->Username = $G_GMAIL_ACCOUNT;
	$mail->Password = $G_GMAIL_PASSWORD;
	$mail->setFrom($G_GMAIL_ACCOUNT, '易停網營運團隊');
	$mail->addAddress($sendto, $sndname);
	
	//$addressBCC = "service@space4car.com";
	//$mail->AddBCC($addressBCC, 'System Account');
	$mail->Subject = $title;
	$mail->msgHTML($content);
	//$mail->AltBody = $content;

	//Attach an image file
	//$mail->addAttachment('VerifyEmailpic1.png');
	if($file_name!="")
		$mail->AddAttachment($file_name);
	
	if (!$mail->send()) {
	    rg_activity_log($conn, $id, "Email發信失敗", $mail->ErrorInfo, $title, $content);
		SendEmail_Backup($conn, $activity, $sndname, $sendto, $title, $content, $is_verify,$file_name);
	    return json_encode(array("result"=> 0,"title"=>"Mailer Error","description"=>$mail->ErrorInfo));
	} else {
	    //echo "Message sent!";
	}
	rg_activity_log($conn, $id, "Email發信", "發信成功", $title, json_encode(array("sendto"=>$sendto, "content"=>$content)));
	return json_encode(array("result"=>1));
}


function SendEmail_Backup($conn, $activity, $sndname, $sendto, $title, $content, $is_verify=0,$file_name="")
{	
	$id="";
	if(IsDebug()){
		$title="測試版信息: ".$title;
	}
	$content = unicodeTool::unicodeDecode($content);
	$G_GMAIL_SMTPSecure = GetSystemParameter($conn, "gmail_smtp_secure");
	$G_GMAIL_HOST = GetSystemParameter($conn, "gmail_host");
	$G_GMAIL_PORT = GetSystemParameter($conn, "gmail_port");
	$G_GMAIL_ACCOUNT = "mail@space4car.com";
	$G_GMAIL_PASSWORD = "uparking2016";
	$G_SYSTEM_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
	#系統信直接發
	//如果成立就要直接發信，不要判斷email是否已驗證
	if($G_SYSTEM_EMAIL==$sendto) {
	}
	elseif($sendto=="upk.rd00001@gmail.com") {
	}
	elseif(!IsDebug() && $sendto=="space4car688@gmail.com") {
	}
	else {
		
		$sql="SELECT m_id,m_email_verify FROM tb_Member WHERE m_email='".$sendto."' AND m_email_verify='1'";
		$result = mysql_query($sql, $conn);
		if(! $result)
		{
			return json_encode(array("result"=>0, "title"=>"搜尋會員ID失敗", "description"=> mysql_error($conn)));
		}
		else if(mysql_num_rows($result) == 0)
		{	
			$id="";
			$m_email_verify=0;
		}
		else if(mysql_num_rows($result) == 1)
		{	
			$ans = mysql_fetch_assoc($result);
			$id=$ans["m_id"];
			$m_email_verify=$ans["m_email_verify"];
		}
		else {
			$id="";
			$m_email_verify=1;
			//信箱有重複但是有驗證 不管一樣發
			/*
			rg_activity_log($conn, $id, "Email發信", "發信失敗(信箱重複)", $title, json_encode(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content)));
			//SendEmail($conn,$activity,"易停網營運團隊",$G_SYSTEM_EMAIL,"Email發信失敗(未認證)",print_r(array("sendto"=>$sendto, "content"=>$content)),true));
			SendEmail($conn,$activity,"易停網營運團隊","upk.rd00001@gmail.com","Email發信失敗(信箱重複)",print_r(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content),true));
			return;*/
		}
		if(($is_verify==0 && $m_email_verify==0)){
			//如果為$is_verify=1 則不判斷該email是不是以驗證 (發送驗證email驗證信)
			//未驗證retrun
			if($sendto!=null){
				rg_activity_log($conn, $id, "Email發信", "發信失敗(未認證)", $title, json_encode(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content)));
				//SendEmail($conn,$activity,"易停網營運團隊",$G_SYSTEM_EMAIL,"Email發信失敗(未認證)",print_r(array("sendto"=>$sendto, "content"=>$content)),true));
				SendEmail($conn,$activity,"易停網營運團隊","upk.rd00001@gmail.com","Email發信失敗(未認證)",print_r(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content),true));
			}else{
				rg_activity_log($conn, $id, "Email發信", "發信失敗(未填寫)", $title, json_encode(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content)));
				//SendEmail($conn,$activity,"易停網營運團隊",$G_SYSTEM_EMAIL,"Email發信失敗(未認證)",print_r(array("sendto"=>$sendto, "content"=>$content)),true));
				SendEmail($conn,$activity,"易停網營運團隊","upk.rd00001@gmail.com","Email發信失敗(未填寫)",print_r(array("m_id"=>$id,"sendto"=>$sendto, "content"=>$content),true));
				
			}
			return;
		}
	}
	
	$language = "zh-tw";
	if($sndname==null)$sndname="易停網會員";
	if($activity == null || $sndname==null || $sendto==null || $title==null || $content==null)
	{
		rg_activity_log($conn, $id, "寄信失敗", "必填欄位未填", "", "");
		$ans = GetSystemCode("1", $language, $conn);
		//mysql_close($conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2].$activity.$sndname.$sendto.$title.$content));
	}
	date_default_timezone_set('Etc/UTC');

	require_once 'PHPMailerAutoload.php';

	$mail = new PHPMailer;

	$mail->isSMTP();

	$mail->SMTPDebug = 0; //2;
	$mail->Debugoutput = 'html';

	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 465;

	$mail->SMTPSecure = 'ssl';

	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;

	$mail->SMTPOptions = array(
	     'ssl' => array(
	     'verify_peer' => false,
	     'verify_peer_name' => false,
	     'allow_self_signed' => true
	     )
	    );
	$mail->IsHTML(true);
	$mail->CharSet = "utf-8";
	$mail->Username = $G_GMAIL_ACCOUNT;
	$mail->Password = $G_GMAIL_PASSWORD;
	$mail->setFrom($G_GMAIL_ACCOUNT, '易停網營運團隊');
	$mail->addAddress($sendto, $sndname);
	
	//$addressBCC = "service@space4car.com";
	//$mail->AddBCC($addressBCC, 'System Account');
	$mail->Subject = $title;
	$mail->msgHTML($content);
	//$mail->AltBody = $content;

	//Attach an image file
	//$mail->addAttachment('VerifyEmailpic1.png');
	if($file_name!="")
		$mail->AddAttachment($file_name);
	
	if (!$mail->send()) {
	    rg_activity_log($conn, $id, "Email發信失敗", $mail->ErrorInfo, $title, $content);
	    return json_encode(array("result"=> 0,"title"=>"Mailer Error","description"=>$mail->ErrorInfo));
	} else {
	    //echo "Message sent!";
	}
	rg_activity_log($conn, $id, "Email發信", "發信成功", $title, json_encode(array("sendto"=>$sendto, "content"=>$content)));
	return json_encode(array("result"=>1));
}

?>
<?php
function PushToAndroid($registration_ids,$title1,$message,$html_string,$array_data=array(),$file_logs=array(),$this_url=null) {
	if($this_url==null || !is_object($this_url)){
		$this_url = new UPK_Url();
	}
	if(!isset($file_logs["result"]) || !isset($file_logs["data"])){
		//給空的話就丟file_logs的預設
		$file_logs["result"]=1;
		$file_logs["data"]=array();
	}
	//$API_KEY = "AIzaSyCdS6AG__wjKe4Bj_FiNMbW2OZmjdnlS8g"; 
	$API_KEY = "AIzaSyCExYYdo6yEeyYf4_Y_IGNS-pds0iQlYV4";
	//only for test branch
	$url = 'https://fcm.googleapis.com/fcm/send';
	
	$arr["to"]=$registration_ids;
	$arr["priority"]= "high";
	$array_data["title"]=$title1;
	$array_data["message"]=$message;
	$title1="";
	$message="";
	//if($title1!=""||$message!="")
	//	$arr["notification"]=array("title"=>$title1,"body"=>$message);
	$arr["data"]=array("html_string"=>$html_string,"file_logs_array"=>$file_logs["data"],"url_array"=>$this_url->get_array());
	if(is_array($array_data)){
		$arr["data"]=array_merge($arr["data"],$array_data);
	}
	
	$json=json_encode($arr);
	$headers = array(
            'Authorization: key='.$API_KEY,
            'Content-Type: application/json'
        );
		
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);	//
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
	$result = curl_exec($curl);
	curl_close($curl);
    return array("json"=>json_decode($json),"result"=>json_decode($result));
	 
}
//TODO: 與android的推波內容太重覆可以重構
function PushToIOS($registration_ids,$title1,$message,$html_string,$array_data=array(),$file_logs=array(),$this_url=null) {
	if($this_url==null || !is_object($this_url)){
		$this_url = new UPK_Url();
	}
	if(!isset($file_logs["result"]) || !isset($file_logs["data"])){
		//給空的話就丟file_logs的預設
		$file_logs["result"]=1;
		$file_logs["data"]=array();
	}
	//$API_KEY = "AIzaSyCdS6AG__wjKe4Bj_FiNMbW2OZmjdnlS8g"; 
	$API_KEY = "AIzaSyCExYYdo6yEeyYf4_Y_IGNS-pds0iQlYV4";
	//only for test branch
	$url = 'https://fcm.googleapis.com/fcm/send';
	$arr["to"]=$registration_ids;
	$arr["notification"]=array("title"=>$title1,"body"=>$message,"click_action"=>"EPCustomPush");
	$arr["data"]=array("html_string"=>$html_string,"file_logs_array"=>$file_logs["data"],"url_array"=>$this_url->get_array());
	$arr["content_available"]=true;
	$arr["mutable_content"]=true;
	if(is_array($array_data)){
		$arr["data"]=array_merge($arr["data"],$array_data);
	}
	
	$json=json_encode($arr);
	$headers = array(
            'Authorization: key='.$API_KEY,
            'Content-Type: application/json'
        );
		
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);	//
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
	$result = curl_exec($curl);
	curl_close($curl);
    return array("json"=>json_decode($json),"result"=>json_decode($result));
}
//type 	1=booking
//		2=pricing
//		3=parkingspace
//		4=parkinglot

function GetPushToFileLog($type,$id)
{
	if ($type == CONST_GET_PUSH_TO_FILELOG::BOOKING_LOG) {
		$this_booking_log = new UPK_BookingLog($id);
		$this_booking_log->GetPricingLog();
		$this_pricing_log = $this_booking_log->pricing_logs;
		$this_pricing_log->GetParkingSpace();
		$this_parking_space = $this_pricing_log->parking_space;
		$this_parking_space->GetParkingLot();
		$this_parking_lot = $this_parking_space->parking_lot;
		$tmp_parking_space_file_logs = $this_parking_space->get_file_logs_array();
		$tmp_parking_lot_file_logs = $this_parking_lot->get_file_logs_array();
		$merged_file_logs_array = array_merge($tmp_parking_space_file_logs, $tmp_parking_lot_file_logs);
		$merged_file_logs = array("result" => 1, "data" => $merged_file_logs_array);
	}
	elseif ($type == CONST_GET_PUSH_TO_FILELOG::PRICING_LOG) {
		$this_pricing_log = new UPK_PricingLog($id);
		$this_pricing_log->GetParkingSpace();
		$this_parking_space = $this_pricing_log->parking_space;
		$this_parking_space->GetParkingLot();
		$this_parking_lot = $this_parking_space->parking_lot;
		$tmp_parking_space_file_logs = $this_parking_space->get_file_logs_array();
		$tmp_parking_lot_file_logs = $this_parking_lot->get_file_logs_array();
		$merged_file_logs_array = array_merge($tmp_parking_space_file_logs, $tmp_parking_lot_file_logs);
		$merged_file_logs = array("result" => 1, "data" => $merged_file_logs_array);
	}
	elseif ($type == CONST_GET_PUSH_TO_FILELOG::PARKING_SPACE) {
		$this_parking_space = new UPK_ParkingSpace($id);
		$this_parking_space->GetParkingLot();
		$this_parking_lot = $this_parking_space->parking_lot;
		$tmp_parking_space_file_logs = $this_parking_space->get_file_logs_array();
		$tmp_parking_lot_file_logs = $this_parking_lot->get_file_logs_array();
		$merged_file_logs_array = array_merge($tmp_parking_space_file_logs, $tmp_parking_lot_file_logs);
		$merged_file_logs = array("result" => 1, "data" => $merged_file_logs_array);
	}
	elseif ($type == CONST_GET_PUSH_TO_FILELOG::PARKING_LOT) {
		$this_parking_lot = new UPK_ParkingLot($id);
		$tmp_parking_lot_file_logs = $this_parking_lot->get_file_logs_array();
		$merged_file_logs = array("result" => 1, "data" => $tmp_parking_lot_file_logs);
	}
	else {
		$merged_file_logs = array("result" => 1, "data" => array());
	}
	return $merged_file_logs;
}
//$send_device -1=所有裝置  0=Android 1=iOS 2=web 3=....
function PushTo($conn,$m_id,$title1,$message,$html_string="",$array_data=array(),$auto_insert=true,$m_bkl_id="",$file_logs=array(),$this_url=null,$send_device=-1) {
	if(IsDebug())
		$url = 'https://sandbox.api.space4car.com/sandbox_api/template/async_pushto.php';
	elseif(IsPreview())
		$url = 'https://api.space4car.com/preview_api/template/async_pushto.php';
	else
		$url = 'https://api.space4car.com/api/template/async_pushto.php';

	$arr["m_id"]=$m_id;
	$arr["title1"]=$title1;
	$arr["message"]=$message;
	$arr["html_string"]=$html_string;
	$arr["array_data"]=$array_data;
	$arr["auto_insert"]=$auto_insert;
	$arr["m_bkl_id"]=$m_bkl_id;
	$arr["file_logs"]=$file_logs;
	$arr["send_device"]=$send_device;
	if($this_url instanceof UPK_Url)
		$arr["url_array"]=$this_url->get_array();
	else $arr["url_array"]=array();
	$arr["this_url"]=$this_url;
	$headers = array('Content-Type: application/json');
	$json=json_encode($arr);
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);	//
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
	curl_setopt($curl, CURLOPT_TIMEOUT, 1);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
	$result = curl_exec($curl);
	curl_close($curl);
	return json_encode(array("json"=>$arr,"result"=>json_decode($result)));
}
function async_PushTo($conn,$m_id,$title1,$message,$html_string,$array_data=array(),$auto_insert=true,$m_bkl_id="",$file_logs=array(),$this_url=null,$send_device=-1){
	if($this_url==null || !is_object($this_url)){
		$this_url = new UPK_Url();
	}
	if(!isset($file_logs["result"]) || !isset($file_logs["data"])){
		//給空的話就丟file_logs的預設
		$file_logs["result"]=1;
		$file_logs["data"]=array();
	} else {
		$tmp_file_log_data=array();
		$i=0;
		//要秀預設圖片的話要先有圖片才換成原始圖片
		if(UPK_File_Logs::isDisplayDefaultId($m_id) && count($file_logs["data"])>0) {
			$this_file_logs = new UPK_File_Logs(CONST_FL_TYPE::DISPLAY_DEFAULT,"",$m_id);
			$tmp_file_log_data = $this_file_logs->get_array();
		}
		else {
			foreach ($file_logs["data"] as $each_file_log) {
				array_push($tmp_file_log_data, $each_file_log);
				$i++;
				if ($i >= 3) {
					break;
				}
			}
		}
		$file_logs["data"]=$tmp_file_log_data;
	}

	global $conn,$dbName;
	check_conn($conn,$dbName);
	if(IsDebug()){
		$title1="測試版信息: ".$title1;
	}
	if(!is_array($array_data)){//非法格式不送推撥
		include_once("/template/email_template.php");
		$G_SYSTEM_ERROR_RECEIVE_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
		$email_body="
		　　寄給：".$m_id."<br>
		　　抬頭：".$title1."<br>
		　　內容：".$message."<br>
		網頁內容：".$html_string."<br>
		陣列資料：".print_r($array_data,true)."<br>
		";
		$tans=EmailTemplate($conn,"易停網",$G_SYSTEM_ERROR_RECEIVE_EMAIL,"易停網 寄送推播失敗",$email_body);
		$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 寄送推播失敗",$email_body);
		return json_encode(array("result"=>0,"title"=>"寄送推播失敗","description"=>"格式不正確"));
	}
	//$array_data["title"]=$title1;
	//$array_data["message"]=$message;
	//$title1="";
	//$message="";
	$and_push = array();
	if($send_device==-1 || $send_device==0) {
		$sql = "SELECT m_pnk_push_key FROM tb_Member_Push_Notification_Key WHERE m_id='" . $m_id . "' AND m_pnk_mobile_type = 0  AND (is_delete=b'0' OR is_delete='0' ) ORDER BY m_pnk_datetime DESC LIMIT 5 ";//測試版is_delete似乎不能用is_delete='0'
//	$and_push["android_sql"]=$sql;
		$result_select = mysql_query($sql, $conn);
		while ($row = mysql_fetch_array($result_select)) {
			$and_push["android"] = PushToAndroid($row["m_pnk_push_key"], $title1, $message, $html_string, $array_data, $file_logs, $this_url);
		}
	}
	if($send_device==-1 || $send_device==1) {
		$sql = "SELECT m_pnk_push_key FROM tb_Member_Push_Notification_Key WHERE m_id='" . $m_id . "' and m_pnk_mobile_type = 1 AND (is_delete=b'0' OR is_delete='0' ) ORDER BY m_pnk_datetime DESC LIMIT 5 ";//測試版is_delete似乎不能用is_delete='0'
//	$and_push["ios_sql"]=$sql;
		$result_select = mysql_query($sql, $conn);
		while ($row = mysql_fetch_array($result_select)) {
			$and_push["ios"] = PushToIOS($row["m_pnk_push_key"], $title1, $message, $html_string, $array_data, $file_logs, $this_url);
		}
	}
	//已經傳送、未讀、現在時間
	//直接推送的時候時候加一筆，要在我的紀錄你面看到
	if($auto_insert) {
		$m_pnl_push_array="";
		if(count($array_data)>0){
			$m_pnl_push_array=json_encode($array_data);
		}
		$sql = "INSERT INTO `tb_Member_Push_Notification_Log`(`m_id`, `m_bkl_id`, `m_pnl_push_key`, `m_pnl_title`, `m_pnl_message`, `m_pnl_is_sent`, `m_pnl_send_datetime`, `m_pnl_push_array`, `m_pnl_is_read`, `m_pnl_file_logs`, `m_pnl_url`) VALUES
		('" . $m_id . "', '".$m_bkl_id."', '', '" . mysql_escape_string($title1) . "', '" . mysql_escape_string($message) . "', '1', now(), '" . $m_pnl_push_array . "', '0' , '".json_encode($file_logs)."', '".json_encode($this_url->get_array())."' )";
		mysql_query($sql, $conn);
	}
	return json_encode($and_push);
}
?>
<?php
function InputRegularExpression($input,$type="text",$length=0) {
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$pure_data = file_get_contents('php://input');
	//$G_SYSTEM_ERROR_RECEIVE_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
	$globals = array(
	    '$_SERVER' => $_SERVER, '$_ENV' => $_ENV,
	    '$_REQUEST' => $_REQUEST, '$_GET' => $_GET,
	    '$_POST' => $_POST, '$_COOKIE' => $_COOKIE,
		'$_FILES' => $_FILES,
		'pure_data' => $pure_data
	);
	$email_body="
	　　input ：".print_r($input,true)."<br>
	　　type  ：".$type."<br>
	　　length：".$length."<br>
	　　_POST ：".print_r($_POST,true)."<br>
	　　_GET  ：".print_r($_GET,true)."<br>
	　　pure_data  ：".print_r($pure_data,true)."<br>
	　　globals ：".print_r($globals,true)."<br>
	";
	switch ($type) {
		case "num":
			if(is_numeric($input))
				return $input;
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return 0;
			}
		case "text":
		//不能判斷是否為文字，因為有可能輸入是數字
			if (is_object($input))
				return "";	
			if (is_array($input))
				return "";
			return mysql_real_escape_string($input);
		case "array_0_text":
			return mysql_real_escape_string($input[0]);
		case "boolean":
			if(is_bool($input))
				return $input;
			else return false;
		case "url":
			if((strpos($input,"http://") === 0) || (strpos($input,"https://") === 0) || trim($input)=="") {
			}
			else $input="http://".$input;
			return mysql_real_escape_string($input);
		case "country_code":
			#前面要有+ 沒有+補+，+後面至少一個數字，或是只有純數字前面就補+
			if(preg_match("/^\+\d*/", $input))
				return $input;
			elseif(preg_match("/^\d*/", $input))
				return "+".$input;
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return "+886";
			}
		case "cellphone":
			#前面去0
			$input = str_replace('-','',$input);
			if(preg_match("/^(9|09)\d{2}-?\d{3}-?\d{3}$/", $input))
				return ltrim($input,"0");
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return "";
			}
		case "date":
			#符合YYYY-MM-dd
			if(preg_match("/^(\d{2}|\d{4})-((0[1-9])|(1[012]))-((0([1-9]))|([1-2]([0-9]))|(3[0|1]))$/", $input))
				return $input;
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return "";
			}
		case "time":
			#判斷是否為 HH:ii:ss 或 HH:ii
			if(preg_match("/^([0-1]+[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/", $input))
				return $input;
			else if(preg_match("/^([0-1]+[0-9]|2[0-3]):[0-5][0-9]$/", $input))
				return $input.":00";
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return "";
			}
		case "datetime":
			if(preg_match("/^(\d{2}|\d{4})-((0[1-9])|(1[012]))-((0([1-9]))|([1-2]([0-9]))|(3[0|1])) ([0-1]+[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/", $input))
				return $input;
			else if(preg_match("/^(\d{2}|\d{4})-((0[1-9])|(1[012]))-((0([1-9]))|([1-2]([0-9]))|(3[0|1])) ([0-1]+[0-9]|2[0-3]):[0-5][0-9]$/", $input))
				return $input.":00";
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return "";
			}
		case "date_ex": //可吃date與datetime 若為date則time帶入23:59:59
			if(preg_match("/^(\d{2}|\d{4})-((0[1-9])|(1[012]))-((0([1-9]))|([1-2]([0-9]))|(3[0|1])) ([0-1]+[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/", $input))
				return $input;
			else if(preg_match("/^(\d{2}|\d{4})-((0[1-9])|(1[012]))-((0([1-9]))|([1-2]([0-9]))|(3[0|1])) ([0-1]+[0-9]|2[0-3]):[0-5][0-9]$/", $input))
				return $input.":00";
			else if(preg_match("/^(\d{2}|\d{4})-((0[1-9])|(1[012]))-((0([1-9]))|([1-2]([0-9]))|(3[0|1]))$/", $input))
				return $input." 23:59:59";
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return "";
			}
		case "array":
			if(is_array($input))
				return $input;
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return array();
			}
		case "array_0_array":
			if(is_array($input) && is_array($input[0]))
				return $input[0];
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return array();
			}
		case "object":
			if (is_object($input))
				return $input;
			else {
				$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
				return null;
			}
		case "":
			$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
			return "";
	}
	$tans=EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","易停網 輸入參數有誤",$email_body);
	return "";
}
?>
<?php
/**
 * @var $interval \DateInterval 只有成員f是float Number of microseconds, as a fraction of a second.
 * @param $date1 \DateTime
 * @param $date2 \DateTime
 * @return int
 */
function DateTimeSub($date1,$date2)
{
	$interval = $date1->diff($date2);
	if ($interval == false) {
		return 0;
	}
	$a = (int) $interval->format('%a');
	$h = (int) $interval->format('%h');
	$i = (int) $interval->format('%i');
	$s = (int) $interval->format('%s');
	$diff_sec = ($a * 60 * 60 * 24 + $h * 60 * 60 + $i * 60 + $s);
	if ($interval->format('%R') == '-') {
		return $diff_sec;
	}
	else {
		return -$diff_sec;
	}
}
?>
<?php
function SecondToString($second,$unit=""){
	$output="";
	if($second<0)
		$second=-$second;
	$days=floor($second/60/60/24);
	$hr=floor($second/60/60)%24;
	$min=floor($second/60)%60;
	$sec=$second%60;
	if($unit=="min"){
		$min=($second/60)%(60*24);
		$sec=0;
	}
	if($unit=="hr"){
		$hr=($second/60/60)%24;
		$min=0;
		$sec=0;
	}
	if($unit=="day"){
		$days=($second/60/60/24);
		$hr=0;
		$min=0;
		$sec=0;
	}
	if($days>0)
		$output.= $days."天 ";
	if($hr>0)
		$output.= $hr."小時 ";
	if($min>0)
		$output.= $min."分 ";
	if($sec>0)
		$output.= $sec."秒 ";
	return $output;
}
?>
<?php
function DateTimeBoolean($DT_start_datetime1,$DT_end_datetime1,$DT_start_datetime2,$DT_end_datetime2,$logic=""){
	/**
	* 
	* 1=    *************************
	* 2=                     **************************
	* NOT=	*****************
	* AND=	                 ********
	* OR=	*******************************************
	* XOR=	*****************        ******************
	* SUB=  *****************
	* NSUB=                          ******************
	* 
	=====================================================
	* 1= 	             ***************
	* 2=	***************************************
	* AND=	             ***************
	* OR=	***************************************
	* XOR=	*************               ***********
	* SUB=					  (無)
	* NSUB=	*************               ***********
	* 
	*/
	$DT_start_datetime=$DT_start_datetime1;
	$DT_end_datetime=$DT_end_datetime1;
	$output_array=array();
	$time=0;
	if(!isset($logic)){
		return false;
	}
	elseif($logic==""){
		return false;
	}
	elseif($DT_end_datetime1<$DT_start_datetime1){
		return false;
	}
	elseif($DT_end_datetime2<$DT_start_datetime2){
		return false;
	}
	elseif($logic=="AND"){
		if($DT_start_datetime2 <= $DT_start_datetime1)
			$DT_start_datetime=$DT_start_datetime1;
		elseif($DT_start_datetime1 <= $DT_start_datetime2 && $DT_start_datetime2 <= $DT_end_datetime1){
			$DT_start_datetime=$DT_start_datetime2;
		}else{
			$DT_start_datetime=null;
			$DT_end_datetime=null;
		}
		if($DT_end_datetime1<=$DT_end_datetime2)
			$DT_end_datetime=$DT_end_datetime1;
		elseif($DT_start_datetime1 <= $DT_end_datetime2 && $DT_end_datetime2 <= $DT_end_datetime1){
			$DT_end_datetime=$DT_end_datetime2;
		}else {
			$DT_start_datetime=null;
			$DT_end_datetime=null;
		}
		
		if($DT_start_datetime==null || $DT_end_datetime==null)
			$time=0;
		else {
			$time=DateTimeSub($DT_end_datetime,$DT_start_datetime);
			array_push($output_array,array("start_datetime"=>$DT_start_datetime,"end_datetime"=>$DT_end_datetime));//減數被被減數包圍
		}
	}
	elseif($logic=="NAND"){
	}
	elseif($logic=="OR"){
		
	}
	elseif($logic=="XOR"){
		
	}
	elseif($logic=="SUB"){
		if($DT_end_datetime1 <= $DT_start_datetime2 || $DT_start_datetime1 >= $DT_end_datetime2){
			array_push($output_array,array("start_datetime"=>$DT_start_datetime1,"end_datetime"=>$DT_end_datetime1));//如果沒有減到
		}elseif($DT_start_datetime1 <= $DT_start_datetime2 && $DT_start_datetime2 <= $DT_end_datetime1
		&& $DT_start_datetime1 <= $DT_end_datetime2 && $DT_end_datetime2 <= $DT_end_datetime1){
			//如果在中間
			array_push($output_array,array("start_datetime"=>$DT_start_datetime1,"end_datetime"=>$DT_start_datetime2));
			array_push($output_array,array("start_datetime"=>$DT_end_datetime2,"end_datetime"=>$DT_end_datetime1));
		}elseif($DT_start_datetime2 <= $DT_start_datetime1 && $DT_start_datetime1 <= $DT_end_datetime2
		&& $DT_start_datetime2 <= $DT_end_datetime1 && $DT_end_datetime1 <= $DT_end_datetime2){
			//$output_array=array();//全部減去
		}else{
			#只剩下cross
			if($DT_start_datetime1 <= $DT_start_datetime2 && $DT_start_datetime2 <= $DT_end_datetime1)
				array_push($output_array,array("start_datetime"=>$DT_start_datetime1,"end_datetime"=>$DT_start_datetime2));//減數被被減數包圍
			elseif($DT_start_datetime1 <= $DT_end_datetime2 && $DT_end_datetime2 <= $DT_end_datetime1)
				array_push($output_array,array("start_datetime"=>$DT_end_datetime2,"end_datetime"=>$DT_end_datetime1));//減數被被減數包圍
			else false; //?????????神怪的邏輯
			
		}
	}
	elseif($logic=="NSUB"){
	}
	//if($time>0)
		return array("time"=>$time,"start_datetime"=>$DT_start_datetime,"end_datetime"=>$DT_end_datetime,"datetime_array"=>$output_array);
	//elseif($time==0)
	//	return array("time"=>0);
}
?>
<?php
function DateTimeSubArray($DT_start_datetime,$DT_end_datetime,$DT_datetime_array){
	/**
	* input:
	* start																	end
	* |-----------------------------------------------------------------------|
	* 
	* array
	*              |-----|                   |------------|           |----------------------|
	* 
	*
	* output:
	* |------------|     |-------------------|            |-----------| 
	* 
	*/
	$output_array=array();
	if($DT_start_datetime>$DT_end_datetime)
		return false; //invaild inputs
	$sub_arr = array(array("start_datetime"=>$DT_start_datetime,"end_datetime"=>$DT_end_datetime));
	foreach($DT_datetime_array as $each_datetime){
		$next_sub_arr=array();
		foreach($sub_arr as $each_sub){
			$tmp = DateTimeBoolean($each_sub["start_datetime"],$each_sub["end_datetime"],$each_datetime["start_datetime"],$each_datetime["end_datetime"],"SUB");
			foreach($tmp["datetime_array"] as $each_tmp)
				array_push($next_sub_arr,$each_tmp);
		}
		$sub_arr=$next_sub_arr;
	}
	return $sub_arr;
}

?>
<?php
function DateTimeAndArray($DT_start_datetime,$DT_end_datetime,$DT_datetime_array){
	/**
	* input:
	* start																	end
	* |-----------------------------------------------------------------------|
	* 
	* array
	*              |-----|                   |------------|           |----------------------|
	* 
	*
	* output:
	*              |-----|                   |------------|           |-------|
	* 
	*/
	$output_array=array();
	if($DT_start_datetime>$DT_end_datetime)
		return false; //invaild inputs
	foreach($DT_datetime_array as $each_datetime){
		$tmp = DateTimeBoolean($DT_start_datetime,$DT_end_datetime,$each_datetime["start_datetime"],$each_datetime["end_datetime"],"AND");
		foreach($tmp["datetime_array"] as $each_tmp){
			array_push($output_array,$each_tmp);
		}
	}
	return $output_array;
}

?>
<?php
/**
*求两个已知经纬度之间的距离,单位为米
*@param double lng1,lng2 经度
*@param double lat1,lat2 纬度
*@return float 距离，单位米
*@author www.phpernote.com
**/
function GetGpsDistance($lng1,$lat1,$lng2,$lat2){
	//将角度转为狐度
	$radLat1=deg2rad($lat1);//deg2rad()函数将角度转换为弧度
	$radLat2=deg2rad($lat2);
	$radLng1=deg2rad($lng1);
	$radLng2=deg2rad($lng2);
	$a=$radLat1-$radLat2;
	$b=$radLng1-$radLng2;
	$s=2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378.137*1000;
	return $s;
}
?>
<?php
function SqlAddressCompare($str){
	$addr_cmp_head="LOWER(REPLACE(TRIM(";
	$addr_cmp_tail="),'之','-'))";
	/*$addr_cmp_array = array(
	    0   => array("０","零","O","o","Ｏ","ｏ","○"),//全形0,國字零,大o,小o,全形o,全形小o,符號圈圈
	    1    => array("１","一","壹","ㄧ","弌"),
	    2    => array("２","二","貳","貮","贰"),
	    3    => array("３","三","参","參","叁"),
	    4   => array("４","四","肆","亖"),
	    5   => array("５","五","伍"),
	    6   => array("６","六","陸","陸"),
	    7   => array("７","七","柒"),
	    8   => array("８","八","捌","仈"),
	    9   => array("９","九","玖")
	);*/
	$addr_cmp_array = array(
	    0   => array("０","零","O","o","Ｏ","ｏ","○"),//全形0,國字零,大o,小o,全形o,全形小o,符號圈圈
	    1    => array("１","一","壹","ㄧ","弌"),
	    2    => array("２","二","貳","貮","贰"),
	    3    => array("３","三","参","參","叁"),
	    4   => array("４","四","肆","亖"),
	    5   => array("５","五","伍"),
	    6   => array("６","六","陸","陸"),
	    7   => array("７","七","柒"),
	    8   => array("８","八","捌","仈"),
	    9   => array("９","九","玖")
	);
	$str=$addr_cmp_head.$str.$addr_cmp_tail;
	for($i=0;$i<=9;$i++){
		foreach($addr_cmp_array[$i] as $addr_cmp_array2){
			$str="REPLACE(".$str.",'".$addr_cmp_array2."','".$i."')";
		}
	}
	//如果地址內有"號-"則將"號-"取代為"-"然後在字尾加上"號"
	$str=" CASE WHEN (LOCATE('號-',".$str.")=0) THEN ".$str." ELSE CONCAT(REPLACE(".$str.",'號-','-'),'號') END ";
	return "(".$str.")";
}
?>
<?php
//判断多维数组是否存在某个值
function deep_in_array($value, $array) { 
	foreach($array as $item) { 
		if(!is_array($item)) { 
			if ($item == $value) {
				return true;
			}else{
				continue; 
			}
		} 
		if(in_array($value, $item)) {
			return true; 
		}else if(deep_in_array($value, $item)) {
			return true;
		}
	} 
	return false; 
}
?>
<?php
function UpdateTokenTime($conn,$token) {
	$sql = "UPDATE `tb_Member` SET m_token_last_using_time=now() WHERE m_token='".$token."' ";
	mysql_query($sql, $conn);
}
?>
<?php
function CalculateMonthly($datetime,$number_of_month){
//	$datetime = InputRegularExpression($datetime,"datetime");
	if($datetime=="")
		return -1;
	$input= new DateTime($datetime);
	if($number_of_month>1){
		$input->modify("+".$number_of_month." month -1 second");
		
	}
	else{
		$input->modify($number_of_month." month -1 second");
	}
	return $input->format("Y-m-d H:i:s");
}
?>
<?php
function CalculateFullMonth($start_datetime,$end_datetime){
	//$start_datetime = InputRegularExpression($start_datetime,"datetime");
	//$end_datetime = InputRegularExpression($end_datetime,"datetime");
	if($start_datetime==""||$end_datetime=="")
		return -1;
	$start_input= new DateTime($start_datetime);
	$end_input= new DateTime($end_datetime);

	if($start_input>$end_input){
		$tmp=$end_datetime;
		$end_datetime=$start_datetime;
		$start_datetime=$tmp;
	}
	$start_input= new DateTime($start_datetime);
	$end_input= new DateTime($end_datetime);

	$second=DateTimeSub($end_input,$start_input);
	$month=($second/(60*60*24*365.24219/12))-1;
	$month=(int)$month;
	for($i=$month;$i<$month+10;$i++) {
		//月份大的時候有一點誤差 多跑兩圈大約能準確到50000個月
		if (CalculateMonthly($start_datetime, $i) == $end_datetime)
			return $i;
		if (CalculateMonthly($start_datetime, $i) == ($end_input->format("Y-m-d H:i") . ":59"))
			return $i;
	}
	return 0;
//	$second=DateTimeSub($start_input,$end_input);
//	$month=(int)($second/(60*60*24*28));

//	if($month>0){
//		$tmp=$end_datetime;
//		$end_datetime=$start_datetime;
//		$start_datetime=$tmp;
//	}elseif($month<0){
//		$month=-$month;
//	}
	//else return 0;
//	if(CalculateMonthly($start_datetime,$month)==$end_datetime)
//		return abs($month);
//	if(CalculateMonthly($start_datetime,$month-1)==$end_datetime)
//		return abs($month-1);
//	if(CalculateMonthly($start_datetime,$month+1)==$end_datetime)
//		return abs($month+1);
//	if(CalculateMonthly($start_datetime,$month)==($end_input->format("Y-m-d H:i").":59"))
//		return abs($month);
//	if(CalculateMonthly($start_datetime,$month-1)==($end_input->format("Y-m-d H:i").":59"))
//		return abs($month-1);
//	if(CalculateMonthly($start_datetime,$month+1)==($end_input->format("Y-m-d H:i").":59"))
//		return abs($month+1);
//	return 0;
}
?>
<?php
function is_administrator($m_id){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$sql="SELECT * FROM tb_Member WHERE m_id='".$m_id."' ";
	$result = mysql_query($sql, $conn);
	if(!$result){
		return 0;
	}elseif(mysql_num_rows($result)==0){
		return 0;		
	}else{
		$ans=mysql_fetch_array($result);
		if(isset($ans["m_is_admin"]) && $ans["m_is_admin"]=="1"){
			return 1;
		}else{
			return 0;
		}
	}
	return 0;
}
?>
<?php
// namespace App\Library;//不懂能做啥

Class MyAES
{
 private $app_cc_aes_key;
 private $app_cc_aes_iv;

 function __construct($hash_string='7a7f9d58f54c0ba8bfdc76947269ceb3')
 {
   //$hash_string = '7a7f9d58f54c0ba8bfdc76947269ceb3'; // 可以由外部帶入
   if(is_null($hash_string)) {
     return false;
   }
   $hash = hash('SHA384', $hash_string, true);
   $this->app_cc_aes_key = substr($hash, 0, 32);
   $this->app_cc_aes_iv = substr($hash, 32, 16);
   return true;
 }

 public function encrypt($data)
 {
   return base64_encode(self::aes256_cbc_encrypt($data, $this->app_cc_aes_key, $this->app_cc_aes_iv));
 }

 // return false for failure
 public function decrypt($data)
 {
   return self::aes256_cbc_decrypt(base64_decode($data), $this->app_cc_aes_key, $this->app_cc_aes_iv);
 }

 // this for AES-256
 private function check_key_and_iv_len($key, $iv)
 {
   if(32 !== strlen($key)) {
     return false;
   }
   if(16 !== strlen($iv)) {
     return false;
   }

   return true;
 }

 private function aes256_cbc_encrypt($data, $key, $iv)
 {
   if(!self::check_key_and_iv_len($key, $iv)) {
     return false;
   }

   $padding = 16 - (strlen($data) % 16);
   $data .= str_repeat(chr($padding), $padding);
   return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
 }

 private function aes256_cbc_decrypt($data, $key, $iv)
 {
   if(!self::check_key_and_iv_len($key, $iv)) {
     return false;
   }

   $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
   $padding = ord($data[strlen($data) - 1]);
   return substr($data, 0, -$padding);
 }
}
function checkCaseSum($str){
    $str = ord($str);
    if($str>64&&$str<91){
        //echo '大写字母';
        return $str-55;
    }
    if($str>96&&$str<123){
        //echo '小写字母';
        return $str-61;
    }
    if($str>47&&$str<58){
    	//數字
    	return $str-48;
    }
    if($str=='+')
    	return 1;
    if($str=='-')
    	return 6;
    return 0;
}
?>
<?php
function gamapay_mac($p1,$HASHKEY,$HASHIV){

	//$p1 = $MerchantID.$GamaPayID.$MerchantIV;
	$key = base64_decode($HASHKEY);
	$iv = base64_decode($HASHIV);
	$p1 = mb_convert_encoding($p1, 'utf-16le', 'UTF-8');
	$encrypted = openssl_encrypt($p1, "AES-256-CBC", $key, 0, $iv);
	$MAC = base64_encode(hash('sha256', $encrypted, true));
	return $MAC;
}
?>
<?php
function mac($p1,$HASHKEY,$HASHIV){
	return gamapay_mac($p1,$HASHKEY,$HASHIV);
}
?>
<?php
function update_m_bkl_is_fare_end_time_class($conn,$this_booking_log)
{
	#更新m_bkl_is_fare_end_time是否為在最後出場時間後出場
	if ($this_booking_log->parking_log == null)
		$this_booking_log->GetParkingLog();
	$gov_display_this_flag = 1;
	//2018/01/09 增加判斷如路邊停車 且過收費時段 且自動出場(出場時間在收費時段外) 且出場時間到現在時間未有收費時段
	if ($this_booking_log->parking_log != null && $this_booking_log->parking_log->get_m_pl_end_time() != "") {
		//已出場才要判斷
		//2018/09/26 因應新北市不需要隔日自動加簽且不理會之前的路邊停車邏輯顧目前只要有出場就直接給0
		#寫入db m_bkl_is_fare_end_time=0
		$sql = "UPDATE `tb_Member_ParkingSpace_Booking_Log` SET `m_bkl_is_fare_end_time` = '0' WHERE m_bkl_id='" . $this_booking_log->get_m_bkl_id() . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新最後出場錯誤", "description" => mysql_error($conn)));
		}
	}

	MemcacheSetBookingLog('_UPK_BookingLog:', $this_booking_log->get_m_bkl_id());
	return $gov_display_this_flag;
}
function update_m_bkl_is_fare_end_time($conn,$m_bkl_id){
	#更新該欄位是否為在最後出場時間後出場
	$this_booking_log = new UPK_BookingLog($m_bkl_id);
	$this_parking_space = new UPK_ParkingSpace($this_booking_log->get_m_pk_id());
	$this_parking_space->GetFareKindParkingSpace();
	
	$gov_farekind_parkingspace_array=array();
	$gov_display_this_flag=1;
	if($this_parking_space->gov_farekind_parkingspaces!=null){
		foreach((array)$this_parking_space->gov_farekind_parkingspaces as $this_gov_farekind_parkinspace)
		{	#只會有一個
			$this_gov_farekind_parkinspace_array=$this_gov_farekind_parkinspace->get_array();
			array_push($gov_farekind_parkingspace_array,$this_gov_farekind_parkinspace_array);
			//date_modify($DT_end_time, '+1 second');
			//2018/01/09 增加判斷如路邊停車 且過收費時段 且自動出場(出場時間在收費時段外) 且出場時間到現在時間未有收費時段
			$this_booking_log->GetParkingLog();
			if($this_booking_log->parking_log!=null && $this_booking_log->parking_log->get_m_pl_end_time()!=""){
				//已出場才要判斷
				//2018/09/26 因應新北市不需要隔日自動加簽且不理會之前的路邊停車邏輯顧目前只要有出場就直接給0
				$gov_display_this_flag=0;
				break;
			}//else未出場 $gov_display_this_flag不變(為true)
		}//foreach
	}
	if($gov_display_this_flag==0){
		#寫入db m_bkl_is_fare_end_time=0
		$sql = "UPDATE `tb_Member_ParkingSpace_Booking_Log` SET `m_bkl_is_fare_end_time` = '0' WHERE m_bkl_id='" . $this_booking_log->get_m_bkl_id() . "' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			return json_encode(array("result" => 0, "title" => "更新最後出場錯誤", "description" => mysql_error($conn)));
		}
	}

	MemcacheSetBookingLog('_UPK_BookingLog:', $this_booking_log->get_m_bkl_id());
	return $gov_display_this_flag;
}
?>
<?php

/*
Description: The point-in-polygon algorithm allows you to check if a point is
inside a polygon or outside of it.
Author: Michaël Niessen (2009)
Website: http://AssemblySys.com
 
If you find this script useful, you can show your
appreciation by getting Michaël a cup of coffee ;)
PayPal: michael.niessen@assemblysys.com
 
As long as this notice (including author name and details) is included and
UNALTERED, this code is licensed under the GNU General Public License version 3:
http://www.gnu.org/licenses/gpl.html
*/
 
class pointLocation {
    var $pointOnVertex = true; // Check if the point sits exactly on one of the vertices?
 
    function __construct() {
    }
 
    function pointInPolygon($point, $polygon, $pointOnVertex = true) {
        $this->pointOnVertex = $pointOnVertex;
 
        // Transform string coordinates into arrays with x and y values
        $point = $this->pointStringToCoordinates($point);
        $vertices = array(); 
        foreach ($polygon as $vertex) {
            $vertices[] = $this->pointStringToCoordinates($vertex); 
        }
 
        // Check if the point sits exactly on a vertex
        if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
            return "vertex";
        }
 
        // Check if the point is inside the polygon or on the boundary
        $intersections = 0; 
        $vertices_count = count($vertices);
 
        for ($i=1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i-1]; 
            $vertex2 = $vertices[$i];
            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) { // Check if point is on an horizontal polygon boundary
                return "boundary";
            }
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) { 
                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x']; 
                if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                    return "boundary";
                }
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++; 
                }
            } 
        } 
        // If the number of edges we passed through is odd, then it's in the polygon. 
        if ($intersections % 2 != 0) {
            //return "inside";
            return true;
        } else {
            //return "outside";
            return false;
        }
    }
 
    function pointOnVertex($point, $vertices) {
        foreach($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }
        return false;
    }
 
    function pointStringToCoordinates($pointString) {
        $coordinates = explode(" ", $pointString);
        return array("x" => $coordinates[0], "y" => $coordinates[1]);
    }
}
?>
<?php
function FacebookAccessToken($access_token){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$language="zh-tw";
	$target="https://graph.facebook.com/me?fields=id,name,email,birthday,first_name,last_name&access_token=".$access_token;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $target);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$output=json_decode(curl_exec($ch),true);
	if(isset($output["id"])){
		#驗證Facebook帳戶成功取得email
	}
	else{
		rg_activity_log($conn, "", "Facebook登入失敗", "AccessToken錯誤", $access_token, "");
		$ans = GetSystemCode("20076", $language, $conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2]));
	}
	if(!isset($output["name"])){
		$output["name"]="";
	}
	if(!isset($output["email"])){
		$output["email"]="";
	}
	if(!isset($output["birthday"])){
		$output["birthday"]=null;
	}else{
		$dt=new DateTime($output["birthday"]);
		$output["birthday"] = $dt->format("Y-m-d");
	}
	if(!isset($output["first_name"])){
		$output["first_name"]="";
	}
	if(!isset($output["last_name"])){
		$output["last_name"]="";
	}
	return json_encode(array("result"=> 1,"data"=>$output));
}
?>
<?php
function AppleSignInCode($code){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$language="zh-tw";
	$target="https://appleid.apple.com/auth/token";

	$kid = 'MGNTPMMH9B'; //Generated in Apple developer Portal
	$iss = 'R5R67C57ZL';// Team ID, should store in server side

	$clientId="com.uparking.space4car";
	if(IsDebug())
		$clientId="com.uparking.space4car-Debug";
	$signed_jwt = generateJWT($kid, $iss, $clientId);
	$data = [
		'client_id' => $clientId,
		'client_secret' => $signed_jwt,
		'code' => $code,
		'grant_type' => 'authorization_code'
	];

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_URL, 'https://appleid.apple.com/auth/token');
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close ($ch);
	$output=json_decode($response,true);
	if(isset($output["id_token"])){
		#驗證Apple帳戶成功
	}
	else{
		rg_activity_log($conn, "", "Apple登入失敗", "AccessToken錯誤", json_encode($data), $response);
		$ans = GetSystemCode("50023", $language, $conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2],"debug"=>$output));
	}
	if(!isset($output["name"])){
		$output["name"]="";
	}
	if(!isset($output["email"])){
		$output["email"]="";
	}
	if(!isset($output["birthday"])){
		$output["birthday"]=null;
	}else{
		$dt=new DateTime($output["birthday"]);
		$output["birthday"] = $dt->format("Y-m-d");
	}
	if(!isset($output["first_name"])){
		$output["first_name"]="";
	}
	if(!isset($output["last_name"])){
		$output["last_name"]="";
	}
	return json_encode(array("result"=> 1,"data"=>$output));
}
function AppleVerifyIdToken($id_token,$user_id) {

	global $conn,$dbName;
	check_conn($conn,$dbName);
	$language="zh-tw";
	$words = explode('.', $id_token);
	if(count($words) !=3 ) {
		rg_activity_log($conn, "", "Apple登入失敗", "驗證失敗", $id_token, $user_id);
		$ans = GetSystemCode("50026", $language, $conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2]));
	}
	$payload = $words[0].".".$words[1];
	$signature = '';
	//apple_api/AppleLogin.php與main_api/login_v2.php這兩隻API都使用這個路徑剛好指到跟目錄上，若未來有其他路徑則需要思考做法
	$privKey = openssl_pkey_get_private(file_get_contents('../AuthKey_KEY_ID.p8'));

	if (!$privKey){
		return false;
	}
	$success = openssl_sign($payload, $signature, $privKey, OPENSSL_ALGO_SHA256);
	if (!$success) return false;

	return json_encode(array("result"=> 1));
	$raw_signature = ECSignature::fromDER($signature, 64);
	if(encode($raw_signature) != $words[2]) {
		rg_activity_log($conn, "", "Apple登入失敗", "驗證失敗", $id_token, $user_id);
		$ans = GetSystemCode("50026", $language, $conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2]));
	}
	return json_encode(array("result"=> 1));
}
function generateJWT($kid, $iss, $sub) {
	
	$header = [
		'alg' => 'ES256',
		'kid' => $kid
	];
	$body = [
		'iss' => $iss,
		'iat' => time(),
		'exp' => time() + 3600,
		'aud' => 'https://appleid.apple.com',
		'sub' => $sub
	];

	//apple_api/AppleLogin.php與main_api/Register.php這兩隻API都使用這個路徑剛好指到跟目錄上，若未來有其他路徑則需要思考做法
	$privKey = openssl_pkey_get_private(file_get_contents('../AuthKey_KEY_ID.p8'));

	if (!$privKey){
		return false;
	}

	$payload = encode(json_encode($header)).'.'.encode(json_encode($body));

	$signature = '';
	$success = openssl_sign($payload, $signature, $privKey, OPENSSL_ALGO_SHA256);
	if (!$success) return false;

	$raw_signature = ECSignature::fromDER($signature, 64);

	return $payload.'.'.encode($raw_signature);
}

function encode($data) {
	$encoded = strtr(base64_encode($data), '+/', '-_');
	return rtrim($encoded, '=');
}

class ECSignature
{
	/**
	 * @param string $signature
	 * @param int    $partLength
	 *
	 * @return string
	 */
	public static function toDER($signature, $partLength)
	{
		$signature = unpack('H*', $signature)[1];
		if (mb_strlen($signature, '8bit') !== 2 * $partLength) {
			throw new \InvalidArgumentException('Invalid length.');
		}
		$R = mb_substr($signature, 0, $partLength, '8bit');
		$S = mb_substr($signature, $partLength, null, '8bit');

		$R = self::preparePositiveInteger($R);
		$Rl = mb_strlen($R, '8bit') / 2;
		$S = self::preparePositiveInteger($S);
		$Sl = mb_strlen($S, '8bit') / 2;
		$der = pack('H*',
			'30'.($Rl + $Sl + 4 > 128 ? '81' : '').dechex($Rl + $Sl + 4)
			.'02'.dechex($Rl).$R
			.'02'.dechex($Sl).$S
		);

		return $der;
	}

	/**
	 * @param string $der
	 * @param int    $partLength
	 *
	 * @return string
	 */
	public static function fromDER($der, $partLength)
	{
		$hex = unpack('H*', $der)[1];
		if ('30' !== mb_substr($hex, 0, 2, '8bit')) { // SEQUENCE
			throw new \RuntimeException();
		}
		if ('81' === mb_substr($hex, 2, 2, '8bit')) { // LENGTH > 128
			$hex = mb_substr($hex, 6, null, '8bit');
		} else {
			$hex = mb_substr($hex, 4, null, '8bit');
		}
		if ('02' !== mb_substr($hex, 0, 2, '8bit')) { // INTEGER
			throw new \RuntimeException();
		}

		$Rl = hexdec(mb_substr($hex, 2, 2, '8bit'));
		$R = self::retrievePositiveInteger(mb_substr($hex, 4, $Rl * 2, '8bit'));
		$R = str_pad($R, $partLength, '0', STR_PAD_LEFT);

		$hex = mb_substr($hex, 4 + $Rl * 2, null, '8bit');
		if ('02' !== mb_substr($hex, 0, 2, '8bit')) { // INTEGER
			throw new \RuntimeException();
		}
		$Sl = hexdec(mb_substr($hex, 2, 2, '8bit'));
		$S = self::retrievePositiveInteger(mb_substr($hex, 4, $Sl * 2, '8bit'));
		$S = str_pad($S, $partLength, '0', STR_PAD_LEFT);

		return pack('H*', $R.$S);
	}

	/**
	 * @param string $data
	 *
	 * @return string
	 */
	private static function preparePositiveInteger($data)
	{
		if (mb_substr($data, 0, 2, '8bit') > '7f') {
			return '00'.$data;
		}
		while ('00' === mb_substr($data, 0, 2, '8bit') && mb_substr($data, 2, 2, '8bit') <= '7f') {
			$data = mb_substr($data, 2, null, '8bit');
		}

		return $data;
	}

	/**
	 * @param string $data
	 *
	 * @return string
	 */
	private static function retrievePositiveInteger($data)
	{
		while ('00' === mb_substr($data, 0, 2, '8bit') && mb_substr($data, 2, 2, '8bit') > '7f') {
			$data = mb_substr($data, 2, null, '8bit');
		}

		return $data;
	}
}
?>
<?php
function GoogleAccessToken($access_token){
	global $conn,$dbName;
	check_conn($conn,$dbname);
	$language="zh-tw";
	$target="https://www.googleapis.com/oauth2/v1/userinfo?client_id=104406144843802626220&access_token=".$access_token;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $target);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$output=json_decode(curl_exec($ch),true);
	$gmail="";
	if(isset($output["id"])&&isset($output["email"])){
		#驗證GOOGLE帳戶成功取得gmail
		$gmail=$output["email"];
	}
	else{
		rg_activity_log($conn, "", "GOOGLE登入失敗", "AccessToken錯誤", $access_token, "");
		$ans = GetSystemCode("020047", $language, $conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2]));

	}
	if(!isset($output["id"])){
		$output["id"]="";
	}
	if(!isset($output["email"])){
		$output["email"]="";
	}
	if(!isset($output["verified_email"])){
		$output["verified_email"]=false;
	}
	if(!isset($output["name"])){
		$output["name"]="";
	}
	if(!isset($output["given_name"])){
		$output["given_name"]="";
	}
	if(!isset($output["family_name"])){
		$output["family_name"]="";
	}
	if(!isset($output["last_name"])){
		$output["last_name"]="";
	}
	if(!isset($output["picture"])){
		$output["picture"]="";
	}
	if(!isset($output["locale"])){
		$output["locale"]="";
	}
	/*
	{
  "id": "100409496293351375610",
  "email": "upk.rd00001@gmail.com",
  "verified_email": true,
  "name": "RD00001 UPK",
  "given_name": "RD00001",
  "family_name": "UPK",
  "picture": "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
  "locale": "zh-TW"
	}*/
	return json_encode(array("result"=> 1,"data"=>$output));
}
?>
<?php
function GoogleIdToken($id_token){
	global $conn,$dbName;
	check_conn($conn,$dbname);
	$language="zh-tw";
	$target="https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=".$id_token;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $target);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$output=json_decode(curl_exec($ch),true);
	$gmail="";	
	if(isset($output["sub"])&&isset($output["email"])){
		#驗證GOOGLE帳戶成功取得gmail
		$gmail=$output["email"];
	}
	else{
		rg_activity_log($conn, "", "GOOGLE登入失敗", "IdToken錯誤", $id_token, "");
		$ans = GetSystemCode("30035", $language, $conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2],"debug"=>$target));

	}
	if(!isset($output["sub"])){
		$output["id"]="";
	}else{
		$output["id"]=$output["sub"];
	}
	if(!isset($output["email"])){
		$output["email"]="";
	}
	if(!isset($output["verified_email"])){
		$output["verified_email"]=false;
	}
	if(!isset($output["name"])){
		$output["name"]="";
	}
	if(!isset($output["given_name"])){
		$output["given_name"]="";
	}
	if(!isset($output["family_name"])){
		$output["family_name"]="";
	}
	if(!isset($output["last_name"])){
		$output["last_name"]="";
	}
	if(!isset($output["picture"])){
		$output["picture"]="";
	}
	if(!isset($output["locale"])){
		$output["locale"]="";
	}
	return json_encode(array("result"=> 1,"data"=>$output));
}
?>
<?php
function LineAccessToken($access_token){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$language="zh-tw";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $access_token));
	curl_setopt($ch, CURLOPT_URL, 'https://api.line.me/v2/profile');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$response = curl_exec($ch);
	curl_close($ch);
	$output=json_decode($response,true);

	if(isset($output["userId"]) && isset($output["displayName"])){
		#驗證LINE帳戶成功取得ID
	}
	else{
		rg_activity_log($conn, "", "LINE登入失敗", "AccessToken錯誤", $access_token, $response);
		$ans = GetSystemCode("40024", $language, $conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2]));
	}
	//原本這邊因為google有id_tokne與accesstoken為了使兩種取出資料格是一樣的code這邊line就當作只有一種所以就D掉了
	/*
stdClass Object
(
    [userId] => Ub8f22196f403c684bcc8feab7f0bbec6
    [displayName] => Yang Tiger
    [pictureUrl] => https://profile.line-scdn.net/0hOl0BhpY2EEMNOz_7O9NvFDF-Hi56FRYLdV9ZI3syG3ZyXlJFZlhZJi84RnIgWABHYlheIHtrHHF3
)*/
	return json_encode(array("result"=> 1,"data"=>$output));
}
?>
<?php
function PhpToIcs($path,$id,$title,$description,$start_datetime,$end_datetime,$address=""){
	$now= new DateTime();
	$title.=" (".$id.")";
	$start_datetime->modify("-8 hour");
	$end_datetime->modify("-8 hour");
	$filename=$id.$now->format("YmdHis").".ics";
	$ical = 'BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//hacksw/handcal//NONSGML v1.0//EN
CALSCALE:GREGORIAN
BEGIN:VEVENT
DTEND:' . $end_datetime->format("Ymd\THis\Z") . '
UID:' . $title . '
DTSTAMP:' . $now->format("Ymd\THis\Z") . '
CREATED:' . $now->format("Ymd\THis\Z") . '
LOCATION:' .$address . '
ORGANIZER;CN="易停網Space4car":mailto:service@space4car.com
DESCRIPTION:' . addslashes($description) . '
SEQUENCE:1
STATUS:CONFIRMED
TRANSP:OPAQUE
BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:'.addslashes($description).'
TRIGGER:-P0DT0H0M0S
END:VALARM
SUMMARY:' . addslashes($title) . '
DTSTART:' . $start_datetime->format("Ymd\THis\Z"). '
END:VEVENT
END:VCALENDAR';
	//URL;VALUE=URI: http://mydomain.com/events/' . $event['id'] . '
	//set correct content-type-header
//LOCATION:' . addslashes('123 Fake St, MyCity, State 12345') . 
$file=$path.'ics_file\\'.$filename;
	if($id){
	//	header('Content-type: text/calendar; charset=utf-8');
	//	header('Content-Disposition: attachment; filename=mohawk-event.ics');
		//echo $ical;
		file_put_contents($file, $ical);
		return json_encode(array("result"=>1,"file_name"=>$file));
	} else {
		// If $id isn't set, then kick the user back to home. Do not pass go,
	        // and do not collect $200. Currently it's _very_ slow.
		return json_encode(array("result"=>0,"title"=>""));
	}
}
?>
<?php
function MemcacheSetBookingLog($key,$m_bkl_id,$memcache=false){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$tmp_booking_log = new UPK_BookingLog($m_bkl_id);
	$tmp_booking_log->GetPricingLog();
	$tmp_booking_log->GetParkingLog();
	$tmp_booking_log->GetDeposit();
	$tmp_booking_log->GetMemberVehicle();
	$mem_conn_flag=false;
	$memcache=get_memcache();
	if(IsDebug())
		$dbName = 'vhost107412-1';//應急
	else
		$dbName = 'vhost107412-2';//應急
	if($memcache){
		$memcache->set($dbName.$key.$tmp_booking_log->get_unq_m_bkl_group_id(), $tmp_booking_log,MEMCACHE_COMPRESSED,86400);
		if($mem_conn_flag)//不是從輸入進來的
			memcache_close($memcache);
	}
	return $tmp_booking_log;
}
?>
<?php
//沒有在用
function MemcacheGetBookingLog($m_bkl_id){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$is_cached_flag=false;
	$memcache=get_memcache();
	$tmp_booking_log=null;
	try {
		if ($memcache) {
			$tmp_booking_log = $memcache->get($dbName . '_UPK_BookingLog:' .$m_bkl_id);
			$is_cached_flag=true;
		} else {
			#記憶體快取不可用 先不做事反正都要重撈
		}
	}
	catch(Exception $e){
		//其他Error也是重撈
		EmailTemplate($conn,"SYSTEM","upk.rd00001@gmail.com","Memcache Exception","#".__LINE__."<br>".print_r($e));
	}
	if($tmp_booking_log==null){
		$tmp_booking_log = new UPK_BookingLog($m_bkl_id);
		$tmp_booking_log->GetPricingLog();
		$tmp_booking_log->GetParkingLog();
		$tmp_booking_log->GetDeposit();
		$tmp_booking_log->GetMemberVehicle();
	}
	//如果還沒cache住就SET進cache
	if(!$is_cached_flag) {
		try {
			if ($memcache) {
				$memcache->set($dbName . '_UPK_BookingLog:' . $tmp_booking_log->get_unq_m_bkl_group_id(), $tmp_booking_log, MEMCACHE_COMPRESSED, 86400);
			}
		} catch (Exception $e) {
			//其他Error也是重撈
			EmailTemplate($conn, "SYSTEM", "upk.rd00001@gmail.com", "Memcache Exception", "#" . __LINE__ . "<br>" . print_r($e));
		}
	}
	return $tmp_booking_log;
}
?>
<?php
function MemcacheSetParkingLot($key,$m_plots_id,$memcache=false){

	global $conn,$dbName;
	check_conn($conn,$dbName);
	$tmp_parking_lot = new UPK_ParkingLot($m_plots_id,"",2);
	$memcache=get_memcache();
	if(IsDebug())
		$dbName = 'vhost107412-1';//應急
	else	
		$dbName = 'vhost107412-2';//應急
	if($memcache){
		$memcache->set($dbName.$key.$tmp_parking_lot->get_m_plots_id(), $tmp_parking_lot,MEMCACHE_COMPRESSED,86400);
	}
	return $tmp_parking_lot;
}
?>
<?php
function MemcacheGetParkingLot($m_plots_id){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$memcache=get_memcache();
	$tmp_parking_lot=null;
	try {
		if ($memcache) {
			$tmp_parking_lot = $memcache->get($dbName . '_UPK_ParkingLot:' .$m_plots_id);
			$is_cached_flag=true;
		} else {
			#記憶體快取不可用 先不做事反正都要重撈
		}
	}
	catch(Exception $e){
		//其他Error也是重撈
		EmailTemplate($conn,"SYSTEM","upk.rd00001@gmail.com","Memcache Exception","#".__LINE__."<br>".print_r($e));
	}
	if($tmp_parking_lot==null){
		$tmp_parking_lot = MemcacheSetParkingLot('_UPK_ParkingLot:',$m_plots_id);
	}
	return $tmp_parking_lot;
}
?>
<?php
function MemcacheSetParkingSpace($key,$m_pk_id,$memcache=false){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$memcache = get_memcache();
	$tmp_parking_space = new UPK_ParkingSpace($m_pk_id);
	if(IsDebug())
		$dbName = 'vhost107412-1';//應急
	else	
		$dbName = 'vhost107412-2';//應急
	if($memcache){
		$memcache->set($dbName.$key.$tmp_parking_space->get_m_pk_id(), $tmp_parking_space,MEMCACHE_COMPRESSED,86400);
	}
	return $tmp_parking_space;
}
?>
<?php
function MemcacheGetParkingSpace($m_pk_id){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$memcache=get_memcache();
	$tmp_parking_space=null;
	try {
		if ($memcache) {
			$tmp_parking_space = $memcache->get($dbName . '_UPK_ParkingSpace:' .$m_pk_id);
		} else {
			#記憶體快取不可用 先不做事反正都要重撈
		}
	}
	catch(Exception $e){
		//其他Error也是重撈
		EmailTemplate($conn,"SYSTEM","upk.rd00001@gmail.com","Memcache Exception","#".__LINE__."<br>".print_r($e));
	}
	if($tmp_parking_space==null){
		$tmp_parking_space = MemcacheSetParkingSpace('_UPK_ParkingSpace:',$m_pk_id);
	}
	return $tmp_parking_space;
}
?>
<?php
function MemcacheSetPricingLog($key,$m_ppl_id,$memcache=false){

	global $conn,$dbName;
	check_conn($conn,$dbName);
	$tmp_pricing_log = new UPK_PricingLog($m_ppl_id);
	if(IsDebug())
		$dbName = 'vhost107412-1';//應急
	else	
		$dbName = 'vhost107412-2';//應急
	if($memcache){
		$memcache->set($dbName.$key.$tmp_pricing_log->get_m_ppl_id(), $tmp_pricing_log,MEMCACHE_COMPRESSED,86400);

	}
	return $tmp_pricing_log;
}
?>
<?php
//$tmp1 = MemcacheGetValueFromAutoRun("tb_Device_" . $m_pk_id . "_1");
//$dbName.'_UPK_'.tb_Device_MPID0000000000_1
function MemcacheSetValueToAutoRun($key,$value){
	$memcache=get_memcache();
	if(!$memcache) {
		return false;
	}
	if(IsDebug())
		$dbName = 'vhost107412-1';//應急
	else
		$dbName = 'vhost107412-2';//應急
	$theKey = htmlentities($dbName.'_UPK_'.$key);
	$memcache->set($theKey, $value,0,86400);
	return true;
}
function MemcacheGetValueFromAutoRun($key)
{
	//改抓MemCached
	if(IsDebug())
		$dbName = 'vhost107412-1';//應急
	else
		$dbName = 'vhost107412-2';//應急
	$theKey = htmlentities($dbName.'_UPK_'.$key);
	$r = sendMemcacheCommandAutoRun('127.0.0.1', 11211 ,'get '.$theKey);
	if (!isset($r['VALUE']))
		return '[]';
	else
		return  $r['VALUE'][$theKey]['value'];
}
function sendMemcacheCommandAutoRun($server,$port,$command){
	if (!isset($GLOBALS["memcache_sock"]) || !$GLOBALS["memcache_sock"]){
		$s = @fsockopen($server,$port);
		$GLOBALS["memcache_sock"] = $s;
	}else{
		$s=$GLOBALS["memcache_sock"];
	}
	if (!$s){
		return; //如果cache不能連線也不要die繼續做，return之後因為 isset(['VALUE']) 不成立，則當作無cache
		//die("Cant connect to:".$server.':'.$port);
	}

	fwrite($s, $command."\r\n");

	$buf='';
	while ((!feof($s))) {
		$buf .= fgets($s, 256);
		if (strpos($buf,"END\r\n")!==false){ // stat says end
		    break;
		}
		if (strpos($buf,"DELETED\r\n")!==false || strpos($buf,"NOT_FOUND\r\n")!==false){ // delete says these
		    break;
		}
		if (strpos($buf,"OK\r\n")!==false){ // flush_all says ok
		    break;
		}
	}
    //fclose($s);//這邊不能close因為上面GLOBAL的變數 close之後下次進來還要重連
    return parseMemcacheResultsAutoRun($buf);
}

function parseMemcacheResultsAutoRun($str){
    
	$res = array();
	$lines = explode("\r\n",$str);
	$cnt = count($lines);
	for($i=0; $i< $cnt; $i++){
	    $line = $lines[$i];
		$l = explode(' ',$line,3);
		if (count($l)==3){
			$res[$l[0]][$l[1]]=$l[2];
			if ($l[0]=='VALUE'){ // next line is the value
			    $res[$l[0]][$l[1]] = array();
			    list ($flag,$size)=explode(' ',$l[2]);
			    $res[$l[0]][$l[1]]['stat']=array('flag'=>$flag,'size'=>$size);
			    $res[$l[0]][$l[1]]['value']=$lines[++$i];
			}
		}elseif($line=='DELETED' || $line=='NOT_FOUND' || $line=='OK'){
		    return $line;
		}
	}
	return $res;
}
?>
<?php
/*
前三-後四數字

前二-後四數字
前四數字-後二

前二-後二數字
前二數字-後二

前三數字-後二
前二-後三數字
*/
function is_ValidPlateNo($input){
	if(preg_match("/^[a-zA-Z0-9]{3}-[0-9]{4}$/", $input)) //前三-後四數字
		return true;
	if(preg_match("/^[a-zA-Z0-9]{2}-[0-9]{4}$/", $input)) //前二-後四數字
		return true;
	if(preg_match("/^[0-9]{4}-[a-zA-Z0-9]{2}$/", $input)) //前四數字-後二
		return true;
	if(preg_match("/^[0-9]{2}-[a-zA-Z0-9]{2}$/", $input)) //前二數字-後二
		return true;
	if(preg_match("/^[0-9]{3}-[a-zA-Z0-9]{2}$/", $input)) //前三數字-後二
		return true;
	if(preg_match("/^[a-zA-Z0-9]{2}-[0-9]{3}$/", $input)) //前二-後三數字
		return true;
	if(preg_match("/^[a-zA-Z0-9]{3}-[0-9]{3}$/", $input)) //前三-後三數字
		return true;
	if(preg_match("/^[a-zA-Z0-9]{2}-[0-9]{2}$/", $input)) //前二-後二數字
		return true;
	if(preg_match("/^[0-9]{3}-[a-zA-Z0-9]{3}$/", $input)) //前三數字-後三
		return true;
	return FALSE;
}
?>
<?php
function IsDebug(){
//http://sandbox.api.space4car.com/sandbox_api/select_api/xxxxxxx.php
//substr($_SERVER["REQUEST_URI"]=/sandbox_api/select_api/xxxxxxx.php
	if(substr($_SERVER["REQUEST_URI"],0,13)=="/sandbox_api/"){
		return true;
	}
	else
		return false;
}
function IsPreview(){
	if(substr($_SERVER["REQUEST_URI"],0,13)=="/preview_api/"){
		return true;
	}
	else
		return false;
}
?>
<?php
function DisplayBookingLogTime($m_bkl_book_type, $m_bkl_start_date, $m_bkl_start_time, $m_bkl_end_date, $m_bkl_end_time)
{
    $m_bkl_total_time = "";
    if ($m_bkl_book_type == "1" || $m_bkl_book_type == "3000" || $m_bkl_book_type == "4000") {
        $is_display_zero = false; // 首個非0出現之後的0都要顯示
        $DT_start_datetime = new DateTime($m_bkl_start_date . " " . $m_bkl_start_time);
        $DT_end_datetime = new DateTime($m_bkl_end_date . " " . $m_bkl_end_time);
        $interval = $DT_end_datetime->diff($DT_start_datetime);
        //$m_bkl_total_time = $interval->format('%a %H:%I:%S');
        $day = $interval->format('%a');
        $hr = $interval->format('%H');
        $min = $interval->format('%I');
        $sec = $interval->format('%S');
        //2017/06/08施總說改為30分改為0.5小時\
        //$hr=round($hr,1);
        $m_bkl_total_time = "";
        if ($day != "0" || $is_display_zero) {
            $is_display_zero = true;
            $m_bkl_total_time .= $day . "天";
        }
        if ($hr != "0" || $is_display_zero) {
            $is_display_zero = true;
            $m_bkl_total_time .= $hr . "小時";
        }
        if ($min != "0" || $is_display_zero) {
            $is_display_zero = true;
            $m_bkl_total_time .= $min . "分";
        }
        if ($sec != "0" || $is_display_zero) {
            $is_display_zero = true;
            $m_bkl_total_time .= $sec . "秒";
        }
        if ($m_bkl_total_time == "")
            $m_bkl_total_time = "0 小時";
    } elseif ($m_bkl_book_type == "2000" || $m_bkl_book_type == "4000") {
        $DT_start_datetime = new DateTime($m_bkl_start_date . " " . $m_bkl_start_time);
        $DT_end_datetime = new DateTime($m_bkl_end_date . " " . $m_bkl_end_time);
        $interval = $DT_end_datetime->diff($DT_start_datetime);
        //$m_bkl_total_time = $interval->format('%a %H:%I:%S');
        $day = $interval->format('%a');
        $hr = $interval->format('%h');
        //2018/09/28心北市路邊停車要改成分鐘數
        $min = $interval->format('%i');
        $sec = $interval->format('%s');
        $m_bkl_total_time = "";
        if ($day != "0")
            $m_bkl_total_time .= $day . "天";
        if ($hr != "0")
            $m_bkl_total_time .= $hr . "小時";
        if ($min != "0" || ($sec != "0" && $m_bkl_total_time != "")) { // 如果有天或小時，秒就要進位
            if ($sec != "0")
                $min = ((int)$min) + 1;
            $m_bkl_total_time .= $min . "分";
        }
        if ($sec != "0" && $m_bkl_total_time == "") {
            //$m_bkl_total_time .= $sec . "秒";
            $m_bkl_total_time .= "1分";//有秒數的情況直接顯示1分鐘
        }
        if ($m_bkl_total_time == "")
            $m_bkl_total_time = "0 小時";
    } else if ($m_bkl_book_type == "1002") {
        $tmp = CalculateFullMonth($m_bkl_start_date . " " . $m_bkl_start_time, $m_bkl_end_date . " " . $m_bkl_end_time);
        if($tmp==0) {
            $DT_start_datetime = new DateTime($m_bkl_start_date . " " . $m_bkl_start_time);
            $DT_end_datetime = new DateTime($m_bkl_end_date . " " . $m_bkl_end_time);
            $interval = $DT_end_datetime->diff($DT_start_datetime);
            $sec = ((($interval->format("%a") * 24) + $interval->format("%H")) * 60 + $interval->format("%i")) * 60 + $interval->format("%s");
            $tmp = round($sec/60/60/24/30,1);
        }
        $m_bkl_total_time = $tmp."個月";
    }
    return $m_bkl_total_time;
}
?>
<?php
function ReturnUnicodeJsonModule($arr){
	return json_encode(json_decode(ReturnJsonModule($arr),true),JSON_UNESCAPED_UNICODE);
}
function ReturnJsonModule($arr){
	if(is_array($arr)){
		if(isset($arr["result"])){
			if($arr["result"]==0){
				if(count($arr)==2){// array("result"=>0,"連線錯誤" => mysql_error())
					foreach($arr as $key=>$value){
						if($key=="result"){
							//$arr["result"]=$value;
						}else{
							if(!isset($arr["title"])&&!isset($arr["description"])){
								$arr["title"]=$key;
								$arr["description"]=$value;
							}elseif(isset($arr["title"])){
								$arr["description"]="";
							}elseif(isset($arr["description"])){
								$arr["title"]="";
							}
						}
					}
					return json_encode($arr);
				}elseif(isset($arr["title"])&&isset($arr["description"])){
					return json_encode($arr);
				}else{
					#真不知到這是哪個case
					global $conn,$dbName;
					check_conn($conn,$dbName);
					include_once("/template/email_template.php");
					EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","未知的return",json_encode($arr));
					if(!isset($arr["title"])){
						$arr["title"]="標題未定義";
					}
					if(!isset($arr["description"])){
						$arr["description"]="描述未定義";
					}
					return json_encode($arr);
				}
			}else return json_encode($arr);
		}else if(count($arr)==1){// array("aaa"=>0)
			foreach($arr as $key=>$value){
				$arr["result"]=0;
				$arr["title"]=$key;
				$arr["description"]=$value;
				global $conn,$dbName;
				check_conn($conn,$dbName);
				include_once("/template/email_template.php");
				EmailTemplate($conn,"易停網","upk.rd00001@gmail.com","未知的return",json_encode($arr));
			}
			return json_encode($arr);
		}else return json_encode($arr);
	}else return json_encode($arr);
}
?>
<?php
function ParkingSpaceRating($conn,$m_pk_id){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$sql="SELECT count(*) as count,sum(m_pel_score) as sum FROM tb_Member_Parking_Evaluation_Log as tb_eva LEFT JOIN tb_Member_ParkingSpace_Booking_Log as tb_bkl ON tb_eva.m_bkl_id=tb_bkl.m_bkl_id WHERE tb_bkl.m_pk_id='".$m_pk_id."' AND m_pel_score!=0 ";
	$result=mysql_query($sql,$conn);
	if(!$result){
		return ReturnJsonModule(array("result"=>0,"title"=>0,"description"=>0));
	}
	$ans=mysql_fetch_array($result);
	if(isset($ans["count"]) && $ans["count"]==0){
		return ReturnJsonModule(array("result"=>1,"count"=>0,"sum"=>0,"score"=>10));
	}else{
		return ReturnJsonModule(array("result"=>1,"count"=>$ans["count"],"sum"=>$ans["sum"],"score"=>round($ans["sum"]/$ans["count"],1)));
	}
}
?>
<?php
function ParkingLotRating($conn,$m_plots_id){
	
}
?>
<?php
function EPM_SendRequest($conn,$epm_bkl_id,$status,$is_return_request=false,$unit=0)
{
	$language = "zh-tw";
	$sql = "SELECT * FROM tb_EPM_Booking_Log WHERE epm_bkl_id='" . $epm_bkl_id . "' AND epm_bkl_cancel='0' ORDER BY epm_bkl_sn DESC";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return ReturnJsonModule(array("result" => 0, "title" => "更新錯誤", "description" => mysql_error()));
	}
	elseif (mysql_num_rows($result) == 0) {
		rg_activity_log($conn, "", "交易失敗", "無此交易紀錄", "無此交易紀錄", "");
		$ans = GetSystemCode("5030013", $language, $conn);
		return ReturnJsonModule(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
	}
	$ans = mysql_fetch_array($result);
	$DT_start_datetime = new DateTime($ans["epm_bkl_start_date"] . " " . $ans["epm_bkl_start_time"]);
	$DT_end_datetime = new DateTime($ans["epm_bkl_end_date"] . " " . $ans["epm_bkl_end_time"]);
	$G_EPM_DATA_RECEIVE_URL = "http://54.95.177.240/FEP/fep/dataReceive.action";
	$G_EPM_DATA_RECEIVE_URL = "http://13.251.223.139/fep/dataReceive.action";
	$epm_token = "630cb222-0a42-dee7-0838-b698ce86d1ff";
	$epm_token = "8a523e11-3155-1071-4147-d79d34a40e25";
	$now_date_time = new DateTime();
	$message_array = array();
	if ($status == "001") {
		$message_array = array(
			"result" => "1",
			"points" => $ans["epm_bkl_points"],
			"parking_space_id" => $ans["epm_pk_id"],
			"booking_id" => $ans["epm_bkl_id"],
			"bay" => $ans["epm_bkl_bay"],
			"start_time" => $DT_start_datetime->format("YmdHi")
		);
	}
	elseif ($status == "002") {
		$message_array = array(
			"parking_space_id" => $ans["epm_pk_id"],
			"booking_id" => $ans["epm_bkl_id"],
			"bay" => $ans["epm_bkl_bay"]
		);
	}
	elseif ($status == "003") {
		$message_array = array(
			"parking_space_id" => $ans["epm_pk_id"],
			"booking_id" => $ans["epm_bkl_id"],
			"bay" => $ans["epm_bkl_bay"],
			"start_time" => $ans["epm_stagestarttime"],
			"unit" => $unit,
			"points" => $ans["epm_bkl_points"]
		);
	}
	else {
		$message_array = array();
	}
	$send_to_epm_array = array(
		"DeviceInfo" => array(
			"uniqno" => "52307327",
			"supply" => "52307327",
			"system" => "PM",
			"event" => "201",
			"status" => $status, // 001=第一次付款 002=續費延時  003=第二次付款
			"feedback" => "Y",
			"etime" => $now_date_time->format("YmdHis"),
			"token" => $epm_token,
			"message" => $message_array
		)
	);
	//echo $send_to_epm_json = json_encode($send_to_epm_array);
	$result = PostJsonTo($G_EPM_DATA_RECEIVE_URL, $send_to_epm_array);

	rg_activity_log($conn, "", "綠創新上傳紀錄", "綠創新上傳紀錄", json_encode($send_to_epm_array), $result);

	$tmp_epm_rql_id = "tmp" . GenerateRandomString(11, '0123456789');
	$sql = "INSERT INTO `tb_EPM_Request_Log`( `epm_rql_id`, `epm_bkl_id`, `epm_rql_request`, `epm_rql_requset_datetime`, `epm_rql_response`, `epm_rql_response_datetime`)
				VALUES ( '" . $tmp_epm_rql_id . "', '" . $epm_bkl_id . "', '" . json_encode($send_to_epm_array) . "', now(), '" . $result . "', now());";
	if (!mysql_query($sql, $conn)) {
		return json_encode(array("result" => 0, "新增失敗" => mysql_error($conn) . $sql));
	}
	$sql = "SELECT epm_rql_sn FROM tb_EPM_Request_Log WHERE epm_rql_id='" . $tmp_epm_rql_id . "'";
	$ans = mysql_fetch_assoc(mysql_query($sql, $conn));
	if (!$ans) {
		return json_encode(array("result" => 0, "title" => "查詢失敗", "description" => mysql_error($conn)));
	}
	$new_id = $ans["epm_rql_sn"];
	Sn2Id("ERQL", $new_id);
	$sql = "UPDATE tb_EPM_Request_Log SET epm_rql_id='" . $new_id . "' WHERE epm_rql_id = '" . $tmp_epm_rql_id . "'";
	if (!mysql_query($sql, $conn)) {
		return json_encode(array("result" => 0, "title" => "更新失敗", "description" => mysql_error($conn)));
	}
	if ($is_return_request) {
		return ReturnJsonModule(array("result" => 1, "url" => $G_EPM_DATA_RECEIVE_URL, "request" => $send_to_epm_array, "response" => $result));
	}
	$result_arr = json_decode($result, true);
	if (is_array($result_arr))
		return ReturnJsonModule(array("result" => 1, "data" => $result_arr));
	else
		return ReturnJsonModule(array("result" => 0, "url" => $G_EPM_DATA_RECEIVE_URL, "request" => $send_to_epm_array, "response" => $result));
}
?>
<?php
//取得會員的一輛車，如果該會員沒有車則自動新增一輛車，回傳一個m_ve_id
//要優先選擇有dash的
function GetMemberVehicle($conn,$token,$m_ve_plate_no=""){
	$vehicle_id="";$pure_data="";
	$language="zh-tw";
	$sql = "SELECT m_id, m_language FROM tb_Member WHERE m_token='".$token."'";
	$result = mysql_query($sql, $conn);
	if(! $result){
		return json_encode(array("result"=>0,"title"=>"搜尋token錯誤","description" => mysql_error()));			
	}
	else if(mysql_num_rows($result) == 0){
		#token失效
		rg_activity_log($conn, "", "查詢車輛失敗", "token失效", "", "");
		$ans = GetSystemCode("9", $language, $conn);
		return json_encode(array("result"=> 0,"systemCode"=>$ans[0],"title"=>$ans[1],"description"=>$ans[2]));	
	}
	$ans=mysql_fetch_assoc($result);
	$id = $ans["m_id"];
	$sql = "SELECT m_ve_id,m_ve_plate_no FROM tb_Member_Vehicle WHERE m_ve_delete='0' AND m_id='".$id."' ";
	if($m_ve_plate_no!="")
		$sql.=" AND UPPER(REPLACE(m_ve_plate_no,'-',''))=UPPER(REPLACE('".$m_ve_plate_no."','-','')) ";
	$result = mysql_query($sql, $conn);
	if(! $result)
	{
		return json_encode(array("result"=>0,"title"=>"查詢車輛失敗","description" => mysql_error($conn)));
	}
	else if(mysql_num_rows($result) == 0)
	{
		#無此紀錄
		include_once("/main_api/RegisterVehicleFunc.php");
		//{"activity":"REGISTER VEHICLE","token":"0afcd43d-13d6-e71f-412c-468212d74bc6","vehicle_plate_number":"TEST-0427","ves_id":"VEID0000000000","trademark":"","model":"","length":"0","width":"0","height":"0","weight":"0","color":"000000","can_planepk":"1","can_machinepk":"1","can_undergoundpk":"1","can_threedpk":"1","can_privatepk":"1","can_publicpk":"1","can_streetpk":"1"}

		if($m_ve_plate_no!="")
			$vehicle_plate_number=$m_ve_plate_no;
		else
			$vehicle_plate_number="車號未輸入";
		$ves_id="VEID0000000000";$trademark="";$model="";$length="0";$width="0";$height="0";$weight="0";$color="000000";$can_planepk="1";$can_machinepk="1";$can_undergoundpk="1";$can_threedpk="1";$can_privatepk="1";$can_publicpk="1";$can_streetpk="1";
		$ttans=RegisterVehicleFunc($pure_data,"REGISTER VEHICLE",$token,$vehicle_plate_number,$color,$can_planepk,$can_machinepk,$can_undergoundpk,$can_threedpk,$can_privatepk,$can_publicpk,$can_streetpk,$trademark,$model,$length,$width,$height,$weight,$ves_id,"0","1","1","1");
		$tans=json_decode($ttans,true);
		if(isset($tans["result"]) && $tans["result"]==0){
			return json_encode($tans);
		}else{
			$vehicle_id=$tans["vehicle_id"];
		}
	}
	else{
		//多台車的情況優先選擇有dash的車
		while($ans=mysql_fetch_array($result)){
			if(isTextContain($ans["m_ve_plate_no"],"-")) {
				$vehicle_id = $ans["m_ve_id"];
				break;
			}
			$vehicle_id = $ans["m_ve_id"];
		}
	}
	return json_encode(array("result"=> 1,"m_ve_id"=>$vehicle_id));
}
?>
<?php
function similar_text_array(&$text_array,$text,&$pos_array=array(),$is_auto_numeric=true){
	//pos紀錄位置用 基本兩array的size一樣
	$tmp_array=array();//"percent"=>0,"text"=>""
	$pos_tmp_array=array();//"percent"=>0,"text"=>""
	$percent=0.0;
	foreach($text_array as $key=>$each_text){
		similar_text($each_text,$text,$percent);//percent為這個字串的相似度
		if(is_numeric($text) && $is_auto_numeric){
			$tmp_numeric_percent=0.0;
			$tmp_each_text=preg_replace('/\D/', '',$each_text);;
			similar_text($tmp_each_text,$text,$tmp_numeric_percent);//percent為這個字串的相似度
			if($tmp_numeric_percent>$percent)
				$percent=$tmp_numeric_percent;//如果純數字比對起來比較相近則用純數子的相似度
		}
		//排序
		for($i=0;$i<=count($tmp_array);$i++){
			if($i==count($tmp_array)){
				array_push($tmp_array,array("percent"=>$percent,"text"=>$each_text));
				if(count($text_array)==count($pos_array))
					array_push($pos_tmp_array,$pos_array[$key]);
				break;
			}
			if(isset($tmp_array[$i]["percent"]) && $tmp_array[$i]["percent"]<$percent){//如果現在這筆比較接近
				array_insert($tmp_array,$i,array($i=>array("percent"=>$percent,"text"=>$each_text)));
				if(count($text_array)==count($pos_array)){
					array_insert($pos_tmp_array,$i,array($i=>$pos_array[$key]));
				}
				break;
			}elseif(!isset($tmp_array[$i]["percent"])){
				//應該都會有吧QQ
	//			echo 1;
			}
		}
	//	print_r($tmp_array);
	}
	//print_r($pos_tmp_array);
	//$pos_array=$pos_tmp_array;
	//$output_array=array("text_array"=>$tmp_array,"pos_array"=>$pos_tmp_array);
	//return $output_array;
	//return $tmp_array;
	$text_array=$tmp_array;
	$pos_array=$pos_tmp_array;
	return json_encode(array("result"=> 1));
}
?>
<?php
function similar_text_array2(&$text_array,$text_array2,&$pos_array=array()){

	$tmp_array=array();//"percent"=>0,"text"=>""
	$pos_tmp_array=array();//"percent"=>0,"text"=>""
	foreach($text_array as $key=>$each_text) {
		$tmp_percent = 0.0;
		$percent=0.0;
		//陣列裡面比較最大的出來
		foreach ($text_array2 as $each_text2) {
			similar_text($each_text, $each_text2, $tmp_percent);//percent為這個字串的相似度
			if(is_numeric($each_text2)){
				$tmp_numeric_percent=0.0;
				$tmp_each_text=preg_replace('/\D/', '',$each_text);
				$tmp_each_text2=preg_replace('/\D/', '',$each_text2);
				similar_text($tmp_each_text,$tmp_each_text2,$tmp_numeric_percent);//percent為這個字串的相似度
				if($tmp_numeric_percent>$tmp_percent)
					$tmp_percent=$tmp_numeric_percent;//如果純數字比對起來比較相近則用純數子的相似度
			}
			if($percent<$tmp_percent)
				$percent=$tmp_percent;
		}
		//排序
		for($i=0;$i<=count($tmp_array);$i++){
			if($i==count($tmp_array)){
				array_push($tmp_array,array("percent"=>$percent,"text"=>$each_text));
				if(count($text_array)==count($pos_array))
					array_push($pos_tmp_array,$pos_array[$key]);
				break;
			}
			if(isset($tmp_array[$i]["percent"]) && $tmp_array[$i]["percent"]<$percent){
				//如果現在這筆比較接近
				array_insert($tmp_array,$i,array($i=>array("percent"=>$percent,"text"=>$each_text)));
				if(count($text_array)==count($pos_array)){
					array_insert($pos_tmp_array,$i,array($i=>$pos_array[$key]));
				}
				break;
			}
			elseif(!isset($tmp_array[$i]["percent"])){
			}
		}
	}
	$text_array=$tmp_array;
	$pos_array=$pos_tmp_array;
	return json_encode(array("result"=> 1));
}
?>
<?php
Class LicenseCompareResult{
	public $license_plate;
	public $percent;
	public $m_bkl_id;
	function __construct($license_plate,$owner_license_plate,$m_bkl_id)
	{
		similar_text($license_plate, $owner_license_plate, $tmp_percent);//percent為這個字串的相似度
		$this->license_plate=$license_plate;
		$this->m_bkl_id=$m_bkl_id;
		$this->percent=$tmp_percent;
	}
	static function cmp($a, $b)
	{
		return $a->percent<$b->percent;
	}
	public function __toString()
	{
		return $this->m_bkl_id;
	}
}
function similar_text_array3($all_license_plate,$owner_license_plate,$all_license_plate_bkld) {
	$result_array = [];
	foreach($all_license_plate as $key=>$each_license_plate){
		$this_licence_compare = new LicenseCompareResult($each_license_plate,$owner_license_plate,$all_license_plate_bkld[$key]);
		array_push($result_array,$this_licence_compare);
	}
	usort($result_array, array('LicenseCompareResult','cmp'));
	return $result_array;
}
?>
<?php
function array_insert (&$array, $position, $insert_array) {
	$first_array = array_splice ($array, 0, $position);
	$array = array_merge ($first_array, $insert_array, $array);
}
?>
<?php
/**
 * @param string $m_id 推播的會員ID
 * @param string $m_plots_id 從哪個停車場推的
 * @return string json
 */
function pushAsByPlot($m_id, $m_plots_id)
{
	global $conn, $dbName;
	check_conn($conn, $dbName);
	$this_parking_lot = new UPK_ParkingLot($m_plots_id);
	$sql = "SELECT * FROM tb_Member_As_Map_Notification WHERE plots_id = '".$m_plots_id."' 
		AND is_delete ='0' ORDER BY last_notification_datetime ASC ";
	$result = mysql_query($sql, $conn);

	if (! $result) {
		return json_encode(["result" => 0, "title" => "查詢失敗", "description" => mysql_error()]);
	}
	elseif (mysql_num_rows($result) > 0) {
		$ans = mysql_fetch_assoc($result);
		$asmn_sn = $ans["asmn_sn"];
		$m_as_id = $ans["as_id"];
		$DT_now = new DateTime();
		$sql = "UPDATE tb_Member_As_Map_Notification SET last_notification_datetime='".$DT_now->format("Y-m-d H:i:s")."' 
		WHERE asmn_sn='".$asmn_sn."' ";
		$result = mysql_query($sql, $conn);
		if (! $result) {
			$G_SYSTEM_ERROR_RECEIVE_EMAIL = "upk.rd00001@gmail.com";
			EmailTemplate($conn, "易停網", $G_SYSTEM_ERROR_RECEIVE_EMAIL, "易停網 推播特約店錯誤", "#2629<br>".$sql);
			return json_encode(["result" => 0, "title" => "查詢失敗", "description" => mysql_error()]);
		}
		return pushStore($m_id, $m_as_id);
	}
	return pushNearStore($conn,$m_id,$this_parking_lot->get_m_plots_lng(),$this_parking_lot->get_m_plots_lat());
}

/**
 * 推送附近店家
 * @param $conn
 * @param $m_id
 * @param $lng
 * @param $lat
 * @param int $distance
 * @return string
 */
function pushNearStore ($conn,$m_id,$lng,$lat,$distance=1000) {
	$sql="select * from (SELECT ( 6371000  * acos( cos( radians(".$lat.") ) * cos( radians( m_as_lat ) ) * cos( radians( m_as_lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( m_as_lat ) ) ) ) AS distance,tb_Member_Authorized_Store.m_as_id,tb_Member_Authorized_Store.m_as_name,tb_Member_Authorized_Store.m_as_sale_description FROM tb_Member_Authorized_Store WHERE m_is_close='0' AND m_as_right_enable='2' AND m_as_delete='0' ) as tb_a WHERE tb_a.distance<'".$distance."' ORDER BY tb_a.distance ASC LIMIT 0,1 ";//範圍1公里內
	$result = mysql_query($sql, $conn);
	if(!$result){
		return json_encode(array("result"=>0,"title"=>"查詢失敗", "description"=> mysql_error()));
	}elseif(mysql_num_rows($result)>0){
		$ans = mysql_fetch_assoc($result);
		return pushStore($m_id,$ans["m_as_id"]);
	}
}
function pushStore($m_id,$m_as_id) {

	global $conn,$dbName;
	check_conn($conn,$dbName);
	$sql="SELECT m_as_id,m_as_name,m_as_sale_description,m_as_url FROM tb_Member_Authorized_Store WHERE m_is_close='0' AND m_as_right_enable='2' AND m_as_delete='0' AND m_as_id='".$m_as_id."' ";//範圍1公里內
	$result = mysql_query($sql, $conn);
	if(!$result){
		$G_SYSTEM_ERROR_RECEIVE_EMAIL="upk.rd00001@gmail.com";
		$tans=EmailTemplate($conn,"易停網",$G_SYSTEM_ERROR_RECEIVE_EMAIL,"易停網 推播特約店錯誤","#2665<br>".$sql);
		return json_encode(array("result"=>0,"title"=>"查詢失敗", "description"=> mysql_error()));
	}
	elseif(mysql_num_rows($result)==1){
		$ans = mysql_fetch_assoc($result);
		$this_file_logs = new UPK_File_Logs(CONST_FL_TYPE::AUTHORIZED_SOTRE,$m_as_id,$m_id);
		$file_logs_array = $this_file_logs->get_array();
		$file_logs_result=array("result"=>1,"data"=>$file_logs_array);
		$this_url = new UPK_Url();
		$this_url->add_url($ans["m_as_url"]);

		$DT_now = new Datetime();
		$this_dashboard_parking = new UPKclass_Dashboard_Parking();
		$allData["dbpg_type"] = CONST_DBPG_TYPE::PUSH_AUTHORIZED_STORE;
		$allData["m_plots_id"] = "";
		$allData["m_pk_id"] = "";
		$allData["m_bk_id"] = "";
		$allData["m_dpt_id"] = "";
		$allData["m_id"] = $m_id;
		$allData["dbpg_create_datetime"] = $DT_now->format("Y-m-d H:i:s");
		$allData["m_ve_id"] = "";
		$allData["dbpg_desc"] = "附近的店家";
		$allData["para1"] = $m_as_id;
		$this_dashboard_parking->insert($allData);
		return PushTo($conn,$m_id,"附近的店家",$ans["m_as_name"]." ".$ans["m_as_sale_description"],"",array("PushNearAuthorizedStore"=>array("m_as_id"=>$ans["m_as_id"])),true,"",$file_logs_result,$this_url);
	}
	return json_encode(["result" => 0, "title" => "特約店推播失敗", "description" => "無該特約店"]);
}

?>
<?php
function isGovPaid($conn,$m_id){
	//路邊停車在未繳費之前不能再次路邊停車 判斷繳費金額為0則代表未繳費
	$sql="SELECT count(*) as cnt FROM tb_Member_ParkingSpace_Booking_Log WHERE m_ve_id IN (SELECT m_ve_id FROM tb_Member_Vehicle WHERE m_id='".$m_id."') AND (m_bkl_estimate_point+m_bkl_estimate_free_point)=0 AND m_bkl_plots_type='100' AND m_bkl_create_datetime>'2019-01-01 00:00:00' AND m_bkl_cancel='0' ";
	//排除舊資料
	$result = mysql_query($sql, $conn);
	if(! $result) {
//		return json_encode(array("result"=>0,"title"=>"搜尋失敗","description" => mysql_error($conn)));
		return 0;
	}
	$ans = mysql_fetch_assoc($result);
	if($ans["cnt"]>0) {
	
		//如果有任何一個路ˇ邊停車沒有付款過則不能再付款result=0
		return 0;//0代表沒有付款
	
	}
	else
		return 1;
}

//0= 有未出場的開單,不要讓使用者開單, 1=都已出場
function isGovAppear($conn,$m_id){
	$sql="SELECT * FROM tb_Member_Parking_Log as tb_pl
		LEFT JOIN tb_Member_ParkingSpace_Booking_Log as tb_bkl ON tb_pl.m_bkl_id=tb_bkl.m_bkl_id
		LEFT JOIN tb_Member_ParkingSpace as tb_pk ON tb_pl.m_pk_id=tb_pk.m_pk_id
		LEFT JOIN tb_Member_ParkingLots as tb_plots ON tb_pk.m_plots_id=tb_plots.m_plots_id
		WHERE tb_pl.m_ve_id IN (SELECT m_ve_id FROM tb_Member_Vehicle WHERE m_id='".$m_id."')
		AND tb_pl.m_pl_end_time is null  AND tb_plots.m_plots_type='100'
		AND tb_bkl.m_bkl_create_datetime>'2018-10-01 00:00:00' AND tb_bkl.m_bkl_cancel='0' ";
	//排除舊資料
	$result = mysql_query($sql, $conn);
	if(! $result) {
		return 0;  //預設為有未出場的開單,不要讓使用者開單
	}
	else if(mysql_num_rows($result) > 0) {
		return 0;
	}
	return 1;
}
?>
<?php
//0= white(免費), 1=black(禁止停車)   , -1 = 不是白名單也不是黑名單(就是正常繳費)
function CheckWhiteBlackList($conn,$type_id, $m_id, $datetime="") {
	//return 0= white , 1=black (要跟DB定義一致), -1 無資料

	$generate_sql_wbl = new TimeComponent("", "", "", "", "");
	$generate_sql_wbl->sql_field_name_init("m_wbl_start_date", "m_wbl_end_date", "m_wbl_start_time", "m_wbl_end_time");
	$sql = "SELECT m_wbl_white_or_black,m_wl_schedule_type,m_wl_schedule_weekday,m_wbl_start_date,m_wbl_end_date,m_wbl_start_time,m_wbl_end_time FROM tb_Member_White_Black_List 
		WHERE m_wbl_delete='0' AND m_wbl_type_id='" . $type_id . "' AND m_wbl_pass_id='" . $m_id . "' ";
	$result = mysql_query($sql, $conn);
	if(! $result) {
		return -1;  //當作不存在
	}
	else if(mysql_num_rows($result) == 0) {
		return -1;  //不存在
	}
	if($datetime!="")
		$DT_now = new DateTime($datetime);
	else
		$DT_now = new DateTime();
	$week_j = $DT_now->format('w');
	while($ans=mysql_fetch_array($result)) {
		$DT_start_datetime = new DateTime($ans["m_wbl_start_date"]." ".$ans["m_wbl_start_time"]);
		$DT_end_datetime = new DateTime($ans["m_wbl_end_date"]." ".$ans["m_wbl_end_time"]);
		$DT_start_time = new DateTime($DT_now->format("Y-m-d")." ".$ans["m_wbl_start_time"]);
		$DT_end_time = new DateTime($DT_now->format("Y-m-d")." ".$ans["m_wbl_end_time"]);

		if($ans["m_wl_schedule_type"]=="1") {
			//星期 資料庫是 日一二三四五六 假設星期五則是1111101 但 禮拜五是pow^5=32 所以要strrev
			$b_j = base_convert(strrev($ans["m_wl_schedule_weekday"]),2,10) & pow(2, $week_j);
			if ($b_j == 0) {//weekly只有一筆，如果1~5與周末收費不同則
				#該日無收費
				continue;
			}
			if($DT_start_time<=$DT_now && $DT_now<=$DT_end_time){
				return (int)$ans["m_wbl_white_or_black"];
			}else{
				continue;
			}
		}
		else {
			//臨時
			if($DT_start_datetime<=$DT_now && $DT_now<=$DT_end_datetime){
				return (int)$ans["m_wbl_white_or_black"];
			}else{
				continue;
			}
		}
	}
	return -1;
}
?>
<?php
function getMicroTime($type=0)//0=string 1=DateTime 2=int 3=float
{
	$t = microtime(true);
	$micro = sprintf("%06d",($t - floor($t)) * 1000000);
	$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
	if($type==0)
		return $d->format("Y-m-d H:i:s.u"); // note at point on "u"
	else if($type==1)
		return $d;
	else if($type==3)
		return (float)($d->getTimestamp()+($micro/1000000));
	else return $d->getTimestamp()*1000000+$micro;
}
?>
<?php
function check_conn(&$conn,&$dbName) {
	//$conn = DB::connection()->getPdo();
	//$conn = $conn::__construct();
	//return true;
	if(gettype($conn) != "resource") {
		include("mysql_connect.php");
		if (!$conn) {
			echo ReturnJsonModule (array("連線錯誤" => mysql_error()));
			exit(0);
		}
		#select db
		mysql_query("SET NAMES utf8;", $conn);
		if (!mysql_select_db($dbName, $conn)) {
			echo ReturnJsonModule (array("連接db錯誤" => mysql_error()));
			exit(0);
		}
	}
	return true;
}
?>
<?php
function get_memcache() {
	if(isset($GLOBALS["memcache"]) && $GLOBALS["memcache"]!=false) {
		return $GLOBALS["memcache"];
	}else{
		$memcache = new Memcache;
		@$memcache->pconnect('localhost', 11211);
		$GLOBALS["memcache"] = $memcache;
		return $GLOBALS["memcache"];
	}
	//$GLOBALS["memcache"]=@memcache_connect('localhost', 11211);
	return $GLOBALS["memcache"];
}
?>
<?php
function isTextContain($haystack,$needle,$is_sensitive=false) {
	if($is_sensitive==false){
		$haystack=strtoupper($haystack);
		$needle=strtoupper($needle);
	}
	if(strpos($haystack,$needle) !== false) {
		return true;
	}
	return false;
}
?>
<?php
function get_m_id($token) {
	global $conn, $dbName;//注意這行
	check_conn($conn,$dbName);
	$id="";
	$sql = "SELECT m_id, m_language FROM tb_Member WHERE m_token='" . $token . "'";
	$result = mysql_query($sql, $conn);
	if (!$result) {
	} else if (mysql_num_rows($result) == 0) {
		#token失效
	}else {
		$ans = mysql_fetch_assoc($result);
		$id = $ans["m_id"];
	}
	return $id;
}
?>
<?php
function update_dpt_id($old_m_dpt_id,$m_bkl_id) {
	global $conn, $dbName;//注意這行
	check_conn($conn,$dbName);
	$sql = "SELECT m_dpt_sn FROM tb_Member_Deposit WHERE m_dpt_id='".$old_m_dpt_id."' AND m_bkl_id='".$m_bkl_id."' ";
	$result = mysql_query($sql, $conn);
	if(!$result) {
		return json_encode(array("result"=>0, "title"=>"更新錯誤", "description"=> mysql_error($conn)));
	} elseif(mysql_num_rows($result)>0) {
		$ans = mysql_fetch_assoc($result);
		$m_dpt_sn=$ans["m_dpt_sn"];
		//select sn的大小判斷為包含自己且比自己舊的相同預約
		$sql = "SELECT count(*) as count FROM tb_Member_Deposit WHERE m_bkl_id='".$m_bkl_id."' AND m_dpt_sn<='".$m_dpt_sn."' ";
		$result = mysql_query($sql, $conn);
		if(!$result) {
			return json_encode(array("result"=>0, "title"=>"更新錯誤", "description"=> mysql_error($conn)));
		}
		$ans=mysql_fetch_assoc($result);
		$count=$ans["count"];
		$m_dpt_id="DPTD".substr($m_bkl_id,4).str_pad($count, 4, "0", STR_PAD_LEFT);
		$sql = "UPDATE tb_Member_Deposit SET m_dpt_id='".$m_dpt_id."' WHERE m_dpt_id='".$old_m_dpt_id."' AND m_bkl_id='".$m_bkl_id."' ";
		if(! mysql_query($sql, $conn)) {
			return json_encode(array("result" => 0, "title" => "更新錯誤", "description" => mysql_error()));
		}
	}else{
		return $old_m_dpt_id;
	}
	return $m_dpt_id;
}
//判斷是否有地磁或人工開單資料
//0=無資料或無開單或地磁無車
//1=有車或有開單
function CheckDvStatusBytbDevice($conn, $m_pk_id, &$luke_Check)
{
	include("mysql_connect.php");
	include_once("fn_package.php");
	$res = -1;

	//從Cache中查地磁
	$tmp1 = MemcacheGetValueFromAutoRun("tb_Device_" . $m_pk_id . "_1");
	$luke_Check[__LINE__] = __LINE__ . ", key:" . "tb_Device_" . $m_pk_id . "_1" . ", Memcached:" . $tmp1;
	if ($tmp1 !== '[]')  //有資料
	{
		$tt = json_decode($tmp1, true);
		if (isset($tt) && isset($tt['status'])) {
			$res = (int)$tt['status'];
			$luke_Check[__LINE__] = __LINE__ . ",device status:" . $res;
			return $res;
		}
	} else {
		$sql_deive = "SELECT dv_sn, dv_status FROM tb_Devices WHERE dv_type_id = '" . $m_pk_id . "' AND dv_type='1' AND is_delete='0' ORDER BY dv_sn DESC ";
		//如果該車位有地磁且dv_status='1'(上面有車)時
		$result_deive = mysql_query($sql_deive, $conn);
		if (!$result_deive) {
		} else if (mysql_num_rows($result_deive) >= 1) {
			$ans_deive = mysql_fetch_assoc($result_deive);
			$res = (int)$ans_deive["dv_status"];
			return $res;  //地磁資料優先顯示
		}
	}
	if ($res <= 0) {
		$tmp = MemcacheGetValueFromAutoRun("tb_Device_" . $m_pk_id . "_4");
		//$tmp = MemcacheGetValueFromAutoRun("Luke_Test");
		$luke_Check[__LINE__] = __LINE__ . ", key:" . "tb_Device_" . $m_pk_id . "_4" . ", Memcached:" . $tmp;
		//如果Memcached有值,就用Memcached的, 否則用DB的

		if ($tmp === '[]') {
			$sql_deive = "SELECT dv_update_time, dv_status FROM tb_Devices WHERE dv_type_id = '" . $m_pk_id . "' AND dv_status='1' AND dv_type='4' AND is_delete='0' ORDER BY dv_sn DESC";
			$result_deive = mysql_query($sql_deive, $conn);
			if (!$result_deive) {

			} else if (mysql_num_rows($result_deive) >= 1)  //如果有人工開單
			{
				//return 1;
				$ans_deive = mysql_fetch_assoc($result_deive);
				//$waiting_min=-2;
				//$waiting_status=4;//  4=禁停   //0=灰P(無開放時段)
				$DT_dv_update_time = new DateTime($ans_deive["dv_update_time"]);
				$DT_dv_update_time->modify("+30 min");
				$DT_now = new DateTime();
				$tmp_min = (int)(DateTimeSub($DT_dv_update_time, $DT_now) / 60);
				//if(-90<$tmp_min&&$tmp_min<30){ // 2018/03/29 waiting_min 顯示負數代表循管員已經多久沒有開單(最久-90分)
				//$waiting_min=$tmp_min;//不管人公開單的時間
				$res = (int)$ans_deive["dv_status"];
				//}
			}
		}
		else {
			$tt = json_decode($tmp, true);
			if (!isset($tt)) {
			} else {
				if (!isset($tt['time'])) {

				} else {
					$luke_Check[__LINE__] = __LINE__ . ",device time:" . $tt['time'];
					$time = $tt['time'] / 1000;
					$time_str = date("Y-m-d H:i:s", $time);
					$DT_dv_update_time = new DateTime($time_str);
					$DT_dv_update_time->modify("+30 min");
					$DT_now = new DateTime();
					//$tmp_min = (int)(DateTimeSub($DT_dv_update_time, $DT_now) / 60);
					//if (-90 < $tmp_min && $tmp_min < 30) { // 2018/03/29 waiting_min 顯示負數代表循管員已經多久沒有開單(最久-90分)
						//$waiting_min=$tmp_min;//不管人公開單的時間
						$res = (int)$tt['status'];
						$luke_Check[__LINE__] = __LINE__ . ",device status:" . $res;
					//}
				}
			}
		}
	}
	return $res;
}
function sql_cellphone_compare($field_name,$cellphone) {
	return " TRIM(LEADING '0' FROM ".$field_name.")=TRIM(LEADING '0' FROM '".$cellphone."') ";
}
function win_sys_current_cpu_usage() {
	$cmd = 'typeperf  -sc 1  "\Processor(_Total)\% Processor Time"';
	exec($cmd, $lines, $retval);
	if($retval == 0) {
		$values = str_getcsv($lines[2]);
		return floatval($values[1]);
	} else {
		return false;
	}
}
function is_m_pd_method_paid($m_pd_method){
	//UPK_BookingLog::GetPaidParkingPointsType()
	if($m_pd_method == CONST_M_PD_METHOD::MEMBER_PAID_CREDIT_CARD
		|| $m_pd_method == CONST_M_PD_METHOD::MEMBER_PAID_THIRD_PARTY
		|| $m_pd_method == CONST_M_PD_METHOD::TRASNFER_PAID)
		return true;//點數是用現金購買的(要開發票) 所以是 true
	else return false; //點數是其他就是免費的 所以是false
}
function isWaitingMinSelf($m_pk_id,$m_id) {
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$generate_sql_bkl=new TimeComponent("","","","","");
	$generate_sql_bkl->sql_field_name_init("m_bkl_start_date","m_bkl_end_date","m_bkl_start_time","m_bkl_end_time");
	$DT_now=new DateTime();
	$now_datetime=$DT_now->format("Y-m-d H:i:s");
	$sql_bkl_is_inside = $generate_sql_bkl->SqlTimeComparison_datetime($now_datetime,$now_datetime,"inside");//只有一個時間點用兩個一樣

	//判斷是否為自己自助計費
	$waiting_min_self=0;
	$waiting_min=0;
	$duplicated_m_id_array=array();
	$sql_bkl="SELECT m_ve_id, m_bkl_start_date,m_bkl_start_time FROM tb_Member_ParkingSpace_Booking_Log WHERE m_pk_id='".$m_pk_id."' AND ".$sql_bkl_is_inside." AND m_bkl_cancel='0'";
	$result_bkl = mysql_query($sql_bkl,$conn);
	if(!$result_bkl) {
	}
	elseif(mysql_num_rows($result_bkl)==0) {
	}
	//有人開單，開始判斷是不是自己開的
	else {
		$ans_bkl = mysql_fetch_assoc($result_bkl);
		$DT_m_bkl_start_datetime=new DateTime($ans_bkl["m_bkl_start_date"]." ".$ans_bkl["m_bkl_start_time"]);
		$sql_ve = "SELECT m_id FROM tb_Member_Vehicle WHERE m_ve_id='" . $ans_bkl["m_ve_id"] . "' ";
		$result_ve = mysql_query($sql_ve, $conn);
		if (!$result_ve) {
		}
		else if (mysql_num_rows($result_ve) >= 1) {
			$ans_ve = mysql_fetch_assoc($result_ve);
			#might be one result
			if($ans_ve["m_id"]==$m_id) {
				$waiting_min_self = 1;
				$waiting_min = ceil((DateTimeSub($DT_now, $DT_m_bkl_start_datetime)) / 60);
			}
			else{
				array_push($duplicated_m_id_array,$ans_ve["m_id"]);
			}
		}
	}
	return array("waiting_min_self"=>$waiting_min_self,"waiting_min"=>$waiting_min,"duplicated_m_id_array"=>$duplicated_m_id_array);
}
function calcParkingPoint($m_id,$total_points)
{
	$member_paid_class = new UPK_MemberPaid($m_id);
	$total_pd_points = $member_paid_class->get_total_pd_points();
	if ($total_points > $total_pd_points) {
		#如果停車點數不足
		$tmp_parking_point = $total_pd_points;
		$tmp_point = $total_points - $total_pd_points;
	}
	else {
		#停車點數足夠
		$tmp_parking_point = $total_points;
		$tmp_point = 0;
	}
	return array("tmp_point"=>$tmp_point,"tmp_parking_point"=>$tmp_parking_point);
}
function updateVehiclePlateNo($m_ve_id,$m_ve_plate_no,$m_id="",$pure_data="")
{
	$this_vehicle_data = new UPK_MemberVehicle($m_ve_id);
	$old_m_ve_plate_no = $this_vehicle_data->get_m_ve_plate_no();
	global $conn, $dbName;
	check_conn($conn, $dbName);
	$sql = "UPDATE tb_Member_Vehicle SET m_ve_plate_no='" . $m_ve_plate_no . "' WHERE  m_ve_delete='0' AND m_ve_id='" . $m_ve_id . "' ";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		return json_encode(array("result" => 0, "title" => "搜尋車輛失敗", "description" => mysql_error($conn)));
	}
	//更新車籍資料之後要重入booking的cache，只撈未出場的就好了避免資料太多
	$sql = "SELECT * FROM tb_Member_Parking_Log WHERE m_pl_end_time IS NULL AND m_ve_id='" . $m_ve_id . "' ";
	$result = mysql_query($sql, $conn);
	if (!$result) {
	}
	else {
		while ($ans = mysql_fetch_assoc($result)) {
			MemcacheSetBookingLog('_UPK_BookingLog:', $ans["m_bkl_id"]);
		}
	}
	$DT_now = new Datetime();
	$this_dashboard_parking = new UPKclass_Dashboard_Parking();
	$allData["dbpg_type"] = CONST_DBPG_TYPE::UPDATE_PLATE_NO;
	$allData["m_plots_id"] = "";
	$allData["m_pk_id"] = "";
	$allData["m_bk_id"] = "";
	$allData["m_dpt_id"] = "";
	$allData["m_id"] = $m_id;
	$allData["dbpg_create_datetime"] = $DT_now->format("Y-m-d H:i:s");
	$allData["m_ve_id"] = $m_ve_id;
	$allData["dbpg_desc"] = "更新車牌號碼成功";
	$allData["para1"] = $old_m_ve_plate_no;
	$allData["para2"] = $m_ve_plate_no;
	$this_dashboard_parking->insert($allData);
	rg_activity_log($conn, $m_id, "更新車牌號碼成功", "", $pure_data, "");
	return json_encode(array("result" => 1));
}

function isRepeatExecute($php_name,$m_id) {
	if($m_id=="630cb222-0a42-dee7-0838-b698ce86d1ff" || //300正式版 會員，變數寫m_id但是帶進來的適用token
		$m_id=="630cb222-0a42-dee7-0838-b698ce86d1fg" || //302正式版 會員，變數寫m_id但是帶進來的適用token
		$m_id=="630cb222-0a42-dee7-0838-b698ce86d1ff"
	) return false;
	// return true  有重複 不能繼續執行
	// return false  無重複 可以繼續執行
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$memcache_key=$php_name.$m_id;
	$memcache = get_memcache();
	$get_cache = $memcache->get($dbName . $memcache_key);
//有取到就不執行
//TODO: 要判斷memcache有沒有開啟 否則會一直判斷成重複執行
	if ($get_cache) {
		return true;
	}

	$random_token = GenerateGuid();
//設定與前面一樣的時間過期，免得timeout後沒有清掉cache
//LUKE縮短為5秒
//LUKE縮短為3秒 2019/12/30
	$memcache->set($dbName . $memcache_key, $random_token, MEMCACHE_COMPRESSED, 3);
//確認
	$get_cache = $memcache->get($dbName . $memcache_key);
	if ($get_cache != $random_token) {
		return true;
	}
	return false;
}
function releaseRepeatExecute($php_name,$m_id){
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$memcache_key=$php_name.$m_id;
	$memcache = get_memcache();
	$memcache->delete($dbName . $memcache_key);
}

function isWhiteBlackManager($m_id, $m_wbl_id) {
    global $conn, $dbName;
    check_conn($conn, $dbName);
	//LUKE:這邊的SQL看不懂,為什麼要檢查tb_Member_White_Black_List
    $sql = "SELECT m_wbl_type_id FROM tb_Member_White_Black_List WHERE m_wbl_id = '" . $m_wbl_id . "' AND m_wbl_delete = 0";
    $result = mysql_query($sql, $conn);
    if(! $result)
    {
        return false;
    }
    else if(mysql_num_rows($result) == 0)
    {
        return false;
    }
    else
    {
        $ans = mysql_fetch_assoc($result);
        $m_wbl_type_id = $ans["m_wbl_type_id"];

        $sql = "SELECT * FROM tb_Whiteblack_Manager WHERE wbm_type_id = '" . $m_wbl_type_id . "' AND wbm_m_id = '" . $m_id . "' AND wbm_delete = 0";
        $result = mysql_query($sql, $conn);
        if(! $result)
        {
            return false;
        }
        else if(mysql_num_rows($result) == 0)
        {
            return false;
        }
        else
        {
        	return true;
        }
    }

    return false;   //Default, just in case code falls here, which should be impossible
}
//LUKE ADD Function 
function CheckMemberIsWhiteBlackManager($m_id, $wbm_type_id) {
    global $conn, $dbName;
    check_conn($conn, $dbName);
	$sql = "SELECT * FROM tb_Whiteblack_Manager WHERE wbm_type_id = '" . $wbm_type_id . "' AND wbm_m_id = '" . $m_id . "' AND wbm_delete = 0";
    $result = mysql_query($sql, $conn);
    if(! $result)
    {
        return false;
    }
    else if(mysql_num_rows($result) == 0)
    {
        return false;
    }
    else
    {
    	return true;
    }
    
}

function isWhiteBlackListTypeId($m_wbl_type_id,$m_wbl_pass_id) {
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$sql="SELECT * FROM tb_Member_White_Black_List WHERE m_wbl_type_id='".$m_wbl_type_id."' AND m_wbl_pass_id='".$m_wbl_pass_id."' AND m_wbl_delete='0' ";
	$result = mysql_query($sql, $conn);
	if(!$result){
		return false;
	}
	else if(mysql_num_rows($result)==0) {
	}
	$DT_now=new DateTime();
	while($ans = mysql_fetch_assoc($result)) {
		$this_white_black_list = new UPK_WhiteBlackList($ans["m_wbl_id"]);
		if ($this_white_black_list->isTimeAvailable($DT_now)) {
			return true;
		}
	}
	return false;
}
function isWhiteBlackManagerTypeId($wbm_type_id,$wbm_m_id) {
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$sql="SELECT * FROM tb_Whiteblack_Manager WHERE wbm_type_id='".$wbm_type_id."' AND wbm_m_id='".$wbm_m_id."' AND wbm_delete='0'  AND wbm_can_read='1' ";
	$result = mysql_query($sql, $conn);
	if(!$result){
		return false;
	}
	else if(mysql_num_rows($result)==0) {
		return false;
	}
	return true;
}
?>
<?php
/**
 * check cell phone format
 * @param  string  $cellphone_country_code
 * @param  integer  $cellphone
 * @return boolean
 */
function isValidPhoneNo($cellphone_country_code, $cellphone)
{
	switch ($cellphone_country_code) {
		case "+886":
			if (preg_match("/^(9|09)[0-9]{2}-[0-9]{3}-[0-9]{3}$/", $cellphone)) {
				return true;    // 9xx-xxx-xxx
			} elseif (preg_match("/^(9|09)[0-9]{2}-[0-9]{6}$/", $cellphone)) {
				return true;    // 09xx-xxxxxx
			} elseif (preg_match("/^(9|09)[0-9]{8}$/", $cellphone)) {
				return true;    // 09xxxxxxxx
			} else {
				return false;
			}
		default:
			return true;
	}
}
?>
