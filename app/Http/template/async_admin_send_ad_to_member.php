<?php
header("Content-type: application/json; charset=utf-8");
mb_internal_encoding('utf-8');
?>

<?php
{
	ignore_user_abort(true);
	ini_set("max_execution_time", "24000");
	include_once("fn_package.php");
	include_once("UPKclass.php");
	$pure_data = file_get_contents('php://input');
	$data_back = json_decode($pure_data);
	if (isset($data_back->{"activity"})) {
		$activity = InputRegularExpression($data_back->{"activity"}, "text");
	}
	else $activity = "";
	if (isset($data_back->{"token"})) {
		$token = InputRegularExpression($data_back->{"token"}, "text");
	}
	else $token = "";
	if (isset($data_back->{"type"})) {
		$type = InputRegularExpression($data_back->{"type"}, "text");
	}
	else $type = "0";
	if (isset($data_back->{"is_send_notification"})) {
		$is_send_notification = InputRegularExpression($data_back->{"is_send_notification"}, "text");
	}
	else $is_send_notification = 0;
	if (isset($data_back->{"is_send_email"})) {
		$is_send_email = InputRegularExpression($data_back->{"is_send_email"}, "text");
	}
	else $is_send_email = 0;
	if (isset($data_back->{"is_send_sms"})) {
		$is_send_sms = InputRegularExpression($data_back->{"is_send_sms"}, "text");
	}
	else $is_send_sms = 0;
	if (isset($data_back->{"content"})) {
		//$content = InputRegularExpression($data_back->{"content"},"text");
		$content = $data_back->{"content"};
	}
	else $content = "";
	if (isset($data_back->{"title"})) {
		$title = InputRegularExpression($data_back->{"title"}, "text");
	}
	else $title = "";
	if (isset($data_back->{"fl_id_array"})) {
		$fl_id_array = InputRegularExpression($data_back->{"fl_id_array"}, "array");
	}
	else $fl_id_array = "";
	if (isset($data_back->{"m_id_array"})) {
		$m_id_array = InputRegularExpression($data_back->{"m_id_array"}, "array");
	}
	else $m_id_array = array();
	if (isset($data_back->{"url"})) {
		$url = InputRegularExpression($data_back->{"url"}, "url");
	}
	else $url = "";
	if (isset($data_back->{"is_all_member"})) {
		$is_all_member = InputRegularExpression($data_back->{"is_all_member"}, "text");
	}
	else $is_all_member = 0;
//0=none 1= only cellphone  2= only email 3= both have cellphone and email
//cellphone means have push notification key
	global $conn, $dbName;
	check_conn($conn, $dbName);
	$language = "zh-tw";
	if ($activity == null || $token == null) {
		rg_activity_log($conn, "", "後台推送廣告失敗", "必填欄位未填", $pure_data, "");
		$ans = GetSystemCode("20096", $language, $conn);
		echo json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
		mysql_close($conn);
		return;
	}
	else if ($activity != "SEND AD TO MEMBER") {
		rg_activity_log($conn, "", "後台推送廣告失敗", "activity錯誤", $pure_data, "");
		$ans = GetSystemCode("20097", $language, $conn);
		echo json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
		mysql_close($conn);
		return;
	}

#確認使用者
	$sql = "SELECT m_id, m_language FROM tb_Member WHERE m_token='" . $token . "'";
	$result = mysql_query($sql, $conn);
	if (!$result) {
		echo json_encode(array("result" => 0, "title" => "後台推送廣告失敗", "description" => mysql_error($conn)));
		return;
	}
	else if (mysql_num_rows($result) == 0) {
		#token失效
		rg_activity_log($conn, "", "後台推送廣告失敗", "token失效", $pure_data, "");
		$ans = GetSystemCode("9", $language, $conn);
		echo json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
		mysql_close($conn);
		return;
	}
	$ans = mysql_fetch_assoc($result);
	$id = $ans["m_id"];
	$language = $ans["m_language"];
	if (!is_administrator($id)) {
		rg_activity_log($conn, "", "進入管理員模式失敗", "會員非管理員", $pure_data, "");
		$ans = GetSystemCode("20068", $language, $conn);
		echo json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
		mysql_close($conn);
		return;
	}
	$email_success_cnt = 0;
	$push_success_cnt = 0;
	$email_failed_cnt = 0;
	$push_failed_cnt = 0;
	$fl_push_data_array = array();
	foreach ($fl_id_array as $each_fl_id) {
		$this_file_log = new UPK_FileLog($each_fl_id);
		array_push($fl_push_data_array, $this_file_log->get_array());
	}
	$fl_push_array = array("result" => 1, "data" => $fl_push_data_array);
	$tiny_url = "";
	if ($url != "") {
		$tiny_url = get_tiny_url($url);
		if ($tiny_url == false)
			$tiny_url = $url;
	}
	$email_url_string = "";
	$sms_url_string = "";
	$this_url = new UPK_Url();
	if ($tiny_url != "") {
		$email_url_string = "<br>網址：" . $tiny_url;
		$sms_url_string = "\n網址：" . $tiny_url;
		$this_url->add_url($tiny_url);
	}
	if($is_all_member==1) {
		//只有推播這樣做，其他要發的話有可能會造成過高的收費(簡訊一則近1元同時發給幾萬個裝置、gmail有上限，超過也需額外付費)
		//而且不要insert notification log只需要手動 insert 1筆
		if(IsDebug()){
			$title="測試版信息: ".$title;
		}
		$m_pnl_push_array="";
		$sql = "INSERT INTO `tb_Member_Push_Notification_Log`(`m_id`, `m_bkl_id`, `m_pnl_push_key`, `m_pnl_title`, `m_pnl_message`, `m_pnl_is_sent`, `m_pnl_send_datetime`, `m_pnl_push_array`, `m_pnl_is_read`, `m_pnl_file_logs`, `m_pnl_url`, `m_pnl_is_all_member`) VALUES
		('', '', '', '" . mysql_escape_string($title) . "', '" . mysql_escape_string($content) . "', '1', now(), '" . $m_pnl_push_array . "', '0' , '".json_encode($fl_push_array)."', '".json_encode($this_url->get_array())."', '1' )";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			echo json_encode(array("result" => 0, "title" => "後台推送廣告失敗", "description" => mysql_error($conn)));
			return;
		}
		$sql = "SELECT DISTINCT m_id FROM tb_Member_Push_Notification_Key WHERE is_delete='0' AND m_pnk_push_notification_enable='1' ";
		$result = mysql_query($sql, $conn);
		if (!$result) {
			echo json_encode(array("result" => 0, "title" => "後台推送廣告失敗", "description" => mysql_error($conn)));
			return;
		}
		while($ans=mysql_fetch_assoc($result)) {
			if ($ans["m_id"] != "") {
				PushTo($conn, $ans["m_id"], $title, $content, "", array(), false, "", $fl_push_array, $this_url, -1);
				$push_success_cnt++;
			}
		}

		$response = json_encode(array(
			"result" => 1,
			"email_success_cnt" => $email_success_cnt,
			"email_failed_cnt" => $email_failed_cnt,
			"push_success_cnt" => $push_success_cnt,
			"push_failed_cnt" => $push_failed_cnt
		));
		EmailTemplate($conn, "易停網管理員", "upk.rd00001@gmail.com",
			"易停網管理員-後台廣告推廣通知信",
			"ID: " . json_encode($m_id_array) . "<br>" . ": " . "<br>標題: " . $title . "<br>內容: " . $content .
			"<br>" . $response);
		echo $response;
		rg_activity_log($conn, $id, "後台推送廣告成功", "", $pure_data, "");
		return;
	}
	if ($type == "1" && is_array($m_id_array) && count($m_id_array) > 0 && $content != "" && $title != "") {

		include_once("/../template/email_template.php");
		//Make copy of content for email and replace content
		$content_email = str_replace("\r\n", "<br>", $content);
		$content_email = str_replace("\n\r", "<br>", $content_email);    //In case the order is the other way round

		foreach ($m_id_array as $m_id) {
			$this_member = new UPK_Member($m_id);
			if ($is_send_email && $this_member->get_m_email_verify() == 1) {
				$tans = json_decode(EmailTemplate($conn, $this_member->get_m_last_name() . " " . $this_member->get_m_first_name(), $this_member->get_m_email(), $title, $content_email . $email_url_string), true);
				if (isset($tans["result"]) && $tans["result"] == "1")
					$email_success_cnt++;
				else
					$email_failed_cnt++;
			}
			if ($is_send_notification) {
				PushTo($conn, $m_id, $title, $content, "", array(), true, "", $fl_push_array, $this_url);
				$push_success_cnt++;
			}
			if ($is_send_sms) {
				include_once("/../main_api/sendsms_func.php");
				$ttans = sendsmsfunc("SEND SMS", $this_member->get_m_cellphone_country_code(), $this_member->get_m_cellphone(), $title . ":" . $content . $sms_url_string);

			}
		}
		$response = json_encode(array(
			"result" => 1,
			"email_success_cnt" => $email_success_cnt,
			"email_failed_cnt" => $email_failed_cnt,
			"push_success_cnt" => $push_success_cnt,
			"push_failed_cnt" => $push_failed_cnt
		));
		EmailTemplate($conn, "易停網管理員", "upk.rd00001@gmail.com",
			"易停網管理員-後台廣告推廣通知信",
			"ID: " . json_encode($m_id_array) . "<br>" . ": " . "<br>標題: " . $title . "<br>內容: " . $content .
			"<br>" . $response);
		echo $response;
		rg_activity_log($conn, $id, "後台推送廣告成功", "", $pure_data, "");
		return;
	}
	else {
		rg_activity_log($conn, "", "後台推送廣告失敗", "必填欄位未填", $pure_data, "");
		$ans = GetSystemCode("20096", $language, $conn);
		echo json_encode(array("result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]));
		mysql_close($conn);
		return;
	}
}
?>
