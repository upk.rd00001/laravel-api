<?php
ini_set("max_execution_time", "120");//2分鐘
header("Content-type: application/json; charset=utf-8");
include_once("/../fn_package.php");
ignore_user_abort(true);

//for($i=0;$i<1000000;$i++)
// for($j=0;$j<4000;$j++);
$pure_data = file_get_contents('php://input');
$data_back = json_decode($pure_data);
if (isset($data_back->{"activity"})) {
	$activity = InputRegularExpression($data_back->{"activity"}, "text");
}
else {
	$activity = null;
}
if (isset($data_back->{"token"})) {
	$token = InputRegularExpression($data_back->{"token"}, "text");
}
else {
	$token = null;
}
if (isset($data_back->{"evse_id"})) {
	$evse_id = InputRegularExpression($data_back->{"evse_id"}, "text");
}
else {
	$evse_id = null;
}
if (isset($data_back->{"mode"})) {//1=force start  2=start  3=force close 4=close 5=status
	$mode = InputRegularExpression($data_back->{"mode"}, "text");
}
else {
	$mode = null;
}

global $conn, $dbName;
check_conn($conn, $dbName);

$language = "zh-tw";
if ($activity == null || $token == null || $evse_id == null || $mode == null) {
	rg_activity_log($conn, "", "控制充電樁失敗", "必填欄位未填", $pure_data, "");
	$ans = GetSystemCode("7050003", $language, $conn);
	echo json_encode(["result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]]);
	mysql_close($conn);
	return;
}
else {
	if ($activity != "ASYNC WINCHARGE") {
		rg_activity_log($conn, "", "控制充電樁失敗", "activity錯誤", $pure_data, "");
		$ans = GetSystemCode("7050004", $language, $conn);
		echo json_encode(["result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]]);
		mysql_close($conn);
		return;
	}
}

#確認使用者
$sql = "SELECT m_id, m_language FROM tb_Member WHERE m_token='".$token."'";
$result = mysql_query($sql, $conn);
if (! $result) {
	echo json_encode(["result" => 0, "title" => "控制充電樁失敗", "description" => mysql_error($conn)]);
	return;
}
else {
	if (mysql_num_rows($result) == 0) {
		#token失效
		rg_activity_log($conn, "", "控制充電樁失敗", "token失效", $pure_data, "");
		$ans = GetSystemCode("9", $language, $conn);
		echo json_encode([
			"result" => 0,
			"systemCode" => $ans[0],
			"title" => $ans[1],
			"description" => $ans[2],
			"debug" => "token失效",
		]);
		mysql_close($conn);
		return;
	}
}
$ans = mysql_fetch_assoc($result);
$id = $ans["m_id"];
$language = $ans["m_language"];

$response_wincharge = [];
$this_wincharge = new UPK_Wincharge($evse_id, $id);
if ($mode == 1) {
	$response_wincharge = $this_wincharge->force_start_charging();
}
elseif ($mode == 2) {
	$response_wincharge = $this_wincharge->start_charging();
}
elseif ($mode == 3) {
	$response_wincharge = $this_wincharge->force_stop_charging();
}
elseif ($mode == 4) {
	$response_wincharge = $this_wincharge->stop_charging();
}
elseif ($mode == 5) {
	$response_wincharge = $this_wincharge->get_status();
}
echo ReturnJsonModule(["result" => 1, "data" => $response_wincharge]);
mysql_close($conn);
return;

?>