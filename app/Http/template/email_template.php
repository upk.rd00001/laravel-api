<?php
include_once("/../fn_package.php");
function EmailTemplate($conn, $sndname, $sendto,$title,$body,$is_verify=0,$file_name=""){
	if($sendto == "") {
		return json_encode(array("result"=>0,"title"=>"必填欄位未填","description"=>"Email不可為空"));
	}
	$verify_email_pic1="https://member.space4car.com/images/VerifyEmailpic1.png";
	$verify_email_pic2="https://member.space4car.com/images/VerifyEmailpic2.png";
	$t_timee = date("Y-m-d H:i:s");
 /*$tans=SendEmail($conn,"SEND MAIL",$sndname, $sendto,$title,"<img src=\"".$verify_email_pic1."\" alt=\"".$t_timee."\"  width=\"300\"><br/><br/>"
					 				."<img src=\"".$verify_email_pic2."\" alt=\"".$t_timee."\">".$sndname." 您好:<br/><br/>"
					 	//			."<img src=\"".$verify_email_pic2."\" alt=\"".$t_timee."\">    感謝您加入 易停網 的會員，<br/>"
					 				.$body."<br/><br/>"
					 				."<img src=\"".$verify_email_pic2."\" alt=\"".$t_timee."\">    =========================<br/>"
					 				."<img src=\"".$verify_email_pic2."\" alt=\"".$t_timee."\">    祝您有個美好的一天!<br/>"
					 				."<img src=\"".$verify_email_pic2."\" alt=\"".$t_timee."\">    易停網團隊 敬上<br/><br/>"
					 				."<img src=\"".$verify_email_pic2."\" alt=\"".$t_timee."\">--提醒您!本郵件由系統自動寄出，請勿回覆此信件--");	
	return $tans;*/
	
	$arr["sndname"]=$sndname;
	$arr["sendto"]=$sendto;
	$arr["title"]=$title;
	$arr["is_verify"]=$is_verify;
	$arr["file_name"]=$file_name;
	$arr["body"]="<img src=\"".$verify_email_pic1."\" alt=\"".$t_timee."\"  width=\"300\"><br/><br/>"
					 				."<img src=\"".$verify_email_pic2."\" >".$sndname." 您好:<br/><br/>"
					 	//			."<img src=\"".$verify_email_pic2."\">    感謝您加入 易停網 的會員，<br/>"
					 				.$body."<br/><br/>"
					 				."<img src=\"".$verify_email_pic2."\" >    =========================<br/>"
					 				."<img src=\"".$verify_email_pic2."\" >    祝您有個美好的一天!<br/>"
					 				."<img src=\"".$verify_email_pic2."\" >    易停網團隊 敬上<br/><br/>"
					 				."<img src=\"".$verify_email_pic2."\" >--提醒您!本郵件由系統自動寄出，請勿回覆此信件--";
	$json=json_encode($arr);
	if(IsDebug())
		$testurl="sandbox.api.space4car.com/sandbox_api/template/async_send_email.php";
	else 
		$testurl="api.space4car.com/api/template/async_send_email.php";	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $testurl);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);	//
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_TIMEOUT, 1);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
	$result = curl_exec($curl);

	rg_activity_log($conn,"","發送信件LOG",$sndname."-".$sendto."-".$title,$json,$result);
	curl_close($curl);
	return json_encode(array("result"=>1));
}
function EmailTemplate_m_id($conn,$m_id,$title,$body,$is_verify=0,$file_name=""){
	$sql="SELECT * FROM tb_Member WHERE m_id='".$m_id."' ";
	$result=mysql_query($sql, $conn);
	if(!$result){
		return json_encode(array("result"=>0,"title"=>"連線錯誤", "description"=> mysql_error()));
	}elseif(mysql_num_rows($result)==0){
		return json_encode(array("result"=>0,"title"=>"無該會員", "description"=> mysql_error()));
	}elseif(mysql_num_rows($result)==1){
		$ans=mysql_fetch_assoc($result);
		if($ans["m_email"]==""){
			return json_encode(array("result"=>0,"title"=>"電子郵件不可為空", "description"=> mysql_error()));
		}
		return EmailTemplate($conn, $ans["m_last_name"]." ". $ans["m_first_name"], $ans["m_email"],$title,$body,$is_verify,$file_name);
	}else{
		return json_encode(array("result"=>0,"title"=>"多個會員ID怎麼回事?", "description"=> mysql_error()));
	}

}
function EmailTemplateInvoice($conn,$m_id,$title,$body,$data=[],$arg=[]){
	//是鼎天發票則要寄給會員
	if(isset($arg["is_tweisc"]))
		$is_tweisc=$arg["is_tweisc"];
	else $is_tweisc=0;
	$sql="SELECT * FROM tb_Member WHERE m_id='".$m_id."' ";
	$result=mysql_query($sql, $conn);
	$is_verify=0;
	$file_name="";
//	$body.=InvoiceHtml($data);
	$email_body = $body."<br>".InvoiceHtml($data);
//	echo $email_body;
	if(!$result){
		return json_encode(array("result"=>0,"title"=>"連線錯誤", "description"=> mysql_error()));
	}elseif(mysql_num_rows($result)==0){
		return json_encode(array("result"=>0,"title"=>"無該會員", "description"=> mysql_error()));
	}elseif(mysql_num_rows($result)==1){
		$ans=mysql_fetch_assoc($result);
		$m_email = $ans["m_email"]; //或許是要寄給該會員
		$m_first_name = $ans["m_first_name"]; //或許是要寄給該會員
		$m_last_name = $ans["m_last_name"]; //或許是要寄給該會員

		if($m_email!="" && $is_tweisc==1) {
			if(isset($arg["CustomerEmail"]))
				$m_email=$arg["CustomerEmail"];
			if(isset($arg["CustomerName"]))
				$sndname=$arg["CustomerName"];
			if($sndname=="")
				$sndname="易停網會員";
			EmailTemplate($conn, $sndname, $m_email, $title, $email_body, $is_verify, $file_name);
		}
		$is_verify=1;
		EmailTemplate($conn, "易停網發票客服", "space4car688@gmail.com",$title,$email_body,$is_verify,$file_name);
		if($m_email=="" && $is_tweisc==1){
			$title="會員信箱為空".$title;
		}
		EmailTemplate($conn, "Yang Tiger", "upk.rd00001@gmail.com",$title,$email_body,$is_verify,$file_name);
		return;
	}else{
		return json_encode(array("result"=>0,"title"=>"多個會員ID怎麼回事?", "description"=> mysql_error()));
	}
}
function InvoiceHtml($data){
	include_once ("/../barcode.php");
	include_once ("/../phpqrcode/qrlib.php");

	$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
	//html PNG location prefix
	$PNG_WEB_DIR = 'temp/';
	if (!file_exists($PNG_TEMP_DIR))
		mkdir($PNG_TEMP_DIR);
	$text_datetime=$data["IIS_Create_Date"];
	$DT_IIS_Create_Date=new DateTime($text_datetime);
	$this_month=(int)$DT_IIS_Create_Date->format("m");
	$this_year=(int)$DT_IIS_Create_Date->format("Y");
	$this_number=$data["IIS_Number"];
	$split= "-"; //要插入的特殊符號
	$text_number = substr($this_number,0,2).$split.substr($this_number,2); //字串前2個字+特殊字符+字串第2個字元後的字串
	$text_year=$this_year-1911; //西曆轉國曆
	$text_attext="";//附加文字
	$text_randcode=$data["IIS_Random_Number"];//隨機四碼
	$text_total=$data["IIS_Sales_Amount"];//總金額
	if(isset($data["Title"]))
		$title=$data["Title"];
	else
		$title="易停網股份有限公司";//
	if(isset($data["IIS_Sales_Identifier"]))
		$text_serialid=$data["IIS_Sales_Identifier"];
	else
		$text_serialid="52307327";//賣家統編
	$this_byerid=$data["IIS_Identifier"];//買家統編
	if($this_byerid=="0000000000"){
		$text_byerid="";
	}else{
		$text_byerid="買方 ".$this_byerid;
	}
	$barcode_month="00";
	if(1<=$this_month && $this_month<=2){
		$text_month="01-02";
		$barcode_month="02";
	}
	elseif(3<=$this_month && $this_month<=4){
		$text_month="03-04";
		$barcode_month="04";
	}
	elseif(5<=$this_month && $this_month<=6){
		$text_month="05-06";
		$barcode_month="06";
	}
	elseif(7<=$this_month && $this_month<=8){
		$text_month="07-08";
		$barcode_month="08";
	}
	elseif(9<=$this_month && $this_month<=10){
		$text_month="09-10";
		$barcode_month="10";
	}
	elseif(11<=$this_month && $this_month<=12){
		$text_month="11-12";
		$barcode_month="12";
	}
	else{
	}
	if(isset($data["PosBarCode"]) && $data["PosBarCode"]!="") {
		$this_barcode = $data["PosBarCode"];//bar code 39
	}
	else{
		$this_barcode=$text_year.$barcode_month.$this_number.$text_randcode;
	}
	$filename_barcode = $PNG_TEMP_DIR . 'BarCode' . md5($this_barcode) . '.png';
	$barcode_imgraw = barcode::code39($this_barcode, 50, 1, $filename_barcode);
	//$filename = $PNG_TEMP_DIR.'test.png';
	$matrixPointSize = 4;// 1~10
	$errorCorrectionLevel = 'L'; // 'L','M','Q','H'
	$qrcode_left_text=$data["QRCode_Left"];
	$qrcode_right_text=$data["QRCode_Right"];
	$filename_left = $PNG_TEMP_DIR.'QRCode_Left'.md5($qrcode_left_text.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
	$qrcodeleft_imgraw = QRcode::base64($qrcode_left_text, $filename_left, $errorCorrectionLevel, $matrixPointSize, 2);
	$filename_right = $PNG_TEMP_DIR.'QRCode_Left'.md5($qrcode_left_text.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
	$qrcoderight_imgraw = QRcode::base64($qrcode_right_text, $filename_right, $errorCorrectionLevel, $matrixPointSize, 2);

	/*$body.="易停網<br>";
	$body.="Space4car<br>";
	$body.="電子發票證明聯<br>";
	$body.=$text_year."年".$text_month."月<br>";
	$body.=$text_number."<br>";
	$body.=$text_datetime." ".$text_attext."<br>";
	$body.="隨機碼:".$text_randcode." 總計:".$text_total."<br>";
	$body.="賣方:".$text_serialid." ".$text_byerid."<br>";
	$body.="<img src='data:image/png;base64,".base64_encode($barcode_imgraw)."'><br>";
	$body.="<img src='data:image/png;base64,".base64_encode($qrcodeleft_imgraw)."'><br>";
	$body.="<img src='data:image/png;base64,".base64_encode($qrcoderight_imgraw)."'><br>";
	//$body .= '<img src="'.$PNG_TEMP_DIR.$PNG_WEB_DIR.basename($filename_left).'" /><hr/>';
	//$body .= '<img src="'.$PNG_TEMP_DIR.$PNG_WEB_DIR.basename($filename_right).'" /><hr/>';
	*/
	$body="";
	/*$body.='<style>
.invoiceContainer{font-family: \"Times New Roman\", Times, serif;}
.invoiceContainer .invoice_inner{ width:5.7cm;}
.invoiceContainer .invoice_inner .invoice{ width:5.7cm; height:9cm;text-align:center;border:1px solid #CCC; padding:5px; }
.invoice h1{font-size:19px; line-height:1.2; max-height:68px;overflow:hidden;font-weight:bold; margin:0;}
.invoice h2{ font-size:21px; font-weight:bold; line-height:1; margin:5px 0 4px;}
.invoice h3{ font-size:24px; margin:0 0 2px 0; line-height:1; font-weight:bold;}
.invoice ul, .invoice_details ul { margin:0; overflow:hidden;}
.invoice ul li, .invoice_details ul li{color:#333; margin:0 0 3px 0!important; line-height:18px; font-size:14px;overflow:hidden;list-style:none !important; width:100%; text-align:left;}
.invoice li span.left, .invoice_details ul li span.left{ float:left;}
.invoice li span.right, .invoice_details ul li span.right{ float:right; text-align:right;}
.invoice .QRCode img{ margin:3px 5px;}
.invoiceContainer .invoice_inner .invoice_details{ width:5.7cm;border:1px solid #CCC; border-top:none; padding:10px; text-align:center;}
.invoice_details h2{ margin:0;font-size:22px; font-weight:bold; margin:5px 0 10px 0; }
.invoice_details ul{ border-bottom:1px dashed #333; margin-bottom:10px;}
.invoice_details .table{ border:none; border-bottom:1px dashed #333; margin-bottom:5px;}
.invoice_details .table tr td, .invoice_details .table tr th{ border:none; font-size:12px; padding:0; text-align:left; padding:3px;}
.invoice_details .table tr th{ text-align:center;}
.invoice_details h4{ text-align:left; margin:0; font-size:14px; font-weight:bold;}
.record ul{ margin-top:20px; margin-left:25px;}
.record ul li{ color:#555; list-style: outside disc; line-height:22px;}</style>';*/
	$body.='<div class="invoice_inner" style="width:5.7cm;">
		<div class="invoice" style="vertical-align: middle;width:5.7cm; height:9cm;text-align:center;border:1px solid #CCC; padding:5px;">
			 <h1 style="font-size:22px;font-size:19px; line-height:1.2; max-height:68px;overflow:hidden;font-weight:bold; margin:0;">
'.$title.'                                        </h1>
			<h2 style="text-align:center; width:5.7cm; font-size:21px; font-weight:bold; line-height:1; margin:5px 0 4px;">
電子發票證明聯</h2>
			<h3 class="date" style=" font-size:24px; margin:0 0 2px 0; line-height:1; font-weight:bold;">
'.$text_year.'年'.$text_month.'月</h3>
			<h3 style=" font-size:24px; margin:0 0 2px 0; line-height:1; font-weight:bold;">
'.$text_number.'</h3>
			<ul style=" margin:0 0 0 0; padding-left: 0; overflow:hidden;">
				<li style="line-height: 15px; color:#333; margin:0 0 3px 0!important; line-height:18px; font-size:14px;overflow:hidden;list-style:none !important; width:100%; text-align:left; padding: 0 0 0 0; "><span class="left" style="float:left;"><font style="font-weight: 500;">'.$text_datetime.'</font></span>
					<span class="right" style="float:right; text-align:right;"><font style="font-weight: 500;">格式 25 </font></span>
				</li>
				<li style="line-height: 15px; color:#333; margin:0 0 3px 0!important; line-height:18px; font-size:14px;overflow:hidden;list-style:none !important; width:100%; text-align:left;"><span class="left" style="float:left;"><font style="font-weight: 500;">隨機碼：'.$text_randcode.'</font></span>
					<span class="right" style="float:right; text-align:right;"><font style="font-weight: 500;">總計：'.$text_total.'</font></span>
				</li>
				<li style="line-height: 15px; color:#333; margin:0 0 3px 0!important; line-height:18px; font-size:14px;overflow:hidden;list-style:none !important; width:100%; text-align:left;"><span class="left" style="float:left;"><font style="font-weight: 500;">賣方 '.$text_serialid.'</font></span>
					<span class="right" style="float:right; text-align:right;"><font style="font-weight: 500;">'.$text_byerid.'</font></span>
				</li>
			</ul>
			<div class="line" style="display: block; margin: 3px auto 7px; width: 5cm; height: 0.8cm; letter-spacing: 0.33cm;"><img src="data:image/bmp;base64,'.base64_encode($barcode_imgraw).'" style="width: 100%; height: 100%;"></div>
			<input type="hidden" id="QRCode_L_Value0" value="'.$qrcode_left_text.'">
			<input type="hidden" id="QRCode_R_Value0" value="'.$qrcode_right_text.'">
			<div id="QRCode_Left0" class="QRCode_Left" index="0" style=" width:2.5cm;height:2.5cm; display:inline-block; margin-right:0.5cm;margin-top:10px; margin:3px 5px; padding-left: 0; " title="'.$qrcode_left_text.'">
			<canvas width="70" height="70" style="display: none;"></canvas><img width="100%" src="data:image/png;base64,'.base64_encode($qrcodeleft_imgraw).'" style="display: block;width:100%; height:100%;"></div>
			<div id="QRCode_Right0" class="QRCode_Right" index="0" style=" width:2.5cm;height:2.5cm; display:inline-block; margin-right:0cm;margin-top:10px; margin:3px 5px; padding-left: 0; " title="'.$qrcode_right_text.'">
			<canvas width="70" height="70" style="display: none;"></canvas><img width="100%" src="data:image/png;base64,'.base64_encode($qrcoderight_imgraw).'" style="display: block;width:100%; height:100%;"></div>
		</div>
		
	</div>';
/*
		<div class="invoice_details">
					<h2>
					<font style="font-weight: 500;">交易明細</font></h2>
				<ul>
					<li style="line-height: 15px;"><span class="left"><font style="font-weight: 500;"> 買受人統編 16170304</font></span></li>
					<li style="line-height: 15px;"><font style="font-weight: 500;"> 買受人名稱 世清股份有限公司</font></li>
					<li style="line-height: 15px;"><font style="font-weight: 500;">2018-12-04 13:12:51</font></li>
				</ul>
				<table width="100%" border="1" class="table" style="word-break:break-all;">
					<tbody>
						<tr>
							<th style="width:33%;"><font style="font-weight: 500;">品名</font></th>
							<th class="cntr"><font style="font-weight: 500;">單價*數量</font></th>
							<th class="bind"><font style="font-weight: 500;">金額</font></th>
						</tr>
							<tr>
								<td colspan="3" style="word-break: break-all;"><font style="font-weight: 500;">Space4car停車媒合服務費</font></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td class="cntr"><font style="font-weight: 500;">90*1</font></td>
								<td class="bind"><font style="font-weight: 500;">90
									TX</font></td>
							</tr>
					</tbody>
				</table>
				<h4>
					<font style="font-weight: 500;">合計 1項</font></h4>
						<h4>
							<font style="font-weight: 500;">銷售額(應稅) $86</font>
						</h4>
					<h4>
						<font style="font-weight: 500;">稅額 $4</font>
					</h4>
				<h4>
					<font style="font-weight: 500;">總計 $90</font></h4>
				<h4 style="word-break: break-all;">
					<font style="font-weight: 500;">備註 易停網電子發票系統</font>
				</h4>
			</div>
	$body.="易停網<br>";
	$body.="Space4car<br>";
	$body.="電子發票證明聯<br>";
	$body.=$text_year."年".$text_month."月<br>";
	$body.=$text_number."<br>";
	$body.=$text_datetime." ".$text_attext."<br>";
	$body.="隨機碼:".$text_randcode." 總計:".$text_total."<br>";
	$body.="賣方:".$text_serialid." ".$text_byerid."<br>";
	$body.="<img src='data:image/png;base64,".base64_encode($barcode_imgraw)."'><br>";
	$body.="<img src='data:image/png;base64,".base64_encode($qrcodeleft_imgraw)."'><br>";
	$body.="<img src='data:image/png;base64,".base64_encode($qrcoderight_imgraw)."'><br>";
	//$body .= '<img src="'.$PNG_TEMP_DIR.$PNG_WEB_DIR.basename($filename_left).'" /><hr/>';
	//$body .= '<img src="'.$PNG_TEMP_DIR.$PNG_WEB_DIR.basename($filename_right).'" /><hr/>';*/
	return $body;
}
?>