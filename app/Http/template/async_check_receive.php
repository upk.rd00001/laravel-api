<?php
ini_set("max_execution_time", "120");//2分鐘
header("Content-type: application/json; charset=utf-8");
include_once("/../fn_package.php");
ignore_user_abort(true);
sleep(30);//建立後30秒才去確認這件事
//for($i=0;$i<1000000;$i++)
// for($j=0;$j<4000;$j++);
$pure_data = file_get_contents('php://input');
$data_back = json_decode($pure_data);
if (isset($data_back->{"activity"})) {
	$activity = InputRegularExpression($data_back->{"activity"}, "text");
}
else {
	$activity = null;
}
if (isset($data_back->{"token"})) {
	$token = InputRegularExpression($data_back->{"token"}, "text");
}
else {
	$token = null;
}

if (isset($data_back->{"m_dpt_id"})) {
	$m_dpt_id = InputRegularExpression($data_back->{"m_dpt_id"}, "text");
}
else {
	$m_dpt_id = null;
}

global $conn, $dbName;
check_conn($conn, $dbName);

$language = "zh-tw";
if ($activity == null || $m_dpt_id == null || $token == null) {
	rg_activity_log($conn, "", "檢查交易回傳失敗", "必填欄位未填", $pure_data, "");
	$ans = GetSystemCode("7050003", $language, $conn);
	echo json_encode(["result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]]);
	mysql_close($conn);
	return;
}
else {
	if ($activity != "CHECK RECEIVE") {
		rg_activity_log($conn, "", "檢查交易回傳失敗", "activity錯誤", $pure_data, "");
		$ans = GetSystemCode("7050004", $language, $conn);
		echo json_encode(["result" => 0, "systemCode" => $ans[0], "title" => $ans[1], "description" => $ans[2]]);
		mysql_close($conn);
		return;
	}
}

#確認使用者
$sql = "SELECT m_id, m_language FROM tb_Member WHERE m_token='".$token."'";
$result = mysql_query($sql, $conn);
if (! $result) {
	echo json_encode(["result" => 0, "title" => "檢查交易回傳失敗", "description" => mysql_error($conn)]);
	return;
}
else {
	if (mysql_num_rows($result) == 0) {
		#token失效
		rg_activity_log($conn, "", "檢查交易回傳失敗", "token失效", $pure_data, "");
		$ans = GetSystemCode("9", $language, $conn);
		echo json_encode([
			"result" => 0,
			"systemCode" => $ans[0],
			"title" => $ans[1],
			"description" => $ans[2],
			"debug" => "token失效",
		]);
		mysql_close($conn);
		return;
	}
}
$ans = mysql_fetch_assoc($result);
$id = $ans["m_id"];
$language = $ans["m_language"];

$this_member_deposit = new UPK_MemberDeposit($m_dpt_id);
if($this_member_deposit->get_m_dpt_trade_platform()==CONST_M_DPT_TRADE_PLATFORM::EC_PAY_BARCODE) {
	if ($this_member_deposit->get_m_dpt_payment_info_log() == "") {
		//barcode成功後一定有此值 感覺不用再判斷是否有正常條碼
		//如果沒有澤推播該會員建立失敗並且作廢(修改trade_platform)
		$sql="UPDATE tb_Member_Deposit SET m_dpt_trade_platform='".CONST_M_DPT_TRADE_PLATFORM::EC_PAY_BARCODE_FAILED."' 
			WHERE m_dpt_id='".$m_dpt_id."'";
		$result = mysql_query($sql, $conn);
		if (! $result) {
			echo json_encode(["result" => 0, "title" => "檢查交易回傳失敗", "description" => mysql_error($conn)]);
			return;
		}
		PushTo($conn,$this_member_deposit->get_m_id(),"超商條碼建立失敗",
			"您於".$this_member_deposit->get_m_dpt_create_datetime().
			"建立的超商條碼建立失敗，請重試一次或使用其他支付方式","");

		$G_SYSTEM_EMAIL = GetSystemParameter($conn, "system_error_receive_email");
		EmailTemplate($conn,"易停網營運團隊",$G_SYSTEM_EMAIL,"超商條碼建立失敗，請協助重建",
			"BKLD = ".$this_member_deposit->get_m_bkl_id()."<br>");
	}
}
echo ReturnJsonModule(["result" => 1, "data" => ""]);
mysql_close($conn);
return;

?>