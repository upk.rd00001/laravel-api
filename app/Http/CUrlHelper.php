<?php
/**
 * Created by PhpStorm.
 * User: Neal
 * Date: 2018/6/5
 * Time: 下午 11:50
 */
namespace App\Http;
class CUrlHelper
{
    static function jsonPost($url, $postData, $headers)
    {
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array_merge(array(
                'Content-Type: application/json'
            ), $headers),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));

        // Send the request
        $response = curl_exec($ch);
        return $response;
        // Check for errors
//        if ($response === FALSE) {
//            die(curl_error($ch));
//        }
    }
}