<?php
function GetTaipeiPolygonfunc(){
	$pointLocation = new pointLocation();
		$simple_taipei_json='
	{
		"coordinates":[
		[
	        [
	          121.4811294519,
	          24.67378805
	        ],
	        [
	          121.5964859,
	          24.72868393
	        ],
	        [
	          121.5957993,
	          24.78729597
	        ],
	        [
	          121.8526047,
	          24.92062858

	        ],
	        [
	          121.842305,
	          24.95300543
	        ],
	        [
	          121.9645279,
	          24.98288421

	        ],
	        [
	          122.0695847,
	          25.00777764
	        ],
	        [
	          121.5579252,
	          25.38360784
	        ],
	        [
	          121.275497,
	          25.11868683
	        ],
	        [
	          121.4018397,
	          25.04716931
	        ],
	        [
	          121.4018397,
	          24.99863801
	        ],
	        [
	          121.3716273,
	          24.97063047
	        ],
	        [
	          121.3290553,
	          24.97809977
	        ],
	        [
	          121.3482814,
	          24.84856772
	        ],
	        [
	          121.3757472,
	          24.83361297

	        ],
	        [
	          121.4066463,
	          24.84420611
	        ],
	        [
	          121.4505916,
	          24.78811451
	        ],
	        [
	          121.4258723,
	          24.76192974
	        ],
	        [
	          121.4811295,
	          24.67378805
	        ]
	    ]
	]
			}';
	    $simple_taipei_poly=array();
	    $simple_taipei_arr=json_decode($simple_taipei_json,true);
	//$points = array("121.3010003 24.9929995","121.39068603515625 24.86151853356795","-20 30","100 10","-10 -10","40 -20","110 -20");//testing point
	foreach($simple_taipei_arr["coordinates"][0] as $each_coor){
		array_push($simple_taipei_poly,$each_coor[0]." ".$each_coor[1]);
	}
	return $simple_taipei_poly;	
	
	//testing case
foreach($points as $key => $point) {
    echo "point " . ($key+1) . " ($point): " . $pointLocation->pointInPolygon($point, $simple_tao_poly) . "<br>";
}
}
?>