<?php

use App\Http\Enum\CONST_M_DPT_TRADE_PLATFORM;
use App\Http\Enum\CONST_M_PD_METHOD;

function generateTimeComponent($mode)
{
	if ($mode == CONST_GENERATE_TIME_COMPONENT::BOOKING_LOG) {
		$generate_sql_bkl = new TimeComponent("", "", "", "", "");
		$generate_sql_bkl->sql_field_name_init("m_bkl_start_date", "m_bkl_end_date", "m_bkl_start_time", "m_bkl_end_time");
		return $generate_sql_bkl;
	}
	else if ($mode == CONST_GENERATE_TIME_COMPONENT::PRICING_LOG) {
		$generate_sql_ppl = new TimeComponent("", "", "", "", "");
		$generate_sql_ppl->sql_field_name_init("m_ppl_start_date", "m_ppl_end_date", "m_ppl_start_time", "m_ppl_end_time");
		return $generate_sql_ppl;
	}
	else if ($mode == CONST_GENERATE_TIME_COMPONENT::WHITE_BLACK_LIST) {
		$generate_sql_wbl = new TimeComponent("", "", "", "", "");
		$generate_sql_wbl->sql_field_name_init("m_wbl_start_date", "m_wbl_end_date", "m_wbl_start_time", "m_wbl_end_time");
		return $generate_sql_wbl;
	}
}
//redeclare
//function startsWith($string, $startString)
//{
//	$len = strlen($startString);
//	return (substr($string, 0, $len) === $startString);
//}

function sql_plate_no_compare($field_name, $m_ve_plate_no)
{
	$m_ve_plate_no = str_replace("-", "", $m_ve_plate_no);
	return " REPLACE(" . $field_name . ",'-','') LIKE '%" . $m_ve_plate_no . "%' ";
}

function get_tiny_url($url)
{
	$i=0;
	do{
		$tiny_url = @file_get_contents('http://tinyurl.com/api-create.php?url=' . urlencode($url));
		$i++;
		if($tiny_url===false) {
			//休息一下再衝刺
			sleep(3);
		}
	}while($tiny_url===false && $i<3);
	return $tiny_url;
}

class unicodeTool
{
	static function unicodeDecode($data)
	{
		$rs = preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'replace_unicode_escape_sequence', $data);
		return $rs;
	}
}
function replace_unicode_escape_sequence($match) {
	return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
}
function timeRemoveSec($time) {
	$tmp_time = InputRegularExpression($time,"time");
	return substr($tmp_time,0,-3);
}
function GetWhiteBlackManagerPlotArray($id){
	$m_plots_id_array = array();
	global $conn,$dbName;
	$language="zh-tw";
	check_conn($conn,$dbName);
	$sql="SELECT wbm_type_id FROM tb_Whiteblack_Manager WHERE wbm_m_id='".$id."' AND wbm_delete='0' ";
	$result = mysql_query($sql, $conn);
	if(! $result) {
		return $m_plots_id_array;
	}
	else if(mysql_num_rows($result) == 0) {
		return $m_plots_id_array;
	}
	while($ans = mysql_fetch_assoc($result)) {
		array_push($m_plots_id_array,$ans["wbm_type_id"]);
	}
	return $m_plots_id_array;
}
//判斷m_pd_method是不是購買的點數
function isParkingPointPaid($m_pd_method){
	if ($m_pd_method == CONST_M_PD_METHOD::MEMBER_PAID_CREDIT_CARD ||
		$m_pd_method == CONST_M_PD_METHOD::MEMBER_PAID_THIRD_PARTY ||
		$m_pd_method == CONST_M_PD_METHOD::TRASNFER_PAID) {
		//1=信用卡 2=第三方
		//如果是用付費的停車點數就
		return true;
	}
	else {
		//3=後台贈送,4=每日送60,5=特約店贈送停車點數 8=會員註冊贈送
		//如果是贈送的停車點數就
		return false;
	}
}
function isReserveNumber($sn)
{
	global $conn,$dbName;
	check_conn($conn,$dbName);
	$member_id_reserve_json = GetSystemParameter($conn,"member_id_reserve_json");
	$member_id_reserve_array = json_decode($member_id_reserve_json,true);
	$num_arr = array();
	$tmp_num = $sn;
	$num_len = floor(log10($sn)) + 1;
	if(is_array($member_id_reserve_array) && in_array($sn,$member_id_reserve_array))
		return true;
	if ($num_len < 4)//至少需4位數以上才保留?
		return false;
	for ($i = 0; $i < $num_len; $i++) {
		$each_number = $tmp_num % 10;
		$tmp_num = floor($tmp_num / 10);
		array_push($num_arr, $each_number);
	}
	//===============判斷同數字連號==============
	$tmp_num = -1;
	$tmp_count = 0;
	$goal_count = 4;//目標連續幾號?
	foreach ($num_arr as $each_num) {
		if ($each_num != $tmp_num)
			$tmp_count = 0;
		$tmp_num = $each_num;
		$tmp_count++;
		if ($tmp_count >= $goal_count) {
			return true;
		}
	}
	//===============判斷同數字連號==============
	//===============判斷數字升順==============
	$is_up_straight = 1;
	$tmp_num = -1;
	foreach ($num_arr as $each_num) {
		if ($tmp_num == -1) //第一次
			$tmp_num = $each_num;
		else {
			if ($tmp_num + 1 != $each_num) {
				$is_up_straight = 0;
			}
			$tmp_num = $each_num;
		}
	}
	if($is_up_straight==1)
		return true;
	//===============判斷數字升順==============
	//===============判斷數字降順==============
	$is_down_straight = 1;
	$tmp_num = -1;
	foreach ($num_arr as $each_num) {
		if ($tmp_num == -1) //第一次
			$tmp_num = $each_num;
		else {
			if ($tmp_num - 1 != $each_num) {
				$is_down_straight = 0;
			}
			$tmp_num = $each_num;
		}
	}
	if($is_down_straight==1)
		return true;
	//===============判斷數字升順==============
}

function is_m_dpt_trade_platform_tappay($m_dpt_trade_platform){
	if($m_dpt_trade_platform==CONST_M_DPT_TRADE_PLATFORM::LINE_PAY)
		return true;
	if($m_dpt_trade_platform==CONST_M_DPT_TRADE_PLATFORM::GOOGLE_PAY)
		return true;
	if($m_dpt_trade_platform==CONST_M_DPT_TRADE_PLATFORM::APPLE_PAY)
		return true;
	if($m_dpt_trade_platform==CONST_M_DPT_TRADE_PLATFORM::DIRECT_PAY)
		return true;
	if($m_dpt_trade_platform==CONST_M_DPT_TRADE_PLATFORM::DIRECT_PAY_BY_CARD)
		return true;
	return false;
}
?>